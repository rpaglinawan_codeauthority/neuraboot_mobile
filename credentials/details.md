# elastic search details

## server url

`35.184.74.6:5454`

## kibana url

`http://35.184.74.6:5455`


## available users

```js
user: elastic
pw: 750657e207894b239c643639e592f71c
user: kibana
pw: 5522ffd18de84a67bddc19cf51794a54

```


## server api access

The app must never call elastic search directly due to security reasons, it's done by the app server api (currently known as firebase functions)
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../modules/material/material.module';
import { PipesModule } from '../pipes/pipes.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoaderBarComponent } from './loader-bar/loader-bar.component';
import { AppToolbarComponent } from './app-toolbar/app-toolbar.component';
import { AppSidenavComponent } from './app-sidenav/app-sidenav.component';
import { AppBreadcrumbComponent } from './app-breadcrumb/app-breadcrumb.component';
import { AppPageTitleComponent } from './app-page-title/app-page-title.component';
import { RouterModule } from '@angular/router';
import { UserAvatarModule } from '../modules/user-avatar/user-avatar.module';
import { AppFixedFabComponent } from './app-fixed-fab/app-fixed-fab.component';
import { CustomFormsModule } from 'ng2-validation';

@NgModule({
    declarations: [
        LoaderBarComponent,
        AppToolbarComponent,
        AppSidenavComponent,
        AppBreadcrumbComponent,
        AppPageTitleComponent,
        AppFixedFabComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        PipesModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        CustomFormsModule,
        UserAvatarModule
    ],
    exports: [
        MaterialModule,
        PipesModule,
        FormsModule,
        UserAvatarModule,
        ReactiveFormsModule,
        CustomFormsModule,
        LoaderBarComponent,
        AppToolbarComponent,
        AppSidenavComponent,
        AppBreadcrumbComponent,
        AppPageTitleComponent,
        AppFixedFabComponent,
    ]
})
export class ComponentsModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppFixedFabComponent } from './app-fixed-fab.component';

describe('AppFixedFabComponent', () => {
  let component: AppFixedFabComponent;
  let fixture: ComponentFixture<AppFixedFabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppFixedFabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppFixedFabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

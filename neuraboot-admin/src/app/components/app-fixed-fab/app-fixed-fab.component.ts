import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
    selector: 'app-fixed-fab',
    templateUrl: './app-fixed-fab.component.html',
    styleUrls: ['./app-fixed-fab.component.scss']
})
export class AppFixedFabComponent implements OnInit {

    constructor(
        public navigation: NavigationService
    ) { }

    ngOnInit() {
    }

}

import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-page-title',
    templateUrl: './app-page-title.component.html',
    styleUrls: ['./app-page-title.component.scss']
})
export class AppPageTitleComponent implements OnInit {

    @Input() title: string;
    @Input() back: string = '/';
    @Input() queryParams: any = '';

    constructor(
        public router: Router
    ) { }

    ngOnInit() {
    }

    navBack() {
        this.router.navigate([this.back], { queryParams: this.queryParams })
    }
}

import { Component, OnInit, Input } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './app-breadcrumb.component.html',
    styleUrls: ['./app-breadcrumb.component.scss']
})
export class AppBreadcrumbComponent implements OnInit {

    constructor(
        public menu: MenuService
    ) { }

    ngOnInit() {
    }
}

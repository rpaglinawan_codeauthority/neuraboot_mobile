import { Component, OnInit, Input } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';

@Component({
    selector: 'app-loader-bar',
    templateUrl: './loader-bar.component.html',
    styleUrls: ['./loader-bar.component.scss']
})
export class LoaderBarComponent implements OnInit {

    @Input() color: string = 'primary';
    @Input() value: number = 0;
    @Input() bufferValue: number = 0;
    @Input() mode: string = 'indeterminate'; //  'determinate' | 'indeterminate' | 'buffer' | 'query'

    constructor(
        public ui: UiService
    ) { }

    ngOnInit() {
    }

}

import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { MenuService } from 'src/app/services/menu.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sidenav',
    templateUrl: './app-sidenav.component.html',
    styleUrls: ['./app-sidenav.component.scss']
})
export class AppSidenavComponent implements OnInit {

    @Input() menu: any[] = [];

    public version: number | string = environment.version;
    public staging: boolean = environment.staging;
    public testing: boolean = environment.testing;

    constructor(
        private menuService: MenuService,
        private router: Router
    ) { }

    ngOnInit() {
    }

    navigateTo(option: any) {
        this.router.navigate([option.route]);
        if (option.closeMenu) this.menuService.close();
    }
}

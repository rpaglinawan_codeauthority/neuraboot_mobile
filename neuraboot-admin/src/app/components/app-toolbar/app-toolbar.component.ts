import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-toolbar',
    templateUrl: './app-toolbar.component.html',
    styleUrls: ['./app-toolbar.component.scss']
})
export class AppToolbarComponent implements OnInit {

    @Input() sidenav: any;

    showUserMenu: boolean = false;

    constructor(
        public auth: AuthService
    ) { }

    ngOnInit() {
    }
}

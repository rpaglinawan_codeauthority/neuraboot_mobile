export interface NavigationInterface {
  option: string;
  route: string;
  icon: string; // material icon https://material.io/icons/
}
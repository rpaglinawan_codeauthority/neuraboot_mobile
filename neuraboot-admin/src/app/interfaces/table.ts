import { ElasticQuery } from "@ionfire/reactive-record";
import { ITdDataTableColumn } from "@covalent/core";
import { Router } from "@angular/router";

export interface TableEntriesSetup {
    service: any,
    query: { query: ElasticQuery } & any,
    path: string
}

export interface TableRowClickEvent {
    row: any,
    column: string,
    router: Router
}

export interface TableColumn extends ITdDataTableColumn {
    avatar?: boolean;
}

export interface TableFinderColumn extends TableColumn {
    number?: boolean
}

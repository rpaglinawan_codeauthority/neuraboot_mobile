import { environment } from "../../environments/environment";

export const IonicStorageModuleConfig: any = {
    name: `__${environment.app}__`,
    storeName: `__${environment.storage}__`,
    driverOrder: ['indexeddb', 'sqlite', 'websql']
  }
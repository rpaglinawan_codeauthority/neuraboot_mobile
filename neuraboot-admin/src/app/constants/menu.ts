
export const SideMenu: any[] = [{
    name: 'dashboard',
    roles: ['admin'],
    placeholder: 'Dashboard',
    icon: 'fas fa-chart-line',
    route: '/dashboard',
    meta: {
        title: 'Dashboard',
        description: ''
    }
}, {
    name: 'feelings',
    roles: ['admin'],
    placeholder: 'Feelings',
    icon: 'fas fa-heartbeat',
    options: [{
        name: 'add',
        placeholder: 'Add feeling',
        route: '/feelings/add',
        icon: 'fas fa-plus',
        meta: {
            title: 'Feelings / Add',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fas fa-heartbeat',
            placeholder: 'Feelings',
            route: '/feelings'
        }, {
            placeholder: 'Manage',
            route: '/feelings'
        }, {
            placeholder: 'Add'
        }]
    }, {
        name: 'manage',
        placeholder: 'Manage feeling',
        route: '/feelings',
        icon: 'fas fa-wrench',
        meta: {
            title: 'Feelings / Manage',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fas fa-heartbeat',
            placeholder: 'Feelings'
        }, {
            placeholder: 'Manage'
        }]
    }, {
        name: 'categories',
        placeholder: 'Categories',
        route: '/feelings/categories',
        icon: 'fas fa-tags',
        meta: {
            title: 'Feelings / Categories',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fas fa-heartbeat',
            placeholder: 'Feelings',
            route: '/feelings'
        }, {
            placeholder: 'Categories'
        }]
    }]
}, {
    name: 'challenges',
    roles: ['admin'],
    placeholder: 'Challenges',
    icon: 'fa fa-trophy',
    options: [{
        name: 'add',
        placeholder: 'Add challenge',
        route: '/challenges/add',
        icon: 'fas fa-plus',
        meta: {
            title: 'Challenges / Add',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fa fa-trophy',
            placeholder: 'Challenges',
            route: '/challenges'
        }, {
            placeholder: 'Manage',
            route: '/challenges'
        }, {
            placeholder: 'Add'
        }]
    }, {
        name: 'manage',
        placeholder: 'Manage challenge',
        route: '/challenges',
        icon: 'fas fa-wrench',
        meta: {
            title: 'Challenges / Manage',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fa fa-trophy',
            placeholder: 'Challenges'
        }, {
            placeholder: 'Manage'
        }]
    }, {
        name: 'categories',
        placeholder: 'Categories',
        route: '/challenges/categories',
        icon: 'fa fa-trophy',
        meta: {
            title: 'Challenges / Categories',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fa fa-trophy',
            placeholder: 'Challenges',
            route: '/challenges/categories'
        }, {
            placeholder: 'Categories'
        }]
    }]
}, {
    name: 'users',
    roles: ['admin'],
    placeholder: 'Users',
    icon: 'fas fa-users',
    options: [{
        name: 'add',
        placeholder: 'Add user',
        route: '/users/add',
        icon: 'fas fa-plus',
        meta: {
            title: 'Users / Add',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fas fa-users',
            placeholder: 'Users',
            route: '/users'
        }, {
            placeholder: 'Manage',
            route: '/users'
        }, {
            placeholder: 'Add'
        }]
    }, {
        name: 'manage',
        placeholder: 'Manage user',
        route: '/users',
        icon: 'fas fa-wrench',
        meta: {
            title: 'Users / Manage',
            description: ''
        },
        breadcrumbs: [{
            icon: 'fas fa-users',
            placeholder: 'Users'
        },
        {
            placeholder: 'Manage'
        }]
    }]
}]

import * as Firebase from 'firebase/app';
import 'firebase/firestore';
import { Injectable } from '@angular/core';
import { ReactiveRecord, RROptions, ClientSetup } from '@ionfire/reactive-record';
import { environment } from 'src/environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';
import { CacheService } from './cache.service';
import { AuthService } from './auth.service';

@Injectable()
export class ChallengeHistoryService extends ReactiveRecord {

    constructor(
        private _cache: CacheService,
        public auth: AuthService
    ) {

        //
        // Set RR
        super(<RROptions>new ClientSetup({
            baseURL: environment.functions.baseUrl,           // eg: https://api.url.com
            endpoint: '/api_challenge/challenge-history',
            collection: 'challenge-history',
            storage: _cache,                                   // storage adapter
            firebase: Firebase,                               // firebase sdk
            config: environment.firebase,                     // firebase web config
            version: Version.get(environment.version),        // route versioning
            token: {
                type: 'Bearer',
                value: auth.getUser().token
            },
            ttl: 60 * 60
        }));
    }
}

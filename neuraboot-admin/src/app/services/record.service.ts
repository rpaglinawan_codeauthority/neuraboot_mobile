import { Injectable } from '@angular/core';
import { RRRequest, RRResponse } from '@ionfire/reactive-record';
import { UiService } from './ui.service';
import { map, tap, catchError } from 'rxjs/operators';
import { ApiErrorResponse } from '../../../../neuraboot-core/interfaces/api-error-response.interface';
import { of } from 'rxjs';
import { isEmpty } from 'lodash';

@Injectable()
export class RecordService {

    public id: string = null;
    public request: RRRequest = {};
    public service: any = false;
    public postGetHook: any;

    public load: boolean = false;
    public entry: any = {};

    constructor(
        public ui: UiService
    ) { }

    setup() {

        //
        // Set loading
        this.setLoading(true);

        //
        // Get entry
        return this.service.findOne(this.request).pipe(
            map((r: RRResponse) => r.data),
            tap((r) => {
                this.entry = r;
                this.setLoading(false);

                return r;
            }),
            tap(this.postGetHook),
            catchError((err: ApiErrorResponse | any) => {
                const message: string = err.message ? err.message : err;

                this.ui.say(message);
                this.setLoading(false);

                return of(err);
            })
        );
    }

    /**
     * Set loading
     *
     * @memberof RecordService
     */
    setLoading(status: boolean = false) {
        this.load = status;
        this.ui.loading = status;
    }

    /**
     * Get update URL
     *
     * @returns
     * @memberof RecordService
     */
    getUpdateUrl() {
        let route = this.service.updateUrl;
        route = route.replace('id', this.id);

        return route;
    }

    /**
     * Get view URL
     *
     * @returns
     * @memberof RecordService
     */
    getViewUrl() {
        let route = this.service.viewUrl;
        route = route.replace('id', this.id);

        return route;
    }

    /**
     * Is empty entry
     *
     * @returns
     * @memberof RecordService
     */
    isEmptyEntry() {
        return (isEmpty(this.entry)) ? true : false;
    }

    /**
     * Update entry
     *
     * @description Updates current entry
     * @param {*} entry
     * @memberof RecordService
     */
    updateEntry(entry) {

        //
        // Validate
        if (isEmpty(entry)) return;

        this.entry = entry;
    }
}

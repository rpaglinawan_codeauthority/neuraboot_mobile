import { TestBed } from '@angular/core/testing';

import { ChallengeHistoryService } from './challenge-history.service';

describe('ChallengeHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChallengeHistoryService = TestBed.get(ChallengeHistoryService);
    expect(service).toBeTruthy();
  });
});

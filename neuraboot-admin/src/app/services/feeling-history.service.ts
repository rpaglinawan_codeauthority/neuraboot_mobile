import * as Firebase from 'firebase/app';
import 'firebase/firestore';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CacheService } from './cache.service';
import { Version } from '../../../../neuraboot-core/utils/version';
import { environment } from '../../environments/environment';

import { ReactiveRecord, ClientSetup, RROptions } from '@ionfire/reactive-record';

@Injectable({
    providedIn: 'root'
})
export class FeelingHistoryService extends ReactiveRecord {
    constructor(public auth: AuthService, public cache: CacheService) {
        super(<RROptions>new ClientSetup({
            baseURL: environment.functions.baseUrl,           // eg: https://api.url.com
            endpoint: '/api_feeling/feeling-history',
            collection: 'feeling-history',
            storage: cache,                                   // storage adapter
            firebase: Firebase,                               // firebase sdk
            config: environment.firebase,                     // firebase web config
            version: Version.get(environment.version),        // route versioning  
            token: {
                type: 'Bearer',
                value: auth.getUser().token
            }
        }));
    }
}

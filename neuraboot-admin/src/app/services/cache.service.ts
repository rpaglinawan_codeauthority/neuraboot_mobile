import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  constructor(public storage: Storage) { }

  get(key: string): Promise<any> {
    return this.storage.get(key);
  };

  /**
   * Set the value for the given key.
   * @param {any} key the key to identify this value
   * @param {any} value the value for this key
   * @returns {Promise} Returns a promise that resolves when the key and value are set
   */
  set(key: string, value: any): Promise<any> {
    return this.storage.set(key, value);
  }

  /**
   * Remove any value associated with this key.
   * @param {any} key the key to identify this value
   * @returns {Promise} Returns a promise that resolves when the value is removed
   */
  remove(key: string): Promise<any> {
    return this.storage.remove(key);
  }

  /**
   * Clear the entire key value store. WARNING: HOT!
   * @returns {Promise} Returns a promise that resolves when the store is cleared
   */
  clear(): Promise<void> {
    return this.storage.clear();
  }
}

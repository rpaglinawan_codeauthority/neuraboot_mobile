import * as Remarkable from 'remarkable';
import { Injectable } from '@angular/core';
import { NavigationInterface } from "../interfaces/navigation-interface";
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { MatSnackBar } from '@angular/material';
import { COUNTRIES } from '../constants/countries';

const md = new Remarkable({
    html: true,        // Enable HTML tags in source
    xhtmlOut: true,        // Use '/' to close single tags (<br />)
    breaks: true,        // Convert '\n' in paragraphs into <br>
    langPrefix: 'language-',  // CSS language prefix for fenced blocks
    linkify: true,        // Autoconvert URL-like text to links

    // Enable some language-neutral replacement + quotes beautification
    typographer: false,

    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Set doubles to '«»' for Russian, '„“' for German.
    quotes: '“”‘’',

    // Highlighter function. Should return escaped HTML,
    // or '' if the source string is not changed
    // highlight: function (/*str, lang*/) { return ''; }
});

@Injectable({
    providedIn: 'root'
})
export class UiService {
    loading: boolean;
    maintenance: boolean;
    showNav: boolean = false;
    showUserNav: boolean = false;

    navigation: NavigationInterface[] = []
    fullscreen: boolean = false;

    constructor(private scroll: ScrollToService, public snackBar: MatSnackBar) {
    }

    navAdd(stack: any[]) {
        this.navClear();
        stack.forEach((item) => {
            this.navigation.push(item);
        })
    }

    navClear() {
        this.navigation = [];
    }

    scrollTop(val?: number) {
        this.scroll.scrollTo({
            target: 'scroll-top'
        });
    }

    say(msg: string, time: number = 5 * 1000, okText: string = 'Ok') {
        this.snackBar.open(msg, okText, { duration: time });
    }

    ask(
        msg: string,
        options: { label?: string, duration?: number },
        callback: any = () => { }
    ) {

        let settings: any = {
            label: 'Yes',
            duration: 5000
        };

        if (options.label) settings.label = options.label;
        if (options.duration) settings.duration = options.duration;

        let alert = this.snackBar
            .open(msg, settings.label, { duration: settings.duration });
        alert.onAction().subscribe(() => {
            callback();
        })
    }

    emojiCountry(countryCode: string) {
        let result: any;
        for (let country in COUNTRIES) {
            if (country === countryCode) result = COUNTRIES[country];
        }
        return result ? result.emoji : countryCode;
    }

    testAvatar(url: string) {
        return new Promise((resolve, reject) => {
            const tester: any = new Image();
            tester.onload = () => {
                // console.log('loaded', url);
                resolve(url);
            };
            tester.onerror = () => {
                const time = setTimeout(() => {
                    //  model.photo_url = './assets/images/user-blank.svg';
                    resolve();
                    clearTimeout(time);
                }, 100)
                // console.log('nops', url)
            };
            tester.src = url;
        })
    }

    markdown(str) {
        return md.render(str)
    }

}

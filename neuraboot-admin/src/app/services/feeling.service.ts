import { Injectable } from '@angular/core';

@Injectable()
export class FeelingService {

    public icon: string = 'fa fa-heartbeat';
    public listUrl: string = '/feelings/categories';
    public viewUrl: string = this.listUrl + '/view/id';

    constructor() { }
}

import * as _ from 'lodash';
import * as moment from 'moment';
import * as Firebase from 'firebase/app';
import 'firebase/database';
import { Injectable } from '@angular/core';
import { CacheService } from './cache.service';
import { FirebaseApi } from '../utils/firebase';;
import { User } from '../../../../neuraboot-core/interfaces/user.interface';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { NavigationService } from './navigation.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService extends FirebaseApi {
    private _user: User = {};

    get user() {
        return _.omit(this._user, ['token', 'token_fcm']);
    }

    set user(instance) {
        this._user = instance;
    }

    //
    // you know.. for testing
    public _moment: any;
    public _message: any;

    get firebase() {
        return this._firebase;
    }

    set firebase(instance) {
        this._firebase = instance;
    }

    get firestore() {
        return this._firestore;
    }

    set firestore(instance) {
        this._firestore = instance;
    }

    constructor(
        private cache: CacheService,
        private nav: NavigationService,
        private http: HttpClient
    ) {
        super(Firebase)
    }

    /**
     * A way to get the full user object
     *
     * @returns
     * @memberof AuthService
     */
    getUser() {
        return this._user;
    }

    /**
     * @param {string} property
     * @param {*} value
     * @memberof AuthService
     */
    setUserProp(property: string, value: any) {
        this._user[property] = value;
    }

    /**
     * Set login data
     * @param user
     */
    async setLoginData(user: User) {
        await this.cache.set('user', user).catch(console.log); // @todo deal with exceptions here
        this.user = user;

        return user;
    }

    /**
     * Redirect after login
     */
    async redirectAfterLogin() {

        //
        // Has password?
        if (!this.user.completed_password)
            await this.nav.goTo('login/password', { queryParams: { isNew: true } });

        //
        // Okay, go home
        else
            await this.nav.goTo('home');

        return;
    }

    async signIn(email: string, password: string) {
        try {
            const result = await this.firebase.auth().signInWithEmailAndPassword(email, password);
            if (result.user) {
                //
                // subscribe for fcm @todo
                //if (this.platform.is('cordova')) {
                // console.log('subscribing..');
                //}
                this._moment = moment().toISOString();
                let data: User = {
                    online_at: this._moment
                }

                // check for existance of user
                let snapshot = await this.firestore.collection('users').doc(result.user.uid).get().catch(console.log);
                let user: User = snapshot.data();

                if (!user) {
                    throw Error(`User not found`);
                } else {
                    //update
                    await this.firestore.collection('users').doc(user.uid).update(data).catch(console.log);

                    //
                    // set token
                    user.token = await this.getToken(user.uid);

                    //
                    // set login data
                    await this.setLoginData(user);

                    // end
                    return user;
                }
            } else {
                throw Error(`Can't connect`);
            }

        } catch (err) {
            // console.log('err', err);
            throw Error(err);
        }

    }


    /**
     *
     *
     * @param {string} displayName
     * @param {string} email
     * @param {string} password
     * @returns
     * @memberof AuthService
     */
    async signUp(displayName: string, email: string, password: string) {
        try {
            this._moment = moment().toISOString();
            const result = await this.firebase.auth().createUserWithEmailAndPassword(email, password);
            if (result.user) {
                //
                // subscribe for fcm @todo
                //if (this.platform.is('cordova')) {
                // console.log('subscribing..');
                //}
                let data: User = {
                    uid: result.user.uid,
                    display_name: displayName,
                    email: email,
                    online_at: this._moment,
                    created_at: this._moment
                }
                //
                // add to firebase
                await this.firestore.collection('users').doc(result.user.uid).set(data, { merge: true }).catch(console.log);

                //
                // set token
                data.token = await this.getToken(data.uid);

                //
                // Set login data
                await this.setLoginData(data);

                return data;

            } else {
                throw (`Can't connect`);
            }

        } catch (err) {
            // console.log(err)
            throw (err);
        }
    }


    async signOut() {
        this.firebase.auth().signOut();
        this.cache.clear();
        this.user = {};
        await this.nav.backTo('/login');
    }


    /**
     *
     *
     * @param {string} email
     * @memberof AuthService
     */
    async forgot(email: string) {
        try {
            await this.firebase.auth().sendPasswordResetEmail(email);
            return true;
        } catch (err) {
            throw (err);
        }
    }

    /**
     *
     *
     * @param {string} email
     * @memberof AuthService
     */
    async updatePassword(newPassword: string) {

        try {

            //
            // Update password on firebase
            await this.firebase.auth().currentUser.updatePassword(newPassword).catch(console.log); // @todo deal with exceptions here

            //
            // Mask completed_password
            this.setUserProp('completed_password', true);
            await this.updateProfile(this.user);

            return true;

        } catch (err) {

            let msg;

            //
            // Filter errors
            switch (err.code) {

                case 'auth/weak-password':
                    msg = 'Password is too weak';
                    break;

                case 'auth/requires-recent-login':
                    msg = 'Login and try again';

                    //
                    // Sign out
                    await this.signOut();
                    break;

                default:
                    msg = 'Something went wrong';
            }
            this._message = msg;
            throw (msg);
        }
    }

    /**
     *
     *
     * @param {object} user
     * @memberof AuthService
     */
    async updateProfile(user: User) {
        try {

            //
            // Update user display name on firebase
            await this.firebase.auth().currentUser.updateProfile(user).catch(console.log); // @todo deal with exceptions here

            //
            // Update the user in firestore
            await this.firestore.collection('users').doc(user.uid).update(user).catch(console.log); // @todo deal with exceptions here

            //
            // Set login data
            this.user.display_name = user.display_name;
            await this.setLoginData(this.user);

            return true;

        } catch (err) {
            throw ('Something went wrong');
        }
    }

    /**
     * Get user web token to enable api
     *
     * @deprecated
     * @param {string} uid
     * @returns {Promise<string>}
     * @memberof AuthService
     */
    getToken(uid: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.http.post(environment.functions.baseUrl + '/api/token/' + uid, {}).subscribe((r: { token: string }) => resolve(r.token), reject);
        });
    }



    /**
     * check for user role
     *
     * @param {string} role
     * @returns
     * @memberof AuthService
     */
    public hasRole(role: string) {
        return this.user.roles ? this.user.roles.includes(role) : false;
    }

    /**
     *
     *
     * @param {string} token
     * @returns
     * @memberof AuthService
     */
    signInWithCustomToken$(token: string) {
        return new Observable((observer) => {
            this.firebase.auth().signInWithCustomToken(token)
                .then((r: any) => {
                    observer.next(r);
                    observer.complete();
                })
                .catch((err: any) => {
                    observer.error(err);
                    observer.complete();
                })
        })
    }
}

import { TestBed } from '@angular/core/testing';

import { FeelingSpecificService } from './feeling-specific.service';

describe('FeelingSpecificService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeelingSpecificService = TestBed.get(FeelingSpecificService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { MatDrawer } from '@angular/material';

export interface Breadcrumb {
    icon?: string,
    placeholder?: string,
    route?: string
}

@Injectable({
    providedIn: 'root'
})
export class MenuService {
    sidenav: MatDrawer
    hasBreadcrumb: boolean = true;
    currentEntryMenu: any; // deprecate
    currentSubMenu: any; // deprecate
    currentActionMenu: any; // deprecate


    breadcrumbs: Breadcrumb[] = []
    toolbar: { title: string, back?: string } = { title: '', back: '/' }
    constructor() {

    }

    close() {
        this.sidenav.close();
    }

    open() {
        this.sidenav.open();
    }
}

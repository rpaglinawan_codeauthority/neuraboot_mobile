import * as Firebase from 'firebase/app';
import 'firebase/firestore';
import { Injectable } from '@angular/core';
import { ReactiveRecord, RROptions, ClientSetup } from '@ionfire/reactive-record';
import { environment } from 'src/environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';
import { CacheService } from './cache.service';
import { AuthService } from './auth.service';
import { UiService } from './ui.service';
import { ChallengeCategoryAvatar } from '../../../../neuraboot-core/constants/challenge-category-avatar';
import { startCase, truncate } from 'lodash';

@Injectable()
export class ChallengeService extends ReactiveRecord {

    public icon: string = 'fa fa-trophy';
    public listUrl: string = '/challenges';
    public createUrl: string = this.listUrl + '/add';
    public updateUrl: string = this.listUrl + '/view/id/edit';
    public viewUrl: string = this.listUrl + '/view/id';
    public title: any = {
        form: 'Challenge'
    };

    constructor(
        public _cache: CacheService,
        public auth: AuthService,
        public ui: UiService
    ) {

        //
        // Set RR
        super(<RROptions>new ClientSetup({
            baseURL: environment.functions.baseUrl,           // eg: https://api.url.com
            endpoint: '/api_challenge',
            collection: 'challenges',
            storage: _cache,                                   // storage adapter
            firebase: Firebase,                               // firebase sdk
            config: environment.firebase,                     // firebase web config
            version: Version.get(environment.version),        // route versioning
            token: {
                type: 'Bearer',
                value: auth.getUser().token
            },
            ttl: 0
        }));

    }

    /**
     * Toggle status
     *
     * @param {string} [id=null]
     * @param {boolean} [status=false]
     * @memberof ChallengeService
     */
    async toggleStatus(id: string = null, status: boolean = false) {
        try {

            this.ui.say('Saving challenge...');
            this.ui.loading = true;

            //
            // Save
            await this.update(id, { active: status }).toPromise();

            //
            // Clear cache
            this._cache.remove('challenges/{"query":{"field":"id","operator":"==","value":"' + id + '"}}');

            //
            // UI
            this.ui.say('Saved!');
            this.ui.loading = false;

            return true;
        } catch (error) {

            //
            // Return
            this.ui.say('Ops, not saved');
            this.ui.loading = false;

            return false;
        }
    }

    /**
     * Transform category name
     *
     * @description Transform category name in
     * @param {string} category
     * @memberof ChallengeService
     */
    transformCategoryName(category: string) {
        return startCase(category);
    }

    /**
     * Get avatar
     *
     * @description Get challenge category avatar
     * @param {*} category
     * @returns
     * @memberof ChallengeService
     */
    getAvatar(category) {
        return 'url(/assets/images/' + ChallengeCategoryAvatar[category] + ')';
    }

    /**
     * Truncate text
     *
     * @description Truncate text
     * @param {string} [text=null]
     * @returns
     * @memberof ChallengeService
     */
    truncateText(text: string = null) {

        //
        // Validate null
        if (!text) return '-- link only --';

        //
        // Validate length
        if (text.length < 65) return text;

        //
        // Truncate
        return truncate(text, {
            length: 65,
            omission: ' [...]'
        });
    }
}

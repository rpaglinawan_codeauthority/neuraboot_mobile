import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class NavigationService {

    public mainFabAction: any = false;
    public reportFabAction: any = false;
    public reportFabActionLoading: any = false;
    public uploadFabAction: any = false;

    constructor(public router: Router) { }

    public goTo(route: string, extras?: NavigationExtras) {
        this.router.navigate([route], extras);
    }

    public backTo(route: string = 'home', extras?: NavigationExtras) {
        this.router.navigate([route], extras);
    }

    /**
     * Set main fab action
     *
     * @description Set click action for the fab outside mat-sidenav-container
     * @param {*} [action=false]
     * @memberof NavigationService
     */
    public setMainFabAction(action: any = false) {

        //
        // Validate action
        // Unset if false
        if (!action || typeof action !== 'function') this.mainFabAction = false;

        //
        // Set action
        this.mainFabAction = action;
    }

    /**
     * Set report fab action
     *
     * @description Set click action for the fab outside mat-sidenav-container
     * @param {*} [action=false]
     * @memberof NavigationService
     */
    public setReportFabAction(action: any = false) {

        //
        // Validate action
        // Unset if false
        if (!action || typeof action !== 'function') this.reportFabAction = false;

        //
        // Set action
        this.reportFabAction = action;
    }

    /**
     * Set upload fab action
     *
     * @description Set click action for the fab outside mat-sidenav-container
     * @param {*} [action=false]
     * @memberof NavigationService
     */
    public setUploadFabAction(action: any = false) {

        //
        // Validate action
        // Unset if false
        if (!action || typeof action !== 'function') this.uploadFabAction = false;

        //
        // Set action
        this.uploadFabAction = action;
    }
}

import { Injectable } from '@angular/core';
import { ReactiveRecord, RROptions, ClientSetup } from '@ionfire/reactive-record';
import { AuthService } from './auth.service';
import { CacheService } from './cache.service';
import { environment } from 'src/environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';
import { startCase } from 'lodash';
import * as Firebase from 'firebase/app';
import 'firebase/firestore';
import { UiService } from './ui.service';

@Injectable()
export class FeelingSpecificService extends ReactiveRecord {

    public icon: string = 'fa fa-heartbeat';
    public listUrl: string = '/feelings';
    public createUrl: string = this.listUrl + '/add';
    public updateUrl: string = this.listUrl + '/view/id/edit';
    public viewUrl: string = this.listUrl + '/view/id';
    public title: any = {
        form: 'Feeling'
    };

    constructor(
        public auth: AuthService,
        public cache: CacheService,
        public ui: UiService
    ) {
        super(<RROptions>new ClientSetup({
            baseURL: environment.functions.baseUrl,           // eg: https://api.url.com
            endpoint: '/api_feeling/feeling-specific',
            collection: 'feeling-specific',
            storage: cache,                                   // storage adapter
            firebase: Firebase,                               // firebase sdk
            config: environment.firebase,                     // firebase web config
            version: Version.get(environment.version),        // route versioning
            token: {
                type: 'Bearer',
                value: auth.getUser().token
            }
        }));

    }

    /**
     * Transform category name
     *
     * @description Transform category name in
     * @param {string} category
     * @memberof FeelingSpecificService
     */
    transformCategoryName(category: string) {
        return category ? startCase(category) : 'All';
    }

    /**
     * Toggle status
     *
     * @param {string} [id=null]
     * @param {boolean} [status=false]
     * @memberof FeelingSpecificService
     */
    async toggleStatus(id: string = null, status: boolean = false) {
        try {

            this.ui.say('Saving feeling...');
            this.ui.loading = true;

            //
            // Save
            await this.update(id, { active: status }).toPromise();

            //
            // Clear cache
            this.cache.remove('feeling-specific/{"query":{"field":"id","operator":"==","value":"' + id + '"}}');

            //
            // UI
            this.ui.say('Saved!');
            this.ui.loading = false;

            return true;
        } catch (error) {

            //
            // Return
            this.ui.say('Ops, not saved');
            this.ui.loading = false;

            return false;
        }
    }
}

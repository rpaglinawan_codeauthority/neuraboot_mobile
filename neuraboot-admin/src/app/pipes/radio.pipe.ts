import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'radio'
})
export class RadioPipe implements PipeTransform {

  transform(value: any, name, inputs): any {
    // console.log(value, name, inputs)
    if (!value) return;
    const input = inputs.find((input) => { return input.name == name });
    if (input)
      return input.options.find((option) => { return option.value == value }).name;
    return null;
  }

}

import { Pipe, PipeTransform } from '@angular/core';
import { UiService } from '../services/ui.service';


@Pipe({
    name: 'markdown'
})
export class MarkdownPipe implements PipeTransform {
    constructor(public ui: UiService) {

    }
    transform(value: any, args?: any): any {
        if (value) return this.ui.markdown(value);
        return value;
    }

}

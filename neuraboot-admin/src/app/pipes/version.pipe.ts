import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'version'
})
export class VersionPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        return value ? `${value.toString().charAt(0)}.${value.toString().charAt(1)}.${value.toString().charAt(2)}.${value.toString().charAt(3)}` : value;
    }

}

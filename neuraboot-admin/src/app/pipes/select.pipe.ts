import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'select'
})
export class SelectPipe implements PipeTransform {

  transform(value: any, name, inputs): any {
    const input = inputs.find((input) => { return input.name == name });
    if (input)
      return input.options.find((option) => { return option.value == value }).name;

    return null;
  }
}

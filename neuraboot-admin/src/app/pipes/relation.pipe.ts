import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

// @todo not working with async ways
@Pipe({
    name: 'relation'
})
export class RelationPipe implements PipeTransform {

    transform(value: any, name, inputs): any {
        //   console.log(value, name, inputs)
        const input = inputs.find((input) => { return input.name == name });
       
        if (input) {
            const option = _.find(input.options, (o) => { return o.value == value });
             console.log(input.name, value)
            return option ? option.name : value;
        }
        return value;
    }
}

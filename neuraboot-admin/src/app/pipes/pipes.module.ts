import { NgModule } from '@angular/core';
import { SafeHtmlPipe } from "./safeHtml";
import { MarkdownPipe } from "./markdown.pipe";
import { FromNowPipe } from "./from-now.pipe";
import { BoolPipe } from "./bool.pipe";
import { RadioPipe } from "./radio.pipe";
import { RelationPipe } from "./relation.pipe";
import { SelectPipe } from "./select.pipe";
import { CapitalizePipe } from './capitalize.pipe';
import { MomentPipe } from './moment.pipe';
import { VersionPipe } from './version.pipe';

@NgModule({
    imports: [],
    declarations: [
        SelectPipe,
        RelationPipe,
        RadioPipe,
        BoolPipe,
        FromNowPipe,
        MarkdownPipe,
        SafeHtmlPipe,
        CapitalizePipe,
        MomentPipe,
        VersionPipe
    ],
    exports: [
        SelectPipe,
        RelationPipe,
        RadioPipe,
        BoolPipe,
        FromNowPipe,
        MarkdownPipe,
        SafeHtmlPipe,
        CapitalizePipe,
        MomentPipe,
        VersionPipe
    ],
    providers: []
})
export class PipesModule { }

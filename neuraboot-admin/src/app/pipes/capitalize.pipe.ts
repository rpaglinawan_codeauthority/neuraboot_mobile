import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

/**
 * Generated class for the FirstNamePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {
  transform(value: string, ...args) {
    return value ? _.capitalize(value.split(' ')[0]) : '';
  }
}

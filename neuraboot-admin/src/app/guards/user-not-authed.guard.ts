import { Injectable } from "@angular/core";
import { CanActivate, CanLoad } from "@angular/router";
import { CanActivateChild } from "@angular/router";
import { CacheService } from "../services/cache.service";
import { NavigationService } from "../services/navigation.service";
import { RRResponse } from "@ionfire/reactive-record";
import { User } from "../../../../neuraboot-core/interfaces/user.interface";

@Injectable({
    providedIn: 'root'
})
@Injectable()
export class UserNotAuthedGuard implements CanActivate, CanActivateChild, CanLoad {

    constructor(public cache: CacheService, public nav: NavigationService) { }


    async canActivate(): Promise<any> {
        const cache: User = await this.cache.get('user');
        const token: string = cache ? cache.token : null;

        if (token) {
            // this.nav.goTo('/dashboard');
            return false;
        }

        return true;
    }

    canActivateChild(): Promise<any> {
        return this.canActivate();
    }

    canLoad(): Promise<any> {
        return this.canActivate();
    }
}

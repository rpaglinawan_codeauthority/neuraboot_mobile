// import { TestBed, tick, fakeAsync } from "@angular/core/testing";
// import { MaterialModule } from "../modules/material/material.module";
// import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
// import { AUTH_SERVICE_PROVIDERS_MOCK } from "../services/auth.service.spec";
// import { AuthedUser } from "./authed-user.guard";
// import { inject } from "@angular/core/testing";
// import { CacheService } from "../services/cache.service";
// import { AuthService } from "../services/auth.service";
// import { NavigationService } from "../services/navigation.service";


// describe('AuthedUserGuard', () => {
//     let expectedAccount;

//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             imports: [MaterialModule],
//             schemas: [CUSTOM_ELEMENTS_SCHEMA],
//             providers: [
//                 ...AUTH_SERVICE_PROVIDERS_MOCK,
//                 AuthedUser,
//                 AuthService,
//                 CacheService,
//                 NavigationService
//             ]
//         });
//     });

//     it('should be created', inject([AuthedUser], (guard: AuthedUser) => {
//         expect(guard).toBeTruthy();
//     }));

//     it('should can activate child routes', inject([AuthedUser], fakeAsync((guard: AuthedUser) => {
//         spyOn(CacheService.prototype, 'get').and.returnValue(Promise.resolve({ uid: 'a1b2c3' }));
//         spyOn(NavigationService.prototype, 'goTo');
//         guard.canActivateChild();
//         tick();
//         expect(CacheService.prototype.get).toHaveBeenCalledWith('auth');
//         expect(guard.auth.user).toEqual({ uid: 'a1b2c3' });
//         expect(NavigationService.prototype.goTo).not.toHaveBeenCalled();
//     })));

//     it('should NOT be able to activate child routes', inject([AuthedUser], fakeAsync((guard: AuthedUser) => {
//         spyOn(CacheService.prototype, 'get').and.returnValue(Promise.resolve(null));
//         spyOn(NavigationService.prototype, 'goTo');
//         guard.canActivateChild();
//         tick();
//         expect(CacheService.prototype.get).toHaveBeenCalledWith('auth');
//         expect(guard.auth.user).toEqual({});
//         expect(NavigationService.prototype.goTo).toHaveBeenCalledWith('/login', false);
//     })));

// })
// import { TestBed, tick, fakeAsync } from "@angular/core/testing";
// import { MaterialModule } from "../modules/material/material.module";
// import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
// import { AUTH_SERVICE_PROVIDERS_MOCK } from "../services/auth.service.spec";
// import { AuthedUser } from "./authed-user.guard";
// import { inject } from "@angular/core/testing";
// import { CacheService } from "../services/cache.service";
// import { AuthService } from "../services/auth.service";
// import { NavigationService } from "../services/navigation.service";
// import { NotAuthedUser } from "./not-authed-user.guard";


// describe('NotAuthedUserGuard', () => {
//     let expectedAccount;

//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             imports: [MaterialModule],
//             schemas: [CUSTOM_ELEMENTS_SCHEMA],
//             providers: [
//                 ...AUTH_SERVICE_PROVIDERS_MOCK,
//                 NotAuthedUser,
//                 CacheService,
//                 NavigationService
//             ]
//         });
//     });

//     it('should be created', inject([NotAuthedUser], (guard: NotAuthedUser) => {
//         expect(guard).toBeTruthy();
//     }));

//     it('should redirect to home if the user is already logged in', inject([NotAuthedUser], fakeAsync((guard: AuthedUser) => {
//         spyOn(CacheService.prototype, 'get').and.returnValue(Promise.resolve({ uid: 'a1b2c3' }));
//         spyOn(NavigationService.prototype, 'goTo');
//         guard.canActivateChild();
//         tick();
//         expect(CacheService.prototype.get).toHaveBeenCalledWith('auth');
//         expect(NavigationService.prototype.goTo).toHaveBeenCalledWith('/home', false);
//     })));

//     it('should can activate child routes', inject([NotAuthedUser], fakeAsync((guard: AuthedUser) => {
//         spyOn(CacheService.prototype, 'get').and.returnValue(Promise.resolve(null));
//         spyOn(NavigationService.prototype, 'goTo');
//         guard.canActivateChild();
//         tick();
//         expect(CacheService.prototype.get).toHaveBeenCalledWith('auth');
//         expect(NavigationService.prototype.goTo).not.toHaveBeenCalled();

//     })));

// })
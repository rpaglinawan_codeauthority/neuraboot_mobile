import { Numbers } from "./numbers";
export const PI = 3.141592653589793238;

describe('Utils / Numbers', () => {
    it('should round PI value', () => {
        expect(Numbers.round(PI, 2)).toBe(3.14);
    });
});
import * as _ from 'lodash';
import * as Firebase from 'firebase/app';

import { TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AUTH_SERVICE_PROVIDERS_MOCK } from '../services/auth.service.spec';
import { FirebaseConnector, FirestoreConnector, FirebaseApi } from './firebase';

//
// Firestore Stub
export const FirestoreStub = ({
    get = Promise.resolve(true),
    set = Promise.resolve(true),
    update = Promise.resolve(true)
}: {
        get?: Promise<any>,
        set?: Promise<any>,
        update?: Promise<any>,
    }): {
        firestore: any,
        collection: any,
        doc: any,
        get: any,
        set: any,
        update: any
    } => {
    const firestoreCollectionDocGetStub: any = jasmine.createSpy('get').and.returnValue(get);

    const firestoreCollectionDocSetStub: any = jasmine.createSpy('set').and.returnValue(set);

    const firestoreCollectionDocUpdateStub: any = jasmine.createSpy('update').and.returnValue(update);

    const firestoreCollectionDocStub: any = jasmine.createSpy('doc').and.returnValue({
        get: firestoreCollectionDocGetStub,
        set: firestoreCollectionDocSetStub,
        update: firestoreCollectionDocUpdateStub
    });

    const firestoreCollectionStub: any = jasmine.createSpy('collection').and.returnValue({
        doc: firestoreCollectionDocStub
    });

    const firestoreStub: any = {
        collection: firestoreCollectionStub
    };

    return {
        firestore: firestoreStub,
        collection: firestoreCollectionStub,
        doc: firestoreCollectionDocStub,
        get: firestoreCollectionDocGetStub,
        set: firestoreCollectionDocSetStub,
        update: firestoreCollectionDocUpdateStub
    };
}


//
// Firebase Stub
export const FirebaseStub = ({
    signInEmail = Promise.resolve(true),
    signInCredential = Promise.resolve(true),
    createUser = Promise.resolve(true),
    signOut = Promise.resolve(true),
    resetPassword = Promise.resolve(true),
    updatePassword = Promise.resolve(true),
    updateProfile = Promise.resolve(true),
    facebookCredential = Promise.resolve(true),
    googleCredential = Promise.resolve(true)
}: {
        signInEmail?: Promise<any>,
        signInCredential?: Promise<any>,
        createUser?: Promise<any>,
        signOut?: Promise<any>,
        resetPassword?: Promise<any>,
        updatePassword?: Promise<any>,
        updateProfile?: Promise<any>,
        facebookCredential?: Promise<any>,
        googleCredential?: Promise<any>
    }): {
        firebase: any,
        auth: any,
        signInWithEmailAndPassword: any,
        signInWithCredential: any,
        createUserWithEmailAndPassword: any,
        signOut: any,
        sendPasswordResetEmail: any,
        updatePassword: any,
        updateProfile: any,
        facebookCredential: any,
        googleCredential: any
    } => {
    const firebaseFacebookAuthProviderCredentialStub: any = jasmine.createSpy('credential').and.returnValue(facebookCredential);
    const firebaseGoogleAuthProviderCredentialStub: any = jasmine.createSpy('credential').and.returnValue(googleCredential);
    const firebaseSignInEmailStub: any = jasmine.createSpy('signInWithEmailAndPassword').and.returnValue(signInEmail);
    const firebaseCreateUserStub: any = jasmine.createSpy('createUserWithEmailAndPassword').and.returnValue(createUser);
    const firebaseSignOutStub: any = jasmine.createSpy('signOut').and.returnValue(signOut);
    const firebaseResetPasswordStub: any = jasmine.createSpy('sendPasswordResetEmail').and.returnValue(resetPassword);
    const firebaseUpdatePasswordStub: any = jasmine.createSpy('updatePassword').and.returnValue(updatePassword);
    const firebaseUpdateProfileStub: any = jasmine.createSpy('updateProfile').and.returnValue(updateProfile);
    const firebaseSignInCredentialStub: any = jasmine.createSpy('signInWithCredential').and.returnValue(signInCredential);

    const firebaseAuthStub: any = jasmine.createSpy('auth').and.returnValue({
        signInWithEmailAndPassword: firebaseSignInEmailStub,
        createUserWithEmailAndPassword: firebaseCreateUserStub,
        signOut: firebaseSignOutStub,
        sendPasswordResetEmail: firebaseResetPasswordStub,
        signInWithCredential: firebaseSignInCredentialStub,
        currentUser: {
            updatePassword: firebaseUpdatePasswordStub,
            updateProfile: firebaseUpdateProfileStub
        }
    });

    const firebaseStub: any = {
        auth: firebaseAuthStub
    };

    _.extend(firebaseStub.auth, {
        FacebookAuthProvider: {
            credential: firebaseFacebookAuthProviderCredentialStub,
        },
        GoogleAuthProvider: {
            credential: firebaseGoogleAuthProviderCredentialStub,
        }
    })

    return {
        firebase: firebaseStub,
        auth: firebaseAuthStub,
        signInWithEmailAndPassword: firebaseSignInEmailStub,
        signInWithCredential: firebaseSignInCredentialStub,
        createUserWithEmailAndPassword: firebaseCreateUserStub,
        signOut: firebaseSignOutStub,
        sendPasswordResetEmail: firebaseResetPasswordStub,
        updatePassword: firebaseUpdatePasswordStub,
        updateProfile: firebaseUpdateProfileStub,
        facebookCredential: firebaseFacebookAuthProviderCredentialStub,
        googleCredential: firebaseGoogleAuthProviderCredentialStub
    };
}


//
// Firebase Spec
describe('Firebase', () => {

    it('should initialize the firebase app', () => {
        const firebase = Firebase;
        spyOn(firebase, 'initializeApp');
        spyOnProperty(firebase, 'apps').and.returnValue([])
        const instance: any = new FirebaseConnector(firebase);
        expect(firebase.initializeApp).toHaveBeenCalled();
    });

    it('should return the initialized firebase app', () => {
        const firebase = Firebase;
        spyOn(firebase, 'initializeApp');
        spyOnProperty(firebase, 'apps').and.returnValue([{ foo: 'app' }])
        const instance: any = new FirebaseConnector(firebase);
        expect(firebase.initializeApp).not.toHaveBeenCalled();
        expect(instance).toBeTruthy();
    });


    it('should initialize the firestore app', () => {
        const initializeAppStub = jasmine.createSpy('initializeApp').and.returnValue(true);

        const firebase = {
            apps: [],
            initializeApp: initializeAppStub
        }

        _.extend(firebase, {
            default: {
                firestore: () => {
                    return {
                        settings: () => { }
                    }
                }
            }
        })

        const instance: any = new FirestoreConnector(firebase);
        expect(initializeAppStub).toHaveBeenCalled();
    });

    it('should return the initialized firestore app', () => {
        const initializeAppStub = jasmine.createSpy('initializeApp').and.returnValue(true);

        const firebase = {
            apps: [{
                foo: 'app',
                firebase_: {
                    firestore: () => {
                        return {
                            settings: () => { }
                        }
                    }
                }

            }],
            initializeApp: initializeAppStub
        }

        _.extend(firebase, {
            default: {
                firestore: () => {
                    return {
                        settings: () => { }
                    }
                }
            }
        })

        const instance: any = new FirestoreConnector(firebase);
        expect(initializeAppStub).not.toHaveBeenCalled();
        expect(instance).toBeTruthy();
    });
    
    it('should instantiate the firebase api', () => {
        const firebase = Firebase;
        const instance = new FirebaseApi(firebase);
        expect(instance._firebase).toBeTruthy();
    });

    it('should instantiate the firestore api', () => {
        const firebase = Firebase;
        const instance = new FirebaseApi(firebase);
        expect(instance._firestore).toBeTruthy();
    });
})
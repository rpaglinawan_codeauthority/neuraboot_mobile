import * as Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import { environment } from '../../environments/environment';

export class FirebaseConnector {
  constructor(firebase) {
    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebase);
      return firebase.default;
    } else {
      //let identifier = utils.guid(1);
      return firebase.apps[0].firebase_;
    }
  }
}

export class FirestoreConnector {
  constructor(firebase) {
    const settings = {/* your settings... */ timestampsInSnapshots: true };

    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebase);
      const firestore = firebase.default.firestore();
      firestore.settings(settings);
      return firestore;
    } else {
      // let identifier = utils.guid(1);
      const firestore = firebase.apps[0].firebase_.firestore();
      firestore.settings(settings);
      return firestore;
    }
  }
}

export class FirebaseApi {

  public _firebase: any;
  public _firestore: any;
  
  get firebase() {
    return this._firebase;
  }

  set firebase(instance) {
    this._firebase = instance;
  }

  get firestore() {
    return this._firestore;
  }

  set firestore(instance) {
    this._firestore = instance;
  }

  constructor(firebase) {
    this.firebase = new FirebaseConnector(firebase);
    this.firestore = new FirestoreConnector(firebase);
  }
}

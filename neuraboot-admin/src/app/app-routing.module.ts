import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAuthedGuard } from './guards/user-authed.guard';
import { MetaGuard } from '@ngx-meta/core';
import { UserNotAuthedGuard } from './guards/user-not-authed.guard';

const routes: Routes = [{
    path: '',
    loadChildren: './modules/auth/auth.module#AuthModule',
    canActivate: [UserAuthedGuard],
    canActivateChild: [MetaGuard]
}, {
    path: 'login',
    loadChildren: './modules/account/account.module#AccountModule',
    canActivateChild: [MetaGuard],
    canActivate: [UserNotAuthedGuard]
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { IonicStorageModuleConfig } from './constants/storage';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material';
import { MetaModule } from '@ngx-meta/core';
import { UserAuthedGuard } from './guards/user-authed.guard';
import { UserNotAuthedGuard } from './guards/user-not-authed.guard';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        MatSnackBarModule,
        MetaModule.forRoot(),
        IonicStorageModule.forRoot(IonicStorageModuleConfig)
    ],
    providers: [
        ScrollToService,
        UserAuthedGuard,
        UserNotAuthedGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersRouteComponent } from './users-route/users-route.component';
import { UserListRouteComponent } from './user-list-route/user-list-route.component';
import { UserListComponent } from './user-list/user-list.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { CovalentTableModule } from '../covalent-table/covalent-table.module';
import { UserEditRouteComponent } from './user-edit-route/user-edit-route.component';
import { UserViewRouteComponent } from './user-view-route/user-view-route.component';
import { ChartsModule } from '../charts/charts.module';
import { UserViewDetailsRouteComponent } from './user-view-details-route/user-view-details-route.component';
import { UserCurrentFeelingComponent } from './user-current-feeling/user-current-feeling.component';

@NgModule({
    declarations: [UsersRouteComponent, UserListRouteComponent, UserListComponent, UserEditRouteComponent, UserViewRouteComponent, UserViewDetailsRouteComponent, UserCurrentFeelingComponent],
    imports: [
        CommonModule,
        UsersRoutingModule,
        ComponentsModule,
        CovalentTableModule,
        ChartsModule
    ]
})
export class UsersModule { }

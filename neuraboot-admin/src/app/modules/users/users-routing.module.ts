import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SideMenu } from 'src/app/constants/menu';
import { UsersRouteComponent } from './users-route/users-route.component';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { UserListRouteComponent } from './user-list-route/user-list-route.component';
import { UserEditRouteComponent } from './user-edit-route/user-edit-route.component';
import { UserViewRouteComponent } from './user-view-route/user-view-route.component';
import { UserViewDetailsRouteComponent } from './user-view-details-route/user-view-details-route.component';

//
// Setup metadata
let userEditMeta: any = {};
let userListMeta: any = {};

SideMenu.map(m => {
    if (m.options) {
        m.options.map(o => {
            if (o.route === '/users/add') {
                userEditMeta = o.meta;
            }
            if (o.route === '/users') {
                userListMeta = o.meta;
            }
        })
    }
});

const routes: Routes = [{
    path: '',
    component: UsersRouteComponent,
    canActivate: [UserAuthedGuard],
    children: [{
        path: '',
        component: UserListRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: userListMeta
        }
    }, {
        path: 'add',
        component: UserEditRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: userListMeta
        }
    }, {
        path: 'edit/:uid',
        component: UserEditRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: userListMeta
        }
    }, {
        path: 'view/:uid',
        component: UserViewRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: userListMeta
        },
        children: [{
            path: '',
            component: UserViewDetailsRouteComponent,
            canActivate: [UserAuthedGuard],
            data: {
                meta: userListMeta
            },
        }]
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { MetaService } from '@ngx-meta/core';
import { MenuService } from 'src/app/services/menu.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';
import { UserRoles } from '../../../../../../neuraboot-core/constants/user.roles';
import { map, tap, catchError } from 'rxjs/operators';
import { RRResponse } from '@ionfire/reactive-record';
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { getCountryEmoji } from '../../../../../../neuraboot-core/constants/country';
import { of, Subscription } from 'rxjs';
import { cloneDeep, remove } from 'lodash';
import clean from 'lodash-clean';
import * as moment from 'moment';
import { CacheService } from 'src/app/services/cache.service';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'app-user-edit-route',
    templateUrl: './user-edit-route.component.html',
    styleUrls: ['./user-edit-route.component.scss'],
    providers: [UserService]
})
export class UserEditRouteComponent implements OnInit {

    public moment: any = moment;
    public countryEmoji: any = getCountryEmoji;
    public firstName: string;
    public data: User = { roles: [] };
    public data$: Subscription;
    public route$: any;
    public userRoles: any[] = cloneDeep(UserRoles.filter(u => u.name != 'all'));

    public editing: boolean;
    public password: string;
    public passwordRepeat: string;

    constructor(
        public auth: AuthService,
        private userService: UserService,
        private meta: MetaService,
        private menuService: MenuService,
        private router: Router,
        private route: ActivatedRoute,
        public ui: UiService,
        public cache: CacheService
    ) {
        this.route$ = this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                const UID: string = this.route.snapshot.params.uid ? this.route.snapshot.params.uid : null;
                if (UID) {
                    this.editing = true;
                    this.ui.scrollTop();
                    this.ui.loading = true;
                    this.data$ = this.userService.findOne({ query: { field: 'uid', operator: '==', value: this.route.snapshot.params.uid } })
                        .pipe(
                            map((r: RRResponse) => r.data),
                            tap((user: User) => {
                                if (!user.roles) user.roles = [];
                                const firstName = this.firstName = user.display_name.split(' ')[0];

                                this.menuService.breadcrumbs = [{
                                    icon: 'fas fa-users',
                                    placeholder: 'Users',
                                    route: '/users'
                                },
                                {
                                    placeholder: 'Manage',
                                    route: '/users'
                                }, {
                                    placeholder: firstName,
                                    route: `/users/${user.uid}`
                                }, {
                                    placeholder: 'Edit'
                                }]

                                this.meta.setTitle(`Users / Manage / ${firstName} / Edit`);
                                this.data = user;
                                this.ui.loading = false;
                            }),
                            catchError((err: ApiErrorResponse | any) => {
                                const message: string = err.message ? err.message : err;
                                console.log(message);
                                this.ui.say(message);
                                this.ui.loading = false;
                                return of(err);
                            })
                        )
                        .subscribe();
                } else {
                    this.editing = false;
                    this.menuService.breadcrumbs = [{
                        icon: 'fas fa-users',
                        placeholder: 'Users',
                        route: '/users'
                    },
                    {
                        placeholder: 'Manage',
                        route: '/users'
                    }, {
                        placeholder: 'Add'
                    }];

                    this.meta.setTitle(`Users / Manage / Add`);
                }
            }
        });
    }

    ngOnInit() { }

    ngOnDestroy() {
        this.route$.unsubscribe();
        if (this.data$) this.data$.unsubscribe();
    }

    applyRole(ev: any, role: any) {
        if (ev.checked) {
            if (!this.data.roles)
                this.data.roles = [];

            if (this.data.roles.indexOf(role.name) < 0) {
                this.data.roles.push(role.name)
            }
        } else {
            remove(this.data.roles, r => r === role.name);
        }
    }

    async submit() {
        this.ui.loading = true;
        this.ui.scrollTop();

        //
        // existing record
        if (this.editing) {
            this.ui.say('Updating user...');
            let data: User = clean(<User>{
                display_name: this.data.display_name,
                email: this.data.email,
                roles: this.data.roles,
                updated_at: moment().toISOString()
            })
            this.userService
                .update(this.data.uid, data).toPromise()
                .then(r => {
                    setTimeout(() => {
                        this.ui.say('The user has been updated');
                        this.router.navigate(['/users/view/' + this.data.uid]);
                        this.ui.loading = false;
                        this.cache.remove('users/{"query":{"field":"uid","operator":"==","value":"' + this.data.uid + '"}}');
                    }, 5000);
                })
                .catch(err => {
                    const message: string = err.message ? err.message : err;
                    console.log(message);
                    this.ui.say(message);
                    this.ui.loading = false;
                });
        } else {
            //
            // new record
            if (this.data.password != this.passwordRepeat) {
                this.ui.say('The password/repeat field must match');
                this.ui.loading = false;
            } else {
                this.ui.say('Creating user...', null, '');

                this.userService.post('/auth/create', this.data).toPromise()
                    .then(r => {
                        setTimeout(() => {
                            this.ui.say('The user has been created');
                            this.router.navigate(['/users']);
                            this.ui.loading = false;
                        }, 5000)
                    })
                    .catch(err => {
                        const message: string = err.message ? err.message : err;
                        console.log(message);
                        this.ui.say(message);
                        this.ui.loading = false;
                    })
            }
        }
    }

    cancel() {
        if (this.data.uid)
            this.router.navigate(['/users/view/' + this.data.uid]);
        else
            this.router.navigate(['/dashboard']);
    }

}

import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
    selector: 'app-user-list-route',
    templateUrl: './user-list-route.component.html',
    styleUrls: ['./user-list-route.component.scss'],
    providers: [UserService]
})
export class UserListRouteComponent implements OnInit {

    constructor(
        public navigation: NavigationService,
        public service: UserService
    ) { }

    ngOnInit() {

        //
        // Set main fab action for customer
        this.navigation.setMainFabAction(this.goToCreateRoute.bind(this));
    }

    ngOnDestroy() {
        this.navigation.setMainFabAction();
    }

    /**
     * Go to create route
     *
     * @memberof UserListRouteComponent
     */
    goToCreateRoute() {
        this.navigation.goTo(this.service.createUrl);
    }

}

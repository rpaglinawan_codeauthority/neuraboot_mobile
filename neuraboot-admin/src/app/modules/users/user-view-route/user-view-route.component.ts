import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';
import { MenuService } from 'src/app/services/menu.service';
import { MetaService } from '@ngx-meta/core';
import { UiService } from 'src/app/services/ui.service';
import { RecordService } from 'src/app/services/record.service';
import { AuthService } from 'src/app/services/auth.service';
import { getCountryEmoji } from '../../../../../../neuraboot-core/constants/country';
import { remove } from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'app-user-view-route',
    templateUrl: './user-view-route.component.html',
    styleUrls: ['./user-view-route.component.scss'],
    providers: [UserService, RecordService]
})
export class UserViewRouteComponent implements OnInit {

    public uid: string = null;
    public data$: Subscription;
    public firstname: string = null;
    public moment = moment;
    public countryEmoji: any = getCountryEmoji;

    constructor(
        public route: ActivatedRoute,
        public userService: UserService,
        public menu: MenuService,
        public meta: MetaService,
        public ui: UiService,
        public record: RecordService,
        public auth: AuthService,
        public router: Router
    ) { }

    ngOnInit() {

        //
        // Set uid
        this.uid = this.route.snapshot.params.uid;

        //
        // Setup record service
        this.record.id = this.uid;
        this.record.service = this.userService;
        this.record.request = { query: { field: 'uid', operator: '==', value: this.uid } };
        this.record.postGetHook = this.postGetHook.bind(this);

        //
        // Load
        this.data$ = this.record.setup().subscribe();
    }

    ngOnDestroy() {
        this.data$.unsubscribe();
    }

    postGetHook(user: User) {
        console.log('user', user);
        if (!user.roles) user.roles = [];

        //
        // Set firstname
        this.firstname = user.display_name.split(' ')[0];

        //
        // Set breadcrumb and meta
        this.menu.breadcrumbs = [{
            icon: 'fas fa-users',
            placeholder: 'Users',
            route: '/users/manage'
        },
        {
            placeholder: 'Manage',
            route: '/users/manage'
        }, {
            placeholder: this.firstname
        }];

        this.meta.setTitle(`Users / Manage / ${this.firstname}`);

        return user;
    }

    reset(email: string) {
        if (!email) return this.ui.say(`This user doesn't have an email address`);
        this.ui.ask(`Send email with password reset instructions for ${this.firstname}?`, {}, async () => {
            this.auth.firebase.auth().sendPasswordResetEmail(email).catch(err => this.ui.say(err));
            this.ui.say('The password reset email has been sent');
        });
    }


    delete(user: User) {
        this.ui.ask(`Are you sure you want to remove ${this.firstname}?`, {}, async () => {
            this.ui.loading = true;
            this.ui.scrollTop();
            this.ui.say(`Removing ${this.firstname}...`);
            // log for future
            await this.auth.firebase.database().ref('deleted-users/' + user.uid).update(user);
            // remove user data
            await this.auth.firestore.collection('users').doc(user.uid).delete().catch(err => this.ui.say(err));
            setTimeout(() => {
                this.ui.say(`${this.firstname} has been removed`);
                this.router.navigate([`/users/manage`]);
                this.ui.loading = false;
            }, 7000); // give a time to update in Elastic
        });
    }


    switchAdmin() {
        const isAdmin = this.record.entry.roles && this.record.entry.roles.indexOf('admin') > -1;
        this.ui.ask(isAdmin ? `Remove admin role from ${this.firstname}?` : `Make ${this.firstname} an administrator?`, {}, async () => {
            if (isAdmin) {
                remove(this.record.entry.roles, r => r === 'admin');
                await this.userService.update(this.record.entry.uid, { roles: this.record.entry.roles }).toPromise()
                    .then(r => {
                        this.ui.say(`${this.firstname} is no longer an admin`);
                    })
                    .catch(err => {
                        const message: string = err.message ? err.message : err;
                        console.log(message);
                        this.ui.say(message);

                    })
            } else {
                if (!this.record.entry.roles) this.record.entry.roles = [];
                this.record.entry.roles.push('admin')
                await this.userService.update(this.record.entry.uid, { roles: this.record.entry.roles }).toPromise()
                    .then(r => {
                        this.ui.say(`${this.firstname} is now an admin`);
                    })
                    .catch(err => {
                        const message: string = err.message ? err.message : err;
                        console.log(message);
                        this.ui.say(message);
                    })
            }
        })
    }

    getCountryEmoji() {
        return this.countryEmoji(this.record.entry.geolocation.country_code, true);
    }
}

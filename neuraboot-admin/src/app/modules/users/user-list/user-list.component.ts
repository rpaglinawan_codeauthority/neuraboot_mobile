import { Component, OnInit } from '@angular/core';
import { TableEntriesSetup, TableRowClickEvent } from 'src/app/interfaces/table';
import { UserRoles } from '../../../../../../neuraboot-core/constants/user.roles';
import { UserTableColumns, UserTableFilters } from '../../../../../../neuraboot-core/constants/user.table';
import { ElasticQueryBase } from '../../../../../../neuraboot-core/constants/elastic';
import { cloneDeep } from 'lodash';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
    providers: [UserService]
})
export class UserListComponent implements OnInit {

    public setup: TableEntriesSetup = {
        service: this.user,
        path: '/find',
        query: cloneDeep(ElasticQueryBase)
    }

    public columns: any[] = UserTableColumns;
    public filters: any[] = UserTableFilters;

    constructor(
        public user: UserService
    ) { }

    ngOnInit() {
        this.setup.query = cloneDeep(ElasticQueryBase);
        this.setup.query.sort = [{
            "online_at": {
                "order": "desc"
            }
        }];

        this.filters[0].options = [...UserRoles];
    }

    rowClick($ev: TableRowClickEvent) {
        $ev.router.navigate(['/users/view/' + $ev.row.uid]);
    }
}

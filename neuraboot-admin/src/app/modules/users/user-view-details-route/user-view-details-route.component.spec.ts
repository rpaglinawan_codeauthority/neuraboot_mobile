import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserViewDetailsRouteComponent } from './user-view-details-route.component';

describe('UserViewDetailsRouteComponent', () => {
  let component: UserViewDetailsRouteComponent;
  let fixture: ComponentFixture<UserViewDetailsRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserViewDetailsRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserViewDetailsRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

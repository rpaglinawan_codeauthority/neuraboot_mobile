import { Component, OnInit } from '@angular/core';
import { RecordService } from 'src/app/services/record.service';
import { FormatDay, FormatMonth } from '../../../../../../neuraboot-core/constants/calendar';
import { QueryUserFeelingCountLastMonth } from '../../../../../../neuraboot-core/queries/user-feeling-count-last-month';
import { QueryUserFeelingCountLastYear } from '../../../../../../neuraboot-core/queries/user-feeling-count-last-year';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { ChartNumberSetup } from '../../../../../../neuraboot-core/interfaces/chart-number.interface';
import { ChallengeHistoryService } from 'src/app/services/challenge-history.service';
import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';
import { QueryUserTopFeelingCategories } from '../../../../../../neuraboot-core/queries/user-top-feeling-categories';
import { QueryUserTopFeelings } from '../../../../../../neuraboot-core/queries/user-top-feelings';

@Component({
    selector: 'app-user-view-details-route',
    templateUrl: './user-view-details-route.component.html',
    styleUrls: ['./user-view-details-route.component.scss'],
    providers: [FeelingHistoryService, ChallengeHistoryService]
})
export class UserViewDetailsRouteComponent implements OnInit {

    public chartLines: any[] = [];
    public chartNumberFeeling: { extra: any, setup: ChartNumberSetup }[] = [];
    public chartNumberChallenge: { extra: any, setup: ChartNumberSetup }[] = [];
    public chartBarTopUserCategories: ChartBarVerticalSetup[] = [];
    public chartBarTopUserFeelings: ChartBarVerticalSetup[] = [];
    public chartBarSecondaryColors: any = {
        domain: ['#33CCFF', '#33FFFF', '#33FF66', '#CCFF33', '#FFCC00', '#FF6600', '#FF3333', '#FF33FF', '#CC33FF', '#6e7fff']
    };

    constructor(
        public record: RecordService,
        public feelingHistory: FeelingHistoryService,
        public challengeHistory: ChallengeHistoryService
    ) { }

    ngOnInit() {
        this.setupChartNumber();
        this.setupChartBar();
        this.setupChartLine();
    }

    setupChartNumber() {

        //
        // chart numbers
        this.chartNumberFeeling = [
            {
                extra: {
                    title: 'Feelings selected',
                    size: 'is-5',
                    icon: 'fas fa-heartbeat'
                },
                setup: {
                    service: this.feelingHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "user.uid.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        ]
        this.chartNumberChallenge = [
            {
                extra: {
                    title: 'Completed challenges',
                    size: 'is-6',
                    icon: 'far fa-check-circle'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "completed"
                                        }
                                    },
                                    {
                                        "match": {
                                            "user.uid.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Cancelled challenges',
                    size: 'is-6',
                    icon: 'fa fa-ban'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "cancelled"
                                        }
                                    },
                                    {
                                        "match": {
                                            "user.uid.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        ]
    }

    setupChartBar() {

        this.chartBarTopUserFeelings = [{
            name: 'top-user-feelings',
            placeholder: 'Top 5 feelings',
            title: `<strong>Top 5</strong> feelings`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryUserTopFeelings(this.record.id),
            service: this.feelingHistory,
            ttl: 60 // in seconds
        }]

        this.chartBarTopUserCategories = [{
            name: 'top-user-feeling-categories',
            placeholder: 'Top 5 feeling categories',
            title: `<strong>Top 5</strong> feeling categories`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryUserTopFeelingCategories(this.record.id),
            service: this.feelingHistory,
            ttl: 60 // in seconds
        }]
    }

    setupChartLine() {

        //
        // Chart lines
        this.chartLines = [
            {
                name: 'feeling-last-month',
                placeholder: 'Feeling Last Month',
                title: `<strong>Feeling count</strong> on last 30 days`,
                xAxisTickFormatting: FormatDay,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryUserFeelingCountLastMonth(this.record.id),
                service: this.feelingHistory,
                ttl: 60
            },
            {
                name: 'last-year',
                placeholder: 'Feeling Last Year',
                title: `<strong>Feeling count</strong> on last year`,
                xAxisTickFormatting: FormatMonth,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryUserFeelingCountLastYear(this.record.id),
                service: this.feelingHistory,
                ttl: 60
            }
        ]
    }
}

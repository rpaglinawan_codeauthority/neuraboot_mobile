import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCurrentFeelingComponent } from './user-current-feeling.component';

describe('UserCurrentFeelingComponent', () => {
  let component: UserCurrentFeelingComponent;
  let fixture: ComponentFixture<UserCurrentFeelingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCurrentFeelingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCurrentFeelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

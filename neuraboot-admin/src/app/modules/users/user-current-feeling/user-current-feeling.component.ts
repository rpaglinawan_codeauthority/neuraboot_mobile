import { Component, OnInit, Input } from '@angular/core';
import { FeelingHistory } from '../../../../../../neuraboot-core/interfaces/feeling-history.interface';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { take } from 'rxjs/operators';
import { isEmpty, orderBy } from 'lodash';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';

@Component({
    selector: 'user-current-feeling',
    templateUrl: './user-current-feeling.component.html',
    styleUrls: ['./user-current-feeling.component.scss'],
    providers: [FeelingHistoryService, FeelingSpecificService]
})
export class UserCurrentFeelingComponent implements OnInit {

    @Input() uid: string = null;

    public entry: FeelingHistory = null;
    public load: boolean = false;

    constructor(
        public feelingHistory: FeelingHistoryService,
        public feelingSpecific: FeelingSpecificService
    ) { }

    ngOnInit() {
        this.setup();
    }

    /**
     * Setup
     *
     * @description Get current feeling and display
     * @memberof UserCurrentFeelingComponent
     */
    async setup() {

        //
        // Set laod
        this.load = true;

        //
        // Get last feeling
        let result = await this.feelingHistory.find({
            size: 1,
            query: { field: 'user.uid', operator: '==', value: this.uid },
            sort: [{
                "created_at": {
                    "order": "desc"
                }
            }]
        }).pipe(
            take(1)
        ).toPromise();

        //
        // Validate entry
        if (isEmpty(result) || isEmpty(result.data)) {
            this.load = false;
            return;
        };

        //
        // Sort
        result = orderBy(result.data, ['created_at'], ['desc']);

        console.log('result sorted', result);

        //
        // Get first and unset load
        this.entry = result[0];
        this.load = false;
    }
}

import { Component, OnInit } from '@angular/core';
import { RecordService } from 'src/app/services/record.service';
import { FormatDay, FormatMonth } from '../../../../../../neuraboot-core/constants/calendar';
import { QueryFeelingSpecificCountLastMonth } from '../../../../../../neuraboot-core/queries/feeling-specific-count-last-month';
import { QueryFeelingSpecificCountLastYear } from '../../../../../../neuraboot-core/queries/feeling-specific-count-last-year';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { ChartNumberSetup } from '../../../../../../neuraboot-core/interfaces/chart-number.interface';

@Component({
    selector: 'app-feeling-view-details-route',
    templateUrl: './feeling-view-details-route.component.html',
    styleUrls: ['./feeling-view-details-route.component.scss'],
    providers: [FeelingHistoryService]
})
export class FeelingViewDetailsRouteComponent implements OnInit {

    public chartLines: any[] = [];
    public chartNumbers: { extra: any, setup: ChartNumberSetup }[] = [];

    constructor(
        public record: RecordService,
        public feelingHistory: FeelingHistoryService
    ) { }

    ngOnInit() {

        this.chartLines = [
            {
                name: 'feeling-specific-last-month',
                placeholder: 'Feeling Last Month',
                title: `<strong>time(s) selected</strong> on last 30 days`,
                xAxisTickFormatting: FormatDay,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryFeelingSpecificCountLastMonth(this.record.entry.name),
                service: this.feelingHistory,
                ttl: 60
            },
            {
                name: 'feeling-specific-last-year',
                placeholder: 'Feeling Last Year',
                title: `<strong>time(s) selected</strong> on last year`,
                xAxisTickFormatting: FormatMonth,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryFeelingSpecificCountLastYear(this.record.entry.name),
                service: this.feelingHistory,
                ttl: 60
            }
        ]

        //
        // chart numbers
        this.chartNumbers = [
            {
                extra: {
                    title: 'Selection(s)',
                    size: 'is-6',
                    icon: 'far fa-calendar-alt'
                },
                setup: {
                    service: this.feelingHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "specific.name.keyword": this.record.entry.name
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Selection(s) today',
                    size: 'is-6',
                    icon: 'far fa-calendar-check',
                },
                setup: {
                    service: this.feelingHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "specific.name.keyword": this.record.entry.name
                                        }
                                    },
                                    {
                                        "range": {
                                            "created_at": {
                                                "gt": "now-1d/d",
                                                "lte": "now/d"
                                            }
                                        }
                                    }

                                ]
                            }
                        }
                    }
                }
            }
        ]
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingViewDetailsRouteComponent } from './feeling-view-details-route.component';

describe('FeelingViewDetailsRouteComponent', () => {
  let component: FeelingViewDetailsRouteComponent;
  let fixture: ComponentFixture<FeelingViewDetailsRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingViewDetailsRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingViewDetailsRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

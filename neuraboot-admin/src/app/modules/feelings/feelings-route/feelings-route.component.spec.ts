import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingsRouteComponent } from './feelings-route.component';

describe('FeelingsRouteComponent', () => {
  let component: FeelingsRouteComponent;
  let fixture: ComponentFixture<FeelingsRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingsRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingsRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

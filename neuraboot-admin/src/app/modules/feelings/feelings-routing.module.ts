import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SideMenu } from 'src/app/constants/menu';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { FeelingsRouteComponent } from './feelings-route/feelings-route.component';
import { FeelingListRouteComponent } from './feeling-list-route/feeling-list-route.component';
import { FeelingAddRouteComponent } from './feeling-add-route/feeling-add-route.component';
import { FeelingViewRouteComponent } from './feeling-view-route/feeling-view-route.component';
import { FeelingViewDetailsRouteComponent } from './feeling-view-details-route/feeling-view-details-route.component';
import { FeelingViewEditRouteComponent } from './feeling-view-edit-route/feeling-view-edit-route.component';
import { FeelingCategoryListRouteComponent } from './feeling-category-list-route/feeling-category-list-route.component';
import { FeelingCategoryViewRouteComponent } from './feeling-category-view-route/feeling-category-view-route.component';

//
// Setup metadata
let metadata: any = {};

SideMenu.map(m => {
    if (m.options) {
        m.options.map(o => {
            if (o.route === '/feelings') {
                metadata = o.meta;
            }
        })
    }
});

const routes: Routes = [{
    path: '',
    component: FeelingsRouteComponent,
    canActivate: [UserAuthedGuard],
    children: [{
        path: '',
        component: FeelingListRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }, {
        path: 'add',
        component: FeelingAddRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }, {
        path: 'view/:id',
        component: FeelingViewRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        },
        children: [{
            path: '',
            component: FeelingViewDetailsRouteComponent,
            canActivate: [UserAuthedGuard],
            data: {
                meta: metadata
            },
        }, {
            path: 'edit',
            component: FeelingViewEditRouteComponent,
            canActivate: [UserAuthedGuard],
            data: {
                meta: metadata
            },
        }]
    }, {
        path: 'categories',
        component: FeelingCategoryListRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }, {
        path: 'categories/view/:id',
        component: FeelingCategoryViewRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeelingsRoutingModule { }

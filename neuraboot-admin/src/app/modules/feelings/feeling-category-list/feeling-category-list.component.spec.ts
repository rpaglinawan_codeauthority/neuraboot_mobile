import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingCategoryListComponent } from './feeling-category-list.component';

describe('FeelingCategoryListComponent', () => {
  let component: FeelingCategoryListComponent;
  let fixture: ComponentFixture<FeelingCategoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingCategoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FeelingCategory } from '../../../../../../neuraboot-core/interfaces/feeling-category.interface';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
    selector: 'feeling-category-list',
    templateUrl: './feeling-category-list.component.html',
    styleUrls: ['./feeling-category-list.component.scss']
})
export class FeelingCategoryListComponent implements OnInit {

    public feelingCategories: FeelingCategory[] = [{
        label: 'Angry',
        value: 'angry',
        icon: 'fa-angry'
    }, {
        label: 'Sad',
        value: 'depressed',
        icon: 'fa-sad-cry'
    }, {
        label: 'Anxious',
        value: 'anxious',
        icon: 'fa-grimace'
    }, {
        label: 'Happy',
        value: 'happy',
        icon: 'fa-smile'
    }];

    constructor(
        public navigation: NavigationService
    ) { }

    ngOnInit() {
    }

}

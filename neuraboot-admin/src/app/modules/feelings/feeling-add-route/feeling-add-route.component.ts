import { Component, OnInit } from '@angular/core';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
    selector: 'app-feeling-add-route',
    templateUrl: './feeling-add-route.component.html',
    styleUrls: ['./feeling-add-route.component.scss'],
    providers: [FeelingSpecificService]
})
export class FeelingAddRouteComponent implements OnInit {

    constructor(
        public service: FeelingSpecificService,
        public navigation: NavigationService
    ) { }

    ngOnInit() {
    }

    /**
     * On save
     *
     * @description On save event
     * @param event
     * @memberof FeelingAddRouteComponent
     */
    onSave(event) {

        //
        // Validate
        if (!event || !event.success) return;

        this.navigation.goTo(this.service.listUrl);
    }

    /**
     * On cancel
     *
     * @description On cancel event
     * @param event
     * @memberof FeelingAddRouteComponent
     */
    onCancel(event) {
        this.navigation.goTo(this.service.listUrl);
    }
}

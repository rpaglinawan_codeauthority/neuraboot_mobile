import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingAddRouteComponent } from './feeling-add-route.component';

describe('FeelingAddRouteComponent', () => {
  let component: FeelingAddRouteComponent;
  let fixture: ComponentFixture<FeelingAddRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingAddRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingAddRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { Router } from '@angular/router';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';

@Component({
    selector: 'app-feeling-list-route',
    templateUrl: './feeling-list-route.component.html',
    styleUrls: ['./feeling-list-route.component.scss'],
    providers: [FeelingSpecificService]
})
export class FeelingListRouteComponent implements OnInit {

    constructor(
        public navigation: NavigationService,
        public service: FeelingSpecificService
    ) { }

    ngOnInit() {

        //
        // Set main fab action for customer
        this.navigation.setMainFabAction(this.goToCreateRoute.bind(this));
    }

    ngOnDestroy() {
        this.navigation.setMainFabAction();
    }

    /**
     * Go to create route
     *
     * @memberof FeelingListRouteComponent
     */
    goToCreateRoute() {
        this.navigation.goTo(this.service.createUrl);
    }
}

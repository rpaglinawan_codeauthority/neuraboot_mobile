import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingListRouteComponent } from './feeling-list-route.component';

describe('FeelingListRouteComponent', () => {
  let component: FeelingListRouteComponent;
  let fixture: ComponentFixture<FeelingListRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingListRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingListRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

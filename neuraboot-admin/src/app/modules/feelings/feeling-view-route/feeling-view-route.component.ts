import { Component, OnInit } from '@angular/core';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';
import { RecordService } from 'src/app/services/record.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';
import { MetaService } from '@ngx-meta/core';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { FeelingSpecific } from '../../../../../../neuraboot-core/interfaces/feeling-specific.interface';

@Component({
    selector: 'app-feeling-view-route',
    templateUrl: './feeling-view-route.component.html',
    styleUrls: ['./feeling-view-route.component.scss'],
    providers: [FeelingSpecificService, RecordService]
})
export class FeelingViewRouteComponent implements OnInit {

    public id: string = null;
    public data$: Subscription;

    constructor(
        public route: ActivatedRoute,
        public service: FeelingSpecificService,
        public menu: MenuService,
        public meta: MetaService,
        public ui: UiService,
        public record: RecordService,
        public auth: AuthService,
        public router: Router
    ) { }

    ngOnInit() {

        //
        // Set id
        this.id = this.route.snapshot.params.id;

        //
        // Setup record service
        this.record.id = this.id;
        this.record.service = this.service;
        this.record.request = { query: { field: 'id', operator: '==', value: this.id } };
        this.record.postGetHook = this.postGetHook.bind(this);

        //
        // Load
        this.data$ = this.record.setup().subscribe();
    }

    ngOnDestroy() {
        this.data$.unsubscribe();
    }

    postGetHook(record: FeelingSpecific) {

        //
        // Set breadcrumb and meta
        this.menu.breadcrumbs = [{
            icon: 'fas fa-heartbeat',
            placeholder: 'Feelings',
            route: '/feelings'
        },
        {
            placeholder: 'Manage',
            route: '/feelings'
        }, {
            placeholder: record.label
        }];

        this.meta.setTitle(`Feelings / Manage / ${record.label}`);

        return record;
    }

    async toggleStatus() {
        let status = !this.record.entry.active;
        let success = await this.service.toggleStatus(this.record.id, status);

        if (success) {
            this.record.entry.active = status;
            this.record.updateEntry(this.record.entry);
        }
    }
}

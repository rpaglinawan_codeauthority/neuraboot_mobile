import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingViewRouteComponent } from './feeling-view-route.component';

describe('FeelingViewRouteComponent', () => {
  let component: FeelingViewRouteComponent;
  let fixture: ComponentFixture<FeelingViewRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingViewRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingViewRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

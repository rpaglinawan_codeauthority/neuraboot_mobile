import { Component, OnInit } from '@angular/core';
import { QueryFeelingCategoriesTop } from '../../../../../../neuraboot-core/queries/feeling-categories-top';
import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { MenuService } from 'src/app/services/menu.service';
import { MetaService } from '@ngx-meta/core';

@Component({
    selector: 'app-feeling-category-list-route',
    templateUrl: './feeling-category-list-route.component.html',
    styleUrls: ['./feeling-category-list-route.component.scss'],
    providers: [FeelingHistoryService]
})
export class FeelingCategoryListRouteComponent implements OnInit {

    public chartBarTopCategories: ChartBarVerticalSetup[] = [];

    constructor(
        public feelingHistory: FeelingHistoryService,
        public menu: MenuService,
        public meta: MetaService
    ) { }

    ngOnInit() {
        this.chartBarTopCategories = [{
            name: 'top-categories',
            placeholder: 'Top categories',
            title: `<strong>Top</strong> categories`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryFeelingCategoriesTop,
            service: this.feelingHistory,
            ttl: 60 // in seconds
        }]

        //
        // Set breadcrumb and meta
        this.menu.breadcrumbs = [{
            icon: 'fas fa-heartbeat',
            placeholder: 'Feelings',
            route: '/feelings'
        },
        {
            placeholder: 'Categories',
            route: '/feelings/categories'
        }];

        this.meta.setTitle(`Feelings / Cartegories`);
    }

}

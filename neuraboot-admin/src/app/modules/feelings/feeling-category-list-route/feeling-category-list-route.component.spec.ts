import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingCategoryListRouteComponent } from './feeling-category-list-route.component';

describe('FeelingCategoryListRouteComponent', () => {
  let component: FeelingCategoryListRouteComponent;
  let fixture: ComponentFixture<FeelingCategoryListRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingCategoryListRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingCategoryListRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingViewEditRouteComponent } from './feeling-view-edit-route.component';

describe('FeelingViewEditRouteComponent', () => {
  let component: FeelingViewEditRouteComponent;
  let fixture: ComponentFixture<FeelingViewEditRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingViewEditRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingViewEditRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

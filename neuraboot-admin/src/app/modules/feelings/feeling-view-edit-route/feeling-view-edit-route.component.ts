import { Component, OnInit } from '@angular/core';
import { RecordService } from 'src/app/services/record.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { MenuService } from 'src/app/services/menu.service';

@Component({
    selector: 'app-feeling-view-edit-route',
    templateUrl: './feeling-view-edit-route.component.html',
    styleUrls: ['./feeling-view-edit-route.component.scss']
})
export class FeelingViewEditRouteComponent implements OnInit {

    constructor(
        public record: RecordService,
        public navigation: NavigationService,
        public menu: MenuService
    ) { }

    ngOnInit() {
    }

    /**
     * On save
     *
     * @description On save event
     * @param event
     * @memberof FeelingViewEditRouteComponent
     */
    onSave(event) {

        //
        // Validate
        if (!event || !event.success) return;

        //
        // Update record
        this.record.updateEntry(event.data);

        //
        // Update breadcrumb
        this.menu.breadcrumbs[this.menu.breadcrumbs.length - 1].placeholder = event.data.label;
    }

    /**
     * On cancel
     *
     * @description On cancel event
     * @param event
     * @memberof FeelingViewEditRouteComponent
     */
    onCancel(event) {
        this.navigation.goTo(this.record.service.listUrl);
    }
}

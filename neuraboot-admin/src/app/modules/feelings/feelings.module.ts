import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeelingsRoutingModule } from './feelings-routing.module';
import { FeelingsRouteComponent } from './feelings-route/feelings-route.component';
import { FeelingListRouteComponent } from './feeling-list-route/feeling-list-route.component';
import { FeelingListComponent } from './feeling-list/feeling-list.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { CovalentTableModule } from '../covalent-table/covalent-table.module';
import { FeelingAddRouteComponent } from './feeling-add-route/feeling-add-route.component';
import { FeelingFormComponent } from './feeling-form/feeling-form.component';
import { MatSlideToggleModule, MatSelectModule } from '@angular/material';
import { FeelingViewRouteComponent } from './feeling-view-route/feeling-view-route.component';
import { FeelingViewDetailsRouteComponent } from './feeling-view-details-route/feeling-view-details-route.component';
import { ChartsModule } from '../charts/charts.module';
import { FeelingViewEditRouteComponent } from './feeling-view-edit-route/feeling-view-edit-route.component';
import { FeelingCategoryListRouteComponent } from './feeling-category-list-route/feeling-category-list-route.component';
import { FeelingCategoryListComponent } from './feeling-category-list/feeling-category-list.component';
import { FeelingCategoryViewRouteComponent } from './feeling-category-view-route/feeling-category-view-route.component';

@NgModule({
    declarations: [FeelingsRouteComponent, FeelingListRouteComponent, FeelingListComponent, FeelingAddRouteComponent, FeelingFormComponent, FeelingViewRouteComponent, FeelingViewDetailsRouteComponent, FeelingViewEditRouteComponent, FeelingCategoryListRouteComponent, FeelingCategoryListComponent, FeelingCategoryViewRouteComponent],
    imports: [
        CommonModule,
        FeelingsRoutingModule,
        ComponentsModule,
        CovalentTableModule,
        ChartsModule,
        MatSlideToggleModule,
        MatSelectModule
    ]
})
export class FeelingsModule { }

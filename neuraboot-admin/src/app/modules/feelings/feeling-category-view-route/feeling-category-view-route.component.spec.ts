import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingCategoryViewRouteComponent } from './feeling-category-view-route.component';

describe('FeelingCategoryViewRouteComponent', () => {
  let component: FeelingCategoryViewRouteComponent;
  let fixture: ComponentFixture<FeelingCategoryViewRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingCategoryViewRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingCategoryViewRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FeelingCategory } from '../../../../../../neuraboot-core/interfaces/feeling-category.interface';
import { ActivatedRoute } from '@angular/router';
import { RecordService } from 'src/app/services/record.service';
import { FeelingService } from 'src/app/services/feeling.service';
import { UiService } from 'src/app/services/ui.service';
import { find } from 'lodash';
import { MenuService } from 'src/app/services/menu.service';
import { MetaService } from '@ngx-meta/core';
import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';
import { QueryFeelingCategoriesTopUsers } from '../../../../../../neuraboot-core/queries/feeling-categories-top-users';
import { QueryFeelingCategoriesTopFeelings } from '../../../../../../neuraboot-core/queries/feeling-categories-top-feelings';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { ChartNumberSetup } from '../../../../../../neuraboot-core/interfaces/chart-number.interface';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';
import { FormatDay, FormatMonth } from '../../../../../../neuraboot-core/constants/calendar';
import { QueryFeelingCategoryCountLastMonth } from '../../../../../../neuraboot-core/queries/feeling-category-count-last-month';
import { QueryFeelingCategoryCountLastYear } from '../../../../../../neuraboot-core/queries/feeling-category-count-last-year';

@Component({
    selector: 'app-feeling-category-view-route',
    templateUrl: './feeling-category-view-route.component.html',
    styleUrls: ['./feeling-category-view-route.component.scss'],
    providers: [FeelingService, FeelingHistoryService, FeelingSpecificService, RecordService]
})
export class FeelingCategoryViewRouteComponent implements OnInit {

    public id: string = null;
    public feelingCategories: FeelingCategory[] = [{
        label: 'Angry',
        value: 'angry',
        icon: 'fa-angry'
    }, {
        label: 'Sad',
        value: 'depressed',
        icon: 'fa-sad-cry'
    }, {
        label: 'Anxious',
        value: 'anxious',
        icon: 'fa-grimace'
    }, {
        label: 'Happy',
        value: 'happy',
        icon: 'fa-smile'
    }];

    public chartLines: any[] = [];
    public chartNumbers: { extra: any, setup: ChartNumberSetup }[] = [];
    public chartBarTopCategoryUsers: ChartBarVerticalSetup[] = [];
    public chartBarTopCategoryFeelings: ChartBarVerticalSetup[] = [];
    public chartBarSecondaryColors: any = {
        domain: ['#33CCFF', '#33FFFF', '#33FF66', '#CCFF33', '#FFCC00', '#FF6600', '#FF3333', '#FF33FF', '#CC33FF', '#6e7fff']
    };

    constructor(
        public route: ActivatedRoute,
        public record: RecordService,
        public service: FeelingService,
        public ui: UiService,
        public menu: MenuService,
        public meta: MetaService,
        public feelingHistory: FeelingHistoryService,
        public feelingSpecific: FeelingSpecificService
    ) { }

    ngOnInit() {

        //
        // Set id
        this.id = this.route.snapshot.params.id;
        this.record.id = this.id;
        this.record.service = this.service;

        //
        // Load
        this.ui.loading = true;

        //
        // Get entry
        let entry = find(this.feelingCategories, ['value', this.id]);
        this.record.updateEntry(entry);

        setTimeout(() => {

            //
            // Set breadcrumb and meta
            this.menu.breadcrumbs = [{
                icon: 'fa fa-heartbeat',
                placeholder: 'Feelings',
                route: '/feelings'
            },
            {
                placeholder: 'Categories',
                route: '/feelings/categories'
            }, {
                placeholder: entry.label
            }];

            this.meta.setTitle(`Feelings / Categories / ${entry.label}`);

        }, 450);

        //
        // Unload
        this.ui.loading = false;

        //
        // Setup charts
        this.setupCharts();
    }

    setupCharts() {

        //
        // Chart bars
        this.chartBarTopCategoryUsers = [{
            name: 'top-category-users',
            placeholder: 'Top 5 users',
            title: `<strong>Top 5</strong> users`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryFeelingCategoriesTopUsers(this.record.id),
            service: this.feelingHistory,
            ttl: 60 // in seconds
        }]

        this.chartBarTopCategoryFeelings = [{
            name: 'top-category-feelings',
            placeholder: 'Top 5 feelings',
            title: `<strong>Top 5</strong> feelings`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryFeelingCategoriesTopFeelings(this.record.id),
            service: this.feelingHistory,
            ttl: 60 // in seconds
        }]

        //
        // Chart numbers
        this.chartNumbers = [
            {
                extra: {
                    title: 'Time(s) selected',
                    size: 'is-6',
                    icon: 'fa fa-users'
                },
                setup: {
                    service: this.feelingHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "category.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Feeling(s)',
                    size: 'is-6',
                    icon: 'fa fa-heartbeat',
                },
                setup: {
                    service: this.feelingSpecific,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "category": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        ]

        this.chartLines = [
            {
                name: 'category-last-month',
                placeholder: 'Category Last Month',
                title: `<strong>Category count</strong> on last 30 days`,
                xAxisTickFormatting: FormatDay,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryFeelingCategoryCountLastMonth(this.record.id),
                service: this.feelingHistory,
                ttl: 60
            },
            {
                name: 'last-year',
                placeholder: 'Feeling Last Year',
                title: `<strong>Feeling count</strong> on last year`,
                xAxisTickFormatting: FormatMonth,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryFeelingCategoryCountLastYear(this.record.id),
                service: this.feelingHistory,
                ttl: 60
            }
        ]
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingListComponent } from './feeling-list.component';

describe('FeelingListComponent', () => {
  let component: FeelingListComponent;
  let fixture: ComponentFixture<FeelingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

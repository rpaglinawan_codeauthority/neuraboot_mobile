import { Component, OnInit } from '@angular/core';
import { ElasticQueryBase } from '../../../../../../neuraboot-core/constants/elastic';
import { TableEntriesSetup, TableRowClickEvent } from 'src/app/interfaces/table';
import { FeelingSpecificTableFilters, FeelingSpecificTableColumns } from '../../../../../../neuraboot-core/constants/feeling-specific.table';
import { FeelingCategories } from '../../../../../../neuraboot-core/constants/feeling.categories';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'feeling-list',
    templateUrl: './feeling-list.component.html',
    styleUrls: ['./feeling-list.component.scss'],
    providers: [FeelingSpecificService]
})
export class FeelingListComponent implements OnInit {

    public setup: TableEntriesSetup = {
        service: this.feelingSpecific,
        path: '/find',
        query: cloneDeep(ElasticQueryBase)
    }

    public columns: any[] = FeelingSpecificTableColumns;
    public filters: any[] = FeelingSpecificTableFilters;

    constructor(
        public feelingSpecific: FeelingSpecificService
    ) { }

    ngOnInit() {
        this.setup.query = cloneDeep(ElasticQueryBase);
        this.setup.query.sort = [{
            "name": {
                "order": "asc"
            }
        }];

        this.filters[0].options = [...FeelingCategories];
    }

    rowClick($ev: TableRowClickEvent) {
        $ev.router.navigate(['/feelings/view/' + $ev.row.id]);
    }

}

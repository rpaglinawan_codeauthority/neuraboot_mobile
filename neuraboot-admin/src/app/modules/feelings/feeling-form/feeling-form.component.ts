import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FeelingSpecific } from '../../../../../../neuraboot-core/interfaces/feeling-specific.interface';
import { FeelingCategory } from '../../../../../../neuraboot-core/interfaces/feeling-category.interface';
import { FeelingCategoryOptions } from '../../../../../../neuraboot-core/constants/feeling-category-options';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { kebabCase, merge } from 'lodash';
import { UiService } from 'src/app/services/ui.service';
import { Guid } from '../../../utils/guid';
import { CacheService } from 'src/app/services/cache.service';

@Component({
    selector: 'feeling-form',
    templateUrl: './feeling-form.component.html',
    styleUrls: ['./feeling-form.component.scss']
})
export class FeelingFormComponent implements OnInit {

    @Output() public onSave = new EventEmitter();
    @Output() public onCancel = new EventEmitter();

    @Input() public edit: boolean = false;
    @Input() public redirect: boolean = false;
    @Input() public data: FeelingSpecific = null;

    public form: FormGroup;
    public model: FeelingSpecific = {
        id: Guid.make(2),
        name: null,
        label: null,
        active: true,
        category: null
    };

    public categories: FeelingCategory[] = FeelingCategoryOptions;

    constructor(
        private fb: FormBuilder,
        public service: FeelingSpecificService,
        public ui: UiService,
        public cache: CacheService
    ) { }

    ngOnInit() {

        //
        // Merge data into model
        merge(this.model, this.data);

        //
        // Create form
        this.createForm();
    }

    /**
     * Create form
     *
     * @description Create form with its validations
     * @memberof FeelingFormComponent
     */
    createForm() {

        let fields: any = {};

        //
        // Fields
        fields.name = [this.model.name, Validators.required];
        fields.label = [this.model.label, Validators.required];
        fields.active = [this.model.active, Validators.required];
        fields.category = [this.model.category];

        //
        // Set id or generate
        fields.id = new FormControl(this.model.id, Validators.required);

        //
        // Form
        this.form = this.fb.group(fields);

        //
        // Disable name
        this.form.get('name').disable();

        //
        // Set on update label
        this.onUpdateLabel();
    }

    /**
     * On submit
     *
     * @description On submit form event
     * @returns
     * @memberof FeelingFormComponent
     */
    async onSubmit() {
        try {

            this.ui.say('Saving feeling...');
            this.ui.loading = true;

            //
            // Save
            await this.service.set(this.form.get('id').value, this.form.getRawValue()).toPromise();

            //
            // Emit on save
            this.onSave.emit({
                success: true,
                data: this.form.getRawValue()
            });

            //
            // Clear cache
            this.cache.remove('feeling-specific/{"query":{"field":"id","operator":"==","value":"' + this.form.get('id').value + '"}}');

            //
            // UI
            this.ui.say('Saved!');
            this.ui.loading = false;

        } catch (error) {

            //
            // Return
            this.ui.say('Ops, not saved');
            this.ui.loading = false;

            this.onSave.emit({
                success: false
            });
        }
    }

    /**
     * Cancel
     *
     * @description Cancel action
     * @memberof FeelingFormComponent
     */
    cancel() {
        this.onCancel.emit();
    }

    /**
     * On update label
     *
     * @description Update name while updating label
     * @memberof FeelingFormComponent
     */
    onUpdateLabel() {

        //
        // Validate
        if (this.edit) return;

        //
        // Listen to label value change
        this.form.get('label').valueChanges.pipe(
            debounceTime(100),
            distinctUntilChanged()
        ).subscribe((newValue: any) => {

            //
            // Set new value in kebab case and update name
            newValue = kebabCase(newValue);
            this.form.get('name').patchValue(newValue);
        });
    }
}

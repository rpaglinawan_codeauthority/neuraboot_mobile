import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { TableEntriesSetup, TableFinderColumn, TableRowClickEvent } from 'src/app/interfaces/table';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { cloneDeep, startCase, toLower } from 'lodash';

@Component({
    selector: 'table-finder',
    templateUrl: './table-finder.component.html',
    styleUrls: ['./table-finder.component.scss']
})
export class TableFinderComponent implements OnInit {

    @ViewChild('tableEntries') tableEntries;
    @Input() setup: TableEntriesSetup;
    @Input() rowClick;
    @Input() columns: TableFinderColumn[];
    @Input() filters: any[] = [/*TABLE_FILTER*/];
    @Input() search: string;
    @Input() currentFilter: any = {};
    @Input() pageSize: number = 10;
    @Input() shouldScroll: boolean = true;

    @Output() onLoad = new EventEmitter();

    public query_bkp: any;
    public routeSub$: Subscription;

    constructor(
        private router: Router
    ) {
        this.routeSub$ = this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                setTimeout(() => {
                    if (this.search) {
                        this.didSearch({ search: this.search });
                    }
                }, 500)
            }
        });
    }

    ngOnInit() {
        this.query_bkp = cloneDeep(this.setup.query);
    }

    ngOnDestroy() {
        this.routeSub$.unsubscribe();
    }

    didSearch($ev: any) {
        const searchString = $ev.search;

        if (searchString) {
            this.currentFilter = {};
            this.setup.query = cloneDeep(this.query_bkp);

            this.columns.map(c => {
                const isNumber = parseFloat(searchString);

                if (c.filter && !c.number) {
                    if (!isNumber) {
                        let q1 = {
                            "match_phrase": {}
                        }
                        q1.match_phrase[c.name] = `.*${startCase(toLower(searchString))}.*`;

                        let q2 = {
                            "regexp": {}
                        }
                        q2.regexp[c.name] = `.*${startCase(toLower(searchString))}.*`;

                        let q3 = {
                            "match_phrase": {}
                        }
                        q3.match_phrase[c.name] = `.*${searchString}.*`;

                        let q4 = {
                            "regexp": {}
                        }
                        q4.regexp[c.name] = `.*${searchString}.*`;


                        this.setup.query.query.bool.should.push(q1);
                        this.setup.query.query.bool.should.push(q2);

                        this.setup.query.query.bool.should.push(q3);
                        this.setup.query.query.bool.should.push(q4);
                    }
                }
                if (c.filter && c.number) {
                    if (isNumber > 0) {
                        let q1 = {
                            "range": {
                            }
                        }
                        q1.range[c.name] = {
                            "gte": isNumber - 10,
                            "lte": isNumber + 10,
                            "boost": 2.0
                        };

                        this.setup.query.query.bool.should.push(q1);
                    }
                }
            });

        } else {
            this.setup.query.query.bool.should = [];
            if (this.search) {
                this.search = '';
                let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
                this.router.navigateByUrl(url);
            }
        }

        this.filter();
    }

    didFilter($ev) {
        this.setup.query = cloneDeep(this.query_bkp);
        if ($ev.filter && $ev.filter.name != 'all') {
            this.setup.query.query.bool.must.push($ev.filter.query);
        }
        this.search = null;
        this.filter();
    }

    didRowClick($ev) {
        const payload: TableRowClickEvent = {
            row: $ev.row,
            column: $ev.column,
            router: this.router
        }
        return this.rowClick(payload);
    }

    didLoad($event) {
        this.onLoad.emit($event);
    }

    filter(startAt: number = 0) {
        this.tableEntries.filter(startAt);
    }
}

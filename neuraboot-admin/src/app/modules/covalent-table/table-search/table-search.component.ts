import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'table-search',
    templateUrl: './table-search.component.html',
    styleUrls: ['./table-search.component.scss']
})
export class TableSearchComponent implements OnInit {

    @Input() search: string;

    @Output() onSearch = new EventEmitter();
    @Output() onClear = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    submit() {
        this.onSearch.emit({ search: this.search })
    }

    clear() {
        this.search = '';
        this.onClear.emit({ search: '' })
    }

}

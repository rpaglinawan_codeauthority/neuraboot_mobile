import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TableEntriesSetup, TableColumn, TableRowClickEvent } from 'src/app/interfaces/table';
import { TdDataTableSortingOrder, TdDataTableService, ITdDataTableSortChangeEvent, IPageChangeEvent } from '@covalent/core';
import { UiService } from 'src/app/services/ui.service';
import { RRResponse } from '@ionfire/reactive-record';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { isEmpty, isEqual } from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'table-entries',
    templateUrl: './table-entries.component.html',
    styleUrls: ['./table-entries.component.scss']
})
export class TableEntriesComponent implements OnInit {

    @Input() setup: TableEntriesSetup;
    @Output() onRowClick = new EventEmitter;
    @Output() onLoad = new EventEmitter;
    @Input() pageSize: number = 10;
    @Input() columns: TableColumn[];
    @Input() shouldScroll: boolean = true;

    public query_check: any;
    public moment: any = moment;

    public data: any[] = [];

    public filteredData: any[] = this.data;
    public filteredTotal: number = this.data.length;

    public clickable: boolean = false;
    public fromRow: number = 0;
    public currentPage: number = 1;

    public initialPage: number = 1;
    public sortBy: string = 'created_at';
    public selectedRows: any[] = [];
    public sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Descending;

    public data$: Subscription;

    constructor(
        public ui: UiService,
        private _dataTableService: TdDataTableService
    ) { }

    ngOnInit(force: boolean = false, startAt: number = 0/*, paging: boolean = false*/): void {
        if (isEmpty(this.setup)) throw 'Missing setup object (TableEntriesSetup)';
        this.initialState();
        this.filter(startAt/*, paging*/);
    }

    ngOnDestroy() {
        if (this.data$) {
            this.data$.unsubscribe();
        }
    }

    initialState() {
        this.currentPage = 1;
        this.fromRow = 1;
        this.initialPage = 1;
    }

    sort(sortEvent: ITdDataTableSortChangeEvent): void {
        this.sortBy = sortEvent.name;
        this.sortOrder = sortEvent.order;
        this.filteredData = this._dataTableService.sortData(this.filteredData, this.sortBy, this.sortOrder);
    }

    page(pagingEvent: IPageChangeEvent): void {
        this.fromRow = pagingEvent.fromRow;
        this.currentPage = pagingEvent.page;
        this.pageSize = pagingEvent.pageSize;
        this.initialPage = pagingEvent.page;
        this.filter(this.fromRow - 1, true);
    }

    async filter(startAt: number = 0, paging: boolean = false) {
        if (!isEqual(this.setup.query, this.query_check)) {
            this.ui.loading = true;
            if (this.shouldScroll) this.ui.scrollTop();

            this.data$ = this.setup.service.post(`${this.setup.path}/?size=${this.pageSize}&from=${startAt}`, this.setup.query, {})
                .pipe(
                    map((r: RRResponse) => r.data),
                    map((data: any) => {

                        //
                        // set total records
                        this.filteredTotal = data.hits.total;
                        this.filteredData = data.hits.hits.map(h => h._source);

                        this.ui.loading = false;
                        this.onLoad.emit({ filtered: this.filteredData, query: this.setup.query, startAt: startAt });
                    })
                )
                .subscribe()
        }
    }

    rowClick(row, column) {
        this.onRowClick.emit(<TableRowClickEvent>{ row: row, column: column })
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CovalentModule } from '../covalent/covalent.module';
import { TableFinderComponent } from './table-finder/table-finder.component';
import { TableSearchComponent } from './table-search/table-search.component';
import { TableFilterComponent } from './table-filter/table-filter.component';
import { TableEntriesComponent } from './table-entries/table-entries.component';
import { MatCardModule, MatInputModule, MatButtonModule, MatIconModule, MatMenuModule, MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { UserAvatarModule } from '../user-avatar/user-avatar.module';

@NgModule({
    declarations: [
        TableFinderComponent,
        TableSearchComponent,
        TableFilterComponent,
        TableEntriesComponent
    ],
    imports: [
        CommonModule,
        CovalentModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatTooltipModule,
        FormsModule,
        UserAvatarModule
    ],
    exports: [
        TableFinderComponent,
        TableSearchComponent,
        TableFilterComponent,
        TableEntriesComponent
    ]
})
export class CovalentTableModule { }

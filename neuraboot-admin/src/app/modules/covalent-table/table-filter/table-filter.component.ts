import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
    selector: 'table-filter',
    templateUrl: './table-filter.component.html',
    styleUrls: ['./table-filter.component.scss']
})
export class TableFilterComponent implements OnInit {

    @Output() onFilter = new EventEmitter();

    @Input() current: any[] = [];
    @Input() filters: any[] = [];

    constructor() { }

    ngOnInit() { }

    didFilter(filter) {
        this.onFilter.emit({ filter: this.current[this.filters.indexOf(filter)] });
    }

    setFilter(item, filter) {
        this.current = [];
        this.current[this.filters.indexOf(filter)] = item;
    }
}

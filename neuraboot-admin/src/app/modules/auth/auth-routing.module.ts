import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthProtectedRouteComponent } from './auth-protected-route/auth-protected-route.component';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';

const routes: Routes = [{
    path: '',
    component: AuthProtectedRouteComponent,
    canActivate: [UserAuthedGuard],
    children: [{
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    }, {
        path: 'dashboard',
        loadChildren: '../dashboard/dashboard.module#DashboardModule',
        canActivate: [UserAuthedGuard]
    }, {
        path: 'feelings',
        loadChildren: '../feelings/feelings.module#FeelingsModule',
        canActivate: [UserAuthedGuard]
    }, {
        path: 'challenges',
        loadChildren: '../challenges/challenges.module#ChallengesModule',
        canActivate: [UserAuthedGuard]
    }, {
        path: 'users',
        loadChildren: '../users/users.module#UsersModule',
        canActivate: [UserAuthedGuard]
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }

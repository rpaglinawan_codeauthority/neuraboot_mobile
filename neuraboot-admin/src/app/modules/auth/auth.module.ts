import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthProtectedRouteComponent } from './auth-protected-route/auth-protected-route.component';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
    declarations: [AuthProtectedRouteComponent],
    imports: [
        CommonModule,
        AuthRoutingModule,
        ComponentsModule
    ]
})
export class AuthModule { }

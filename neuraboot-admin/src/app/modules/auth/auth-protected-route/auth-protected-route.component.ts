import { Component, OnInit, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';
import { UiService } from 'src/app/services/ui.service';
import { SideMenu } from 'src/app/constants/menu';
import { MetaService } from '@ngx-meta/core';
import { cloneDeep, remove } from 'lodash';

@Component({
    selector: 'app-auth-protected-route',
    templateUrl: './auth-protected-route.component.html',
    styleUrls: ['./auth-protected-route.component.scss']
})
export class AuthProtectedRouteComponent implements OnInit {

    @ViewChild('sidenav') public sidenav;

    public sidenavMode: string = '';
    public isSidenavOpen: boolean = false;
    public showUserMenu: boolean = false;
    public menu: any = cloneDeep(SideMenu);
    public currentMenu: any;
    public currentSubMenu: any;

    constructor(
        public auth: AuthService,
        private router: Router,
        public route: ActivatedRoute,
        public metaService: MetaService,
        public menuService: MenuService,
        private renderer: Renderer2,
        public ui: UiService
    ) {

        //
        // First call to adjust to the current screen
        this.onWindowResize();

        //
        // Watch for window resize
        this.renderer.listen(window, 'resize', (event) => {

            //
            // Set window resize event in timeout
            const resizeTimeout = setTimeout(() => {
                this.onWindowResize();

                //
                // Clear timeout
                clearTimeout(resizeTimeout);
            }, 500);
        });

        this.router.events.subscribe((val) => {

            //
            // Validate auth
            if (!this.auth.user.uid) return;

            //
            // Navigation start event
            if (val instanceof NavigationStart) {
                this.menuService.toolbar = { title: '', back: '/' };
                this.ui.scrollTop();
            }

            //
            // Navigation end event
            if (val instanceof NavigationEnd) {
                this.ui.scrollTop();

                SideMenu.map(m => {

                    //
                    // check for roles
                    if (m.roles && m.roles.length) {
                        let authorized = false;
                        m.roles.map(r => this.auth.hasRole(r) ? authorized = true : authorized = authorized);
                        if (!authorized) {
                            remove(this.menu, current => current.name === m.name);
                        };
                    }

                    //
                    // check for custom filters
                    if (m.filter) {
                        if (!m.filter(this.auth.user)) {
                            remove(this.menu, current => current.name === m.name);
                        };
                    }

                    if (m.meta) {
                        this.metaService.setTitle(m.meta.title);
                    }
                    if (m.options) {
                        m.options.map(o => {
                            if (window.location.pathname === o.route) {
                                if (o.breadcrumbs) {
                                    this.menuService.breadcrumbs = o.breadcrumbs;
                                }
                                if (o.meta) {
                                    this.metaService.setTitle(o.meta.title);
                                }
                            }
                        })
                    }
                });
            }
        });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.menuService.sidenav = this.sidenav;
        }, 500)
    }

    public onWindowResize() {

        //
        // Desktop version
        if (window.innerWidth > 967) {
            this.sidenavMode = 'side';
            this.isSidenavOpen = true;
        }

        //
        // Mobile verson
        else {
            this.sidenavMode = 'over';
            this.isSidenavOpen = false;
        }
    }
}

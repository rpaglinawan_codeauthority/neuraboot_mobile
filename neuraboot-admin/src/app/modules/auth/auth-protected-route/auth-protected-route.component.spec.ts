import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthProtectedRouteComponent } from './auth-protected-route.component';

describe('AuthProtectedRouteComponent', () => {
  let component: AuthProtectedRouteComponent;
  let fixture: ComponentFixture<AuthProtectedRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthProtectedRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthProtectedRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

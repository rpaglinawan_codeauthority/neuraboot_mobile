import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';
import { MatSidenavModule, MatToolbarModule, MatIconModule, MatProgressBarModule, MatInputModule, MatButtonModule, MatCardModule, MatProgressSpinnerModule, MatCheckboxModule, MatMenuModule, MatSnackBarModule, MatExpansionModule, MatListModule, MatTooltipModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

export const MATERIAL_COLORS: string[] = [
    'F44336', 'E91E63', '9C27B0', '673AB7', '3F51B5', '2196F3', '03A9F4', '00BCD4', '009688', '4CAF50', '8BC34A', 'CDDC39', 'FFEB3B', 'FFC107', 'FF9800', 'FF5722', '795548', '9E9E9E', '607D8B'];

@NgModule({
    imports: [
        CommonModule,
        MatMenuModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatCheckboxModule,
        CdkTableModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatSnackBarModule,
        MatExpansionModule,
        MatListModule,
        MatTooltipModule,
        FlexLayoutModule
    ],
    exports: [
        MatMenuModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatCheckboxModule,
        CdkTableModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatSnackBarModule,
        MatExpansionModule,
        MatListModule,
        MatTooltipModule,
        FlexLayoutModule
    ]
})
export class MaterialModule { }

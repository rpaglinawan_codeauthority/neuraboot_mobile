import * as _ from 'lodash';
import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, Injector, InjectionToken } from '@angular/core';


import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';
import { getCountryEmoji } from '../../../../../../neuraboot-core/constants/country';

import { map } from 'rxjs/operators/map';

import { RRResponse } from '@ionfire/reactive-record';
import { Subscription, Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { UiService } from 'src/app/services/ui.service';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'chart-bar',
    templateUrl: './chart-bar.component.html',
    styleUrls: ['./chart-bar.component.scss']
})
export class ChartBarComponent implements OnInit {

    @Input() setup: ChartBarVerticalSetup[] = []
    // [{
    //   name: 'top5-countries',
    //   placeholder: 'Top 5 countries',
    //   title: `<strong>Top 5</strong> countries`,
    //   formatChartXLabel: getCountryEmoji,

    //   charts: [{
    //     name: 'count',
    //     series: []
    //   }],

    //   path: '/find',
    //   query: QueryUsersTop5Countries,
    //   service: this.userService,
    //   ttl: 60 // in seconds
    // }];

    @Input() view: any[] = [700, 300];

    // options
    @Input() showXAxis: boolean = true;
    @Input() showYAxis: boolean = true;
    @Input() gradient: boolean = true;
    @Input() showLegend: boolean = false;
    @Input() showXAxisLabel: boolean = false;
    @Input() xAxisLabel: string = 'Date';
    @Input() showYAxisLabel: boolean = false;
    @Input() yAxisLabel: string = 'Sign-up';

    @Input() colorScheme: any = {
        domain: ['#00974b', '#f48996', '#073c8c', '#d5064c', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
    };

    @Input() path: string = '/find';

    // line, area
    //@Input() autoScale: boolean = true;

    @Input() tooltipTemplate: any;

    @Output() onSelect = new EventEmitter();

    total: any;
    index: number = 0;
    loading: boolean = true;
    data$: Subscription;


    constructor(private ui: UiService) { }

    ngOnInit() { }

    async ngAfterViewInit() {
        if (_.isEmpty(this.setup[this.index].service)) throw "service expected";
        this.loading = true;
        this.data$ = this.request(this.path, this.setup[this.index].query, { ttl: this.setup[this.index].ttl || 0 })
            .pipe(
                map((r: RRResponse) => r.data),
                tap((result: any) => {
                    if (typeof this.setup[this.index].transform === 'function') {
                        //  console.log(this.index)
                        this.setup[this.index].transform(this.index, result, this.setup);
                    } else {

                        let data = [];
                        const aggr_key1: any = Object.keys(result.aggregations);
                        const aggr_key2: any = Object.keys(result.aggregations[aggr_key1]);

                        let path = !_.isEmpty(result.aggregations[aggr_key1[0]][aggr_key2[1]]) ? result.aggregations[aggr_key1[0]][aggr_key2[1]] : result.aggregations[aggr_key1[0]];

                        if (result && result.aggregations) {
                            path.buckets.map(b => {
                                const xlabelFn: any = this.setup[this.index].formatChartXLabel ? this.setup[this.index].formatChartXLabel : false;
                                const name = xlabelFn ? xlabelFn(b.key) : b.key;
                                data.push({ name: name, value: b.doc_count });
                            })
                            // console.log(data)

                            this.setup[this.index].charts[0].series = data;
                            // this.total = numberFormat(result.hits.total, 2);
                        }

                    }
                    this.loading = false;
                }),
                catchError((err: ApiErrorResponse | any) => {
                    const message: string = err.message ? err.message : err;
                    console.log(message);
                    this.ui.say(message);
                    return of(err);
                })
            )
            .subscribe()
    }

    request(...args): Observable<RRResponse> {
        return <Observable<RRResponse>>this.setup[this.index].service.post(...args);
    }

    didSelect(event) {
        this.onSelect.emit(event);
    }

    ngOnDestroy() {
        if (this.data$) this.data$.unsubscribe();
    }

    dataExists(data: any) {
        return !_.isEmpty(data);
    }

}

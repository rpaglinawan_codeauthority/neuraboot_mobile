import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';
import { ChartNumberSetup } from '../../../../../../neuraboot-core/interfaces/chart-number.interface';
import { map, catchError, tap } from 'rxjs/operators';
import { Subscription, of } from 'rxjs';
import { UiService } from 'src/app/services/ui.service';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { Numbers } from 'src/app/utils/numbers';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'chart-number',
    templateUrl: './chart-number.component.html',
    styleUrls: ['./chart-number.component.scss']
})
export class ChartNumberComponent implements OnInit {
    @Input() setup: ChartNumberSetup = <ChartNumberSetup>{};
    @Input() value: any = 0;
    @Input() loading: boolean = false;
    @Input() color: string = '#7584ff';
    @Input() icon: string = 'fas fa-star';
    @Input() title: string = 'Users';
    @Input() size: string = 'is-4';
    @Input() format: any;

    isAggs: boolean = false;

    data$: Subscription;

    constructor(private ui: UiService) { }



    async ngOnInit() {
        if (this.setup.path) {
            if (_.isEmpty(this.setup.service)) throw "service expected";
            this.loading = true;
            this.data$ = this.setup.service
                .post(this.setup.path, this.setup.query, { ttl: this.setup.ttl })
                .pipe(
                    map((r: any) => r.data),
                    tap((response: any) => {
                        // auto format
                        if (!this.format) {

                            const format = (value) => { return Numbers.format(value ? value : 0, 2) || 0 };
                            let paths = _.compact(this.setup.path.split('/'));

                            if (paths.includes('count')) {
                                this.value = format(response.count);

                            } else {
                                let key = '';
                                let field = '';
                                for (let k in this.setup.query) {
                                    if (k === 'aggs') {
                                        for (let y in this.setup.query[k]) {
                                            key = y;
                                            for (let z in this.setup.query[k][y]) {
                                                field = this.setup.query[k][y][z].field;
                                            }
                                        }
                                    }
                                }
                                if (key && field) {
                                    this.isAggs = true;
                                    this.value = format(response.aggregations[key][field]);
                                }
                                // console.log(response, this.query)
                            }
                        } else {
                            // custom format (for complex queries)
                            this.value = this.format(response);
                        }

                        this.loading = false;
                    }),
                    catchError((err: ApiErrorResponse | any) => {
                        const message: string = err.message ? err.message : err;
                        console.log(message);
                        this.ui.say(message);
                        return of(err);
                    })
                )
                .subscribe();
        }
    }

    ngOnDestroy() {
        if (this.data$) this.data$.unsubscribe();
    }
}

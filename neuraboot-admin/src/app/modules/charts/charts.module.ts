import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartBarComponent } from './chart-bar/chart-bar.component';
import { ChartLineComponent } from './chart-line/chart-line.component';
import { ChartNumberComponent } from './chart-number/chart-number.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatCardModule, MatIconModule, MatButtonModule, MatProgressSpinnerModule, MatMenuModule } from '@angular/material';

@NgModule({
    declarations: [
        ChartLineComponent,
        ChartBarComponent,
        ChartNumberComponent
    ],
    imports: [
        CommonModule,
        NgxChartsModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatMenuModule
    ],
    exports: [
        ChartLineComponent,
        ChartBarComponent,
        ChartNumberComponent
    ]
})
export class ChartsModule { }

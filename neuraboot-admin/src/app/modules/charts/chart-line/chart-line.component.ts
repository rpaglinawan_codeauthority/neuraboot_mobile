import * as _ from 'lodash';

import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ChartLineSetup } from '../../../../../../neuraboot-core/interfaces/chart-line.interface';
import { UserService } from 'src/app/services/user.service';
import { map, catchError } from 'rxjs/operators';
import { RRResponse } from '@ionfire/reactive-record';
import { Numbers } from 'src/app/utils/numbers';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { UiService } from 'src/app/services/ui.service';
import { of, Subscription } from 'rxjs';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'chart-line',
    templateUrl: './chart-line.component.html',
    styleUrls: ['./chart-line.component.scss']
})
export class ChartLineComponent implements OnInit {

    @Input() setup: ChartLineSetup[] = [];

    //
    // options
    @Input() view: any[] = [];
    @Input() showXAxis: boolean = true;
    @Input() showYAxis: boolean = true;
    @Input() gradient: boolean = true;
    @Input() showLegend: boolean = false;
    @Input() showXAxisLabel: boolean = false;
    @Input() xAxisLabel: string = 'Date';
    @Input() showYAxisLabel: boolean = false;
    @Input() yAxisLabel: string = 'Sign-up';

    @Input() colorScheme = {
        domain: ['#a8385d', '#7aa3e5', '#55C22D', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
    };

    // line, area
    @Input() autoScale: boolean = true;

    total: any;
    index: number = 0;
    loading: boolean = true;
    data$: Subscription;

    constructor(
        private ui: UiService
    ) {
        this.index = 0;
    }

    ngOnInit() { }

    async ngAfterViewInit() {
        if (_.isEmpty(this.setup)) throw 'missing setup object';
        this.loading = true;

        this.data$ = this.setup[this.index].service
            .post(this.setup[this.index].path, this.setup[this.index].query)
            .pipe(
                map((r: RRResponse) => r.data),
                map((response: any) => {
                    let data = [];
                    const aggr = Object.keys(response.aggregations);
                    const aggr_key = aggr[0];
                    if (response && response.aggregations && response.aggregations[aggr_key]) {
                        response.aggregations[aggr_key].buckets.map(b => data.push({ name: this.setup[this.index].xAxisTickFormatting(b.key_as_string), value: b.doc_count }))
                        this.setup[this.index].charts[0].series = data;
                        this.total = Numbers.format(response.hits.total, 2);
                    }
                    // console.log(data)
                    this.loading = false;
                }),
                catchError((err: ApiErrorResponse | any) => {
                    const message: string = err.message ? err.message : err;
                    console.log(message);
                    this.ui.say(message);
                    return of(err);
                })
            )
            .subscribe();
    }

    onSelect(event) {
        //  console.log(event);
    }

    dataExists(data: any) {
        return !_.isEmpty(data);
    }

    getCount(model) {
        // console.log(model)
    }

    ngOnDestroy() {
        if (this.data$) this.data$.unsubscribe();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { ChartsModule } from '../charts/charts.module';
import { DashboardRouteComponent } from './dashboard-route/dashboard-route.component';

@NgModule({
    declarations: [DashboardRouteComponent],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ComponentsModule,
        ChartsModule
    ]
})
export class DashboardModule { }

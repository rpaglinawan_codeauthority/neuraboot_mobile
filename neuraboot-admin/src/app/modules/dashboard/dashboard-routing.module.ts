import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SideMenu } from 'src/app/constants/menu';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { DashboardRouteComponent } from './dashboard-route/dashboard-route.component';

//
// Setup metadata
let metadata: any = {};

SideMenu.map(m => {
    if (m.options) {
        m.options.map(o => {
            if (o.route === '/dashboard') {
                metadata = o.meta;
            }
        })
    }
});

const routes: Routes = [{
    path: '',
    component: DashboardRouteComponent,
    canActivate: [UserAuthedGuard]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }

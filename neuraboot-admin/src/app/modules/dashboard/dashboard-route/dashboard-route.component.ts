import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/services/user.service';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';

import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';
import { getCountryEmoji } from '../../../../../../neuraboot-core/constants/country';
import { ChartNumberSetup } from '../../../../../../neuraboot-core/interfaces/chart-number.interface';
import { ChartLineSetup } from '../../../../../../neuraboot-core/interfaces/chart-line.interface';
import { FormatDay, FormatMonth } from '../../../../../../neuraboot-core/constants/calendar';
import { QueryUsersSignUpLastMonth } from '../../../../../../neuraboot-core/queries/users-signup-last-month';
import { QueryUsersTop5Countries } from '../../../../../../neuraboot-core/queries/users-top-5-countries';
import { QueryUsersTop5Cities } from '../../../../../../neuraboot-core/queries/users-top-5-cities';
import { QueryUsersTop5States } from '../../../../../../neuraboot-core/queries/users-top-5-states';
import { ChallengeHistoryService } from 'src/app/services/challenge-history.service';

@Component({
    selector: 'app-dashboard-route',
    templateUrl: './dashboard-route.component.html',
    styleUrls: ['./dashboard-route.component.scss'],
    providers: [UserService, FeelingHistoryService, ChallengeHistoryService]
})
export class DashboardRouteComponent implements OnInit {

    public chartBarTop5Countries: ChartBarVerticalSetup[] = [];
    public chartBarTop5Location: ChartBarVerticalSetup[] = [];
    public chartBarTop5LocationColor: any = {
        domain: ['#33CCFF', '#33FFFF', '#33FF66', '#CCFF33', '#FFCC00', '#FF6600', '#FF3333', '#FF33FF', '#CC33FF', '#6e7fff']
    };

    public chartNumbers: { extra: any, setup: ChartNumberSetup }[] = []
    public chartLines: ChartLineSetup[] = []

    constructor(
        public userService: UserService,
        public feelingHistoryService: FeelingHistoryService,
        public challengeHistory: ChallengeHistoryService
    ) { }

    ngOnInit() {

        this.setupChartBar();
        this.setupChartNumber();
        this.setupChartLine();
    }

    setupChartBar() {
        this.chartBarTop5Countries = [{
            name: 'top5-countries',
            placeholder: 'Top 5 countries',
            title: `<strong>Top 5</strong> countries`,
            formatChartXLabel: getCountryEmoji,

            charts: [{
                name: 'count',
                series: []
            }],

            path: '/find',
            query: QueryUsersTop5Countries,
            service: this.userService,
            ttl: 60 // in seconds
        }]

        this.chartBarTop5Location = [

            //
            // Top5 cities
            {
                name: 'top5-cities',
                placeholder: 'Top 5 cities',
                title: `<strong>Top 5</strong> cities`,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryUsersTop5Cities,
                service: this.userService,
                ttl: 60 // in seconds
            },

            //
            // Top5 states
            {
                name: 'top5-states',
                placeholder: 'Top 5 states',
                title: `<strong>Top 5</strong> states`,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryUsersTop5States,
                service: this.userService,
                ttl: 60 // in seconds
            }
        ]
    }

    setupChartNumber() {
        this.chartNumbers = [
            {
                extra: {
                    title: 'Users',
                    size: 'is-5',
                    icon: 'fas fa-users'
                },
                setup: {
                    service: this.userService,
                    path: '/count',
                    ttl: 30
                }
            },
            {
                extra: {
                    title: 'User(s) online',
                    size: 'is-6',
                    icon: 'fas fa-signal',
                },
                setup: {
                    service: this.userService,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "range": {
                                "online_at": {
                                    "gte": "now-1H",
                                    "lt": "now"
                                }
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'User(s) online last 24h',
                    size: 'is-6',
                    icon: 'far fa-clock',
                },
                setup: {
                    service: this.userService,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "range": {
                                "online_at": {
                                    "gte": "now-24H",
                                    "lt": "now"
                                }
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: `User Feelings`,
                    size: 'is-6',
                    icon: 'fas fa-heartbeat',
                },
                setup: {
                    service: this.feelingHistoryService,
                    path: '/count',
                    ttl: 30
                }
            },
            {
                extra: {
                    title: 'Completed challenges',
                    size: 'is-6',
                    icon: 'far fa-check-circle'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "completed"
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Cancelled challenges',
                    size: 'is-6',
                    icon: 'fa fa-ban'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "cancelled"
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        ]
    }

    setupChartLine() {

        this.chartLines = [
            {
                name: 'last-month',
                placeholder: 'Last month',
                title: `<strong>users signed-up</strong> on last 30 days`,
                xAxisTickFormatting: FormatDay,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryUsersSignUpLastMonth,
                service: this.userService,
                ttl: 60
            },
            {
                name: 'last-year',
                placeholder: 'Last year',
                title: `<strong>users signed-up</strong> on last year`,
                xAxisTickFormatting: FormatMonth,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryUsersSignUpLastMonth,
                service: this.userService,
                ttl: 60
            }
        ]
    }
}

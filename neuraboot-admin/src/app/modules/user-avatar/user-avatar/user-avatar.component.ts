import { Component, OnInit, Input } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';

@Component({
    selector: 'user-avatar',
    templateUrl: './user-avatar.component.html',
    styleUrls: ['./user-avatar.component.scss']
})
export class UserAvatarComponent implements OnInit {

    @Input() online: boolean;
    @Input() url: string;

    public avatarIndex: boolean[] = [];
    public user: User = { photo_url: '' };
    public loading: boolean = true;

    constructor(
        public ui: UiService
    ) { }

    ngOnInit() {
    }

    ngOnChanges(changes) {
        if (changes.url && changes.url.currentValue) {
            this.ui.testAvatar(changes.url.currentValue).then(this.avatarCallback.bind(this));
        }
    }

    avatarCallback(urlTested: string) {
        if (urlTested) this.user.photo_url = urlTested;
        else this.user.photo_url = './assets/images/user-blank.svg';

        this.loading = false;
    }
}

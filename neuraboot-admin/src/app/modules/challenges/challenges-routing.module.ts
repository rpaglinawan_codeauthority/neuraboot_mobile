import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SideMenu } from 'src/app/constants/menu';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { ChallengesRouteComponent } from './challenges-route/challenges-route.component';
import { ChallengeListRouteComponent } from './challenge-list-route/challenge-list-route.component';
import { ChallengeAddRouteComponent } from './challenge-add-route/challenge-add-route.component';
import { ChallengeViewRouteComponent } from './challenge-view-route/challenge-view-route.component';
import { ChallengeViewDetailsRouteComponent } from './challenge-view-details-route/challenge-view-details-route.component';
import { ChallengeViewEditRouteComponent } from './challenge-view-edit-route/challenge-view-edit-route.component';
import { ChallengeCategoryListRouteComponent } from './challenge-category-list-route/challenge-category-list-route.component';
import { ChallengeCategoryViewRouteComponent } from './challenge-category-view-route/challenge-category-view-route.component';

//
// Setup metadata
let metadata: any = {};

SideMenu.map(m => {
    if (m.options) {
        m.options.map(o => {
            if (o.route === '/challenges') {
                metadata = o.meta;
            }
        })
    }
});

const routes: Routes = [{
    path: '',
    component: ChallengesRouteComponent,
    canActivate: [UserAuthedGuard],
    children: [{
        path: '',
        component: ChallengeListRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }, {
        path: 'add',
        component: ChallengeAddRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }, {
        path: 'view/:id',
        component: ChallengeViewRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        },
        children: [{
            path: '',
            component: ChallengeViewDetailsRouteComponent,
            canActivate: [UserAuthedGuard],
            data: {
                meta: metadata
            },
        }, {
            path: 'edit',
            component: ChallengeViewEditRouteComponent,
            canActivate: [UserAuthedGuard],
            data: {
                meta: metadata
            }
        }]
    }, {
        path: 'categories',
        component: ChallengeCategoryListRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }, {
        path: 'categories/view/:id',
        component: ChallengeCategoryViewRouteComponent,
        canActivate: [UserAuthedGuard],
        data: {
            meta: metadata
        }
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChallengesRoutingModule { }

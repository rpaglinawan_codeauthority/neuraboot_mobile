import { Component, OnInit } from '@angular/core';
import { ChallengeService } from 'src/app/services/challenge.service';
import { RecordService } from 'src/app/services/record.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';
import { MetaService } from '@ngx-meta/core';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { Challenge } from '../../../../../../neuraboot-core/interfaces/challenge.interface';

@Component({
    selector: 'app-challenge-view-route',
    templateUrl: './challenge-view-route.component.html',
    styleUrls: ['./challenge-view-route.component.scss'],
    providers: [ChallengeService, RecordService]
})
export class ChallengeViewRouteComponent implements OnInit {

    public id: string = null;
    public data$: Subscription;

    constructor(
        public route: ActivatedRoute,
        public service: ChallengeService,
        public menu: MenuService,
        public meta: MetaService,
        public ui: UiService,
        public record: RecordService,
        public auth: AuthService,
        public router: Router
    ) { }

    ngOnInit() {
        //
        // Set id
        this.id = this.route.snapshot.params.id;

        //
        // Setup record service
        this.record.id = this.id;
        this.record.service = this.service;
        this.record.request = { query: { field: 'id', operator: '==', value: this.id } };
        this.record.postGetHook = this.postGetHook.bind(this);

        //
        // Load
        this.data$ = this.record.setup().subscribe();
    }

    ngOnDestroy() {
        this.data$.unsubscribe();
    }

    postGetHook(record: Challenge) {

        //
        // Set breadcrumb and meta
        this.menu.breadcrumbs = [{
            icon: 'fa fa-trophy',
            placeholder: 'Challenges',
            route: '/challenges'
        },
        {
            placeholder: 'Manage',
            route: '/challenges'
        }, {
            placeholder: record.id
        }];

        this.meta.setTitle(`Challenges / Manage / ${record.id}`);

        return record;
    }

    async toggleStatus() {
        let status = !this.record.entry.active;
        let success = await this.service.toggleStatus(this.record.id, status);

        if (success) {
            this.record.entry.active = status;
            this.record.updateEntry(this.record.entry);
        }
    }

}

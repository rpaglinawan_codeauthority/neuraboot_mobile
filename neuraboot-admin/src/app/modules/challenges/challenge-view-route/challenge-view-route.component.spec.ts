import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeViewRouteComponent } from './challenge-view-route.component';

describe('ChallengeViewRouteComponent', () => {
  let component: ChallengeViewRouteComponent;
  let fixture: ComponentFixture<ChallengeViewRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeViewRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeViewRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

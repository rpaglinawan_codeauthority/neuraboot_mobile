import { Component, OnInit } from '@angular/core';
import { ChallengeCategoryAvatar } from '../../../../../../neuraboot-core/constants/challenge-category-avatar';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
    selector: 'challenge-category-list',
    templateUrl: './challenge-category-list.component.html',
    styleUrls: ['./challenge-category-list.component.scss']
})
export class ChallengeCategoryListComponent implements OnInit {
    public challengeCategories: any[] = [
        {
            label: 'Connections',
            value: 'connections',
            image: ChallengeCategoryAvatar.connections
        },
        {
            label: 'Fuel',
            value: 'fuel',
            image: ChallengeCategoryAvatar.fuel
        },
        {
            label: 'Energy release',
            value: 'energy-release',
            image: ChallengeCategoryAvatar['energy-release']
        },
        {
            label: 'Awareness',
            value: 'awareness',
            image: ChallengeCategoryAvatar.awareness
        },
        {
            label: 'Challenge',
            value: 'challenge',
            image: ChallengeCategoryAvatar.challenge
        }
    ];

    constructor(public navigation: NavigationService) {}

    ngOnInit() {}
}

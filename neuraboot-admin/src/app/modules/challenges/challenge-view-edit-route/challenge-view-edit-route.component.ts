import { Component, OnInit } from '@angular/core';
import { RecordService } from 'src/app/services/record.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { MenuService } from 'src/app/services/menu.service';

@Component({
    selector: 'app-challenge-view-edit-route',
    templateUrl: './challenge-view-edit-route.component.html',
    styleUrls: ['./challenge-view-edit-route.component.scss']
})
export class ChallengeViewEditRouteComponent implements OnInit {

    constructor(
        public record: RecordService,
        public navigation: NavigationService,
        public menu: MenuService
    ) { }

    ngOnInit() {
    }

    /**
     * On save
     *
     * @description On save event
     * @param event
     * @memberof ChallengeViewEditRouteComponent
     */
    onSave(event) {

        //
        // Validate
        if (!event || !event.success) return;

        //
        // Update record
        this.record.updateEntry(event.data);

        //
        // Update breadcrumb
        this.menu.breadcrumbs[this.menu.breadcrumbs.length - 1].placeholder = event.data.label;
    }

    /**
     * On cancel
     *
     * @description On cancel event
     * @param event
     * @memberof ChallengeViewEditRouteComponent
     */
    onCancel(event) {
        this.navigation.goTo(this.record.service.listUrl);
    }
}

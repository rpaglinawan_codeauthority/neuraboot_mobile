import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeViewEditRouteComponent } from './challenge-view-edit-route.component';

describe('ChallengeViewEditRouteComponent', () => {
  let component: ChallengeViewEditRouteComponent;
  let fixture: ComponentFixture<ChallengeViewEditRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeViewEditRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeViewEditRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

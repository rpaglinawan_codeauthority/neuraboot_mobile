import { Component, OnInit } from '@angular/core';
import { ChallengeService } from 'src/app/services/challenge.service';
import { ChallengeHistoryService } from 'src/app/services/challenge-history.service';
import { RecordService } from 'src/app/services/record.service';
import { ChallengeCategoryAvatar } from '../../../../../../neuraboot-core/constants/challenge-category-avatar';
import { ChartNumberSetup } from '../../../../../../neuraboot-core/interfaces/chart-number.interface';
import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';
import { QueryChallengeCategoriesTopUsers } from '../../../../../../neuraboot-core/queries/challenge-categories-top-users';
import { QueryChallengeCategoryCountLastMonth } from '../../../../../../neuraboot-core/queries/challenge-category-count-last-month';
import { QueryChallengeCategoryCountLastYear } from '../../../../../../neuraboot-core/queries/challenge-category-count-last-year';
import { ActivatedRoute } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';
import { MenuService } from 'src/app/services/menu.service';
import { MetaService } from '@ngx-meta/core';
import { find } from 'lodash';
import { FormatDay, FormatMonth } from '../../../../../../neuraboot-core/constants/calendar';

@Component({
    selector: 'app-challenge-category-view-route',
    templateUrl: './challenge-category-view-route.component.html',
    styleUrls: ['./challenge-category-view-route.component.scss'],
    providers: [ChallengeService, ChallengeHistoryService, RecordService]
})
export class ChallengeCategoryViewRouteComponent implements OnInit {

    public id: string = null;
    public challengeCategories: any[] = [{
        label: 'Connections',
        value: 'connections',
        image: ChallengeCategoryAvatar.connections
    }, {
        label: 'Fuel',
        value: 'fuel',
        image: ChallengeCategoryAvatar.fuel
    }, {
        label: 'Energy release',
        value: 'energy-release',
        image: ChallengeCategoryAvatar['energy-release']
    }, {
        label: 'Awareness',
        value: 'awareness',
        image: ChallengeCategoryAvatar.awareness
    }, {
        label: 'Challenge',
        value: 'challenge',
        image: ChallengeCategoryAvatar.challenge
    }];

    public chartLines: any[] = [];
    public chartNumbers: { extra: any, setup: ChartNumberSetup }[] = [];
    public chartBarTopCategoryUsers: ChartBarVerticalSetup[] = [];
    public chartBarTopCategoryChallenges: ChartBarVerticalSetup[] = [];
    public chartBarSecondaryColors: any = {
        domain: ['#33CCFF', '#33FFFF', '#33FF66', '#CCFF33', '#FFCC00', '#FF6600', '#FF3333', '#FF33FF', '#CC33FF', '#6e7fff']
    };

    constructor(
        public route: ActivatedRoute,
        public record: RecordService,
        public service: ChallengeService,
        public ui: UiService,
        public menu: MenuService,
        public meta: MetaService,
        public challengeHistory: ChallengeHistoryService
    ) { }

    ngOnInit() {

        //
        // Set id
        this.id = this.route.snapshot.params.id;
        this.record.id = this.id;
        this.record.service = this.service;

        //
        // Load
        this.ui.loading = true;

        //
        // Get entry
        let entry = find(this.challengeCategories, ['value', this.id]);
        this.record.updateEntry(entry);

        setTimeout(() => {

            //
            // Set breadcrumb and meta
            this.menu.breadcrumbs = [{
                icon: 'fa fa-trophy',
                placeholder: 'Challenges',
                route: '/challenges'
            },
            {
                placeholder: 'Categories',
                route: '/challenges/categories'
            }, {
                placeholder: this.record.service.transformCategoryName(this.record.id)
            }];

            this.meta.setTitle(`Challenges / Categories / ${this.record.service.transformCategoryName(this.record.id)}`);

        }, 450);

        //
        // Unload
        this.ui.loading = false;

        //
        // Setup charts
        this.setupCharts();
    }

    setupCharts() {

        //
        // Chart bars
        this.chartBarTopCategoryUsers = [{
            name: 'top-category-users',
            placeholder: 'Top 5 users',
            title: `<strong>Top 5</strong> users`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryChallengeCategoriesTopUsers(this.record.id),
            service: this.challengeHistory,
            ttl: 60 // in seconds
        }]

        //
        // Chart numbers
        this.chartNumbers = [
            {
                extra: {
                    title: 'Time(s) selected',
                    size: 'is-6',
                    icon: 'fa fa-users'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "challenge.category.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Challenge(s)',
                    size: 'is-6',
                    icon: 'fa fa-trophy',
                },
                setup: {
                    service: this.record.service,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "category": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Completed',
                    size: 'is-6',
                    icon: 'far fa-check-circle'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "completed"
                                        }
                                    },
                                    {
                                        "match": {
                                            "challenge.category.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Cancelled',
                    size: 'is-6',
                    icon: 'fa fa-ban'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "cancelled"
                                        }
                                    },
                                    {
                                        "match": {
                                            "challenge.category.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        ]

        this.chartLines = [
            {
                name: 'category-last-month',
                placeholder: 'Category Last Month',
                title: `<strong>Category count</strong> on last 30 days`,
                xAxisTickFormatting: FormatDay,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryChallengeCategoryCountLastMonth(this.record.id),
                service: this.challengeHistory,
                ttl: 60
            },
            {
                name: 'last-year',
                placeholder: 'Challenge Last Year',
                title: `<strong>Challenge count</strong> on last year`,
                xAxisTickFormatting: FormatMonth,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryChallengeCategoryCountLastYear(this.record.id),
                service: this.challengeHistory,
                ttl: 60
            }
        ]
    }
}

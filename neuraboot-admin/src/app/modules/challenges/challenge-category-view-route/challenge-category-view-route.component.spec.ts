import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeCategoryViewRouteComponent } from './challenge-category-view-route.component';

describe('ChallengeCategoryViewRouteComponent', () => {
  let component: ChallengeCategoryViewRouteComponent;
  let fixture: ComponentFixture<ChallengeCategoryViewRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeCategoryViewRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeCategoryViewRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

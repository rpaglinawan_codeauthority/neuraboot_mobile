import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Challenge } from '../../../../../../neuraboot-core/interfaces/challenge.interface';
import { ChallengeCategoryOptions } from '../../../../../../neuraboot-core/constants/challenge-category-options';
import { Guid } from 'src/app/utils/guid';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ChallengeService } from 'src/app/services/challenge.service';
import { UiService } from 'src/app/services/ui.service';
import { CacheService } from 'src/app/services/cache.service';
import { merge } from 'lodash';
import { CustomValidators } from 'ng2-validation';
import * as moment from 'moment';

@Component({
    selector: 'challenge-form',
    templateUrl: './challenge-form.component.html',
    styleUrls: ['./challenge-form.component.scss']
})
export class ChallengeFormComponent implements OnInit {
    @Output() public onSave = new EventEmitter();
    @Output() public onCancel = new EventEmitter();

    @Input() public edit: boolean = false;
    @Input() public redirect: boolean = false;
    @Input() public data: Challenge = null;

    public feeling: any[] = [{
        label: 'Angry',
        value: 'angry'
    }, {
        label: 'Anxious',
        value: 'anxious'
    }, {
        label: 'Sad',
        value: 'depressed'
    }];
    public form: FormGroup;
    public model: Challenge = {
        id: Guid.make(2),
        category: null,
        feeling: null,
        text: null,
        active: true,
        link: null,
        label: null,
        created_at: moment().toISOString(),
        updated_at: moment().toISOString()
    };

    public categories: any[] = ChallengeCategoryOptions;

    constructor(private fb: FormBuilder, public service: ChallengeService, public ui: UiService, public cache: CacheService) {}

    ngOnInit() {
        //
        // Merge data into model
        merge(this.model, this.data);

        //
        // Create form
        this.createForm();
    }

    /**
     * Create form
     *
     * @description Create form with its validations
     * @memberof ChallengeFormComponent
     */
    createForm() {
        let fields: any = {};

        //
        // Fields
        fields.text = [this.model.text];
        fields.label = [this.model.label];
        fields.link = [this.model.link, [CustomValidators.url]];
        fields.active = [this.model.active, Validators.required];
        fields.category = [this.model.category, Validators.required];
        fields.feeling = [this.model.feeling];
        fields.created_at = [this.model.created_at];
        fields.updated_at = [this.model.updated_at];

        //
        // Set id or generate
        fields.id = new FormControl(this.model.id, Validators.required);

        //
        // Form
        this.form = this.fb.group(fields);
    }

    /**
     * On submit
     *
     * @description On submit form event
     * @returns
     * @memberof ChallengeFormComponent
     */
    async onSubmit() {
        try {
            //
            // Validate
            if (!this.form.get('text').value && !this.form.get('label').value && !this.form.get('link').value) {
                return this.ui.say('Challenges must have a text or label and link');
            } else if (
                !this.form.get('text').value &&
                ((this.form.get('label').value && !this.form.get('link').value) ||
                    (!this.form.get('label').value && this.form.get('link').value))
            ) {
                return this.ui.say("Label and link are necessary when you don't have a text");
            }

            this.ui.say('Saving challenge...');
            this.ui.loading = true;

            //
            // Update timestamp
            if (this.edit) this.form.get('updated_at').patchValue(moment().toISOString());

            //
            // Save
            await this.service.set(this.form.get('id').value, this.form.getRawValue()).toPromise();

            //
            // Emit on save
            this.onSave.emit({
                success: true,
                data: this.form.getRawValue()
            });

            //
            // Clear cache
            this.cache.remove('challenges/{"query":{"field":"id","operator":"==","value":"' + this.form.get('id').value + '"}}');

            //
            // UI
            this.ui.say('Saved!');
            this.ui.loading = false;
        } catch (error) {
            //
            // Return
            this.ui.say('Ops, not saved');
            this.ui.loading = false;

            this.onSave.emit({
                success: false
            });
        }
    }

    /**
     * Cancel
     *
     * @description Cancel action
     * @memberof ChallengeFormComponent
     */
    cancel() {
        this.onCancel.emit();
    }
}

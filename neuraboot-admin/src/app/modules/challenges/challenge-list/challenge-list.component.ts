import { Component, OnInit } from '@angular/core';
import { ChallengeService } from 'src/app/services/challenge.service';
import { TableEntriesSetup, TableRowClickEvent } from 'src/app/interfaces/table';
import { ElasticQueryBase } from '../../../../../../neuraboot-core/constants/elastic';
import { ChallengeTableFilters, ChallengeTableColumns } from '../../../../../../neuraboot-core/constants/challenge.table';
import { ChallengeCategories } from '../../../../../../neuraboot-core/constants/challenge.categories';
import { cloneDeep } from 'lodash';

@Component({
    selector: 'challenge-list',
    templateUrl: './challenge-list.component.html',
    styleUrls: ['./challenge-list.component.scss'],
    providers: [ChallengeService]
})
export class ChallengeListComponent implements OnInit {
    public setup: TableEntriesSetup = {
        service: this.service,
        path: '/find',
        query: cloneDeep(ElasticQueryBase)
    };

    public columns: any[] = ChallengeTableColumns;
    public filters: any[] = ChallengeTableFilters;

    constructor(public service: ChallengeService) {}

    ngOnInit() {
        this.setup.query = cloneDeep(ElasticQueryBase);
        this.setup.query.sort = [
            {
                updated_at: {
                    order: 'asc'
                }
            }
        ];

        this.filters[0].options = [...ChallengeCategories];
    }

    rowClick($ev: TableRowClickEvent) {
        $ev.router.navigate(['/challenges/view/' + $ev.row.id]);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChallengesRoutingModule } from './challenges-routing.module';
import { ChallengesRouteComponent } from './challenges-route/challenges-route.component';
import { ChallengeListRouteComponent } from './challenge-list-route/challenge-list-route.component';
import { ChallengeListComponent } from './challenge-list/challenge-list.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { CovalentTableModule } from '../covalent-table/covalent-table.module';
import { ChartsModule } from '../charts/charts.module';
import { ChallengeAddRouteComponent } from './challenge-add-route/challenge-add-route.component';
import { ChallengeFormComponent } from './challenge-form/challenge-form.component';
import { MatSlideToggleModule, MatSelectModule } from '@angular/material';
import { ChallengeViewRouteComponent } from './challenge-view-route/challenge-view-route.component';
import { ChallengeViewDetailsRouteComponent } from './challenge-view-details-route/challenge-view-details-route.component';
import { ChallengeViewEditRouteComponent } from './challenge-view-edit-route/challenge-view-edit-route.component';
import { ChallengeCategoryListRouteComponent } from './challenge-category-list-route/challenge-category-list-route.component';
import { ChallengeCategoryListComponent } from './challenge-category-list/challenge-category-list.component';
import { ChallengeCategoryViewRouteComponent } from './challenge-category-view-route/challenge-category-view-route.component';

@NgModule({
    declarations: [ChallengesRouteComponent, ChallengeListRouteComponent, ChallengeListComponent, ChallengeAddRouteComponent, ChallengeFormComponent, ChallengeViewRouteComponent, ChallengeViewDetailsRouteComponent, ChallengeViewEditRouteComponent, ChallengeCategoryListRouteComponent, ChallengeCategoryListComponent, ChallengeCategoryViewRouteComponent],
    imports: [
        CommonModule,
        ChallengesRoutingModule,
        ComponentsModule,
        CovalentTableModule,
        ChartsModule,
        MatSlideToggleModule,
        MatSelectModule
    ]
})
export class ChallengesModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeAddRouteComponent } from './challenge-add-route.component';

describe('ChallengeAddRouteComponent', () => {
  let component: ChallengeAddRouteComponent;
  let fixture: ComponentFixture<ChallengeAddRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeAddRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeAddRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

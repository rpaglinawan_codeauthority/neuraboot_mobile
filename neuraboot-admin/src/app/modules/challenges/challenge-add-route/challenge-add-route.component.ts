import { Component, OnInit } from '@angular/core';
import { ChallengeService } from 'src/app/services/challenge.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
    selector: 'app-challenge-add-route',
    templateUrl: './challenge-add-route.component.html',
    styleUrls: ['./challenge-add-route.component.scss'],
    providers: [ChallengeService]
})
export class ChallengeAddRouteComponent implements OnInit {
    constructor(public service: ChallengeService, public navigation: NavigationService) {}

    ngOnInit() {}

    /**
     * On save
     *
     * @description On save event
     * @param event
     * @memberof ChallengeAddRouteComponent
     */
    onSave(event) {
        //
        // Validate
        if (!event || !event.success) return;

        this.navigation.goTo(this.service.viewUrl.replace('id', event.data.id));
    }

    /**
     * On cancel
     *
     * @description On cancel event
     * @param event
     * @memberof ChallengeAddRouteComponent
     */
    onCancel(event) {
        this.navigation.goTo(this.service.listUrl);
    }
}

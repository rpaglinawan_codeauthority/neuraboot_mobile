import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeListRouteComponent } from './challenge-list-route.component';

describe('ChallengeListRouteComponent', () => {
  let component: ChallengeListRouteComponent;
  let fixture: ComponentFixture<ChallengeListRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeListRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeListRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

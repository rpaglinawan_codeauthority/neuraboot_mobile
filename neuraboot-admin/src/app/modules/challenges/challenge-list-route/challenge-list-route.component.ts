import { Component, OnInit } from '@angular/core';
import { ChallengeService } from 'src/app/services/challenge.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
    selector: 'app-challenge-list-route',
    templateUrl: './challenge-list-route.component.html',
    styleUrls: ['./challenge-list-route.component.scss'],
    providers: [ChallengeService]
})
export class ChallengeListRouteComponent implements OnInit {

    constructor(
        public navigation: NavigationService,
        public service: ChallengeService
    ) { }

    ngOnInit() {

        //
        // Set main fab action for customer
        this.navigation.setMainFabAction(this.goToCreateRoute.bind(this));
    }

    ngOnDestroy() {
        this.navigation.setMainFabAction();
    }

    /**
     * Go to create route
     *
     * @memberof ChallengeListRouteComponent
     */
    goToCreateRoute() {
        this.navigation.goTo(this.service.createUrl);
    }

}

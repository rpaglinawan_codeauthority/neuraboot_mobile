import { Component, OnInit } from '@angular/core';
import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';
import { QueryChallengeCategoriesTop } from '../../../../../../neuraboot-core/queries/challenge-categories-top';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { MenuService } from 'src/app/services/menu.service';
import { MetaService } from '@ngx-meta/core';
import { ChallengeHistoryService } from 'src/app/services/challenge-history.service';

@Component({
    selector: 'app-challenge-category-list-route',
    templateUrl: './challenge-category-list-route.component.html',
    styleUrls: ['./challenge-category-list-route.component.scss'],
    providers: [ChallengeHistoryService]
})
export class ChallengeCategoryListRouteComponent implements OnInit {

    public chartBarTopCategories: ChartBarVerticalSetup[] = [];

    constructor(
        public challengeHistory: ChallengeHistoryService,
        public menu: MenuService,
        public meta: MetaService
    ) { }

    ngOnInit() {
        this.chartBarTopCategories = [{
            name: 'top-categories',
            placeholder: 'Top categories',
            title: `<strong>Top</strong> categories`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryChallengeCategoriesTop,
            service: this.challengeHistory,
            ttl: 60 // in seconds
        }]

        //
        // Set breadcrumb and meta
        this.menu.breadcrumbs = [{
            icon: 'fas fa-trophy',
            placeholder: 'Challenges',
            route: '/challenges'
        },
        {
            placeholder: 'Categories',
            route: '/challenges/categories'
        }];

        this.meta.setTitle(`Challenges / Cartegories`);
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeCategoryListRouteComponent } from './challenge-category-list-route.component';

describe('ChallengeCategoryListRouteComponent', () => {
  let component: ChallengeCategoryListRouteComponent;
  let fixture: ComponentFixture<ChallengeCategoryListRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeCategoryListRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeCategoryListRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

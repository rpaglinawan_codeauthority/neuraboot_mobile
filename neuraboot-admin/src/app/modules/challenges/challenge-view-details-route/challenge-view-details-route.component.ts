import { Component, OnInit } from '@angular/core';
import { FormatDay, FormatMonth } from '../../../../../../neuraboot-core/constants/calendar';
import { ChallengeHistoryService } from 'src/app/services/challenge-history.service';
import { RecordService } from 'src/app/services/record.service';
import { QueryChallengeHistoryCountLastMonth } from '../../../../../../neuraboot-core/queries/challenge-history-count-last-month';
import { QueryChallengeHistoryCountLastYear } from '../../../../../../neuraboot-core/queries/challenge-history-count-last-year';
import { QueryChallengeTopUsers } from '../../../../../../neuraboot-core/queries/challenge-top-users';
import { ChartNumberSetup } from '../../../../../../neuraboot-core/interfaces/chart-number.interface';
import { ChartBarVerticalSetup } from '../../../../../../neuraboot-core/interfaces/chart-bar-vertical.interface';

@Component({
    selector: 'app-challenge-view-details-route',
    templateUrl: './challenge-view-details-route.component.html',
    styleUrls: ['./challenge-view-details-route.component.scss'],
    providers: [ChallengeHistoryService]
})
export class ChallengeViewDetailsRouteComponent implements OnInit {

    public chartLines: any[] = [];
    public chartNumbers: { extra: any, setup: ChartNumberSetup }[] = [];
    public chartBarTopChallengeUsers: ChartBarVerticalSetup[] = [];

    constructor(
        public challengeHistory: ChallengeHistoryService,
        public record: RecordService
    ) { }

    ngOnInit() {
        this.setupChartLines();
        this.setupChartNumbers();
        this.setupChartBars();
    }

    setupChartLines() {

        this.chartLines = [
            {
                name: 'challenge-last-month',
                placeholder: 'Challenge Last Month',
                title: `<strong>time(s) selected</strong> on last 30 days`,
                xAxisTickFormatting: FormatDay,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryChallengeHistoryCountLastMonth(this.record.id),
                service: this.challengeHistory,
                ttl: 60
            },
            {
                name: 'feeling-specific-last-year',
                placeholder: 'Feeling Last Year',
                title: `<strong>time(s) selected</strong> on last year`,
                xAxisTickFormatting: FormatMonth,

                charts: [{
                    name: 'count',
                    series: []
                }],

                path: '/find',
                query: QueryChallengeHistoryCountLastYear(this.record.id),
                service: this.challengeHistory,
                ttl: 60
            }
        ]

    }

    setupChartNumbers() {

        //
        // chart numbers
        this.chartNumbers = [
            {
                extra: {
                    title: 'Selection(s)',
                    size: 'is-6',
                    icon: 'far fa-calendar-alt'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "challenge.id": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Selection(s) today',
                    size: 'is-6',
                    icon: 'far fa-calendar-check',
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "challenge.id": this.record.id
                                        }
                                    },
                                    {
                                        "range": {
                                            "created_at": {
                                                "gt": "now-1d/d",
                                                "lte": "now/d"
                                            }
                                        }
                                    }

                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Completed',
                    size: 'is-6',
                    icon: 'far fa-check-circle'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "completed"
                                        }
                                    },
                                    {
                                        "match": {
                                            "challenge.id.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            {
                extra: {
                    title: 'Cancelled',
                    size: 'is-6',
                    icon: 'fa fa-ban'
                },
                setup: {
                    service: this.challengeHistory,
                    path: '/count',
                    ttl: 30,
                    query: {
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "status": "cancelled"
                                        }
                                    },
                                    {
                                        "match": {
                                            "challenge.id.keyword": this.record.id
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            },
        ]
    }

    setupChartBars() {
        console.log('id', this.record.id);
        this.chartBarTopChallengeUsers = [{
            name: 'top-challenge-users',
            placeholder: 'Top 5 users',
            title: `<strong>Top 5</strong> users`,
            charts: [{
                name: 'count',
                series: []
            }],
            path: '/find',
            query: QueryChallengeTopUsers(this.record.id),
            service: this.challengeHistory,
            ttl: 60 // in seconds
        }]
    }
}

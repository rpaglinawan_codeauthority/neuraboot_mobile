import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeViewDetailsRouteComponent } from './challenge-view-details-route.component';

describe('ChallengeViewDetailsRouteComponent', () => {
  let component: ChallengeViewDetailsRouteComponent;
  let fixture: ComponentFixture<ChallengeViewDetailsRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeViewDetailsRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeViewDetailsRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

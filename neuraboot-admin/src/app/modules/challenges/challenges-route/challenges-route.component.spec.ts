import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengesRouteComponent } from './challenges-route.component';

describe('ChallengesRouteComponent', () => {
  let component: ChallengesRouteComponent;
  let fixture: ComponentFixture<ChallengesRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengesRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengesRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

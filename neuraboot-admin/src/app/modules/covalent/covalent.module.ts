import { NgModule } from '@angular/core';
import { CovalentVirtualScrollModule, CovalentDataTableModule, CovalentPagingModule, CovalentLayoutModule, CovalentSearchModule } from '@covalent/core';

@NgModule({
    declarations: [],
    imports: [CovalentVirtualScrollModule, CovalentDataTableModule, CovalentPagingModule, CovalentLayoutModule, CovalentSearchModule],
    exports: [CovalentVirtualScrollModule, CovalentDataTableModule, CovalentPagingModule, CovalentLayoutModule, CovalentSearchModule]
})
export class CovalentModule { }

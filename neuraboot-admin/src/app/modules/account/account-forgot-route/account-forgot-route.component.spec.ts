import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountForgotRouteComponent } from './account-forgot-route.component';

describe('AccountForgotRouteComponent', () => {
  let component: AccountForgotRouteComponent;
  let fixture: ComponentFixture<AccountForgotRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountForgotRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountForgotRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-account-route',
    templateUrl: './account-route.component.html',
    styleUrls: ['./account-route.component.scss']
})
export class AccountRouteComponent implements OnInit {

    public version: number | string = environment.version;
    public staging: boolean = environment.staging;
    public testing: boolean = environment.testing;

    constructor() { }

    ngOnInit() {
    }

}

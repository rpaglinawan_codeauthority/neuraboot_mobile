import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountRouteComponent } from './account-route/account-route.component';
import { AccountSignInRouteComponent } from './account-sign-in-route/account-sign-in-route.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { AccountLoginFormComponent } from './account-login-form/account-login-form.component';
import { AccountForgotRouteComponent } from './account-forgot-route/account-forgot-route.component';
import { AccountForgotFormComponent } from './account-forgot-form/account-forgot-form.component';

@NgModule({
    declarations: [AccountRouteComponent, AccountSignInRouteComponent, AccountLoginFormComponent, AccountForgotRouteComponent, AccountForgotFormComponent],
    imports: [
        CommonModule,
        AccountRoutingModule,
        ComponentsModule
    ]
})
export class AccountModule { }

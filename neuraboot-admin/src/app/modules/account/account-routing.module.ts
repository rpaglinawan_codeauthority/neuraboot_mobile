import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountRouteComponent } from './account-route/account-route.component';
import { AccountSignInRouteComponent } from './account-sign-in-route/account-sign-in-route.component';
import { AccountForgotRouteComponent } from './account-forgot-route/account-forgot-route.component';
import { UserNotAuthedGuard } from 'src/app/guards/user-not-authed.guard';

const routes: Routes = [{
    path: '',
    component: AccountRouteComponent,
    canActivate: [UserNotAuthedGuard],
    children: [{
        path: '',
        component: AccountSignInRouteComponent,
        canActivate: [UserNotAuthedGuard],
    }, {
        path: 'forgot',
        component: AccountForgotRouteComponent,
        canActivate: [UserNotAuthedGuard],
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AccountRoutingModule { }

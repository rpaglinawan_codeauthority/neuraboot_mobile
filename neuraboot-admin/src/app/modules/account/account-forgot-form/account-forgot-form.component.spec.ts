import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountForgotFormComponent } from './account-forgot-form.component';

describe('AccountForgotFormComponent', () => {
  let component: AccountForgotFormComponent;
  let fixture: ComponentFixture<AccountForgotFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountForgotFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountForgotFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

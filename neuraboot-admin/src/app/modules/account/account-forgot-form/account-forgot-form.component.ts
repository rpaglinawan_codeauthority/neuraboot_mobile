import { Component, OnInit, ViewChildren, Input } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'account-forgot-form',
    templateUrl: './account-forgot-form.component.html',
    styleUrls: ['./account-forgot-form.component.scss']
})
export class AccountForgotFormComponent implements OnInit {

    @ViewChildren('input') public vc; // vc = view children
    @Input() public lite: boolean = false;

    public entry: any = {};
    public loading: boolean;

    constructor(
        public ui: UiService,
        public auth: AuthService,
        public router: Router
    ) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        setTimeout(() => {
            if (this.vc.first.nativeElement.focus) this.vc.first.nativeElement.focus();
        }, 1000)
    }

    async submit() {
        this.loading = true;
        this.ui.loading = true;
        this.auth.forgot(this.entry.email)
            .catch(async err => {
                this.loading = false;
                this.ui.loading = false;
                this.ui.ask(err.message, { label: 'Ok' }, () => {
                    this.vc.first.nativeElement.focus();
                })
            })
            .then(async (result) => {
                if (result) {
                    this.ui.say('Success! verify your email inbox');
                    this.router.navigate(['/account/sign-in']);
                    this.loading = false;
                    this.ui.loading = false;
                }
            })
    }
}

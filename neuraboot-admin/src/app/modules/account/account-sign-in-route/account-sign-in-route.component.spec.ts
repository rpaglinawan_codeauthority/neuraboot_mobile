import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountSignInRouteComponent } from './account-sign-in-route.component';

describe('AccountSignInRouteComponent', () => {
  let component: AccountSignInRouteComponent;
  let fixture: ComponentFixture<AccountSignInRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountSignInRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountSignInRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

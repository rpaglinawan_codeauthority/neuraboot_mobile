import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UiService } from '../../../services/ui.service';
import { AuthService } from '../../../services/auth.service';
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';
import { CacheService } from '../../../services/cache.service';
import { UserService } from '../../../services/user.service';
import { RRResponse } from '@ionfire/reactive-record';
import { mergeMap, catchError, map, takeUntil, tap } from 'rxjs/operators';
import { forkJoin, Subject, from } from 'rxjs';

import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { ApiUserAuthResponse } from '../../../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
import { NavigationService } from '../../../services/navigation.service';
import { formatError } from '../../../../../../neuraboot-core/constants/error';
import { merge, toLower } from 'lodash';

@Component({
    selector: 'account-login-form',
    templateUrl: './account-login-form.component.html',
    styleUrls: ['./account-login-form.component.scss'],
    providers: [UserService]
})
export class AccountLoginFormComponent implements OnInit {

    @ViewChild('emailField') public emailField: ElementRef;
    @ViewChild('passwordField') public passwordField: ElementRef;

    public entry: { email?: string, password?: string } = {};
    public loading: boolean;

    constructor(
        public auth: AuthService,
        public ui: UiService,
        private cache: CacheService,
        private userService: UserService,
        private nav: NavigationService
    ) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        setTimeout(() => {
            if (this.emailField.nativeElement.focus)
                this.emailField.nativeElement.focus();
        }, 1000)
    }

    async submit() {
        this.loading = true;
        this.ui.loading = true;

        //
        // Get current cache
        let cache = await this.cache.get('user') || {};

        //
        // error handler
        const error$ = new Subject();

        //
        // Login user
        this.userService.post('/auth/email', this.entry, { key: 'user', forceCache: true, forceNetwork: true, transformCache: (data: RRResponse) => data.data.user })
            .pipe(

                //
                // transform response
                map((r: any) => r.data),

                //
                // stop stream on any local error
                takeUntil(error$),

                //
                // validate user `admin` role
                map((r: ApiUserAuthResponse) => {
                    const user = r.user;

                    if (!user.roles || !user.roles.includes('admin')) {
                        const msg: string = `User doesn't have privileges`;
                        return error$.error(formatError(msg));
                    }

                    return r;
                }),

                //
                // auth within firebase
                mergeMap((r: ApiUserAuthResponse) => {
                    return forkJoin(this.auth.signInWithCustomToken$(r.token.auth).pipe(
                        map(() => {
                            return r; // return previous result to next block since we dont need the result from signInWithCustomToken
                        })
                    ));
                }),

                //
                // format user/token object and set to the session
                map((r: ApiUserAuthResponse[]) => {
                    const user: User = r[0].user;
                    user.token = r[0].token.client;

                    //
                    // Merge cache with user
                    merge(cache, user);
                }),

                //
                // set login cache
                mergeMap(() => {
                    return forkJoin(from(this.auth.setLoginData(cache)).pipe(
                        map((user) => {
                            // console.log(user);
                            return user;
                        })
                    ))
                }),

                //
                // redirect to the first route
                tap(async () => {
                    this.loading = false;
                    this.ui.loading = false;
                    await this.nav.goTo('dashboard');
                }),

                //
                // deal with exceptions
                catchError(async (err: ApiErrorResponse) => {
                    this.loading = false;
                    this.ui.loading = false;
                    this.ui.ask(err.message, { duration: 10 * 1000, label: 'Ok' }, () => {
                        if (toLower(err.message).includes('password')) {
                            this.entry.password = '';
                            this.passwordField.nativeElement.focus();
                        } else {
                            this.entry = {};
                            this.emailField.nativeElement.focus();
                        }
                    });

                })
            )
            .toPromise();
    }
}

export const environment = {
    production: true,
    version: '0010',
    testing: false,
    staging: false,
    app: 'neuraboot',
    storage: 'admin',
    firebase: {
      apiKey: "AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ",
      authDomain: "neuraboot.firebaseapp.com",
      databaseURL: "https://neuraboot.firebaseio.com",
      projectId: "neuraboot",
      storageBucket: "neuraboot.appspot.com",
      messagingSenderId: "21924514688"
    },
    google: {
      webClientId: '21924514688-f2b87b25vmmsb1g9itsggheua5bs6v8d.apps.googleusercontent.com',
      scopes: 'profile email',
      mapsKey: ''
    },
    functions: {
      // baseUrl: 'http://localhost:5000/neuraboot/us-central1'   // dev
      baseUrl: 'https://us-central1-neuraboot.cloudfunctions.net' // prod
    }
  };

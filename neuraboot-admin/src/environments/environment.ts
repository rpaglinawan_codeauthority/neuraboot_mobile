// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    version: '0010',
    production: false,
    testing: false,
    staging: false,
    app: 'neuraboot',
    storage: 'admin',
    firebase: {
        apiKey: "AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ",
        authDomain: "neuraboot.firebaseapp.com",
        databaseURL: "https://neuraboot.firebaseio.com",
        projectId: "neuraboot",
        storageBucket: "neuraboot.appspot.com",
        messagingSenderId: "21924514688"
    },
    google: {
        webClientId: '21924514688-f2b87b25vmmsb1g9itsggheua5bs6v8d.apps.googleusercontent.com',
        scopes: 'profile email',
        mapsKey: ''
    },
    functions: {
        // baseUrl: 'http://localhost:5000/neuraboot/us-central1'   // dev
        baseUrl: 'https://us-central1-neuraboot.cloudfunctions.net' // prod
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

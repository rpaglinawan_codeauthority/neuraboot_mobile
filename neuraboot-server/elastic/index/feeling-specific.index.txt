PUT feeling_specific_v1
{
  "settings": {
    "index": {
      "number_of_replicas": 0,
      "number_of_shards": 6
    },
    "index.mapping.total_fields.limit": 1000000000
  },
  "mappings": {
    "feeling": {
      "properties": {
        "id": {
          "type": "keyword"
        },
        "name": {
          "type": "keyword"
        },
        "label": {
          "type": "text"
        },
        "active": {
          "type": "boolean"
        },
        "category": {
          "type": "keyword"
        }
      }
    }
  }
}

DELETE /feeling_specific_v1
PUT whoami_history_v1
{
  "settings": {
    "index": {
      "number_of_replicas": 0,
      "number_of_shards": 6
    },
    "index.mapping.total_fields.limit": 1000000000
  },
  "mappings": {
    "history": {
      "properties": {
        "id": {
          "type": "keyword"
        },
        "created_at": {
          "type": "date"
        },
        "updated_at": {
          "type": "date"
        }
      }
    }
  }
}
PUT challenge-level_v1
{
  "settings": {
    "index": {
      "number_of_replicas": 0,
      "number_of_shards": 6
    },
    "index.mapping.total_fields.limit": 1000000000
  },
  "mappings": {
    "history": {
      "properties": {
        "id": {
          "type": "keyword"
        },
        "connections_progress": {
          "type": "float"
        },
        "connections_attempted": {
          "type": "float"
        },
        "connections_level": {
          "type": "float"
        },
        "connections_total": {
          "type": "float"
        },
        "connections_streak_day": {
          "type": "float"
        },
        "connections_streak_max": {
          "type": "float"
        },
        "fuel_progress": {
          "type": "float"
        },
        "fuel_attempted": {
          "type": "float"
        },
        "fuel_level": {
          "type": "float"
        },
        "fuel_total": {
          "type": "float"
        },
        "fuel_streak_day": {
          "type": "float"
        },
        "fuel_streak_max": {
          "type": "float"
        },
        "energy_release_progress": {
          "type": "float"
        },
        "energy_release_attempted": {
          "type": "float"
        },
        "energy_release_level": {
          "type": "float"
        },
        "energy_release_total": {
          "type": "float"
        },
        "energy_release_streak_day": {
          "type": "float"
        },
        "energy_release_streak_max": {
          "type": "float"
        },
        "awareness_progress": {
          "type": "float"
        },
        "awareness_attempted": {
          "type": "float"
        },
        "awareness_level": {
          "type": "float"
        },
        "awareness_total": {
          "type": "float"
        },
        "awareness_streak_day": {
          "type": "float"
        },
        "awareness_streak_max": {
          "type": "float"
        },
        "challenge_progress": {
          "type": "float"
        },
        "challenge_attempted": {
          "type": "float"
        },
        "challenge_level": {
          "type": "float"
        },
        "challenge_total": {
          "type": "float"
        },
        "challenge_streak_day": {
          "type": "float"
        },
        "challenge_streak_max": {
          "type": "float"
        },
        "user": {
          "type": "nested",
          "properties": {
            "uid": {
              "type": "keyword"
            }
          }
        },
        "created_at": {
          "type": "date"
        },
        "updated_at": {
          "type": "date"
        }
      }
    }
  }
}

DELETE /challenge-level_v1

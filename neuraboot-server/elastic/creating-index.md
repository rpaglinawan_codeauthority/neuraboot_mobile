# elastic data index

to get full power of elastic queries, we need to model our data by creating an index structure.

for more info check out the [elastic docs for index creation](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html)

you can test and debug elastic queries using the dev tools (kibana) 


## user index

here is an example for configure an user index

```json
PUT users_v1
{
  "settings": {
    "index": {
      "number_of_replicas": 0
    },
    "index.mapping.total_fields.limit": 1000000000
  },
  "mappings": {
    "user": {
      "properties": {
        "uid": {
          "type": "keyword"
        },
        "active": {
          "type": "boolean"
        },
        "display_name": {
          "type": "keyword"
        },
        "photo_url": {
          "type": "keyword"
        },
        "email": {
          "type": "keyword"
        },
        "created_at": {
          "type": "date"
        },
        "updated_at": {
          "type": "date"
        },
        "online_at": {
          "type": "date"
        }
      }
    }
  }
}
```

## user alias

using the aliased approach, we can have multiple versions of indexes with a single endpoint

```json
POST /_aliases
{
  "actions": [
    {
      "add": {
        "index": "users_v1",
        "alias": "users"
      }
    }
  ]
}
```

this way the elastic endpoint would be `/users` currently pointing to `users_v1` index.


> there are some restrictions that may prevent you from changing an index once you define it. taking the aliased approach will help to manipulate indexes with zero downtime in a production scenario
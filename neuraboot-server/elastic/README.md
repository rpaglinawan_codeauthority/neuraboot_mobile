# elastic search setup

### gcloud configuration 

go to google cloud console and then

1. create a [new vm instance](https://console.cloud.google.com/compute/instancesAdd)
    
    - 2 vCPU
    - ubuntu 18 LTS
    - `es-rules` in network tags 

2. go to [external ip adresses](https://console.cloud.google.com/networking/addresses/list) and make sure the ip in use by vm is `static`

3. create the following [firewall rules](https://console.cloud.google.com/networking/firewalls/list) for the specified target tag `es-rules`
    
    > es-tcp-5454 Ingress IP ranges: 0.0.0.0/0 tcp:5454 Allow 1000
    >
    > es-tcp-5455 Ingress IP ranges: 0.0.0.0/0 tcp:5455 Allow 1000

## install java

access the vm instance using `ssh`, it can be done throughout [gcloud console](https://console.cloud.google.com/compute/instances). 

1. `sudo apt-get update`
2. `sudo apt-get install default-jre`


### install elastic 6.x

1. `wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -`
2. `sudo apt-get install apt-transport-https`
3. `echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list`
4. `sudo apt-get update && sudo apt-get install elasticsearch`

### install latest kibana

1. `sudo apt-get install kibana`


### initialize elastic service

elasticsearch is not started automatically after installation. do the following to configure the startup service

1. `sudo /bin/systemctl daemon-reload`
2. `sudo /bin/systemctl enable elasticsearch.service`
3. `sudo systemctl start elasticsearch.service`

### initialize kibana service

kibana is not started automatically after installation. do the following to configure the startup service

1. `sudo /bin/systemctl enable kibana.service`
2. `sudo systemctl start kibana.service`
3. `sudo /etc/init.d/kibana start`


## change java options

1. `sudo nano /etc/elasticsearch/jvm.options`
2. edit to `-Xms4g`, `-Xmx4g`
3. restart elastic `sudo /etc/init.d/elasticsearch restart`


### check elastic status

```sh
sudo /etc/init.d/elasticsearch status
```

### check kibana status

```sh
sudo /etc/init.d/kibana status
```

## setup nginx as a reverse proxy for elastic

learn more at [this post](https://www.elastic.co/blog/playing-http-tricks-nginx)

1. `sudo apt-get install nginx && sudo apt-get install apache2-utils`
2. `sudo systemctl status nginx`
3. shutdown nginx `sudo nginx -s quit`
4. generate elastic password `sudo htpasswd -c /etc/nginx/.htpasswd elastic` enter a new password 
5. generate kibana password `sudo htpasswd /etc/nginx/.htpasswd kibana` enter a new password 
6. delete default config file `sudo rm /etc/nginx/nginx.conf`
7. copy and paste [this nginx config](./nginx.conf) to `sudo nano /etc/nginx/nginx.conf` *(ctrl+o ctrl+x to save)*
8. start nginx `sudo systemctl start nginx`
9.  check nginx `sudo systemctl status nginx`
10. test `curl -i elastic:password@localhost:5454`
 



should output

```json
{
  "name" : "ZiQPYTS",
  "cluster_name" : "elasticsearch",
  "version" : {
    "number" : "6.4.0",
  },
  "tagline" : "You Know, for Search"
}
```


🎉  at this point the elastic environment should be ready to go. you can try access on browser by using the static ip address plus port 5454 (elastic) or 5555 (kibana) 

## next steps

1. [creating index](./creating-index.md)
2. [creating index pattern](./creating-index-pattern.md)
3. [browse app index](./index)

## other resources

- [elastic details](./details.md) **not delivered in git*
- change an user pw using ssh
```js 
curl -u elastic -XPUT 'http://localhost:9200/_xpack/security/user/THE_USER/_password?pretty' -H 'Content-Type: application/json' -d '
{
"password" : "THE_NEW_PW"
}'
```
- `sudo /usr/share/elasticsearch/bin/x-pack/setup-passwords interactive`

### cloning an elastic environment

#### auth first

```
gcloud login
```

#### create new disk from a snapshot

```
gcloud compute disks create neura-elastic-disk --source-snapshot \
 https://www.googleapis.com/compute/v1/projects/ontime-manager/global/snapshots/ontime-elastic-snapshot --project neuraboot
```

#### create the compute instance from cloned disk

```
gcloud compute instances create es-neura-master \
--project neuraboot --disk name=neura-elastic-disk,boot=yes
```

### fresh install of x-pack plugin

1. `sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove x-pack --purge`
2. `sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install x-pack`
3. restart elastic `sudo /etc/init.d/elasticsearch restart` (can take a while)
4. setup passwords `sudo /usr/share/elasticsearch/bin/elasticsearch-setup-passwords auto` (then copy and paste to details.md)
5. install x-pack into kibana `sudo /usr/share/elasticsearch/bin/kibana-plugin install x-pack`

## install elastic using a defined version (6.4.0)

learn more at [elastic docs](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html).

1. `wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.4.0.deb`
2. `wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.4.0.deb.sha512`
3. `shasum -a 512 -c elasticsearch-6.4.0.deb.sha512` 
4. `sudo dpkg -i elasticsearch-6.4.0.deb`

## install kibana using a defined version (6.4.0)
1. `wget https://artifacts.elastic.co/downloads/kibana/kibana-6.4.0-amd64.deb`
2. `shasum -a 512 kibana-6.4.0-amd64.deb`
3. `sudo dpkg -i kibana-6.4.0-amd64.deb`

## install search guard (alternative to xpack plugin)

learn more at [this post](https://sematext.com/blog/elasticsearch-kibana-security-search-guard/)

1. `sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install -b com.floragunn:search-guard-6:6.4.0-23.0`
2. `sudo chmod +x /usr/share/elasticsearch/plugins/search-guard-6/tools/install_demo_configuration.sh`
3. `sudo /usr/share/elasticsearch/plugins/search-guard-6/tools/install_demo_configuration.sh`
4. `sudo chmod +x /usr/share/elasticsearch/plugins/search-guard-6/tools/sgadmin.sh`
5. `sudo /usr/share/elasticsearch/plugins/search-guard-6/tools/sgadmin.sh -cd ../sgconfig/ -icl -nhnv -cacert ../../../config/root-ca.pem -cert ../../../config/kirk.pem -key ../../../config/kirk-key.pem `


## kill nginx 

`sudo kill -QUIT $( cat /run/nginx.pid )`

## list ports in use

`lsof -i :443`


### check if elastic is running

```sh
curl -X GET "localhost:5454/"
```

## test keep alive

`curl 'elastic:EwmylqBm277IXiYdHstic@localhost:5454/_nodes/stats/http?pretty' | grep total_opened`
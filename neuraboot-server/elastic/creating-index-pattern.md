# creating index pattern

the index pattern will help to increase performance on queries. this is very simple to get up, but first we need to have some data indexed already. you can use the dev tools in kibana to insert some testing data.


1. access kibana
2. go to Management > Kibana > Index Patterns
3. define the index pattern. eg: `users_v1` and click next
4. select the `Time Filter field name`, it can be the `created_at` field
5. click on `Create index pattern`

🎉  now it's perfect for a production scenario
# neuraboot-server
NeuraBoot helps people take charge of their emotions and notify their loved ones at times they need the most help.

## Requirements
- Node v8.11.1
- Npm v5.6.0
- Typescript 2.7.2 set on VSCode
- Firebase CLI (`npm i -g firebase-tools`)
- File: `service-account.json` at `functions` root folder *ignored by git*
- File: `details.md` at `elastic` folder *ignored by git*


## Firebase setup

### Step 1

Once you got the `service-account.json` file inside functions folder, export it for enabling firebase shell to work.

execute the following command from functions folder level.

```
export GOOGLE_APPLICATION_CREDENTIALS="service-account.json" 
```

### Step 2

Issue npm install from functions folder level

### Step 3

Login to firebase. If you're logged in with another account, issue `logout` first.

```
firebase logout
firebase login
```

### Step 4

Test the shell

```js
firebase serve --only functions
// or
npm run serve
```

Then access the url `http://localhost:5000/neuraboot/us-central1/api`, it should display message `Neuraboot API 1.0.0 loaded. Testing Mode ON`.

## Watch changes while coding

```
npm run build:w
```

## Deploy to firebase functions

```
firebase deploy --only functions
```
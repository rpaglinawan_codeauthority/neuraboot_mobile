const _ = require('lodash');
const environment = require('../../neuraboot-server/functions/lib/neuraboot-server/environment').environment;

const fs = require('fs');
const request = require('request-promise');

const firebaseConfig = require('./firebase.json');
const Firebase = require('firebase');
require('firebase/firestore');

const firebase = Firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();

const ELASTICSEARCH_CONFIG = {
    username: environment.elastic.app_username,
    password: environment.elastic.app_password,
    url: environment.elastic.url
}

firestore
    .collection('challenges')
    .get()
    .then(async snapshot => {
        let result = [];

        snapshot.forEach((snap, i) => {
            let data = snap.data();
            result.push(data);
        });



        console.log('start indexing of ' + result.length);
        await Promise.all(await result.map(async (item, i) => {

            console.log('indexing challenges', item.id);

            let elasticSearchConfig = ELASTICSEARCH_CONFIG;
            let elasticSearchUrl = elasticSearchConfig.url + '/challenges/challenge/' + item.id;
            let elasticSearchMethod = item ? 'POST' : 'DELETE';

            let elasticsearchRequest = {
                method: elasticSearchMethod,
                uri: elasticSearchUrl,
                auth: {
                    username: elasticSearchConfig.username,
                    password: elasticSearchConfig.password,
                },
                body: item,
                json: true
            };

            return request(elasticsearchRequest).then(response => {
                console.log('elasticsearch response', response);
            });

        }))


    })
    .catch(console.log);
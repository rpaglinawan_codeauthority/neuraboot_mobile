const Guid = require('../functions/lib/neuraboot-server/functions/src/utils/guid').Guid;
const async = require('async');
const _ = require('lodash');
const moment = require('moment');

const Firebase = require('./lib/firebase');
const firebase = Firebase.firebase;
const firestore = Firebase.firestore;

//
// Migration logic

const fake = require('../../neuraboot-core/fakes/challenge-word.fake');
const ids = [];
let id;

async.each(fake.Challenges, (entry, cb) => {
    while (true) {
        id = Guid.make(2);
        if (!ids.includes(id)) break;
    }
    ids.push(id);
    entry.id = id;
    console.log(id);
    firestore.collection('challenges').doc(entry.id).set(entry, {
        merge: true
    }).then(r => cb());

}, () => {
    console.log('migration done', ids.length);
});

const environment = require('../../functions/lib/neuraboot-server/environment').environment;
const settings = {/* your settings... */ timestampsInSnapshots: true };
const Firebase = require('firebase');
const firebase = Firebase.initializeApp(environment.firebase.config);
const firestore = firebase.firestore();
firestore.settings(settings);

module.exports = {
    firebase: firebase,
    firestore: firestore
}
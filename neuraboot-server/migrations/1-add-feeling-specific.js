// const Guid = require('../functions/lib/functions/src/utils/guid').Guid;

const _ = require('lodash');
const moment = require('moment');

const Firebase = require('./lib/firebase');
const firebase = Firebase.firebase;
const firestore = Firebase.firestore;

//
// Migration logic

const feelingSpecifics = require('../../neuraboot-core/fakes/feeling-specific.fake');

feelingSpecifics.map(feeling => {
    firestore.collection('feeling-specific').doc(feeling.id).set(feeling);
});

console.log('migration done');

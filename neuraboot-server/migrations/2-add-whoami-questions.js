// const Guid = require('../functions/lib/functions/src/utils/guid').Guid;

const _ = require('lodash');
const moment = require('moment');

const Firebase = require('./lib/firebase');
const firebase = Firebase.firebase;
const firestore = Firebase.firestore;

//
// Migration logic

const fake = require('../../neuraboot-core/fakes/whoami.fake');

fake.WhoamiQuestions.map(question => {
    firestore.collection('whoami-questions').doc(question.id).set(question);
});

console.log('migration done');
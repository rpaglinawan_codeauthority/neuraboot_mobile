import * as Twilio from 'twilio';
import { environment } from '../../../environment';
import { Observable, PartialObserver, throwError } from 'rxjs';

/**
 *
 *
 * @export
 * @class SMS
 */
export class SMS {
    twilio: any = Twilio(environment.twilio.sid, environment.twilio.token);

    // constructor() { }

    send(from: string, to: string, body: string): Observable<any> {
        return new Observable((observer: PartialObserver<any>) => {
            if (!from) throwError('missing from argument');
            if (!to) throwError('missing to argument');
            if (!body) throwError('missing body argument');

            to = to.split('-').join('');

            this.twilio.messages.create({
                body: body,
                to: to, // Text this number
                from: from // From a valid Twilio number
            }).then((result) => {
                observer.next(result);
                observer.complete();
            }).catch(err => {
                observer.error(err);
                observer.complete();
            });
        })
    }
}
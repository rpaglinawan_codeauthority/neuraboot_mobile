import * as SparkPost from 'sparkpost';
import * as _ from 'lodash';
import { environment } from '../../../environment';


const SPARK_API = environment.sparkpost.apiKey;
const FROM = environment.sparkpost.from
const FROM_ADDR = '';
const SUBJECT = '';
const RECIPIENTS = [{
  address: ''
},
{
  address: ''
}
];
const SANDBOX = false;
const TRANSACTIONAL = true;
const TRACK_OPEN = true;
const TRACK_CLICK = true;
const REPLY_TO = null;
const TEMPLATE_ID = null;
const CAMPAIGN_ID = '';
const DESCRIPTION = '';
const HTML = ``;
// default options
const _options = {
  from: FROM,
  subject: SUBJECT,
  sandbox: SANDBOX,
  transactional: TRANSACTIONAL,
  track_open: TRACK_OPEN,
  track_click: TRACK_CLICK,
  reply_to: REPLY_TO,
  campaign_id: CAMPAIGN_ID,
  description: DESCRIPTION,
  template_id: TEMPLATE_ID,
  html: HTML,
  recipients: RECIPIENTS
}

// spark instance
const sparky = new SparkPost(SPARK_API);


export class Email {
  public html;
  public unsubscribe;
  public from;
  public subject;
  public description;
  public content;
  public template_id;
  public reply_to;
  public sandbox;
  public transactional;
  public track_open;
  public track_click;
  public recipients;
  public campaign_id;

  constructor(options = {}) {
    _.extend(this, _options); // default
    _.extend(this, options); // parameters

    // add addr on footer
    this.html += '' + FROM_ADDR + '';
    // add unsubscribe link
    if (this.unsubscribe) this.html += this.unsubscribe;
  }


  send() {
    return new Promise((resolve, reject) => {
      // compile template
      let html = EMAIL_TEMPLATE();
      const compiled = _.template(html);
      html = compiled({
        'body': this.html
      });

      const content: any = {
        from: this.from,
        subject: this.subject,
        description: this.description,
        html: html
      }

      if (this.reply_to)
        content.reply_to = this.reply_to;

      if (this.template_id)
        content.template_id = this.template_id

      sparky.transmissions.send({
        options: {
          sandbox: this.sandbox,
          transactional: this.transactional,
          open_tracking: this.track_open,
          click_tracking: this.track_click
        },
        substitution_data: {},
        content: content,
        recipients: this.recipients,
        description: this.description,
        campaign_id: this.campaign_id
      })
        .then(data => {
          // console.log('Woohoo! Email sent!');
          resolve(data);
        })
        .catch(reject);
    })

  }


  supression() {
    return {
      get: (email) => {
        return new Promise((resolve, reject) => {
          sparky.suppressionList.get(email)
            .then(data => {
              resolve(data);
            })
            .catch(err => {
              reject(err);
            });
        });
      },
      remove: (email) => {
        return new Promise((resolve, reject) => {
          sparky.suppressionList.delete(email)
            .then(data => {
              console.log('email ' + email + ' removed from supression list');
              resolve(data);
            })
            .catch(err => {
              console.log('email ' + email + ' not exists in supression list');
              resolve(err);
            });
        });
      }
    }
  }
}

function EMAIL_TEMPLATE() {
  return `
  <html xmlns="http://www.w3.org/1999/xhtml"
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <meta name="viewport" content="width=device-width">
        <style type="text/css" emb-not-inline="">
body {
  margin: 0;
  padding: 0;
}
table {
  border-collapse: collapse;
  table-layout: fixed;
}
* {
  line-height: inherit;
}
[x-apple-data-detectors],
[href^="tel"],
[href^="sms"] {
  color: inherit !important;
  text-decoration: none !important;
}
html {
  margin: 0;
  padding: 0;
}
body {
  margin: 0;
  padding: 0;
  -webkit-text-size-adjust: 100%;
}
table {
  border-collapse: collapse;
  table-layout: fixed;

}

tr td:nth-child(odd) {
    background: #fff;
    border-radius: 10px;
}


</style>
  <body>
  <div style="  max-width: 550px;  margin: 0 auto;     display: block;">
  <img class="logo" style="max-width: 180px; display: block;" src="${environment.sparkpost.logoUrl}" />
  <br />
  <%= body %>
  </div>
</body>
</html>
  `;
}
import * as jwt from 'jsonwebtoken';
import { environment } from '../../../environment';

export class Token {
    /**
     * @static
     * @param {*} admin
     * @returns
     * @memberof Token
     */
    public static get(admin) {
        return (req, res) => {
            if (!req.params.uid) return res.status(404).send({});
            admin.firestore().collection('users').doc(req.params.uid).get().then(snapshot => {
                const user = snapshot.data();
                if (user) {
                    res.status(201).send({
                        token: jwt.sign({
                            uid: user.uid,
                            display_name: user.display_name,
                            email: user.email,
                            roles: user.roles,
                            segment: user.segment,
                            manager: user.manager,
                            owner: user.owner
                        }, environment.jwtSecret)
                    });
                } else {
                    res.status(404).send({});
                }
            }).catch(err => res.status(500).send(err));
        }
    }
}
import * as routeVersion from 'express-routes-versioning';
import * as jwt from 'express-jwt';
import * as express from 'express';
import * as cors from 'cors';

import { environment } from '../../../environment';
import { Version } from './version';

import * as bodyParser from 'body-parser';

/**
 * Route versioning
 *
 * @export
 * @class Route
 */
export class Route {

    /**
     *
     *
     * @static
     * @param {boolean} secured
     * @returns
     * @memberof Route
     */
    public static tokenVerify(secured: boolean) {
        if (!secured) {
            return (req, res, next) => {
                next();
            }
        } else {
            return jwt({ secret: environment.jwtSecret });
        }
    }

    /**
     *
     *
     * @static
     * @param {*} api
     * @param {string} [method='get']
     * @param {string} [endpoint='']
     * @param {string[]} [version=['~1.0.0']]
     * @param {any[]} [executeFn=[(req, res) => { res.send() }]]
     * @param {*} [notFoundFn=this.versionErrorHandler]
     * @param {boolean} [secured=true]
     * @memberof Route
     */
    public static addVersion(api, method: string = 'get', endpoint: string = '', version: string[] = ['~1.0.0'], executeFn: any[] = [(req, res) => { res.send() }], notFoundFn: any = this.versionErrorHandler, secured: boolean = true) {
        const options = {};
        version.forEach((v, i) => {
            options[v] = executeFn[i];
        })
        api[method](endpoint, this.tokenVerify(secured), routeVersion()(options, notFoundFn));
    }

    /**
     *
     *
     * @static
     * @param {*} req
     * @param {*} res
     * @returns
     * @memberof Route
     */
    public static versionErrorHandler(req, res) {
        return res.status(406).send({ code: 'version', message: `Request not acceptable` });
    }

    /**
     *
     *
     * @static
     * @param {*} err
     * @param {*} req
     * @param {*} res
     * @param {*} next
     * @memberof Route
     */
    public static authErrorHandler(err, req, res, next) {
        if (err.name === 'UnauthorizedError') {
            res.status(401).send({ code: 'auth/token', message: `Unauthorized` });
        }
    }


    /**
     * Debug an endpoint
     *
     * @static
     * @param {*} res
     * @memberof Route
     */
    public static test(res) {
        res.send(`${environment.appName} API ${Version.get(environment.version)} loaded. ${environment.testing ? 'Testing Mode ON' : environment.stating ? 'Staging Mode ON' : ''}`);
    }


    /**
     * 
     *
     * @static
     * @param {*} api
     * @returns
     * @memberof Route
     */
    public static onRequest(api) {
        return (req, res) => {
            // https://some-firebase-app-id.cloudfunctions.net/app
            // without trailing "/" will have req.path = null, req.url = null
            // which won't match to your app.get('/', ...) route 

            if (!req.path) {

                // prepending "/" keeps query params, path params intact
                req.url = `/${req.originalUrl}`

            }
            return api(req, res);
        }
    }


    /**
     * Register a new API endpoint
     *
     * @static
     * @returns api
     * @memberof Route
     */
    public static register() {
        const api = express();

        //
        // Configure API
        api.use(cors());
        api.options('*', cors());

        //
        // Express setup
        api.use(cors());
        api.use(bodyParser.json()); // for parsing application/json
        api.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

        return api;
    }

}
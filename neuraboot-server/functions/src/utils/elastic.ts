import * as _ from 'lodash';
import * as request from 'request-promise';
import { environment } from "../../../environment";


export class Elastic {


    /**
     * Index data on ElasticSearch from firestore triggers
     *
     * @static
     * @param {string} [endpoint='/users/user']
     * @param {{ identifier?: string, omit?: string[] }} [extra={ identifier: 'id', omit: [] }]
     * @returns {(change, context) => any}
     * @memberof Elastic
     */
    public static index(endpoint: string = '/users/user', extra: { identifier?: string, omit?: string[] } = { identifier: 'id', omit: [] }): (change, context) => any {
        return (change, context) => {
            return new Promise((resolve, reject) => {
                const beforeData = change.before.data(); // data before the write
                const afterData = change.after.data(); // data after the write

                const data = afterData ? afterData : beforeData;

                console.log(`indexing ${endpoint}`, data[extra.identifier]);

                const elasticSearchConfig = environment.elastic;
                const elasticSearchUrl = elasticSearchConfig.url + endpoint + '/' + data[extra.identifier];
                const elasticSearchMethod = afterData ? 'POST' : 'DELETE';

                const elasticsearchRequest = {
                    method: elasticSearchMethod,
                    uri: elasticSearchUrl,
                    auth: {
                        username: elasticSearchConfig.app_username,
                        password: elasticSearchConfig.app_password,
                    },
                    body: _.omit(data, extra.omit),
                    json: true
                };

                return request(elasticsearchRequest).then(response => {
                    console.log('elasticsearch response', response);
                    resolve();
                }).catch(err => {
                    console.log(err);
                    reject(err);
                });
            });
        }

    }


    /**
     *
     *
     * @static
     * @param {string} [endpoint='/users']
     * @returns
     * @memberof Elastic
     */
    public static search(endpoint: string = '/users') {
        return (req, res) => {
            let elasticSearchUrl = environment.elastic.url + endpoint + '/_search';
            let query = '?';
            for (const k in req.query) {
                query += k + '=' + req.query[k] + '&';
            }
            if (query !== '?') elasticSearchUrl += query;

            const elasticSearchMethod = 'POST';

            const elasticsearchRequest = {
                method: elasticSearchMethod,
                uri: elasticSearchUrl,
                auth: {
                    username: environment.elastic.app_username,
                    password: environment.elastic.app_password,
                },
                body: req.body,
                json: true
            }

            request(elasticsearchRequest).then(response => {
                res.json(response);
            }).catch(err => {
                res.status(404).json(err.error);
            });
        }
    }

    public static count(endpoint: string = '/users') {
        return (req, res) => {
            let elasticSearchUrl = environment.elastic.url + endpoint + '/_count';
            let query = '?';
            for (const k in req.query) {
                query += k + '=' + req.query[k] + '&';
            }
            if (query !== '?') elasticSearchUrl += query;

            const elasticSearchMethod = 'POST';

            const elasticsearchRequest = {
                method: elasticSearchMethod,
                uri: elasticSearchUrl,
                auth: {
                    username: environment.elastic.app_username,
                    password: environment.elastic.app_password,
                },
                body: req.body,
                json: true
            }

            request(elasticsearchRequest).then(response => {
                res.json(response);
            }).catch(err => {
                res.status(404).json(err.error);
            });
        }
    }
}
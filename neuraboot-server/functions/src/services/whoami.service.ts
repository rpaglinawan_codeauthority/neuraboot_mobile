//
// primary deps
// import * as _ from 'lodash';
import { ReactiveRecord } from '@ionfire/reactive-record';
//
// core imports
// import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';
// import { formatError } from '../../../../../../neuraboot-core/constants/error';
// import { ApiRequest } from '../../../../../../neuraboot-core/interfaces/api-request.interface';
// import { ApiUserAuthResponse } from '../../../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
// import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
// import { ApiJwt } from '../../../../../../neuraboot-core/interfaces/api-jwt.interface';

//
// other stuff
// import { environment } from '../../../environment';

/**
 * 
 * 
 * @export
 * @class WhoamiService
 * @implements {ApiRequest}
 */
export class WhoamiService extends ReactiveRecord {
    admin: any;         // firebase admin


    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof UserLogin
     */
    constructor(admin: any) {
        super({
            collection: 'whoami-history',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });
        //
        // set firebase admin to be used along the class
        this.admin = admin;
    }
}
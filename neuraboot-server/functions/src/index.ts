//
// deps
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

//
// utils
import { Route } from './utils/route';
import { Elastic } from './utils/elastic';
import { Token } from './utils/token';
//
// v1 resources
import { UserAuth } from './api/v1/user/auth';
import { UserGeo } from './api/v1/user/geo';
import { WhoamiSupporters } from './api/v1/whoami/supporters';
import { ChallengePublicRoute } from './api/v1/challenge/challenge-public.route';
import { Notification } from './api/v1/notification/notification';

//
// Initialize firebase stuff
admin.initializeApp();
admin.firestore().settings({ timestampsInSnapshots: true });

/*--------  Primary API routing  --------*/

//
// Primary api routes
const api = Route.register();

//
// Debug route
api.get('/', (req, res) => Route.test(res));

//
// Get user token @deprecated
api.post('/token/:uid', Token.get(admin));

/*--------  Notification routing  --------*/

//
// Register User api
const api_notification = Route.register();

//
// debug route
api_notification.get('/', (req, res) => Route.test(res));

//
// routes
api_notification.get('/topic/daily-action-step', new Notification(admin).hit('dailyActionStepTopic'));

/*--------  User routing  --------*/

//
// Register User api
const api_user = Route.register();

//
// debug route
api_user.get('/', (req, res) => Route.test(res));

//
// routes
api_user.post('/auth/email', new UserAuth(admin).hit('email'));
api_user.post('/auth/create', new UserAuth(admin).hit('create'));
api_user.post('/auth/facebook', new UserAuth(admin).hit('facebook'));
api_user.patch('/geo/watch', new UserGeo(admin).hit('watch')); // @todo protect route
Route.addVersion(api_user, 'post', '/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/users')]);
Route.addVersion(api_user, 'post', '/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/users')]);

/*--------  Who am i routing  --------*/

//
// Register Who am I
const api_whoami = Route.register();

//
// debug route
api_whoami.get('/', (req, res) => Route.test(res));

//
// routes
Route.addVersion(api_whoami, 'post', '/history/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/whoami-history')]);

/*--------  Feeling routing  --------*/

//
// Register Feeling api
const api_feeling = Route.register();

//
// debug route
api_feeling.get('/', (req, res) => Route.test(res));

//
// routes
Route.addVersion(api_feeling, 'post', '/feeling-history/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/feeling-history')]);
Route.addVersion(api_feeling, 'post', '/feeling-history/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/feeling-history')]);

Route.addVersion(api_feeling, 'post', '/feeling-specific/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/feeling_specific')]);
Route.addVersion(api_feeling, 'post', '/feeling-specific/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/feeling_specific')]);

Route.addVersion(api_feeling, 'post', '/history/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/feeling-history')]);
Route.addVersion(api_feeling, 'post', '/history/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/feeling-history')]);

Route.addVersion(api_feeling, 'post', '/specific/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/feeling_specific')]);
Route.addVersion(api_feeling, 'post', '/specific/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/feeling_specific')]);

/*--------  Challenge routing  --------*/

//
// Register Challenge api
const api_challenge = Route.register();

//
// debug route
api_challenge.get('/', (req, res) => Route.test(res));
Route.addVersion(api_challenge, 'post', '/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/challenges')]);
Route.addVersion(api_challenge, 'post', '/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/challenges')]);
Route.addVersion(api_challenge, 'post', '/challenge-history/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/challenge-history')]);
Route.addVersion(api_challenge, 'post', '/challenge-history/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/challenge-history')]);
Route.addVersion(api_challenge, 'post', '/history/complete', ['~0.0.9', '~1.0.0'], [new ChallengePublicRoute(admin).hit('complete')]);
Route.addVersion(api_challenge, 'post', '/history/cancel', ['~0.0.9', '~1.0.0'], [new ChallengePublicRoute(admin).hit('cancel')]);

/*--------  Action step routing  --------*/

//
// Register Action step api
const api_action_step = Route.register();

//
// debug route
api_action_step.get('/', (req, res) => Route.test(res));
Route.addVersion(api_action_step, 'post', '/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/challenges')]);
Route.addVersion(api_action_step, 'post', '/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/challenges')]);
Route.addVersion(api_action_step, 'post', '/history/find', ['~0.0.9', '~1.0.0'], [Elastic.search('/challenge-history')]);
Route.addVersion(api_action_step, 'post', '/history/count', ['~0.0.9', '~1.0.0'], [Elastic.count('/challenge-history')]);
Route.addVersion(api_action_step, 'post', '/history/complete', ['~0.0.9', '~1.0.0'], [new ChallengePublicRoute(admin).hit('complete')]);
Route.addVersion(api_action_step, 'post', '/history/cancel', ['~0.0.9', '~1.0.0'], [new ChallengePublicRoute(admin).hit('cancel')]);

/*--------  Error handler  --------*/

//
// Error handler (jwt)
api.use(Route.authErrorHandler);
api_notification.use(Route.authErrorHandler);
api_user.use(Route.authErrorHandler);
api_whoami.use(Route.authErrorHandler);
api_feeling.use(Route.authErrorHandler);
api_challenge.use(Route.authErrorHandler);
api_action_step.use(Route.authErrorHandler);

/*--------  Export API routing  --------*/

//
// Export Api's to Firebase Functions
exports.api = functions.https.onRequest(Route.onRequest(api));
exports.api_notification = functions.https.onRequest(Route.onRequest(api_notification));
exports.api_user = functions.https.onRequest(Route.onRequest(api_user));
exports.api_whoami = functions.https.onRequest(Route.onRequest(api_whoami));
exports.api_feeling = functions.https.onRequest(Route.onRequest(api_feeling));
exports.api_challenge = functions.https.onRequest(Route.onRequest(api_challenge));
exports.api_action_step = functions.https.onRequest(Route.onRequest(api_action_step));

/*--------  Elastic triggers --------*/

//
// Indexing data in Elastic Search
exports.index_users = functions.firestore
    .document('users/{id}')
    .onWrite(Elastic.index('/users/user', { identifier: 'uid', omit: ['token'] }));
exports.index_challenges = functions.firestore.document('challenges/{id}').onWrite(Elastic.index('/challenges/challenge'));
exports.index_whoami_history = functions.firestore.document('whoami-history/{id}').onWrite(Elastic.index('/whoami-history/history'));
exports.index_feeling_history = functions.firestore.document('feeling-history/{id}').onWrite(Elastic.index('/feeling-history/history'));
exports.index_feeling_specific = functions.firestore.document('feeling-specific/{id}').onWrite(Elastic.index('/feeling_specific/feeling'));
exports.index_challenges = functions.firestore.document('challenges/{id}').onWrite(Elastic.index('/challenges/challenge'));
exports.index_challenge_history = functions.firestore
    .document('challenge-history/{id}')
    .onWrite(Elastic.index('/challenge-history/history'));

/*--------  Firestore triggers  --------*/

//
// Firestore Triggers
exports.trigger_notify_supporters = functions.firestore
    .document('feeling-history/{historyID}')
    .onCreate(new WhoamiSupporters(admin).hit('feeling'));

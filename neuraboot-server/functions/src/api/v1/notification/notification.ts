//
// primary deps
const FirebaseAuth = require('firebaseauth');
import * as _ from 'lodash';
import * as moment from 'moment';
import * as jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import { ReactiveRecord, RRResponse } from '@ionfire/reactive-record';

//
// core imports
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';
import { formatError } from '../../../../../../neuraboot-core/constants/error';
import { ApiRequest } from '../../../../../../neuraboot-core/interfaces/api-request.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';

//
// other stuff
import { environment } from '../../../../../environment';

import * as fcm from 'fcm-notification';
var FCM = new fcm('./service-account.json');

/**
 * Handles notification
 *
 * @export
 * @class Notification
 * @implements {ApiRequest}
 */
export class Notification extends ReactiveRecord implements ApiRequest {
    req: Request; // express request
    res: Response; // express response
    admin: any; // firebase admin
    firebase: any; // firebase auth

    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof UserLogin
     */
    constructor(admin: any) {
        super({
            collection: 'users',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });
        //
        // set the firebase auth & admin instances to be used along the class
        this.admin = admin;
        this.firebase = new FirebaseAuth(environment.firebase.webApiKey);
    }

    /**
     * Start point of any request
     * see `index.ts` to see how does it is implemented
     *
     * @param {Request} req
     * @param {Response} res
     * @memberof UserAuth
     */
    public hit(method: string) {
        return (req: Request, res: Response) => {
            this.req = req;
            this.res = res;
            //
            // call what is needed to be done
            this[method]();
        };
    }

    /**
     * Daily action step topic
     *
     *
     * @memberof Notification
     */
    public dailyActionStepTopic() {
        //
        // Topic
        const topic: string = 'dailyActionStepTopic';

        //
        // Title and message
        const title: string = 'How do you feel today?';
        const message: string = 'We’re at your service. Log in to find an action step perfect for your mood right now.';

        var data = {
            notification: {
                title: title,
                body: message
            },
            topic: topic
        };

        return new Promise((resolve, reject) => {
            //
            // Send message
            FCM.send(data, (err, response) => {
                if (err) {
                    console.log('error found', err);
                    this.res.status(500).send(this.exception(err));
                    return reject(err);
                } else {
                    console.log('response here', response);
                    this.res.status(200).send(response);
                    return resolve(response);
                }
            });
        });
    }

    protected exception(err) {
        console.log(err);
        const response: ApiErrorResponse = formatError(err);
        return response;
    }
}

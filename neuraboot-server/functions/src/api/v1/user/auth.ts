//
// primary deps
const FirebaseAuth = require('firebaseauth');
import * as _ from 'lodash';
import * as moment from 'moment';
import * as jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import { ReactiveRecord, RRResponse } from '@ionfire/reactive-record';
import { map, catchError, takeUntil, mergeMap, tap, filter, switchMap, take } from 'rxjs/operators';
import { of, Subject, Observable, forkJoin, concat } from 'rxjs';

//
// core imports
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';
import { formatError } from '../../../../../../neuraboot-core/constants/error';
import { ApiRequest } from '../../../../../../neuraboot-core/interfaces/api-request.interface';
import { ApiUserAuthResponse } from '../../../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { ApiJwt } from '../../../../../../neuraboot-core/interfaces/api-jwt.interface';

//
// other stuff
import { environment } from '../../../../../environment';

/**
 * Handles user login through email and social providers
 *
 * @export
 * @class UserLogin
 * @implements {ApiRequest}
 */
export class UserAuth extends ReactiveRecord implements ApiRequest {
    req: Request; // express request
    res: Response; // express response
    admin: any; // firebase admin
    firebase: any; // firebase auth

    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof UserLogin
     */
    constructor(admin: any) {
        super({
            collection: 'users',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });
        //
        // set the firebase auth & admin instances to be used along the class
        this.admin = admin;
        this.firebase = new FirebaseAuth(environment.firebase.webApiKey);
    }

    /**
     * Start point of any request
     * see `index.ts` to see how does it is implemented
     *
     * @param {Request} req
     * @param {Response} res
     * @memberof UserAuth
     */
    public hit(method: string) {
        return (req: Request, res: Response) => {
            this.req = req;
            this.res = res;
            //
            // call what is needed to be done
            this[method]();
        };
    }

    /**
     * Sign in an user by email
     *
     * it should return the user object plus token object
     * the token object will contain the firebase auth token
     * used to authenticating user in by client side
     * and the app client token used to validate/protect some routes
     *
     * @param {string} [email=this.req.body.email]
     * @param {string} [password=this.req.body.password]
     * @memberof UserLogin
     */
    public email(email: string = this.req.body.email, password: string = this.req.body.password) {
        //
        // primary exceptions
        if (!email) return this.res.status(400).send(formatError('email required'));
        if (!password) return this.res.status(400).send(formatError('password required'));

        //
        // stream error handler
        const error$ = new Subject();

        //
        // start stream
        return this.findOne({
            query: [
                //
                // email must match
                {
                    field: 'email',
                    operator: '==',
                    value: email
                }
                //
                // @todo account must be active
                // {
                //     field: 'active',
                //     operator: '==',
                //     value: true
                // }
            ]
        })
            .pipe(
                //
                // transform response
                map((response: RRResponse) => response.data),
                //
                // stop stream if an error happens
                takeUntil(error$),
                //
                // verify user existence
                map((userExists: User) => {
                    //
                    // exception
                    if (!userExists) {
                        const msg: string = 'User not found';
                        this.res.status(404).send(formatError(msg));
                        return error$.next();
                    }
                    //
                    // keep in the flow
                    return userExists;
                }),
                //
                // update user in database (defaults to online_at)
                switchMap((user: User) => {
                    return this.updateUserDetail$(user.uid).pipe(map(() => user));
                }),
                //
                // firebase auth
                mergeMap((userVerified: User) => {
                    //
                    // auth in firebase
                    return forkJoin(
                        this.signInEmailPassword$(email, password).pipe(
                            map(() => {
                                //
                                // return user to next block
                                return userVerified;
                            })
                        )
                    );
                }),
                //
                // generate firebase token
                switchMap((result: User[]) => this.generateFirebaseToken$(result[0])),
                //
                // generate client token
                switchMap((r: ApiUserAuthResponse) => this.generateClientToken$(r)),
                //
                // return api response
                tap((r: ApiUserAuthResponse) => this.success(r)),
                //
                // deal with exceptions
                catchError(err => this.exception(err))
            )
            .toPromise();
    }

    /**
     * creates a new user
     *
     * @param {string} [email=this.req.body.email]
     * @param {string} [password=this.req.body.password]
     * @param {string} [displayName=this.req.body.display_name]
     * @returns
     * @memberof UserAuth
     */
    public create(
        email: string = this.req.body.email,
        password: string = this.req.body.password,
        display_name: string = this.req.body.display_name
    ) {
        //
        // primary exceptions
        if (!email) return this.res.status(400).send(formatError('email required'));
        if (!password) return this.res.status(400).send(formatError('password required'));
        if (!display_name) return this.res.status(400).send(formatError('display_name required'));

        //
        // stream error handler
        const error$ = new Subject();
        console.log('email', email);
        //
        // start stream
        return this.findOne({
            query: [
                //
                // email must match
                {
                    field: 'email',
                    operator: '==',
                    value: email
                }
            ]
        })
            .pipe(
                //
                // transform response
                map((response: RRResponse) => response.data),
                //
                // stop stream if an error happens
                takeUntil(error$),
                //
                // verify user existence
                map((userExists: User) => {
                    console.log('userExists', userExists);
                    //
                    // exception
                    if (userExists) {
                        const msg: string = `User already exists, try to create a new password in the 'Forgot password' section.`;
                        this.res.status(400).send(formatError(msg, 'auth/user-exists'));
                        return error$.next();
                    }
                    //
                    // keep in the flow
                    return;
                }),
                //
                // create firebase user
                mergeMap(() => {
                    //
                    // auth in firebase
                    return forkJoin(
                        this.createEmailPassword$(email, password, display_name).pipe(
                            //
                            // stop stream if an error happens
                            takeUntil(error$),
                            //
                            // grab user ID
                            map((result: any) => {
                                const uid = _.get(result, 'user.id');
                                return uid ? uid : error$.error('Fail to generate UID');
                            })
                        )
                    );
                }),
                //
                // save user to database
                mergeMap((result: string[]) => {
                    const uid: string = result[0];
                    const user: User = {
                        display_name: display_name,
                        email: email,
                        uid: uid,
                        online_at: moment().toISOString(),
                        created_at: moment().toISOString(),
                        roles: ['user']
                    };
                    return forkJoin(
                        this.setUserDetail$(uid, user).pipe(
                            map(() => {
                                return user;
                            })
                        )
                    );
                }),
                //
                // generate firebase token
                switchMap((result: User[]) => this.generateFirebaseToken$(result[0])),
                //
                // generate client token
                switchMap((r: ApiUserAuthResponse) => this.generateClientToken$(r)),
                //
                // return api response
                tap((r: ApiUserAuthResponse) => this.success(r)),
                //
                // deal with exceptions
                catchError(err => this.exception(err))
            )
            .toPromise();
    }

    /**
     * auth with facebook
     *
     * @memberof UserAuth
     */
    public facebook() {
        const userRequest: User = this.req.body.user;
        //
        // primary exceptions
        if (_.isEmpty(userRequest)) this.res.status(400).send(formatError('user required'));
        if (!userRequest.uid) this.res.status(400).send(formatError('uid required'));
        if (!userRequest.display_name) this.res.status(400).send(formatError('display_name required'));
        if (!userRequest.email) this.res.status(400).send(formatError('email required'));
        if (!userRequest.facebook_uid) this.res.status(400).send(formatError('fb uid required'));

        this.findOne({
            query: [
                //
                // uid must match
                {
                    field: 'uid',
                    operator: '==',
                    value: userRequest.uid
                }
            ]
        })
            .pipe(
                //
                // transform response
                map((response: RRResponse) => response.data)
            )
            .toPromise()
            .then((user: User) => {
                //
                // create stream
                const create$ = of(user).pipe(
                    //
                    // proceed only if user doesn't exists already
                    filter((user: User) => _.isEmpty(user)),
                    //
                    // create user in database
                    switchMap(() => this.createUser$(userRequest)),
                    //
                    // generate firebase token
                    switchMap((user: User) => this.generateFirebaseToken$(user)),
                    //
                    // generate client token
                    switchMap((r: ApiUserAuthResponse) => this.generateClientToken$(r)),
                    //
                    // return api response
                    switchMap((r: ApiUserAuthResponse) => {
                        r.user.completed_password = true; // runtime variable for client, doesnt need to be persisted
                        return of(r);
                    })
                );

                //
                // update stream
                const update$ = of(user).pipe(
                    //
                    // proceed only if user exists already
                    filter((user: User) => !_.isEmpty(user)),
                    //
                    // update user in database (defaults to online_at)
                    switchMap((user: User) => {
                        _.merge(user, <User>{
                            facebook_connected: true,
                            facebook_uid: userRequest.facebook_uid,
                            photo_url: userRequest.photo_url,
                            online_at: moment().toISOString()
                        });
                        return this.updateUserDetail$(user.uid, user).pipe(map(() => user));
                    }),
                    //
                    // generate firebase token
                    switchMap((result: User) => this.generateFirebaseToken$(result)),
                    //
                    // generate client token
                    switchMap((r: ApiUserAuthResponse) => this.generateClientToken$(r)),
                    //
                    // deal with exceptions
                    catchError(err => this.exception(err))
                );

                concat(create$, update$)
                    .pipe(
                        take(1),
                        //
                        // return api response
                        tap((r: ApiUserAuthResponse) => this.success(r)),
                        //
                        // deal with exceptions
                        catchError(err => this.exception(err))
                    )
                    .toPromise()
                    .catch(console.log);
            });
    }

    // //
    // // @todo
    // private google() {

    // }

    // //
    // // @todo
    // private twitter() {

    // }

    /**
     * firebase auth by email & password
     *
     * @param {string} email
     * @param {string} password
     * @returns
     * @memberof UserLogin
     */
    protected signInEmailPassword$(email: string, password: string) {
        return new Observable(observer => {
            this.firebase.signInWithEmail(email, password, (err, result) => {
                if (err) observer.error(err);
                observer.next(result);
                observer.complete();
            });
        });
    }

    /**
     * create firebase user
     *
     * @param {string} email
     * @param {string} password
     * @param {string} displayName
     * @returns
     * @memberof UserAuth
     */
    protected createEmailPassword$(email: string, password: string, displayName: string) {
        return new Observable(observer => {
            this.firebase.registerWithEmail(email, password, { name: displayName }, (err, result) => {
                if (err) observer.error(err);
                observer.next(result);
                observer.complete();
            });
        });
    }

    /**
     * create firebase token
     *
     * @param {string} uid
     * @param {*} [claims={}]
     * @returns
     * @memberof UserLogin
     */
    protected createFirebaseToken$(uid: string, claims: any = {}) {
        return new Observable(observer => {
            this.admin
                .auth()
                .createCustomToken(uid, claims)
                .then(customToken => {
                    // Send token back to client
                    observer.next(customToken);
                    observer.complete();
                })
                .catch(err => {
                    observer.error(err);
                    observer.complete();
                    console.log('Error creating custom token:', err);
                });
        });
    }

    /**
     * create client token
     *
     * @param {User} user
     * @returns {Observable<string>}
     * @memberof UserAuth
     */
    protected createClientToken$(user: User): Observable<string> {
        return new Observable(observer => {
            observer.next(
                jwt.sign(
                    <ApiJwt>{
                        uid: user.uid,
                        display_name: user.display_name,
                        email: user.email,
                        roles: user.roles
                    },
                    environment.jwtSecret
                )
            );
            observer.complete();
        });
    }

    /**
     * update user details like `online_at`
     *
     * @param {string} uid
     * @param {User} [details={ online_at: moment().toISOString() }]
     * @returns
     * @memberof UserAuth
     */
    protected updateUserDetail$(uid: string, details: User = { online_at: moment().toISOString() }) {
        return this.update(uid, details);
    }

    /**
     * set user details
     *
     * @protected
     * @param {string} uid
     * @param {User} [details={ online_at: moment().toISOString() }]
     * @returns {Observable<User>}
     * @memberof UserAuth
     */
    protected setUserDetail$(uid: string, details: User = { online_at: moment().toISOString() }): Observable<User> {
        return new Observable(observer => {
            this.set(uid, _.pickBy(details, _.identity))
                .toPromise()
                .then(() => {
                    observer.next(details);
                    observer.complete();
                })
                .catch(err => {
                    observer.error(err);
                    observer.complete();
                });
        });
    }

    protected createUser$(userRequest: User) {
        _.merge(userRequest, {
            online_at: moment().toISOString(),
            created_at: moment().toISOString(),
            roles: ['user']
        });
        return this.setUserDetail$(userRequest.uid, userRequest);
    }

    protected generateFirebaseToken$(user: User) {
        //
        // create firebase custom token with roles
        return this.createFirebaseToken$(user.uid, { roles: user.roles }).pipe(
            //
            // generate token
            map((authToken: string) => {
                //
                // format success response
                const response: ApiUserAuthResponse = {
                    user: user,
                    token: {
                        client: null, // should be generated on next block
                        auth: authToken
                    }
                };
                //
                // pass response to next block
                return response;
            })
        );
    }

    protected generateClientToken$(response: ApiUserAuthResponse) {
        return this.createClientToken$(response.user).pipe(
            //
            // pass response to next block
            map((clientToken: string) => {
                //
                // set the client token
                response.token.client = clientToken;
                return response;
            })
        );
    }

    protected success(response: ApiUserAuthResponse) {
        this.res.status(200).send(response); // api response
        return of(response); // just to finish the promise
    }

    protected exception(err) {
        console.log(err);
        const response: ApiErrorResponse = formatError(err);
        this.res.status(500).send(response);
        return of(response);
    }
}

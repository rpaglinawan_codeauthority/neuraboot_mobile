//
// primary deps
const FirebaseAuth = require('firebaseauth');
import * as _ from 'lodash';
import * as request from "request-promise";

import { Request, Response } from 'express';
import { ReactiveRecord, RRResponse } from '@ionfire/reactive-record';
import { map, catchError, takeUntil, mergeMap, tap } from 'rxjs/operators';
import { of, Subject, Observable, forkJoin } from 'rxjs';
import { fromPromise } from 'rxjs/internal/observable/fromPromise';
//
// core imports
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';
import { Geopoint } from '../../../../../../neuraboot-core/interfaces/geopoint.interface';
import { formatError } from '../../../../../../neuraboot-core/constants/error';
import { ApiRequest } from '../../../../../../neuraboot-core/interfaces/api-request.interface';
import { ApiUserAuthResponse } from '../../../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';

//
// other stuff
import { environment } from '../../../../../environment';
import { Geolocation } from '../../../../../../neuraboot-core/interfaces/geolocation.interface';


export class UserGeo extends ReactiveRecord implements ApiRequest {
    req: Request;           // express request
    res: Response;          // express response
    admin: any;             // firebase admin
    firebase: any;          // firebase auth

    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof UserLogin
     */
    constructor(admin: any) {
        super({
            collection: 'users',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });
        //
        // set the firebase auth & admin instances to be used along the class
        this.admin = admin;
        this.firebase = new FirebaseAuth(environment.firebase.webApiKey);
    }

    /**
     * Start point of any request
     * see `index.ts` to see how does it is implemented
     * 
     * @param {Request} req
     * @param {Response} res
     * @memberof UserAuth
     */
    hit(method: string) {
        return (req: Request, res: Response) => {
            this.req = req;
            this.res = res;
            //
            // call what is needed to be done
            this[method]();
        }
    }




    /**
     * 
     * Watch user geoposition to persist geolocation
     * 
     * @param {string} [uid=this.req.body.uid]
     * @param {Geopoint} [geopoint=this.req.body.geopoint = {}]
     * @returns
     * @memberof UserGeo
     */
    public watch(uid: string = this.req.body.uid, geopoint: Geopoint = this.req.body.geopoint) {
        //
        // primary exceptions
        if (!uid) return this.res.status(400).send(formatError('user required'));
        if (!geopoint || !geopoint.lat || !geopoint.lon) return this.res.status(400).send(formatError('geopoint required'));

        //
        // stream error handler
        const error$ = new Subject();

        //
        // start stream
        return this.location$(geopoint).pipe(
            //
            // stop stream if an error happens
            takeUntil(error$),
            //
            // verify geo existence
            map((geolocation: Geolocation) => {
                //
                // exception
                if (_.isEmpty(geolocation)) {
                    const msg: string = 'geolocation empty';
                    this.res.status(201).send(formatError(msg));

                    return error$.next(msg);
                }
                //
                // keep in the flow
                return geolocation;
            }),
            //
            // verify user existence
            mergeMap((geolocation: Geolocation) => {
                return forkJoin(this.findOne({
                    query: [
                        //
                        // uid must match
                        {
                            field: 'uid',
                            operator: '==',
                            value: uid
                        },
                        //
                        // @todo account must be active
                        // {
                        //     field: 'active',
                        //     operator: '==',
                        //     value: true
                        // }
                    ]
                }).pipe(
                    //
                    // transform response
                    map((response: RRResponse) => response.data),
                    map((user: User) => {
                        if (!_.isEmpty(user)) {
                            //
                            // keep in the flow
                            return [user, geolocation];
                        } else {
                            const msg: string = `user doesn't exists or is inactive`;
                            this.res.status(400).send(formatError(msg));
                            return error$.next();
                        }
                    })
                ))
            }),
            //
            // update user object
            mergeMap((_result: any[]) => {
                const user: User = _result[0][0];
                const geolocation: Geolocation = _result[0][1];
                user.geopoint = geopoint;
                user.geolocation = _.merge(user.geolocation, geolocation);
                return forkJoin(this.update(uid, user).pipe(
                    map(() => {
                        return geolocation;
                    })
                ))
            }),
            //
            // success return
            tap((_result: any[]) => {
                const geolocation: Geolocation = _result[0];
                this.res.status(201).send(geolocation);    // api response
                console.log(`Geolocation has been updated for user ${uid}`);
                return of(geolocation);
            }),
            //
            // deal with exceptions
            catchError((err) => {
                console.log(err);
                const response: ApiErrorResponse = formatError(err);
                this.res.status(500).send(response);
                return of(response);
            })
        ).toPromise();
    }



    /**
     * Resolve a Geolocation
     *
     * @param {Geopoint} geopoint
     * @returns {Observable<Geolocation>}
     * @memberof UserGeo
     */
    public location$(geopoint: Geopoint): Observable<Geolocation> {
        return new Observable((observer) => {
            const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${geopoint.lat},${geopoint.lon}&sensor=true&key=${environment.google.mapsKey}`
            const method = 'GET';
            const options = {
                method: method,
                uri: url,
                json: true
            };
            request(options).then(response => {
                if (response.results && response.status === 'OK') {
                    let city = '';
                    const countryCode = response.results[0].address_components.find(ac => ac.types.includes('country')).short_name;
                    const country = response.results[0].address_components.find(ac => ac.types.includes('country')).long_name;
                    const state = response.results[0].address_components.find(ac => ac.types.includes('administrative_area_level_1')).long_name;

                    try {
                        city = response.results[0].address_components.find(ac => ac.types.includes('locality')).long_name;
                    } catch (err) {
                        try {
                            city = response.results[0].address_components.find(ac => ac.types.includes('administrative_area_level_2')).long_name;
                        } catch (err) { }
                    }

                    const result: Geolocation = {
                        country_code: countryCode,
                        country: country,
                        city: city,
                        state: state
                    }

                    observer.next(result);
                    observer.complete();
                } else {
                    observer.error(response.data ? response.data : response);
                    observer.complete();
                }
            }).catch(err => {
                observer.error(err.message ? err.message : err);
                observer.complete();
            });
        })
    }

}
//
// primary deps
import * as _ from 'lodash';
import * as async from 'async';
import { ReactiveRecord, RRResponse } from '@ionfire/reactive-record';
import { map, catchError, takeUntil, mergeMap } from 'rxjs/operators';
import { of, Subject, Observable, forkJoin } from 'rxjs';

//
// core imports
import { SUPPORTER_QUESTION_ID, LOVE_LANGUAGE_QUESTION_ID } from '../../../../../../neuraboot-core/constants/supporters';
import { FeelingHistory } from '../../../../../../neuraboot-core/interfaces/feeling-history.interface';
import { Whoami } from '../../../../../../neuraboot-core/interfaces/whoami.interface';
import { WhoamiQuestionItem } from '../../../../../../neuraboot-core/interfaces/whoami-question-item.interface';
import { WhoamiQuestionList } from '../../../../../../neuraboot-core/interfaces/whoami-question-list.interface';
import { WhoamiQuestion } from '../../../../../../neuraboot-core/interfaces/whoami-question.interface';

//
// other stuff
import { Email } from '../../../utils/email';
import { SMS } from '../../../utils/sms';
import { environment } from '../../../../../environment';

interface Answer {
    supporters: WhoamiQuestionItem[];
    loveLanguage?: WhoamiQuestionItem[];
}

export class WhoamiSupporters extends ReactiveRecord {
    snap: any; // firestore snap
    context: any; // firestore context
    admin: any; // firebase admin
    feelingLevel: string[] & any = ['very', 'extremely']; // level triggers

    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof WhoamiSupporters
     */
    constructor(admin: any) {
        super({
            collection: 'whoami-history',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });
        //
        // set the firebase instance to be used along the class
        this.admin = admin;
    }

    /**
     * Start point of any request
     * see `index.ts` to see how does it is implemented
     *
     * @param {string} method
     * @returns
     * @memberof WhoamiSupporters
     */
    hit(method: string) {
        return (snap, context) => {
            this.snap = snap;
            this.context = context;
            //
            // call what is needed to be done
            return this[method]();
        };
    }

    /**
     * Send email/sms to supporters according to user feeling `very` or `extremely`
     *
     * @returns
     * @memberof WhoamiSupporters
     */
    public async feeling() {
        //
        // stream error handler
        const error$ = new Subject();
        const errorPrefix: string = 'WhoamiSupporters.feeling(): ';
        //
        // history exception
        const history: FeelingHistory = this.snap.data();
        if (!history || !history.id) return console.log(errorPrefix + 'missing history object');
        //
        // level exception
        if (!this.feelingLevel.includes(history.level)) return console.log(errorPrefix + 'user is fine, nothing to do.');
        //
        // Permission to notify exception
        if (!history.notifySupporters) return console.log(errorPrefix + 'user skipped supporter notification.');
        //
        // user exception
        if (!history.user || !history.user.uid) return console.log(errorPrefix + 'missing user object');
        //
        // start stream
        return this.findOne({
            query: [
                //
                // email must match
                {
                    field: 'user.uid',
                    operator: '==',
                    value: history.user.uid
                }
                //
                // @todo account must be active
                // {
                //     field: 'active',
                //     operator: '==',
                //     value: true
                // }
            ]
        })
            .pipe(
                //
                // transform response
                map((response: RRResponse) => response.data),
                //
                // stop stream if an error happens
                takeUntil(error$),
                //
                // verify whoami existence
                map((whoami: Whoami) => {
                    //
                    // exception
                    if (!whoami.id) {
                        console.log(errorPrefix + 'invalid whoami history');
                        return error$.next();
                    }
                    //
                    // keep in the flow
                    return whoami;
                }),
                //
                // grab supporters
                map((whoami: Whoami) => {
                    //
                    // Find supporters in question
                    const supporterQuestion: WhoamiQuestion = whoami.questions.find(
                        (supporterQuestion: WhoamiQuestionList) => supporterQuestion.id === SUPPORTER_QUESTION_ID
                    );
                    if (!supporterQuestion.selected.length) {
                        console.log(errorPrefix + 'non-existent supporters');
                        return error$.next();
                    }

                    //
                    // Set supporter in answer
                    const answer: Answer = {
                        supporters: supporterQuestion.selected,
                        loveLanguage: []
                    };

                    //
                    // Find love language in question
                    const loveLanguageQuestion: WhoamiQuestion = whoami.questions.find(
                        (loveLanguageQuestion: WhoamiQuestionList) => loveLanguageQuestion.id === LOVE_LANGUAGE_QUESTION_ID
                    );

                    //
                    // Set love language in answer
                    if (loveLanguageQuestion.selected.length) {
                        answer.loveLanguage = loveLanguageQuestion.selected;
                    }
                    console.log('map whoami stage answer:', answer);
                    return answer;
                }),
                //
                // send email to supporters
                mergeMap((answer: Answer) => {
                    console.log('mergeMap sendEmail answer:', answer);
                    return forkJoin(
                        this.sendEmail(history, answer).pipe(answer => {
                            return answer; // keep supporters in the next block to be sending SMS
                        })
                    );
                }),
                //
                // send sms to supporters
                mergeMap((_answer: Answer[]) => {
                    console.log('mergeMap sendSms answer[]:', _answer);
                    const answer = _answer[0];
                    console.log('mergeMap sendSms answer:', answer);
                    return forkJoin(this.sendSms(history, answer));
                }),
                //
                // finish stream
                map(() => {
                    console.log('all done');
                    return of();
                }),
                //
                // deal with exceptions
                catchError(err => {
                    console.log(errorPrefix + err);
                    return of(errorPrefix + err);
                })
            )
            .toPromise();
    }

    /**
     * @param {FeelingHistory} history
     * @param {Answer} answer
     * @returns {Observable<WhoamiQuestionItem[]>}
     * @memberof WhoamiSupporters
     */
    private sendEmail(history: FeelingHistory, answer: Answer): Observable<Answer> {
        return new Observable(observer => {
            const subject = 'feeling alert';

            //
            // Get message
            const message = this.getFeelingMessage(history, answer);

            //
            // create recipients
            const emails = _.uniqBy(
                _.compact(
                    answer.supporters.map((s: any) =>
                        s.email
                            ? {
                                  address: {
                                      email: s.email,
                                      name: s.name
                                  }
                              }
                            : null
                    )
                ),
                item => item.address.email
            );

            //
            // check for email list existence
            if (!emails.length) {
                console.log('email list empty, escaping');
                observer.next(answer);
                return observer.complete();
            }

            //
            // create new email instance
            const email = new Email({
                reply_to: history.user.email,
                subject: subject,
                transactional: true,
                recipients: emails,
                html: message
            });

            // fake email success
            // observer.next(supporters);
            // observer.complete();

            //
            // send email to supporters
            email
                .send()
                .then((/*transmission: any*/) => {
                    // //
                    // // @todo Set the transmission data
                    // //
                    // data.email_id = transmission.results.id;
                    // data.email_accepted = transmission.results.total_accepted_recipients;
                    // data.email_rejected = transmission.results.total_rejected_recipients;

                    // //
                    // // Set the sent field
                    // //
                    // data.email_sent_at = moment().toISOString();

                    //
                    // keep in the flow
                    observer.next(answer);
                    observer.complete();
                })
                .catch(err => {
                    console.log(err);
                    observer.error('error sending email');
                    observer.complete();
                });
        });
    }

    /**
     * @param {FeelingHistory} history
     * @param {Answer} answer
     * @returns {Observable<WhoamiQuestionItem[]>}
     * @memberof WhoamiSupporters
     */
    private sendSms(history: FeelingHistory, answer: Answer): Observable<Answer> {
        return new Observable(observer => {
            //
            // Get message
            const message = this.getFeelingMessage(history, answer);

            //
            // create recipients
            const devices = _.uniqBy(
                _.compact(
                    answer.supporters.map((s: any) =>
                        s.phone
                            ? {
                                  phone: s.phone,
                                  name: s.name
                              }
                            : null
                    )
                ),
                item => item.phone
            );

            //
            // check for device list existence
            if (!devices.length) {
                console.log('device list empty, escaping');
                observer.next(answer);
                return observer.complete();
            }

            //
            // create new sms instance
            const sms = new SMS();
            const success: any = [];
            const errors: any = [];

            async.each(
                devices,
                (dev, cb) => {
                    sms.send(environment.twilio.from, dev.phone, message)
                        .toPromise()
                        .then(r => {
                            success.push(dev);
                            cb();
                        })
                        .catch(err => {
                            errors.push(err);
                            cb();
                        });
                },
                () => {
                    console.log(`sms sent successfully to ${success.length} of ${devices.length}`);
                    if (errors.length) {
                        console.log(`failed for ${errors.length}`);
                        console.log(errors);
                    }
                    observer.next();
                    observer.complete();
                }
            );
        });
    }

    /**
     * Get feeling message
     *
     * @description get feeling message for sms/email
     * @private
     * @param {FeelingHistory} history
     * @param {Answer} answer
     * @returns
     * @memberof WhoamiSupporters
     */
    private getFeelingMessage(history: FeelingHistory, answer: Answer) {
        console.log('getFeelingMessage answer:', answer);

        // Susanne is currently scoring Extremely Happy on the NeuraBoot Mood Scale.
        // She is feeling Thankful. As a reminder,
        // her love language is Acts of Service and she appreciates you being in her life!

        //
        // Set first part of the message
        let message: string = `${this.getFirstname(history.user.display_name)} is currently scoring ${history.level} ${this.getFeelingCategoryName(
            history.category
        )} on the NeuraBoot Mood Scale.`;

        //
        // Set specific feeling
        message += ` ${this.getFirstname(history.user.display_name)} is feeling ${history.specific[0].name}.`

        //
        // Set love language, if available
        if (answer.loveLanguage.length) {
            //
            // Pick a random value from selected
            const randomLoveLanguage: WhoamiQuestionItem = _.sampleSize(answer.loveLanguage)[0];
            console.log('getFeelingMessage random love language:', randomLoveLanguage);
            //
            // Set the value
            message += ` As a reminder, ${this.getFirstname(history.user.display_name)}'s love language is ${randomLoveLanguage.title}!`;
        }

        //
        // Set last part of the message
        // message += ` - Sent from ${environment.appName}`;

        return message;
    }

    /**
     * Get firstname
     *
     * @private
     * @param {string} name
     * @returns
     * @memberof WhoamiSupporters
     */
    private getFirstname(name: string) {
        //
        // Hold display_name
        let firstname: string = name;

        //
        // Validate user
        if (!firstname || !firstname.length) return firstname;

        //
        // Return only firstname
        return firstname.split(' ')[0];
    }

    /**
     * Get feeling category name
     *
     * @private
     * @param {string} name
     * @returns
     * @memberof WhoamiSupporters
     */
    private getFeelingCategoryName(name: string) {
        //
        // Hold categories
        const categories: any = {
            angry: 'angry',
            depressed: 'sad',
            anxious: 'anxious',
            happy: 'happy'
        };

        //
        // Return translated category name
        return categories[name];
    }
}

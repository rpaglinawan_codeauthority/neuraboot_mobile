//
// primary deps
const FirebaseAuth = require('firebaseauth');
import * as _ from 'lodash';
import { Request, Response } from 'express';
import { ReactiveRecord } from '@ionfire/reactive-record';

//
// core imports
import { formatError } from '../../../../../../neuraboot-core/constants/error';
import { ApiRequest } from '../../../../../../neuraboot-core/interfaces/api-request.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';

//
// other stuff
import { environment } from '../../../../../environment';
import { ChallengeHistoryService } from './challenge-history.service';
import { ChallengeHistory } from '../../../../../../neuraboot-core/interfaces/challenge-history.interface';
import { of, Subject, forkJoin } from 'rxjs';
import { tap, takeUntil, catchError } from 'rxjs/operators';
import { ChallengeLevelService } from './challenge-level.service';

/**
 * Handles challenge routes
 *
 * @export
 * @class ChallengeRoute
 * @implements {ApiRequest}
 */
export class ChallengePublicRoute extends ReactiveRecord implements ApiRequest {
    req: Request; // express request
    res: Response; // express response
    admin: any; // firebase admin
    firebase: any; // firebase auth

    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof ChallengePublicRoute
     */
    constructor(admin: any) {
        super({
            collection: 'users',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });
        //
        // set the firebase auth & admin instances to be used along the class
        this.admin = admin;
        this.firebase = new FirebaseAuth(environment.firebase.webApiKey);
    }

    /**
     * Start point of any request
     * see `index.ts` to see how does it is implemented
     *
     * @param {Request} req
     * @param {Response} res
     * @memberof ChallengePublicRoute
     */
    hit(method: string) {
        return (req: Request, res: Response) => {
            this.req = req;
            this.res = res;
            //
            // call what is needed to be done
            this[method]();
        };
    }

    /**
     * Complete
     *
     * @param {ChallengeHistory} history
     * @returns
     * @memberof ChallengePublicRoute
     */
    async complete(history: ChallengeHistory = this.req.body.history) {
        //
        // stream error handler
        const error$ = new Subject();

        //
        // Instantiate services
        const challengeHistory: ChallengeHistoryService = new ChallengeHistoryService(
            this.admin
        );
        const challengeLevel: ChallengeLevelService = new ChallengeLevelService(
            this.admin
        );

        //
        // Complete challenge history
        history.status = 'completed';

        //
        // Update challenge history
        const completeChallenge$ = challengeHistory.toggle(history);

        //
        // Calculate level
        const calculateLevel$ = challengeLevel.level(history);

        //
        // Join history and level streams
        return forkJoin(completeChallenge$, calculateLevel$)
            .pipe(
                //
                // stop stream if an error happens
                takeUntil(error$),

                //
                // Success
                tap(result => {
                    console.log('result', result);
                    this.res.status(201).send(result[1]);
                    return result;
                }),

                //
                // deal with exceptions
                catchError(err => {
                    console.log(err);
                    const response: ApiErrorResponse = formatError(err);
                    this.res.status(500).send(response);
                    return of(response);
                })
            )
            .toPromise();
    }

    /**
     * Cancel
     *
     * @param {ChallengeHistory} history
     * @returns
     * @memberof ChallengePublicRoute
     */
    async cancel(history: ChallengeHistory = this.req.body.history) {
        //
        // stream error handler
        const error$ = new Subject();

        //
        // Instantiate services
        const challengeHistory: ChallengeHistoryService = new ChallengeHistoryService(
            this.admin
        );
        const challengeLevel: ChallengeLevelService = new ChallengeLevelService(
            this.admin
        );

        //
        // Complete challenge history
        history.status = 'cancelled';

        //
        // Update challenge history
        const completeChallenge$ = challengeHistory.toggle(history);

        //
        // Calculate level
        const calculateLevel$ = challengeLevel.level(history);

        //
        // Join history and level streams
        return forkJoin(completeChallenge$, calculateLevel$)
            .pipe(
                //
                // stop stream if an error happens
                takeUntil(error$),

                //
                // Success
                tap(result => {
                    console.log('result', result);
                    this.res.status(201).send(true);
                    return result;
                }),

                //
                // deal with exceptions
                catchError(err => {
                    console.log(err);
                    const response: ApiErrorResponse = formatError(err);
                    this.res.status(500).send(response);
                    return of(response);
                })
            )
            .toPromise();
    }
}

//
// primary deps
import { ReactiveRecord } from '@ionfire/reactive-record';

//
// core imports
import { ChallengeHistory } from '../../../../../../neuraboot-core/interfaces/challenge-history.interface';

export class ChallengeHistoryService extends ReactiveRecord {

    public admin: any;                                             // firebase admin
    public levelUp: number = 10;                                   // amount to level up

    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof ChallengeHistoryService
     */
    constructor(admin: any) {
        super({
            collection: 'challenge-history',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });
        console.log('new challenge history service');
        //
        // set the firebase instance to be used along the class
        this.admin = admin;
    }

    /**
     * Toggle
     *
     * @description toggle challenge history status
     * @param {ChallengeHistory} history
     * @memberof ChallengeHistoryService
     */
    toggle(history: ChallengeHistory) {

        //
        // Toggle status
        return this.update(history.id, { status: history.status });
    }
}
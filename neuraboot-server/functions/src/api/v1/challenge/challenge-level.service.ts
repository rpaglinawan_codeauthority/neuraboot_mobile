//
// primary deps
import * as moment from 'moment';
import { snakeCase, isEmpty } from 'lodash';
import { ReactiveRecord, RRResponse } from '@ionfire/reactive-record';

//
// core imports
import { ChallengeHistory } from '../../../../../../neuraboot-core/interfaces/challenge-history.interface';
import { ChallengeLevel } from '../../../../../../neuraboot-core/interfaces/challenge-level.interface';

//
// other stuff
import { Subject, of, throwError } from 'rxjs';
import {
    map,
    takeUntil,
    catchError,
    flatMap,
    tap,
    switchMap,
    mapTo
} from 'rxjs/operators';
import { Guid } from '../../../utils/guid';

export class ChallengeLevelService extends ReactiveRecord {
    public admin: any; // firebase admin
    public levelUp: number = 10; // amount to level up
    public categories: string[] = [
        'connections',
        'fuel',
        'energy_release',
        'awareness',
        'challenge'
    ];

    /**
     * see `index.ts` to see how does it is implemented
     *
     * @param {*} admin
     * @memberof ChallengeHistoryService
     */
    constructor(admin: any) {
        super({
            collection: 'challenge-level',
            connector: {
                firebase: admin.firebase,
                firestore: admin.firestore()
            }
        });

        //
        // set the firebase instance to be used along the class
        this.admin = admin;
    }

    /**
     * Level
     *
     * @param {ChallengeHistory} history
     * @returns
     * @memberof ChallengeLevelService
     */
    public level(history) {
        console.log('level');
        //
        // stream error handler
        const error$ = new Subject();
        const errorPrefix: string = 'ChallengeLevel.level(): ';

        //
        // Challenge level stream
        return of(history).pipe(
            //
            // Stop stream if an error happens
            takeUntil(error$),

            //
            // Validations
            tap(() => {
                console.log('validate history');
                //
                // Validate history
                if (!history || !history.id)
                    return throwError(errorPrefix + 'missing history object');
                console.log('validate status');
                //
                // Validate status = completed/cancelled
                console.log('status', history.status);
                if (
                    history.status !== 'completed' &&
                    history.status !== 'cancelled'
                )
                    return throwError(
                        errorPrefix + 'status must be completed/cancelled'
                    );
                console.log('validate user');
                //
                // Validate user
                console.log('history.user', history.user);
                if (!history.user || !history.user.uid)
                    return throwError(errorPrefix + 'missing user object');
                console.log('validate challenge');
                //
                // Validate challenge
                console.log('history.challenge', history.challenge);
                if (!history.challenge || !history.challenge.id)
                    return throwError(errorPrefix + 'missing challenge object');
                console.log('validate fine');
                return true;
            }),

            //
            // Get current level
            flatMap(response =>
                this.findOne({
                    query: [
                        //
                        // email must match
                        {
                            field: 'user.uid',
                            operator: '==',
                            value: history.user.uid
                        }
                    ]
                })
            ),

            //
            // Transform response
            map((response: RRResponse) => response.data),

            //
            // Verify existence
            // create if not found
            map((challengeLevel: ChallengeLevel) => {
                //
                // No entry found, create one
                if (isEmpty(challengeLevel)) {
                    const insertData: ChallengeLevel = this.getInsertData(
                        history
                    );
                    return insertData;
                }

                //
                // Entry found
                else {
                    console.log('exists');
                    //
                    // keep in the flow
                    return challengeLevel;
                }
            }),

            //
            // Set level
            switchMap((challengeLevel: ChallengeLevel) =>
                this.set(challengeLevel.id, challengeLevel).pipe(
                    mapTo(challengeLevel)
                )
            ),

            //
            // Calculate
            // update current level
            // update current streak
            map((challengeLevel: ChallengeLevel) => {
                //
                // Calculate progress
                challengeLevel = this.calculateProgress(
                    challengeLevel,
                    history
                );
                return challengeLevel;
            }),

            //
            // Set level
            switchMap((challengeLevel: ChallengeLevel) =>
                this.set(challengeLevel.id, challengeLevel).pipe(
                    mapTo(challengeLevel)
                )
            ),

            //
            // finish stream
            map((challengeLevel: ChallengeLevel) => {
                console.log('all done');
                return challengeLevel;
            }),

            //
            // deal with exceptions
            catchError(err => {
                console.log(errorPrefix + err);
                return of(errorPrefix + err);
            })
        );
    }

    /**
     * Calculate progress
     *
     * @description Calculate current user progress with new completed challenge
     * @param {ChallengeLevel} challengeLevel
     * @param {ChallengeHistory} history
     * @returns
     * @memberof ChallengeProgress
     */
    calculateProgress(
        challengeLevel: ChallengeLevel,
        history: ChallengeHistory
    ) {
        //
        // Get current category
        // transform to snakeCase
        const category: string = snakeCase(history.challenge.category);

        //
        // Map props
        let progress: number = this.mapCategoryProperty(
            challengeLevel,
            category,
            'progress'
        );
        let attempted: number = this.mapCategoryProperty(
            challengeLevel,
            category,
            'attempted'
        );
        let level: number = this.mapCategoryProperty(
            challengeLevel,
            category,
            'level'
        );
        let total: number = this.mapCategoryProperty(
            challengeLevel,
            category,
            'total'
        );
        let streakDay: number = this.mapCategoryProperty(
            challengeLevel,
            category,
            'streak_day'
        );
        let streakMax: number = this.mapCategoryProperty(
            challengeLevel,
            category,
            'streak_max'
        );
        let categoryHistory: ChallengeHistory = this.mapCategoryProperty(
            challengeLevel,
            category,
            'category_history'
        );

        //
        // Cancelled / attempted
        if (history.status === 'cancelled') {
            attempted++;
        }

        //
        // Completed
        else {
            //
            // Increment progress and total
            progress++;
            total++;

            //
            // Streak day and max
            // inc if: no category history, first time [day/max]
            if (isEmpty(categoryHistory)) {
                streakDay = 1;
                streakMax = 1;
            }

            //
            // inc if: last category history created_at is yesterday [day]
            // inc if: last category history created_at is yesterday and streak day is greater then streak max [max]
            else if (this.isYesterday(categoryHistory.created_at)) {
                streakDay++;

                //
                // inc if day is greater then max or 1
                if (streakDay === 1 || streakDay > streakMax)
                    streakMax = streakDay;
            }

            //
            // reset if: last category history created_at is not yesterday [day]
            else if (!this.isYesterday(categoryHistory.created_at)) {
                streakDay = 1;
            }

            //
            // Treat case where streakMax is 0
            if (streakMax === 0) streakMax = 1;
        }

        //
        // Update last history
        categoryHistory = history;

        //
        // Update challenge level properties
        console.log('new progress', progress);

        challengeLevel = this.mapSetCategoryProperty(
            challengeLevel,
            category,
            'progress',
            progress
        );
        challengeLevel = this.mapSetCategoryProperty(
            challengeLevel,
            category,
            'attempted',
            attempted
        );
        challengeLevel = this.mapSetCategoryProperty(
            challengeLevel,
            category,
            'level',
            level
        );
        challengeLevel = this.mapSetCategoryProperty(
            challengeLevel,
            category,
            'total',
            total
        );
        challengeLevel = this.mapSetCategoryProperty(
            challengeLevel,
            category,
            'streak_day',
            streakDay
        );
        challengeLevel = this.mapSetCategoryProperty(
            challengeLevel,
            category,
            'streak_max',
            streakMax
        );
        challengeLevel = this.mapSetCategoryProperty(
            challengeLevel,
            category,
            'category_history',
            categoryHistory
        );

        //
        // Check default level and excludedCategories
        if (!challengeLevel.excludedCategories)
            challengeLevel.excludedCategories = [];
        if (!challengeLevel.level) challengeLevel.level = 0;

        //
        // Set excluded categories
        // Exclude all categories with progress of 10
        const excludedCategories: string[] = [];
        this.categories.forEach(category => {
            //
            // Reset current progress if progress is 10
            console.log('category', category);
            console.log(
                'challengeLevel[' + category + '_progress]',
                challengeLevel[category + '_progress']
            );
            if (challengeLevel[category + '_progress'] >= 10) {
                excludedCategories.push(category);
            }
        });

        console.log(
            'challengeLevel.excludedCategories',
            challengeLevel.excludedCategories
        );

        console.log('excludedCategories', excludedCategories);

        //
        // Case all categories are excluded
        // Set next level
        // Reset progress to 1
        if (excludedCategories.length === this.categories.length) {
            //
            // Global level up
            challengeLevel.level++;

            //
            // Set all category level up and reset progress to 1
            this.categories.forEach(category => {
                challengeLevel[category + '_level'] = challengeLevel.level;
                challengeLevel[category + '_progress'] = 0;
            });

            //
            // Reset excludedCategories
            challengeLevel.excludedCategories = [];

            //
            // Set level up
            challengeLevel.levelUp = true;
        }

        //
        // Not a new level
        else {
            //
            // Update excluded categories
            challengeLevel.excludedCategories = excludedCategories;

            //
            // Clear level up
            challengeLevel.levelUp = false;
        }

        return challengeLevel;
    }

    /**
     * Get insert data
     *
     * @description Returns empty insert data for challenge level
     * @param {ChallengeHistory} challengeHistory
     * @returns {ChallengeLevel}
     * @memberof ChallengeProgress
     */
    getInsertData(challengeHistory: ChallengeHistory) {
        //
        // Generate new id
        const newID = Guid.make(2);

        //
        // Generate insert data
        const data: ChallengeLevel = {
            id: newID,
            connections_progress: 0,
            connections_attempted: 0,
            connections_level: 0,
            connections_total: 0,
            connections_streak_day: 0,
            connections_streak_max: 0,
            fuel_progress: 0,
            fuel_attempted: 0,
            fuel_level: 0,
            fuel_total: 0,
            fuel_streak_day: 0,
            fuel_streak_max: 0,
            energy_release_progress: 0,
            energy_release_attempted: 0,
            energy_release_level: 0,
            energy_release_total: 0,
            energy_release_streak_day: 0,
            energy_release_streak_max: 0,
            awareness_progress: 0,
            awareness_attempted: 0,
            awareness_level: 0,
            awareness_total: 0,
            awareness_streak_day: 0,
            awareness_streak_max: 0,
            challenge_progress: 0,
            challenge_attempted: 0,
            challenge_level: 0,
            challenge_total: 0,
            challenge_streak_day: 0,
            challenge_streak_max: 0,
            level: 0,
            levelUp: false,
            excludedCategories: [],
            user: challengeHistory.user,
            created_at: moment().toISOString(),
            updated_at: moment().toISOString()
        };

        return data;
    }

    /**
     * Map category property
     *
     * @description Map challengeLevel properties with selected category
     * @param {ChallengeLevel} challengeLevel
     * @param {string} category
     * @param {string} field
     * @returns
     * @memberof ChallengeProgress
     */
    mapCategoryProperty(
        challengeLevel: ChallengeLevel,
        category: string,
        field: string
    ) {
        return challengeLevel[category + '_' + field];
    }

    /**
     * Map set category property
     *
     * @description Map challengeLevel properties with selected category and set new value
     * @param {ChallengeLevel} challengeLevel
     * @param {string} category
     * @param {string} field
     * @param {*} value
     * @returns
     * @memberof ChallengeProgress
     */
    mapSetCategoryProperty(
        challengeLevel: ChallengeLevel,
        category: string,
        field: string,
        value
    ) {
        challengeLevel[category + '_' + field] = value;
        return challengeLevel;
    }

    /**
     * Is yesterday
     *
     * @description Find if a given date is yesterday
     * @param {*} created_at
     * @returns {boolean}
     * @memberof ChallengeProgress
     */
    isYesterday(created_at) {
        //
        // Set today date to compare
        const today: any = moment();
        const otherDate: any = moment(created_at);
        console.log('today', today.format('DD/MM'));
        console.log('otherDate', otherDate.format('DD/MM'));
        //
        // Get difference in days
        const diff = today.diff(otherDate, 'days');
        console.log('diff', diff);
        //
        // Yesterday if result is 1
        return diff === 1 ? true : false;
    }
}

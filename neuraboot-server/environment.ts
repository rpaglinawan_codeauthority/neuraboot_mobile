
const env: any = {
  testing: true,
  staging: false,
  version: 1000,
  appName: 'Neuraboot',
  appSlug: 'neuraboot',
  jwtSecret: '25154f3b1-da323-44d3-8c725-zf55af610840',
  google: {
    mapsKey: 'AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ' // browser key
  },
  elastic: {
    app_username: 'elastic',
    app_password: '750657e207894b239c643639e592f71c',
    url: 'http://35.184.74.6:5454'
  },
  firebase: {
    webApiKey: 'AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ'
  },
  twilio: {
    sid: 'AC4eea112248edd64e645a92e6c92349ff',
    token: '4d2940aaf07f1cd32cbc353db0ebf67a',
    from: '+18649993440'
  },
  sparkpost: {
    apiKey: 'e3d880a06ac07cdd9a106758989d8d3e0ec554df',
    logoUrl: 'https://goo.gl/Uc94Br',
    from: {
      name: 'Neuraboot',
      email: 'noreply@app.neuraboot.com'
    }
  }
}

if (env.testing) {
  //
  // Test (local)
  env.firebase.config = {
    apiKey: "AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ",
    authDomain: "neuraboot.firebaseapp.com",
    databaseURL: "https://neuraboot.firebaseio.com",
    projectId: "neuraboot",
    storageBucket: "neuraboot.appspot.com",
    messagingSenderId: "21924514688"
  };

} else if (env.staging) {
  //
  // Staging (dev)
  env.firebase.config = {
    apiKey: "AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ",
    authDomain: "neuraboot.firebaseapp.com",
    databaseURL: "https://neuraboot.firebaseio.com",
    projectId: "neuraboot",
    storageBucket: "neuraboot.appspot.com",
    messagingSenderId: "21924514688"
  };

} else {
  //
  // Production
  env.firebase.config = {
    apiKey: "AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ",
    authDomain: "neuraboot.firebaseapp.com",
    databaseURL: "https://neuraboot.firebaseio.com",
    projectId: "neuraboot",
    storageBucket: "neuraboot.appspot.com",
    messagingSenderId: "21924514688"
  };
}

export const environment = env;
class NeuraStrings {
    public firstStep:String = 'First, fill out your profile to list someone you would like notified when you need support.';

    public secondStep:String = 'Throughout the day, check your mood.  If your mood is very intense, we’ll give you the option to let us notify your supporters.';

    public tertiaryStep:String = 'At any time, you can complete an action step, using the button at the bottom of the screen, to earn badges and build emotional muscle!';
}
export const environment = {
    version: '0013',
    production: true,
    testing: false,
    storage: 'app',
    name: 'neuraboot',
    title: 'Neuraboot',
    store: 'app',
    app: 'neuraboot',
    app_id: 'com.sun.neuraboot',
    app_store_id: '',
    firebase: {
        apiKey: 'AIzaSyCHC8nQVrhgxQR7N9GwAgGxMn2n6k8irMQ',
        authDomain: 'neuraboot.firebaseapp.com',
        databaseURL: 'https://neuraboot.firebaseio.com',
        projectId: 'neuraboot',
        storageBucket: 'neuraboot.appspot.com',
        messagingSenderId: '21924514688'
    },
    google: {
        webClientId: '21924514688-f2b87b25vmmsb1g9itsggheua5bs6v8d.apps.googleusercontent.com',
        scopes: 'profile email'
    },
    functions: {
        baseUrl: 'https://us-central1-neuraboot.cloudfunctions.net'
    }
};

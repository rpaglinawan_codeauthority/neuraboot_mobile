import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { CacheService } from '../services/cache.service';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';
import { User } from '../../../../neuraboot-core/interfaces/user.interface';
import { isEmpty } from 'lodash';
import { getState$ } from '@reative/state';

@Injectable({
    providedIn: 'root'
})
export class UserNotAuthedGuard implements CanActivate, CanActivateChild {
    constructor(private cache: CacheService, private auth: AuthService, private nav: NavController) {}

    async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
        //
        // check for user cache & firebase auth instance
        const user: User = (await this.cache.get('user')) || {};

        //
        // User is logged in?
        if (!isEmpty(user) && !isEmpty(user.uid)) {
            this.nav.navigateRoot(['/home']);
            return false;
        }

        //
        // Check for wizard/video
        const wizardVideoState = await this.cache.get('wizard:video');
        if (!wizardVideoState) {
            this.nav.navigateRoot(['/wizard']);
            return false;
        }

        return true;
    }
    canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
        return this.canActivate(next, state);
    }
}

import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { CacheService } from '../services/cache.service';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';
import { User } from '../../../../neuraboot-core/interfaces/user.interface';
import { Subscription } from '../../../../neuraboot-core/interfaces/subscription.interface';
import { isEmpty } from 'lodash';
import { getState$ } from '@reative/state';
@Injectable({
    providedIn: 'root'
})
export class UserAuthedWithSubscriptionGuard implements CanActivate, CanActivateChild {
    constructor(private cache: CacheService, private auth: AuthService, private nav: NavController) {}

    async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {

        console.log("hit canActivate():");

        //
        // check for user cache & firebase auth instance
        const user: User = (await this.cache.get('user')) || {};

        //
        // Validate user
        if (isEmpty(user) || isEmpty(user.uid) || isEmpty(user.token)) {
            this.nav.navigateRoot(['/account-login']);
            console.log("user is not auth");
            return false;
        } else {
            //
            // Set user
            this.auth.setUser(user);

            console.log("user is auth");
            //
            // Look for subscription
            const subscription: Subscription = await getState$('subscription').toPromise();
            console.log('subscription', subscription);
            //
            // Validate subscription
            if (!isEmpty(subscription) && subscription.status) return true;

            //
            // Not subscribe, go to subscribe page
            this.nav.navigateRoot(['/subscribe']);
            return false;
        }
    }
    canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
        return this.canActivate(next, state);
    }
}

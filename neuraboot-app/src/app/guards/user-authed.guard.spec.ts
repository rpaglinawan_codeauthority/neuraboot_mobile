import { TestBed, async, inject } from '@angular/core/testing';

import { UserAuthedGuard } from './user-authed.guard';

describe('UserAuthedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserAuthedGuard]
    });
  });

  it('should ...', inject([UserAuthedGuard], (guard: UserAuthedGuard) => {
    expect(guard).toBeTruthy();
  }));
});

import { TestBed, async, inject } from '@angular/core/testing';

import { UserNotAuthedGuard } from './user-not-authed.guard';

describe('UserNotAuthedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserNotAuthedGuard]
    });
  });

  it('should ...', inject([UserNotAuthedGuard], (guard: UserNotAuthedGuard) => {
    expect(guard).toBeTruthy();
  }));
});

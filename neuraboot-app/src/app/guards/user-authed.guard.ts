import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { CacheService } from '../services/cache.service';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';
import { User } from '../../../../neuraboot-core/interfaces/user.interface';
import { isEmpty } from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class UserAuthedGuard implements CanActivate, CanActivateChild {
    constructor(private cache: CacheService, private auth: AuthService, private nav: NavController) {}

    async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
        //
        // check for user cache & firebase auth instance
        const user: User = (await this.cache.get('user')) || {};

        if (user && user.uid && user.token) {
            this.auth.setUser(user);
            return true;
        }

        this.nav.navigateRoot(['/account-login']);
        return false;
    }
    canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
        return this.canActivate(next, state);
    }
}

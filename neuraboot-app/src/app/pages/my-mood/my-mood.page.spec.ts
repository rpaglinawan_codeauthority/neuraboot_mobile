import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyMoodPage } from './my-mood.page';

describe('MyMoodPage', () => {
  let component: MyMoodPage;
  let fixture: ComponentFixture<MyMoodPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyMoodPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyMoodPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

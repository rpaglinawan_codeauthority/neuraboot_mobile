import { Component, OnInit } from '@angular/core';
import { FeelingService } from 'src/app/services/feeling.service';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { AuthService } from 'src/app/services/auth.service';
import { UiService } from 'src/app/services/ui.service';
import { NavigationService } from 'src/app/navigation.service';
import { ActivatedRoute } from '@angular/router';
import { isEmpty } from 'lodash';
import { ActionStepService } from 'src/app/services/action-step.service';
import { NavController, MenuController } from '@ionic/angular';
import { ActionStepHistoryService } from 'src/app/services/action-step-history.service';

@Component({
    selector: 'app-my-mood',
    templateUrl: './my-mood.page.html',
    styleUrls: ['./my-mood.page.scss'],
    providers: [FeelingService, FeelingHistoryService, ActionStepService, ActionStepHistoryService]
})
export class MyMoodPage implements OnInit {
    constructor(
        public feeling: FeelingService,
        private auth: AuthService,
        private ui: UiService,
        private nav: NavController,
        private route: ActivatedRoute,
        public actionStepHistory: ActionStepHistoryService,
        public menu: MenuController
    ) {}

    ngOnInit() {
        console.log('nav', this.nav);
    }

    async ionViewWillEnter() {
        //
        // Disable swipe gesture
        this.ui.swipeGesture = false;

        //
        // Disable menu swipe
        this.menu.swipeEnable(false);

        //
        // Force action step ?
        this.feeling.forceActionStep = !isEmpty(this.route.snapshot.params.actionStep);

        //
        // Check if there is any active action step
        if (this.auth.getUser().active_challenge) {
            //
            // Set alert buttons
            const buttons: any[] = [
                {
                    text: 'Mark As Completed',
                    handler: blah => {
                        this.actionStepHistory.completeActive();
                    }
                },
                {
                    text: 'Cancel Action Step',
                    handler: blah => {
                        this.actionStepHistory.cancelActive();
                    }
                },
                {
                    text: 'Go back',
                    handler: blah => {
                        this.nav.navigateBack(['/home']);
                    }
                }
            ];

            const alert = await this.ui.alert(
                'Action Step Found',
                'You have an active action step, what do you want to do?',
                buttons,
                false
            );
            alert.present();
        }

        //
        // Restore feelings from cache
        await this.feeling.restoreFromCache();
    }

    ionViewWillLeave() {
        //
        // Enable swipe gesture
        this.ui.swipeGesture = true;

        //
        // Enable menu swipe
        this.menu.swipeEnable(true);

        //
        // Remove leader line
        this.removeLeaderLine();

        //
        // Remove view segment
        this.feeling.segment = null;

        //
        // Remove action step category
        this.feeling.actionStepCategory = false;
    }

    ionViewDidLeave() {
        //
        // Remove leader line
        this.removeLeaderLine();
    }

    /**
     * Remove leader line
     *
     * @description Remove leader line
     * @memberof HowdoifeelPage
     */
    removeLeaderLine() {
        document.querySelector('.lines-container').innerHTML = '';
    }
}

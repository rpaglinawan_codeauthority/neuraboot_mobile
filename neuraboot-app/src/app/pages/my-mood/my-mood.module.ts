import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MyMoodPage } from './my-mood.page';
import { UserAuthedWithSubscriptionGuard } from 'src/app/guards/user-authed-with-subscription.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { MyMoodScaleSegmentComponent } from './my-mood-scale-segment/my-mood-scale-segment.component';
import { MoodModule } from 'src/app/modules/mood/mood.module';
import { MyMoodActionStepSegmentComponent } from './my-mood-action-step-segment/my-mood-action-step-segment.component';
import { ActionStepModule } from 'src/app/modules/action-step/action-step.module';
import { ModalModule } from 'src/app/modules/modal/modal.module';

const routes: Routes = [
    {
        path: '',
        component: MyMoodPage,
        // canActivate: [UserAuthedWithSubscriptionGuard]
    },
    {
        path: ':actionStep',
        component: MyMoodPage,
        // canActivate: [UserAuthedWithSubscriptionGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MoodModule,
        ActionStepModule,
        ModalModule
    ],
    declarations: [MyMoodPage, MyMoodScaleSegmentComponent, MyMoodActionStepSegmentComponent]
})
export class MyMoodPageModule {}

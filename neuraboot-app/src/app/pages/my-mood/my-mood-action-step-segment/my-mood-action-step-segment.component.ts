import { Component, OnInit } from '@angular/core';
import { ActionStepLevelService } from 'src/app/services/action-step-level.service';
import { FeelingService } from 'src/app/services/feeling.service';
import { UiService } from 'src/app/services/ui.service';
import { ActionStepService } from 'src/app/services/action-step.service';
import { ActionStepLevel } from '../../../../../../neuraboot-core/interfaces/action-step-level.interface';
import { Subscription } from 'rxjs';
import { snakeCase, truncate } from 'lodash';
import { NavController, ModalController } from '@ionic/angular';
import { ActionStepHistoryService } from 'src/app/services/action-step-history.service';
import { ActionStepDescriptionModalComponent } from 'src/app/modules/action-step/action-step-description-modal/action-step-description-modal.component';

@Component({
    selector: 'my-mood-action-step-segment',
    templateUrl: './my-mood-action-step-segment.component.html',
    styleUrls: ['./my-mood-action-step-segment.component.scss'],
    providers: [ActionStepLevelService, ActionStepHistoryService]
})
export class MyMoodActionStepSegmentComponent implements OnInit {
    public level$: Subscription;
    public level: ActionStepLevel = this.actionStepLevel.default;
    public selectedResumeCategory: string = null;

    constructor(
        public feeling: FeelingService,
        public ui: UiService,
        public actionStep: ActionStepService,
        public actionStepLevel: ActionStepLevelService,
        public actionStepHistory: ActionStepHistoryService,
        private nav: NavController,
        private modal: ModalController
    ) {}

    async ngOnInit() {
        //
        // Get level
        const loader = await this.ui.loader(60 * 1000);
        await loader.present();

        this.level$ = this.actionStepLevel.level$.subscribe((level: ActionStepLevel) => {
            this.level = level;

            loader.dismiss();

            //
            // Start page
            this.startPage();
        });
    }

    ngOnDestroy() {
        this.actionStep.entries = [];
        this.actionStep.selected = null;
        this.level$.unsubscribe();
    }

    async startPage() {
        //
        // Choose category for reboot
        if (this.feeling.forceActionStep && !this.feeling.actionStepCategory) return false;

        try {
            setTimeout(async () => {
                console.log('startPage(): this.feeling.current', this.feeling.current);
                this.setEntries(false, this.feeling.current.category);
            }, 150);
        } catch (error) {
            console.log('error', error);
        }
    }

    /**
     * Set category
     *
     * @description Select an action step category and set entry
     * @param {string} category
     * @memberof MyMoodActionStepSegmentComponent
     */
    async selectCategory(category: string) {
        //
        // Set buttons and actions
        const buttons: any[] = [
            {
                text: 'Get a new action step',
                handler: async () => {
                    this.feeling.actionStepCategory = category;
                    this.setEntries(category);
                }
            },
            {
                text: 'Get more info',
                handler: async () => {
                    this.selectedResumeCategory = snakeCase(category);
                    // this.modalOverlaySkyResume = true;
                }
            }
        ];

        //
        // Present
        const alert = await this.ui.alert('What to do?', 'Choose an option', buttons, false);
        alert.present();
    }

    /**
     * Set entries
     *
     * @description set entries
     * @param {(string | boolean)} [category=false]
     * @memberof MyMoodActionStepSegmentComponent
     */
    async setEntries(category: string | boolean = false, feeling: string | boolean = false) {
        const entries = await this.actionStep.getEntries(category, this.level.excludedCategories, feeling).toPromise();
        this.actionStep.setEntries(entries);
    }

    openBrowser(link: string) {
        this.ui.browser(link);
    }

    /**
     * Is excluded category
     *
     * @description Check if this is an excluded category
     * @param {*} category
     * @returns
     * @memberof MyMoodActionStepSegmentComponent
     */
    isExcludedCategory(category) {
        return this.level.excludedCategories.includes(category);
    }

    getTruncatedText(text: string = null) {
        //
        // Validate
        if (!text) return text;

        //
        // Truncate text
        text = text;

        return text;
    }

    /**
     * Present description modal
     *
     * @memberof MyMoodActionStepSegmentComponent
     */
    async presentDescriptionModal() {
        //
        // Create modal
        const modal = await this.modal.create({
            component: ActionStepDescriptionModalComponent,
            componentProps: {
                actionStep: this.actionStep.selected
            },
            showBackdrop: false,
            animated: false
        });

        //
        // Present modal
        await modal.present();
    }

    // @todo: action step resume modal
    onCloseResume(event) {
        // this.modalOverlaySkyResume = false;
        // this.selectedResumeCategory = null;
    }

    /**
     * Decline
     *
     * @memberof MyMoodActionStepSegmentComponent
     */
    async decline() {
        //
        // Set is last slide
        const isLastSlide: boolean = await this.actionStep.isLastSlide();

        //
        // Action for last slider
        if (isLastSlide) {
            //
            // Return to the beginning
            this.feeling.restart();
            this.nav.navigateRoot(['/home']);
        }

        //
        // Action when is not last slider
        else {
            this.actionStep.goToNextSlide();
        }
    }

    /**
     * Aceept
     *
     * @description Accept selected action step, persist to history and restart my mood
     * @memberof MyMoodActionStepSegmentComponent
     */
    async accept() {
        //
        // Persist challenge history
        await this.actionStepHistory.persist(this.actionStep.selected);

        //
        // Return to the beginning
        this.feeling.restart();

        //
        // Go to my progress
        this.nav.navigateRoot(['/home']);
    }
}

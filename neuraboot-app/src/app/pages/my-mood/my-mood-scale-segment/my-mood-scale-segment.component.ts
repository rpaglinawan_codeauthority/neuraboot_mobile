import { Component, OnInit } from '@angular/core';
import { FeelingService } from 'src/app/services/feeling.service';

@Component({
    selector: 'my-mood-scale-segment',
    templateUrl: './my-mood-scale-segment.component.html',
    styleUrls: ['./my-mood-scale-segment.component.scss']
})
export class MyMoodScaleSegmentComponent implements OnInit {
    constructor(public feeling: FeelingService) {}

    ngOnInit() {}
}

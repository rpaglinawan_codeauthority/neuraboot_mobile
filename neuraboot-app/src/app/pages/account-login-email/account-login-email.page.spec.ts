import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountLoginEmailPage } from './account-login-email.page';

describe('AccountLoginEmailPage', () => {
  let component: AccountLoginEmailPage;
  let fixture: ComponentFixture<AccountLoginEmailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountLoginEmailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountLoginEmailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountLoginEmailPage } from './account-login-email.page';
import { AccountModule } from 'src/app/modules/account/account.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material';
import { UserNotAuthedGuard } from 'src/app/guards/user-not-authed.guard';

const routes: Routes = [
    {
        path: '',
        component: AccountLoginEmailPage,
        canActivate: [UserNotAuthedGuard]
    }
];

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes), FlexLayoutModule, MatButtonModule, AccountModule],
    declarations: [AccountLoginEmailPage]
})
export class AccountLoginEmailPageModule {}

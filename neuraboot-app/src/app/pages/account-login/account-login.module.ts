import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountLoginPage } from './account-login.page';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material';
import { UserNotAuthedGuard } from 'src/app/guards/user-not-authed.guard';

const routes: Routes = [
    {
        path: '',
        component: AccountLoginPage,
        canActivate: [UserNotAuthedGuard]
    }
];

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes), FlexLayoutModule, MatButtonModule],
    declarations: [AccountLoginPage]
})
export class AccountLoginPageModule {}

import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { User } from '../../../../../neuraboot-core/interfaces/user.interface';
import { ApiUserAuthResponse } from '../../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
import { of } from 'rxjs';
import { map, mergeMap, flatMap, tap, catchError } from 'rxjs/operators';
import { isEmpty } from 'lodash';
import { environment } from 'src/environments/environment';
import { Response } from '@reative/records';
import { Subscription } from '../../../../../neuraboot-core/interfaces/subscription.interface';
import { setState } from '@reative/state';
import { SubscriptionService } from 'src/app/services/subscription.service';
import * as moment from 'moment';
import { firestore } from '@reative/firebase';

@Component({
    selector: 'app-account-login-social',
    templateUrl: './account-login-social.page.html',
    styleUrls: ['./account-login-social.page.scss'],
    providers: [UserService, SubscriptionService]
})
export class AccountLoginSocialPage implements OnInit {
    public loading: boolean = false;

    constructor(
        public user: UserService,
        public ui: UiService,
        public auth: AuthService,
        public nav: NavController,
        public subscription: SubscriptionService
    ) {}

    ngOnInit() {}

    /**
     * Auth facebook
     *
     * @memberof AccountLoginSocialPage
     */
    public async authFacebook() {
        this.loading = true;
        let loader = await this.ui.loader(60 * 1000, 'Authorizing');
        await loader.present();

        //
        // Get auth response
        const authResponse: any = await this.auth.signInFacebook().catch(async err => {
            //
            // Exception
            loader.dismiss();
            this.loading = false;

            if (err != 'User cancelled.') {
                const invalidToken: boolean = JSON.stringify(err).includes('invalid access_token');

                //
                // invalid token fallback
                if (invalidToken) {
                    await this.auth.logoutFacebook();
                    this.authFacebook();
                } else {
                    if (err.code === 'auth/account-exists-with-different-credential')
                        err.message = `
                It looks like you've already registered before, try signing in with your email or request a new access in the "forgot password" section. Once authenticated, you will be able to connect your Facebook account.
                `;
                    const alert = await this.ui.alert('Error', err.message);
                    alert.present();
                }
            }
        });

        loader.dismiss();

        const provider = authResponse.providerData.find(pd => pd.providerId === 'facebook.com') || {};

        // alert(JSON.stringify(authResponse))
        // alert(JSON.stringify(provider))

        if (!isEmpty(provider) && authResponse.email) {
            //
            // Set user data
            const payload: User = {
                uid: authResponse.uid,
                display_name: authResponse.displayName,
                email: authResponse.email,
                photo_url: provider.photoURL + '?width=1024',
                facebook_uid: provider.photoURL.replace(/\D/g, ''),
                facebook_connected: true
            };

            //
            // Change loader
            await loader.dismiss();
            loader = await this.ui.loader(60 * 1000, 'Authenticating');
            await loader.present();

            this.user.$collection
                .network(true)
                .cache(false)
                .key('user')
                .transform((data: Response) => data.data.user)
                .post('/auth/facebook', { user: payload })
                .pipe(
                    //
                    // auth within firebase
                    mergeMap((r: ApiUserAuthResponse) => this.auth.authWithFirebase$(r)),

                    //
                    // format user/token object and set to the session
                    flatMap((r: ApiUserAuthResponse[]) => this.auth.setUserSession(r)),

                    //
                    // redirect to the first route
                    tap(() => this.loginAuthorized(loader)),

                    //
                    // deal with exceptions
                    catchError(async err => {
                        catchError(err => this.exception(err, loader));
                    })
                )
                .toPromise();
        } else {
            await this.auth.logoutFacebook(); // to ensure things will not break
            const alert = await this.ui.alert(
                `Warning`,
                `
          Error while authorizing, you must share your email address. Visit your Facebook App Settings and remove ${environment.title} from there before you try again, ensure to also remove the app from your phone.
          `
            );
            alert.present();
        }
    }

    async authGoogle() {
        //
        // Set loading
        let loader = await this.ui.loader(60 * 1000, 'Authorizing');
        loader.present();

        try {
            //
            // sign-in with google
            const result: any = await this.auth.signInGoogle();
            console.log('AccountLoginSocial.authGoogle(): google login result', result);
            await this.completeLogin(result);

            this.loginAuthorized(loader);
        } catch (err) {
            await loader.dismiss();

            let alert = await this.ui.alert('Error', err.message);
            alert.present();
        }
    }

    /**
     * Login
     *
     * @param {{ uid: string, displayName: string, photoURL: string, email: string, facebook_uid: string }} result
     * @returns {Promise<{ currentUser: User, isNew: boolean }>}
     * @memberof LoginSocialPage
     */
    async completeLogin(result: {
        uid: string;
        displayName: string;
        photoURL: string;
        email: string;
        facebook_uid: string;
        google_uid: string;
    }): Promise<{ currentUser: User; isNew: boolean }> {
        try {
            const currentDate = moment().toISOString();
            //
            // Set data
            let data: User = {
                uid: result.uid,
                display_name: result.displayName,
                photo_url: result.photoURL,
                email: result.email,
                online_at: currentDate,
                google_connected: result.google_uid ? true : false,
                google_uid: result.google_uid ? result.google_uid : null
            };

            //
            // check for valid user
            let snapshot = await this.auth.$collection
                .firestore()
                .collection('users')
                .doc(result.uid)
                .get();

            let userExist = snapshot.exists ? snapshot.data() : false;

            //
            // set created_at
            if (!userExist) {
                data.created_at = currentDate;
                data.completed_password = false;
            }

            //
            // set to db
            await firestore()
                .collection('users')
                .doc(result.uid)
                .set(data, { merge: true });

            //
            // Start session
            let currentUser = userExist ? { ...userExist, ...data } : data;

            //
            // set token
            currentUser.token = await this.auth.getToken(currentUser.uid);
            await this.auth.setUserSession([{ user: currentUser, token: { client: currentUser.token, auth: null } }]);

            return { currentUser: currentUser, isNew: userExist ? false : true };
        } catch (error) {
            console.log('AccountLoginSocial.completeLogin(): social login error:', error);
            return error;
        }
    }

    /**
     * Login authorized
     *
     * @description Hook after login was authorized
     * @param {*} loader
     * @memberof AccountLoginSocialPage
     */
    async loginAuthorized(loader) {
        //
        // Get subscription
        const subscription: Subscription = await this.subscription.$collection
            .cache(false)
            .network(true)
            .where('user.uid', '==', this.auth.user.uid)
            .findOne()
            .toPromise();

        //
        // Set subscription
        if (!isEmpty(subscription)) {
            //
            // Set subscription
            setState(
                'subscription',
                { data: subscription },
                {
                    merge: false
                }
            );
        }

        this.auth.redirectAfterLogin();

        //
        // Remove loading
        loader.dismiss();
        this.loading = false;
    }

    /**
     * Exception
     *
     * @param {*} err
     * @param {*} loader
     * @returns
     * @memberof AccountLoginSocialPage
     */
    exception(err, loader) {
        this.loading = false;
        return this.auth.exception(err, loader);
    }
}

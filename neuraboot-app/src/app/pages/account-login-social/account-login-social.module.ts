import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountLoginSocialPage } from './account-login-social.page';
import { UserNotAuthedGuard } from 'src/app/guards/user-not-authed.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule } from '@angular/material';

const routes: Routes = [
    {
        path: '',
        component: AccountLoginSocialPage,
        canActivate: [UserNotAuthedGuard]
    }
];

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes), FlexLayoutModule, MatButtonModule, MatIconModule],
    declarations: [AccountLoginSocialPage]
})
export class AccountLoginSocialPageModule {}

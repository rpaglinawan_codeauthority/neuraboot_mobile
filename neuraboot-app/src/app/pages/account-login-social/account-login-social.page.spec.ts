import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountLoginSocialPage } from './account-login-social.page';

describe('AccountLoginSocialPage', () => {
  let component: AccountLoginSocialPage;
  let fixture: ComponentFixture<AccountLoginSocialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountLoginSocialPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountLoginSocialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { SubscriptionCodeService } from 'src/app/services/subscription-code.service';
import { SubscriptionService } from 'src/app/services/subscription.service';
import { SubscriptionCode } from '../../../../../neuraboot-core/interfaces/subscription-code.interface';
import { Subscription } from '../../../../../neuraboot-core/interfaces/subscription.interface';
import { Guid } from '@reative/records';
import { select, setState, getState, getState$ } from '@reative/state';
import { isEmpty, toLower } from 'lodash';
import { Observable, BehaviorSubject } from 'rxjs';
import { take, switchMap, distinctUntilChanged } from 'rxjs/operators';
import { Platform, NavController, AlertController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
    selector: 'app-subscribe',
    templateUrl: './subscribe.page.html',
    styleUrls: ['./subscribe.page.scss'],
    providers: [SubscriptionCodeService, SubscriptionService]
})
export class SubscribePage implements OnInit {

    public useCodeState = new BehaviorSubject<boolean>(false);
    public useCode$: Observable<boolean> = this.useCodeState.asObservable();

    public productData = new BehaviorSubject<any>(null);
    public products$: Observable<any> = this.productData.asObservable();

    public form: FormGroup;

    public hasLoaded = new BehaviorSubject<boolean>(false);
    public hasLoaded$: Observable<any> = this.hasLoaded.asObservable();

    constructor(
        public iap: InAppPurchase,
        public ui: UiService,
        private fb: FormBuilder,
        public subscription: SubscriptionService,
        public subscriptionCode: SubscriptionCodeService,
        public platform: Platform,
        public auth: AuthService,
        public nav: NavController,
        private alertController: AlertController
    ) { }

    ngOnInit() {
        document.getElementById("promoField").hidden = true;
        this.productData.pipe(
            distinctUntilChanged(),
        );
        this.getProducts();
        this.createForm();
    }

    ionViewDidLoad() {
        console.log("fired off viewDidLoad():");
    }

    triggerPop() {
        this.productData.complete;
    }

    createForm() {
        let fields: any = {};

        //
        // Fields
        fields.code = [null, [Validators.required]];

        //
        // Form
        this.form = this.fb.group(fields);
    }

    triggerPop() {
        this.productData.complete;
    }

    async getProducts() {
        try {

            console.log("getting products");
            //
            // Set loading
            const loader = await this.ui.loader();
            await loader.present();

            //
            // Check length
            const productValidation = await getState$('subscription:products').toPromise();
            console.log('getProducts(): productValidation', productValidation);

            if (!isEmpty(productValidation)) await loader.dismiss();

            //
            // Get products
            const products = await this.iap.getProducts(['monthly', 'annual']);

            //
            // Set products
            this.productData.next(products);

            await loader.dismiss();
            this.productData.complete();
            this.hasLoaded.next(true);

            console.log('getProducts(): products', products);

        } catch (error) {
            console.log('getProducts(): error', error);
        }
    }

    async toggleCodeForm() {
        //
        // Get use code value
        const useCode: boolean = await this.useCode$.pipe(take(1)).toPromise();

        //
        // Set new value
        this.useCodeState.next(!useCode);
    }

    async onSubmitCodeForm() {
        //
        // Set loading
        const loader = await this.ui.loader();
        await loader.present();

        try {
            //
            // Try to get code
            const validCode: SubscriptionCode = await this.subscriptionCode.$collection
                .where('code', '==', toLower(this.form.get('code').value))
                .where('status', '==', true)
                .findOne()
                .toPromise();

            //
            // Validate
            if (isEmpty(validCode)) {
                this.ui.say('The code is not valid');
                loader.dismiss();

                return;
            }

            //
            // Make subscription
            await this.createSubscription('code', toLower(this.form.get('code').value));

            //
            // Finish
            this.ui.say('Subscribed, enjoy!');
            loader.dismiss();

            //
            // Redirect to home
            this.nav.navigateRoot(['/home']);
        } catch (error) {
            loader.dismiss();
            console.log('onSubmitCodeForm(): error', error);
            return error;
        }
    }

    async createSubscription(paymentType: string = 'store', code: string = null, storeData: any = {}, product: any = {}) {
        console.log('createSubscription(): start');
        return new Promise(async (resolve, reject) => {
            try {
                //
                // Set os
                const os: string = this.platform.is('ios') ? 'ios' : 'android';
                console.log('createSubscription(): os', os);
                //
                // Try to get a previous subscription
                const currentSubscription: Subscription = await this.subscription.$collection
                    .cache(false)
                    .network(true)
                    .where('user.uid', '==', this.auth.user.uid)
                    .findOne()
                    .toPromise();
                console.log('createSubscription(): currentSubscription', currentSubscription);
                //
                // Subscription data
                const subscriptionData: Subscription = {
                    id: !isEmpty(currentSubscription) && !isEmpty(currentSubscription.id) ? currentSubscription.id : Guid.make(2),
                    status: true,
                    payment_type: paymentType,
                    code: code,
                    os: os,
                    user: {
                        uid: this.auth.user.uid,
                        email: this.auth.user.email,
                        display_name: this.auth.user.display_name,
                        photo_url: this.auth.user.photo_url ? this.auth.user.photo_url : null
                    },
                    store: storeData,
                    product: product,
                    created_at: moment().toISOString(),
                    updated_at: moment().toISOString()
                };
                console.log('createSubscription(): subscriptionData', subscriptionData);

                //
                // Create subscription
                await this.subscription.$collection.post(subscriptionData.id, subscriptionData).toPromise();
                console.log('createSubscription(): set completed');
                //
                // Set subscription
                setState(
                    'subscription',
                    { data: subscriptionData },
                    {
                        merge: false
                    }
                );

                return resolve(subscriptionData);
            } catch (error) {
                console.log('createSubscription(): error', error);
                return reject('Error creating subscription');
            }
        });
    }

    async subscribe(product) {
        //
        // Confirm subscription

        // TODO(rpaglinawan): Check to see if user is new subscribe for a free week trial 

        // TODO(rpaglinawan): One of two options can be chosen  
        const confirmation: boolean = await this.subscriptionTermConfirmation();
        if (!confirmation) {
            return;
        }

        //
        // Set loading
        const loader = await this.ui.loader(50000);
        await loader.present();

        try {
            //
            // Subscribe to product
            const subscribeData: any = await this.iap.subscribe(product.productId);

            //
            // Consume
            await this.iap.consume(subscribeData.productType, subscribeData.receipt, subscribeData.signature);

            //
            // Create subscription
            await this.createSubscription('store', null, subscribeData, product);

            //
            // Finish
            this.ui.say('Subscribed, enjoy!');
            loader.dismiss();

            //
            // Redirect to home
            this.nav.navigateRoot(['/home']);
        } catch (error) {
            this.ui.say('Error subscribing, try again');
            loader.dismiss();

            console.log('subscribe(): error', error);
            return 'Error subscribing';
        }
    }

    async showPromotionField() {
        document.getElementById("promoField").hidden = false;

    }

    async subscriptionTermConfirmation(): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            const alert = await this.alertController.create({
                header: 'Subscription Details',
                message:
                    'You are about to subscribe, this is an auto-renew subscription. <br><br> Subscription will be charged to your credit card through your iTunes account. <br><br> Your subscription will automatically renew unless canceled at least 24 hours before the end of the current period. <br><br> You will not be able to cancel the subscription once activated. <br><br> Manage your subscriptions in Account Settings after purchase. <br><br> Please read our <a href="https://www.neuraboot.com/terms-of-service-agreement/">terms of service</a> before continuing.',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: blah => {
                            return resolve(false);
                        }
                    },
                    {
                        text: 'Confirm',
                        handler: () => {
                            return resolve(true);
                        }
                    }
                ]
            });

            await alert.present();
        });
    }

    openLink(url) {
        this.ui.browser(url);
    }

    async showPromotionField() {
        document.getElementById("promoField").hidden = false;
    }
}

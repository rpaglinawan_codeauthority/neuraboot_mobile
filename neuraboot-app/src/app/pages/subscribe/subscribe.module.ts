import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SubscribePage } from './subscribe.page';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatInputModule, MatIconModule } from '@angular/material';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';

const routes: Routes = [
    {
        path: '',
        component: SubscribePage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        ReactiveFormsModule
    ],
    declarations: [SubscribePage],
    providers: [InAppPurchase]
})
export class SubscribePageModule {}

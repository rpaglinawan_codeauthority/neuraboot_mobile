import { Component, OnInit } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { Observable } from 'rxjs';
import { Subscription } from '../../../../../neuraboot-core/interfaces/subscription.interface';
import { select, getState$ } from '@reative/state';
import { NavigationService } from 'src/app/navigation.service';

@Component({
    selector: 'app-account-subscription',
    templateUrl: './account-subscription.page.html',
    styleUrls: ['./account-subscription.page.scss']
})
export class AccountSubscriptionPage implements OnInit {
    public subscription$: Observable<Subscription> = getState$('subscription');

    constructor(public ui: UiService, public navigation: NavigationService) {}

    ngOnInit() {}

    openLink(url) {
        this.ui.browser(url);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountSubscriptionPage } from './account-subscription.page';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatCardModule } from '@angular/material';

const routes: Routes = [
    {
        path: '',
        component: AccountSubscriptionPage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule
    ],
    declarations: [AccountSubscriptionPage]
})
export class AccountSubscriptionPageModule {}

import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/navigation.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
    selector: 'app-account-basic-info',
    templateUrl: './account-basic-info.page.html',
    styleUrls: ['./account-basic-info.page.scss']
})
export class AccountBasicInfoPage implements OnInit {
    constructor(public navigation: NavigationService, public ui: UiService) {}

    ngOnInit() {}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountLoginCreatePasswordPage } from './account-login-create-password.page';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { AccountModule } from 'src/app/modules/account/account.module';

const routes: Routes = [
    {
        path: '',
        component: AccountLoginCreatePasswordPage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        AccountModule
    ],
    declarations: [AccountLoginCreatePasswordPage]
})
export class AccountLoginCreatePasswordPageModule {}

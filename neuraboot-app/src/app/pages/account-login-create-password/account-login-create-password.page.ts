import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/navigation.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
    selector: 'app-account-login-create-password',
    templateUrl: './account-login-create-password.page.html',
    styleUrls: ['./account-login-create-password.page.scss']
})
export class AccountLoginCreatePasswordPage implements OnInit {
    constructor(public navigation: NavigationService, public ui: UiService) {}

    ngOnInit() {}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MyProfilePage } from './my-profile.page';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatTabsModule } from '@angular/material';
import { ProfileModule } from 'src/app/modules/profile/profile.module';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';

const routes: Routes = [
    {
        path: '',
        component: MyProfilePage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatTabsModule,
        ProfileModule
    ],
    declarations: [MyProfilePage]
})
export class MyProfilePageModule {}

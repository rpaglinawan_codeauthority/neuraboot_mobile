import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileQuestionFormComponent } from 'src/app/modules/profile/profile-question-form/profile-question-form.component';

@Component({
    selector: 'app-my-profile',
    templateUrl: './my-profile.page.html',
    styleUrls: ['./my-profile.page.scss']
})
export class MyProfilePage implements OnInit {
    @ViewChild(ProfileQuestionFormComponent, { static: true }) public profileQuestionForm: ProfileQuestionFormComponent;

    constructor() {}

    ngOnInit() {}
}

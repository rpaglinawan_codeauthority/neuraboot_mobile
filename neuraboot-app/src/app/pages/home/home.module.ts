import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { ProfileProgressModule } from 'src/app/modules/profile-progress/profile-progress.module';
import { ActionStepDisplayModule } from 'src/app/modules/action-step-display/action-step-display.module';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';

const routes: Routes = [
    {
        path: '',
        component: HomePage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        ProfileProgressModule,
        ActionStepDisplayModule
    ],
    declarations: [HomePage]
})
export class HomePageModule {}

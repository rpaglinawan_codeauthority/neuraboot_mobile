import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { NavigationService } from 'src/app/navigation.service';
import { ProfileProgressComponent } from 'src/app/modules/profile-progress/profile-progress/profile-progress.component';
import { UiService } from 'src/app/services/ui.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss']
})
export class HomePage implements OnInit {
    @ViewChild(ProfileProgressComponent, { static: true }) public profileProgress: ProfileProgressComponent;

    constructor(public navigation: NavigationService, public ui: UiService) {}

    ngOnInit() {}

    ionViewWillEnter() {
        this.profileProgress.load();
    }

    ionViewWillLeave() {
        this.profileProgress.unload();
    }
}

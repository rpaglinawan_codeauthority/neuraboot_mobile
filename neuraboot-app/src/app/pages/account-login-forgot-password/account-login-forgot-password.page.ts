import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-account-login-forgot-password',
    templateUrl: './account-login-forgot-password.page.html',
    styleUrls: ['./account-login-forgot-password.page.scss']
})
export class AccountLoginForgotPasswordPage implements OnInit {
    @ViewChild('emailField', { static: true }) emailField: ElementRef;

    public entry: { email?: string } = {};
    public loading: boolean = false;

    constructor(public nav: NavController, private ui: UiService, private auth: AuthService) {}

    ngOnInit() {}

    async submit() {
        //
        // Loader
        const loader = await this.ui.loader(60 * 1000);
        loader.present();

        //
        // Query
        const result = await this.auth
            .forgot(this.entry.email)

            //
            // Catch
            .catch(async err => {
                //
                // Remove loader
                await loader.dismiss();

                //
                // Show alert
                const alert = await this.ui.alert('Warning', err.message);
                await alert.present();

                //
                // On dismiss
                await alert.onDidDismiss();

                //
                // Focus email element
                this.emailField.nativeElement.focus();
            });

        //
        // Result
        if (result) {
            //
            // Remove loader
            await loader.dismiss();

            //
            // Show alert
            const alert = await this.ui.alert('Success', `Verify your email inbox`);
            await alert.present();

            //
            // On dismiss
            await alert.onDidDismiss();

            //
            // Go to login
            await this.nav.navigateRoot(['account-login-email']);
        }
    }
}

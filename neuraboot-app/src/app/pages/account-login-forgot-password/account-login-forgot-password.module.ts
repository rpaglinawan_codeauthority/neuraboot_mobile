import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountLoginForgotPasswordPage } from './account-login-forgot-password.page';
import { UserNotAuthedGuard } from 'src/app/guards/user-not-authed.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';
import { AccountModule } from 'src/app/modules/account/account.module';

const routes: Routes = [
    {
        path: '',
        component: AccountLoginForgotPasswordPage,
        canActivate: [UserNotAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        AccountModule
    ],
    declarations: [AccountLoginForgotPasswordPage]
})
export class AccountLoginForgotPasswordPageModule {}

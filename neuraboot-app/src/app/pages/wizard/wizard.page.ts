import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { setState } from '@reative/state';
import { NavController } from '@ionic/angular';
import { CacheService } from 'src/app/services/cache.service';

@Component({
    selector: 'app-wizard',
    templateUrl: './wizard.page.html',
    styleUrls: ['./wizard.page.scss']
})
export class WizardPage implements OnInit {
    public slider: any;
    public videoId: string = '3K-vTqJf0qg';
    public videoUrl: any = null;
    public options = {
        centeredSlides: true,
        allowTouchMove: false
    };

    constructor(private router: Router, private domSanitizer: DomSanitizer, public nav: NavController, private cache: CacheService) {
        //
        // Force component refresh
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        };

        //
        // set video url
        this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(
            `https://www.youtube.com/embed/3K-vTqJf0qg?autoplay=1&mute=1&enablejsapi=1`
        );
    }

    ngOnInit() {}

    /**
     * On load slider
     *
     * @param {*} event
     * @memberof WizardPage
     */
    onLoadSlider(event) {
        this.slider = event.target;
    }

    async completeVideo() {
        //
        // Set wizard:video as completed
        await this.cache.set('wizard:video', true);

        //
        // Go to home
        this.nav.navigateRoot(['/account']);
    }
}

import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/navigation.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss']
})
export class AccountPage implements OnInit {
    constructor(public navigation: NavigationService, public ui: UiService) {}

    ngOnInit() {}
}

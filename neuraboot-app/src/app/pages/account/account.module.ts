import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountPage } from './account.page';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule } from '@angular/material';

const routes: Routes = [
    {
        path: '',
        component: AccountPage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes), FlexLayoutModule, MatButtonModule, MatIconModule],
    declarations: [AccountPage]
})
export class AccountPageModule {}

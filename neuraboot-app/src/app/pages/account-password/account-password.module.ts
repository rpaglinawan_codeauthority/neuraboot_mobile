import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccountPasswordPage } from './account-password.page';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { AccountModule } from 'src/app/modules/account/account.module';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';

const routes: Routes = [
    {
        path: '',
        component: AccountPasswordPage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        AccountModule
    ],
    declarations: [AccountPasswordPage]
})
export class AccountPasswordPageModule {}

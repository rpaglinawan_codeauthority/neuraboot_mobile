import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/navigation.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
    selector: 'app-account-password',
    templateUrl: './account-password.page.html',
    styleUrls: ['./account-password.page.scss']
})
export class AccountPasswordPage implements OnInit {
    constructor(public navigation: NavigationService, public ui: UiService) {}

    ngOnInit() {}
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MoodHistoryChartComponent } from 'src/app/modules/mood/mood-history-chart/mood-history-chart.component';
import { ActionStepHistoryChartGroupComponent } from 'src/app/modules/action-step/action-step-history-chart-group/action-step-history-chart-group.component';
import { ActionStepHistoryListComponent } from 'src/app/modules/action-step/action-step-history-list/action-step-history-list.component';

@Component({
    selector: 'my-progress-activity-segment',
    templateUrl: './my-progress-activity-segment.component.html',
    styleUrls: ['./my-progress-activity-segment.component.scss']
})
export class MyProgressActivitySegmentComponent implements OnInit {
    @ViewChild(MoodHistoryChartComponent, { static: true })
    public moodHistoryChart: MoodHistoryChartComponent;

    @ViewChild(ActionStepHistoryChartGroupComponent, { static: true })
    public actionStepHistoryChartGroup: ActionStepHistoryChartGroupComponent;

    @ViewChild(ActionStepHistoryListComponent, { static: true })
    public actionStepHistoryList: ActionStepHistoryListComponent;

    constructor() {}

    ngOnInit() {}

    /**
     * On update
     *
     * @description Updates all listing
     *
     * @param {*} event
     * @memberof MyProgressActivitySegmentComponent
     */
    onUpdate(event) {
        //
        // Update mood chart
        if (this.moodHistoryChart.update) this.moodHistoryChart.update.bind(this.moodHistoryChart)();

        //
        // Update action step chart group
        if (this.actionStepHistoryChartGroup.chartUpdate) this.actionStepHistoryChartGroup.chartUpdate();

        //
        // Update action step history list
        if (this.actionStepHistoryList.update) this.actionStepHistoryList.update.bind(this.actionStepHistoryList)();
    }
}

import { Component, OnInit, Input } from '@angular/core';
import { ActionStepLevel } from '../../../../../../neuraboot-core/interfaces/action-step-level.interface';
import { ActionStepLevelService } from 'src/app/services/action-step-level.service';
import { ProgressLevelResumeModalComponent } from 'src/app/modules/progress/progress-level-resume-modal/progress-level-resume-modal.component';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'my-progress-level-segment',
    templateUrl: './my-progress-level-segment.component.html',
    styleUrls: ['./my-progress-level-segment.component.scss'],
    providers: [ActionStepLevelService]
})
export class MyProgressLevelSegmentComponent implements OnInit {
    @Input() public level: ActionStepLevel = this.actionStepLevel.default;

    constructor(private actionStepLevel: ActionStepLevelService, private modal: ModalController) {}

    ngOnInit() {}

    /**
     * Present modal
     *
     * @param {string} category
     * @memberof MyProgressLevelSegmentComponent
     */
    async presentModal(category: string) {
        //
        // Create modal
        const modal = await this.modal.create({
            component: ProgressLevelResumeModalComponent,
            componentProps: {
                level: this.level,
                category: category
            },
            showBackdrop: false,
            animated: false
        });

        //
        // Present modal
        await modal.present();
    }
}

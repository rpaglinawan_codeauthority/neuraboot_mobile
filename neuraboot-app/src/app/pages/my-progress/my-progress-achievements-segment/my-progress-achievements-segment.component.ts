import { Component, OnInit } from '@angular/core';
import { Achievement } from '../../../../../../neuraboot-core/interfaces/achievement.interface';

@Component({
    selector: 'my-progress-achievements-segment',
    templateUrl: './my-progress-achievements-segment.component.html',
    styleUrls: ['./my-progress-achievements-segment.component.scss']
})
export class MyProgressAchievementsSegmentComponent implements OnInit {
    public achievements: Achievement[] = [
        {
            completed: true,
            medal: 'medal-heart'
        },
        {
            completed: true,
            medal: 'medal-first'
        },
        {
            completed: false,
            medal: 'medal-heart'
        },
        {
            completed: false,
            medal: 'medal-heart'
        },
        {
            completed: false,
            medal: 'medal-heart'
        },
        {
            completed: false,
            medal: 'medal-heart'
        },
        {
            completed: false,
            medal: 'medal-heart'
        },
        {
            completed: false,
            medal: 'medal-heart'
        },
        {
            completed: false,
            medal: 'medal-heart'
        }
    ];

    constructor() {}

    ngOnInit() {}

    /**
     * Get image
     *
     * @param {Achievement} item
     * @memberof MyProgressAchievementsSegmentComponent
     */
    getImage(item: Achievement) {
        //
        // Set default
        let medal: string = item.medal;
        let image: string = '/assets/images/{medal}.svg';

        //
        // Change for non completed
        if (!item.completed) medal += '-not-completed';

        //
        // Set background
        image = image.replace('{medal}', medal);

        return image;
    }
}

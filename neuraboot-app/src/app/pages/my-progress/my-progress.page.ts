import { Component, OnInit } from '@angular/core';
import { ActionStepService } from 'src/app/services/action-step.service';
import { ActionStepHistoryService } from 'src/app/services/action-step-history.service';
import { ActionStepLevelService } from 'src/app/services/action-step-level.service';
import { Subscription } from 'rxjs';
import { ActionStepLevel } from '../../../../../neuraboot-core/interfaces/action-step-level.interface';
import { Segment } from '../../../../../neuraboot-core/interfaces/segment.interface';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-my-progress',
    templateUrl: './my-progress.page.html',
    styleUrls: ['./my-progress.page.scss'],
    providers: [ActionStepService, ActionStepHistoryService, ActionStepLevelService]
})
export class MyProgressPage implements OnInit {
    public level$: Subscription;
    public level: ActionStepLevel = this.actionStepLevel.default;
    public activeSegment: string;
    public segments: Segment[] = [
        {
            label: 'Level',
            slug: 'level'
        },
        {
            label: 'Activity',
            slug: 'activity'
        },
        {
            label: 'Achievements',
            slug: 'achievements'
        }
    ];

    constructor(public nav: NavController, public actionStepLevel: ActionStepLevelService) {}

    ngOnInit() {
        setTimeout(() => {
            this.activeSegment = 'level';
            console.log('levels', this.level);
        }, 600);
    }

    ionViewWillEnter() {
        this.level$ = this.actionStepLevel.level$.subscribe(level => (this.level = level));
    }

    ionViewWillLeave() {
        this.level$.unsubscribe();
    }

    /**
     * Change segment
     *
     * @default Change active segment
     * @param {string} slug
     * @memberof MyProgressPage
     */
    changeSegment(slug: string) {
        //
        // Update level if "level" or "stats"
        if (['level', 'stats'].includes(slug)) {
            this.level$.unsubscribe();
            this.level$ = this.actionStepLevel.level$.subscribe(level => (this.level = level));
        }

        //
        // Set active segment
        this.activeSegment = slug;
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MyProgressPage } from './my-progress.page';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatTabsModule } from '@angular/material';
import { MyProgressLevelSegmentComponent } from './my-progress-level-segment/my-progress-level-segment.component';
import { MyProgressActivitySegmentComponent } from './my-progress-activity-segment/my-progress-activity-segment.component';
import { ProgressModule } from 'src/app/modules/progress/progress.module';
import { MyProgressAchievementsSegmentComponent } from './my-progress-achievements-segment/my-progress-achievements-segment.component';
import { UserAuthedGuard } from 'src/app/guards/user-authed.guard';

const routes: Routes = [
    {
        path: '',
        component: MyProgressPage,
        canActivate: [UserAuthedGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatTabsModule,
        ProgressModule
    ],
    declarations: [
        MyProgressPage,
        MyProgressLevelSegmentComponent,
        MyProgressActivitySegmentComponent,
        MyProgressAchievementsSegmentComponent
    ],
    exports: [MyProgressLevelSegmentComponent, MyProgressActivitySegmentComponent, MyProgressAchievementsSegmentComponent]
})
export class MyProgressPageModule {}

import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HowItWorksPage } from './how-it-works.page';
import { MatButtonModule, MatIconModule, MatTabsModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: HowItWorksPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),

    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
  ],
  declarations: [HowItWorksPage]
})
export class HowItWorksPageModule { }

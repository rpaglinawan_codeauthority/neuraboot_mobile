import * as moment from 'moment';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';
import { Geopoint } from '../../../../neuraboot-core/interfaces/geopoint.interface';
import { isEmpty } from 'lodash';
import { Records, Collection, FirebaseDriver } from '@reative/records';
import { AxiosRequestConfig } from 'axios';
import { Plugins } from '@capacitor/core';
import { firebase } from '@reative/firebase';

const { FacebookLogin } = Plugins;

@Injectable()
@Collection({
    name: 'users',
    endpoint: '/api_user'
})
export class UserService {
    public online: boolean;
    public cacheKey: string = 'users/';

    public $collection: Records;

    constructor(public auth: AuthService) {
        this.$collection.http((config: AxiosRequestConfig) => {
            config.headers = {
                Authorization: `Bearer ${auth.getUser().token}`,
                'accept-version': Version.get(environment.version)
            };
        });
    }

    /**
     * Return user geopoin
     *
     * @returns {Geopoint}
     * @memberof UserService
     */
    geopoint(): Geopoint {
        const lat: number = !isEmpty(this.auth.user.geopoint) ? this.auth.user.geopoint.lat : 0;
        const lon: number = !isEmpty(this.auth.user.geopoint) ? this.auth.user.geopoint.lon : 0;
        return {
            lat: lat,
            lon: lon
        };
    }

    socialConnect(provider: string = 'facebook.com', scope: any[] = ['public_profile', 'email']): void {
        if (!this.$collection.firebase().auth().currentUser) {
            throw `This action requires a recent login. Please logout and try again.`;
        }
        //
        // Update
        //
        const updateUser = (photo_url, social_uid) => {
            if (!this.auth.user.photo_url) {
                this.auth.setUserProp('photo_url', photo_url);
            }
            this.auth.setUserProp(provider.replace('.com', '_connected'), true);
            this.auth.setUserProp(provider.replace('.com', '_uid'), social_uid);
            this.auth.setUserProp('online_at', moment().toISOString());
            this.$collection.update(this.auth.user).toPromise();
        };
        //
        // fb login stuff
        //
        const fbLogin = async (facebookResponse, token) => {
            return new Promise(async (resolve, reject) => {
                console.log(JSON.stringify(facebookResponse));
                const credential = await firebase().auth.FacebookAuthProvider.credential(token.accessToken.token);
                let authUser: any = await firebase()
                    .auth()
                    .currentUser.linkWithCredential(credential)
                    .catch(reject);
                console.log('fbLogin(): authUser.uid', authUser.uid);
                if (authUser) {
                    const facebook_uid = authUser.uid;
                    const photo_url = authUser.photoURL;
                    updateUser(photo_url, facebook_uid);
                    resolve(authUser);
                }
            });
        };
        //
        // @todo google login

        //
        // action
        switch (provider) {
            case 'facebook.com':
                if (!this.auth.user.facebook_connected) {
                    FacebookLogin.getCurrentAccessToken()
                        .then(async facebookResponse => {
                            if (!isEmpty(facebookResponse)) {
                                const token = facebookResponse;
                                if (token) {
                                    fbLogin(facebookResponse, token).catch(console.log);
                                }
                            } else {
                                FacebookLogin.login({ permissions: scope })
                                    .then(async (facebookResponse: any) => {
                                        const token = await FacebookLogin.getCurrentAccessToken().catch(console.log);
                                        if (token) {
                                            fbLogin(facebookResponse, token).catch(console.log);
                                        }
                                    })
                                    .catch(console.log);
                            }
                        })
                        .catch(console.log);
                } else if (this.auth.user.facebook_connected) {
                    firebase()
                        .auth()
                        .currentUser.unlink(provider)
                        .catch(console.log);
                    this.auth.setUserProp(provider.replace('.com', '_connected'), false);
                    this.auth.setUserProp('online_at', moment().toISOString());
                    this.$collection.post(this.auth.user.uid, this.auth.user).toPromise();
                    FacebookLogin.logout();
                }
                break;
            case 'google.com':
                // @todo 
                // (rpaglinawan) What is this todo?
                break;
        }
    }
}

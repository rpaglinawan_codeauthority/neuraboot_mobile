import { TestBed } from '@angular/core/testing';

import { FeelingService } from './feeling.service';

describe('FeelingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeelingService = TestBed.get(FeelingService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Collection, Records } from '@reative/records';
import { AuthService } from './auth.service';
import { AxiosRequestConfig } from 'axios';
import { environment } from 'src/environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';
import { ActionStepLevelDefaultData } from '../../../../neuraboot-core/constants/action-step-level-default-data';
import { ActionStepCategoryOptionsFormatted } from '../../../../neuraboot-core/constants/action-step-category-options-formatted';
import { ActionStepLevel } from '../../../../neuraboot-core/interfaces/action-step-level.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { isEmpty } from 'lodash';

// @todo: change collection name in firebase database
@Injectable()
@Collection({
    name: 'challenge-level',
    endpoint: '/api_action_step/level'
})
export class ActionStepLevelService {
    public level$: Observable<ActionStepLevel> = this.getUserLevels();
    public default: ActionStepLevel = ActionStepLevelDefaultData;

    public categoryOptions: any = ActionStepCategoryOptionsFormatted;

    public $collection: Records;

    constructor(public auth: AuthService) {
        this.$collection.http((config: AxiosRequestConfig) => {
            config.headers = {
                Authorization: `Bearer ${auth.getUser().token}`,
                'accept-version': Version.get(environment.version)
            };
        });
    }

    /**
     * Get user levels
     *
     * @description Get user levels
     * @memberof ActionStepLevelService
     */
    getUserLevels() {
        return <any>this.$collection
            .where('user.uid', '==', this.auth.user.uid)
            .key('action-step-level/level')
            .findOne()
            .pipe(
                //
                // set local entries
                map((levels: ActionStepLevel) => {
                    //
                    // Validate if is empty
                    if (isEmpty(levels)) {
                        levels = this.default;
                    }

                    return levels;
                })
            );
    }
}

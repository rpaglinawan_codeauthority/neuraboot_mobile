import { Injectable } from '@angular/core';
import { StorageAdapter } from '@reative/records';
import { storage } from '@reative/cache';
import { isEmpty, map as _map } from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class CacheService {
    storage: StorageAdapter;

    constructor() {
        this.storage = storage();
    }

    get(key: string): Promise<any> {
        return this.storage.get(key);
    }

    /**
     * Set the value for the given key.
     * @param {any} key the key to identify this value
     * @param {any} value the value for this key
     * @returns {Promise} Returns a promise that resolves when the key and value are set
     */
    set(key: string, value: any): Promise<any> {
        return this.storage.set(key, value);
    }

    /**
     * Remove any value associated with this key.
     * @param {any} key the key to identify this value
     * @returns {Promise} Returns a promise that resolves when the value is removed
     */
    remove(key: string): Promise<any> {
        return this.storage.remove(key);
    }

    /**
     * Clear the entire key value store. WARNING: HOT!
     * @returns {Promise} Returns a promise that resolves when the store is cleared
     */
    clear(): Promise<void> {
        return this.storage.clear();
    }

    /**
     * Update firestore entries
     *
     * @param {string} cacheKey
     * @param {*} data
     * @returns
     * @memberof CacheService
     */
    async updateFirestoreEntries(cacheKey: string, data: any) {
        //
        // Get cached data
        const cachedData: any = await this.get(cacheKey);

        //
        // Update cached data
        cachedData.data = data;
        return await this.set(cacheKey, cachedData);
    }

    /**
     * Update firestore entry
     *
     * @param {string} cacheKey
     * @param {*} data
     * @param {string} searchKey
     * @returns
     * @memberof CacheService
     */
    async updateFirestoreEntry(cacheKey: string, data: any, searchKey: string) {
        //
        // Get cache
        const cachedData: any = await this.get(cacheKey);

        //
        // Validate
        if (!isEmpty(cachedData.data)) {
            //
            // Map entries and replace current program
            cachedData.data = _map(cachedData.data, row => {
                if (row[searchKey] === data[searchKey]) row = data;
                return row;
            });
        }

        //
        // Update cache
        return await this.set(cacheKey, cachedData);
    }
}

import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Collection, Records } from '@reative/records';
import { AxiosRequestConfig } from 'axios';
import { environment } from 'src/environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';
import { ActionStep } from '../../../../neuraboot-core/interfaces/action-step.interface';
import { ActionStepBackground } from '../../../../neuraboot-core/interfaces/action-step-background.interface';
import { ActionStepBackgrounds } from '../../../../neuraboot-core/fakes/action-step-background.fake';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { isEmpty, camelCase } from 'lodash';
import { IonSlides } from '@ionic/angular';

@Injectable()
@Collection({
    name: 'challenges',
    endpoint: '/api_action_step'
})
export class ActionStepService {
    public limit: number = 32;
    public entries: ActionStep[] = [];
    public backgrounds: ActionStepBackground = ActionStepBackgrounds;
    public selected: ActionStep = {};
    public slider: IonSlides;

    public $collection: Records;

    constructor(public auth: AuthService) {
        this.$collection.http((config: AxiosRequestConfig) => {
            config.headers = {
                Authorization: `Bearer ${auth.getUser().token}`,
                'accept-version': Version.get(environment.version)
            };
        });
    }

    /**
     * Randomize entries
     *
     * @param {ActionStep} entries
     * @returns
     * @memberof ActionStepService
     */
    randomize(entries: ActionStep[]) {
        //
        // Validate entries
        if (isEmpty(entries)) return [];

        //
        // Randomize entries
        let result = [];
        let randomIndex = 0;

        //
        // Set limit
        const limit = entries.length <= this.limit ? entries.length : this.limit;

        //
        // Set included indexes
        const includedIndexes: number[] = [];

        for (let index = 0; index < limit; index++) {
            //
            // Get a randon index
            randomIndex = this.generateRandom(entries.length, includedIndexes);

            //
            // Push to included indexes
            includedIndexes.push(randomIndex);

            //
            // Validate if index is not added yet
            result.push(entries /*.filter(i => i.link)*/[randomIndex]);
        }

        return result;
    }

    generateRandom(max, includedIndexes) {
        var num = Math.floor(Math.random() * max);
        return includedIndexes.includes(num) ? this.generateRandom(max, includedIndexes) : num;
    }

    /**
     * Get background image
     *
     * @param {string} category
     * @returns
     * @memberof ActionStepService
     */
    getBackgroundImage(category: string) {
        //
        // Validate
        if (isEmpty(category)) return null;

        //
        // Find background from category
        let categorySlug = camelCase(category);
        let result = this.backgrounds[categorySlug];

        return result ? 'url(' + result + ')' : null;
    }

    /**
     * Go to next slide
     *
     * @memberof ActionStepService
     */
    goToNextSlide() {
        this.slider.slideNext();
    }

    /**
     * Change selected action step
     *
     * @memberof ActionStepService
     */
    async changeSelectedActionStep() {
        //
        // Get active index
        const activeIndex: number = await this.slider.getActiveIndex();
        this.selected = this.entries[activeIndex];
    }

    /**
     * Is last slide
     *
     * @returns
     * @memberof ActionStepService
     */
    isLastSlide() {
        return this.slider.isEnd();
    }

    /**
     * Set entries
     *
     * @description Set entries to service
     * @param {*} entries
     * @memberof ActionStepService
     */
    setEntries(entries: ActionStep[]) {
        this.entries = entries;
        this.selected = entries[0];
    }

    /**
     * Get entries
     *
     * @description Get entries
     * @param {(string | boolean)} [category=false]
     * @returns
     * @memberof ActionStepService
     */
    getEntries(category: string | boolean = false, excludedCategories: string[] = [], feeling: string | boolean = false) {
        //
        // Get query
        const query = this.getQuery(category, excludedCategories, feeling);

        //
        // Return query
        return <any>this.$collection
            .cache(false)
            .network(true)
            .post('/find', query)
            .pipe(
                //
                // transform response
                map((actionSteps: ActionStep[]) => this.randomize(actionSteps)),

                //
                // set local entries
                map((entries: ActionStep[]) => {
                    return entries;
                }),

                take(1)
            );
    }

    /**
     * Get query
     *
     * @description Generate and get query
     * @param {(string | boolean)} [category=false]
     * @param {string[]} [excludedCategories=[]]
     * @returns
     * @memberof ActionStepService
     */
    getQuery(category: string | boolean = false, excludedCategories: string[] = [], feeling: string | boolean = false) {
        //
        // Initial query
        const query: any = {
            from: 0,
            size: 50,
            query: {
                bool: {
                    must: [
                        {
                            match: {
                                active: true
                            }
                        }
                    ],
                    must_not: [
                        {
                            exists: {
                                field: 'word'
                            }
                        }
                    ]
                }
            }
        };

        //
        // Filter by category
        if (category)
            query.query.bool.must.push({
                match: {
                    category: category
                }
            });

        //
        // Fitler by excluded categories
        if (excludedCategories) {
            excludedCategories.forEach(categoryRow => {
                query.query.bool.must_not.push({
                    match: {
                        category: categoryRow
                    }
                });
            });
        }

        //
        // Filter by feeling
        if (feeling && feeling !== 'happy') {
            query.query.bool.must.push({
                match: {
                    feeling: feeling
                }
            });
        } else {
            query.query.bool.must_not.push({
                match: {
                    feeling: 'depressed'
                }
            });

            query.query.bool.must_not.push({
                match: {
                    feeling: 'angry'
                }
            });

            query.query.bool.must_not.push({
                match: {
                    feeling: 'anxious'
                }
            });
        }

        return query;
    }
}

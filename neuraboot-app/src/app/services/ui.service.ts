import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { LoadingController, AlertController } from '@ionic/angular';
import { capitalize, startCase } from 'lodash';
import { Plugins } from '@capacitor/core';

const { Browser } = Plugins;

@Injectable({
    providedIn: 'root'
})
export class UiService {
    public swipeGesture: boolean = true;

    constructor(public snackBar: MatSnackBar, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {}

    /**
     * Say something to user
     *
     * @param {string} msg
     * @param {number} [time=10 * 1000]
     * @param {string} [okText='Ok']
     * @param {*} [verticalPosition='bottom']
     * @memberof UiService
     */
    say(msg: string, time: number = 10 * 1000, okText: string = 'Ok', verticalPosition: any = 'bottom') {
        this.snackBar.open(msg, okText, {
            duration: time,
            verticalPosition: verticalPosition
        });
    }

    /**
     * Ask something to user
     *
     * @param {string} msg
     * @param {{ label?: string, duration?: number, verticalPosition?: string, panelClass?: string }} options
     * @param {*} [callback=() => { }]
     * @memberof UiService
     */
    ask(
        msg: string,
        options: {
            label?: string;
            duration?: number;
            verticalPosition?: string;
            panelClass?: string;
        },
        callback: any = () => {}
    ) {
        let settings: any = {
            label: 'Yes',
            duration: 5000,
            verticalPosition: 'bottom',
            panelClass: ''
        };

        if (options.label) settings.label = options.label;
        if (options.duration) settings.duration = options.duration;
        if (options.verticalPosition) settings.verticalPosition = options.verticalPosition;
        if (options.panelClass) settings.panelClass = options.panelClass;

        let alert = this.snackBar.open(msg, settings.label, {
            duration: settings.duration,
            verticalPosition: settings.verticalPosition,
            panelClass: settings.panelClass
        });
        alert.onAction().subscribe(() => {
            callback();
        });
    }

    /**
     *
     *
     * @param {*} title
     * @param {string} [subtitle='']
     * @param {any[]} [buttons=['Ok']]
     * @returns
     * @memberof UiService
     */
    alert(title, subtitle: string = '', buttons: any[] = ['Ok'], enableBackdropDismiss: boolean = true, message: string = null) {
        //
        // available options from ionic
        //
        // header?: string;
        // subHeader?: string;
        // message?: string;
        // cssClass?: string | string[];
        // mode?: string;
        // inputs?: AlertInput[];
        // buttons?: (AlertButton | string)[];
        // enableBackdropDismiss?: boolean;
        // translucent?: boolean;
        return this.alertCtrl.create({
            header: title,
            subHeader: subtitle,
            message: message,
            buttons: buttons,
            backdropDismiss: enableBackdropDismiss
        });
    }

    // https://beta.ionicframework.com/docs/api/loading
    loader(duration: number = 3000, content: string = '', translucent: boolean = true) {
        return <any>this.loadingCtrl.create({
            message: content,
            duration: duration,
            translucent: translucent
        });
    }

    browser(url: string) {
        Browser.open({ url: url });
    }

    capitalizeString(value: string) {
        return capitalize(value);
    }

    startCaseString(value: string) {
        return startCase(value);
    }
}

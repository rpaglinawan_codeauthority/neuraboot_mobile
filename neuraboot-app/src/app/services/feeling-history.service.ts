import { Injectable } from '@angular/core';
import { Version } from '../../../../neuraboot-core/utils/version';
import { Collection, Records } from '@reative/records';
import { AuthService } from './auth.service';
import { AxiosRequestConfig } from 'axios';
import { environment } from 'src/environments/environment';

@Injectable()
@Collection({
    name: 'feeling-history',
    endpoint: '/api_feeling/history'
})
export class FeelingHistoryService {
    public $collection: Records;

    constructor(public auth: AuthService) {
        this.$collection.http((config: AxiosRequestConfig) => {
            config.headers = {
                Authorization: `Bearer ${auth.getUser().token}`,
                'accept-version': Version.get(environment.version)
            };
        });
    }
}

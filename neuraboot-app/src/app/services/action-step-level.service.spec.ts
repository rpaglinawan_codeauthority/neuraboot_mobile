import { TestBed } from '@angular/core/testing';

import { ActionStepLevelService } from './action-step-level.service';

describe('ActionStepLevelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActionStepLevelService = TestBed.get(ActionStepLevelService);
    expect(service).toBeTruthy();
  });
});

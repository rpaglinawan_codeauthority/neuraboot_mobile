import { TestBed } from '@angular/core/testing';

import { SubscriptionCodeService } from './subscription-code.service';

describe('SubscriptionCodeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubscriptionCodeService = TestBed.get(SubscriptionCodeService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Collection, Records } from '@reative/records';
import { AuthService } from './auth.service';
import { AxiosRequestConfig } from 'axios';
import { environment } from 'src/environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';

@Injectable()
@Collection({
    name: 'whoami-questions',
    endpoint: '/api_whoami'
})
export class QuestionService {
    public $collection: Records;

    constructor(public auth: AuthService) {
        this.$collection.http((config: AxiosRequestConfig) => {
            config.headers = {
                Authorization: `Bearer ${auth.getUser().token}`,
                'accept-version': Version.get(environment.version)
            };
        });
    }
}

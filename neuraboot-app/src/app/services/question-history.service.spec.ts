import { TestBed } from '@angular/core/testing';

import { QuestionHistoryService } from './question-history.service';

describe('QuestionHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuestionHistoryService = TestBed.get(QuestionHistoryService);
    expect(service).toBeTruthy();
  });
});

import { Injectable, ElementRef } from '@angular/core';
import { FeelingHistory } from '../../../../neuraboot-core/interfaces/feeling-history.interface';
import { FeelingCategory } from '../../../../neuraboot-core/interfaces/feeling-category.interface';
import { FeelingLevel } from '../../../../neuraboot-core/interfaces/feeling-level.interface';
import { FeelingCategoryOptions } from '../../../../neuraboot-core/constants/feeling-category-options';
import { FeelingLevelOptions } from '../../../../neuraboot-core/constants/feeling-level-options';
import { AuthService } from './auth.service';
import { CacheService } from './cache.service';
import { PopoverController } from '@ionic/angular';
import { UiService } from './ui.service';
import { FeelingHistoryService } from './feeling-history.service';
import { isEmpty, find, extend } from 'lodash';
import { Guid } from '../utils/guid';
import * as moment from 'moment';

@Injectable()
export class FeelingService {
    public load: boolean = true;
    public calculating: boolean = false;
    public segment: string = null;
    public forceActionStep: boolean = false;
    public actionStepCategory: any = false;

    public cacheSlug: string = 'feeling/';
    public cacheSlugForm: string = this.cacheSlug + 'form';
    public cacheSlugSegment: string = this.cacheSlug + 'segment';

    public current: FeelingHistory = {};
    public currentColor: string = 'default-default';

    public leaderLineStart: any;
    public leaderLineEnd: any;

    public elementJournal: ElementRef;
    public elementEllipse: ElementRef;

    public categories: FeelingCategory[] = FeelingCategoryOptions.slice(1);
    public levels: FeelingLevel[] = FeelingLevelOptions.slice(1);

    constructor(
        private auth: AuthService,
        private cache: CacheService,
        private popover: PopoverController,
        private ui: UiService,
        public feelingHistoryService: FeelingHistoryService
    ) {}

    /**
     * Restore from cache
     *
     * @description Restore form from cache
     * @memberof FeelingService
     */
    async restoreFromCache() {
        this.load = true;

        //
        // Force action step ?
        if (this.forceActionStep) {
            return this.changeSegment('action-step', true);
        }

        //
        // Set default segment
        else {
            this.changeSegment('mood-scale');
        }

        //
        // Wait a bit to start
        // @todo: improve so we can remove setTimeout
        setTimeout(() => {
            this.load = false;
        }, 1000);
    }

    /**
     * Get category label
     *
     * @returns
     * @memberof FeelingService
     */
    getCategoryLabel() {
        //
        // Validate
        if (isEmpty(this.current.category)) return;

        //
        // Select and return
        let category: FeelingCategory = find(this.categories, ['value', this.current.category]);
        return category.label;
    }

    /**
     * Get level label
     *
     * @returns
     * @memberof FeelingService
     */
    getLevelLabel() {
        //
        // Validate
        if (isEmpty(this.current.level)) return;

        //
        // Select and return
        let level: FeelingLevel = find(this.levels, ['value', this.current.level]);
        return level.label;
    }

    /**
     * Set selected point
     *
     * @param {*} point
     * @memberof FeelingService
     */
    async setSelectedPoint(point) {
        //
        // Set current
        this.current.category = point.xValue;
        this.current.level = point.yValue;
        this.current.point = point;

        //
        // Set new color
        this.currentColor = this.current.category + '-' + this.current.level;

        //
        // Update cache
        await this.cache.set(this.cacheSlugForm, this.current);
        return this.current;
    }

    /**
     * Start wizard
     *
     * @description Show popover with instructions on first access
     * @memberof FeelingService
     */
    async startWizard() {
        //
        // Validate
        if (this.auth.user.completed_feeling_wizard) return;

        //
        // Trigger first popover
        this.elementJournal.nativeElement.click();
    }

    /**
     * Valid form
     *
     * @param formName
     * @memberof FeelingService
     */
    validForm(formName: string = 'all') {
        //
        // Must have current
        if (isEmpty(this.current)) return false;

        //
        // Feeling form
        if (formName === 'feeling' || formName === 'all') {
            //
            // Must have category
            if (isEmpty(this.current.category)) return false;
        }

        //
        // Specific form validation
        if (formName === 'specific' || formName === 'all') {
            //
            // Must have at least one specific feeling
            if (isEmpty(this.current.specific)) return false;
        }

        return true;
    }

    /**
     * Save
     *
     * @param {boolean} [notify=false]
     * @returns
     * @memberof FeelingService
     */
    async save(notify: boolean = false) {
        return new Promise(async resolve => {
            //
            // Set calculating
            this.calculating = true;

            //
            // persists on db
            await this.persist(notify);

            //
            // Unset calculating
            this.calculating = false;

            //
            // Set action step segment
            this.changeSegment('action-step');

            return resolve(true);
        });
    }

    /**
     * Restart
     *
     * @description reset cache, properties and restore
     * @memberof FeelingService
     */
    async restart() {
        //
        // Reset form
        this.current = {};
        await this.cache.set(this.cacheSlugForm, this.current);

        //
        // Restore from cache to start the form again
        await this.restoreFromCache();

        //
        // Reset selected color
        this.currentColor = 'default-default';

        //
        // Reset segment
        this.segment = null;
        await this.cache.set(this.cacheSlugSegment, this.current);

        //
        // Remove leader line
        document.querySelector('.lines-container').innerHTML = '';
    }

    /**
     * Persists on database
     *
     * @param {boolean} [notify=false]
     * @returns
     * @memberof FeelingService
     */
    async persist(notify: boolean = false) {
        const newFeelingID = Guid.make(2);
        return await this.feelingHistoryService.$collection
            .set(
                newFeelingID,
                extend(this.current, <FeelingHistory>{
                    id: newFeelingID,
                    user: this.auth.user,
                    created_at: moment().toISOString(),
                    notifySupporters: notify
                })
            )
            .toPromise();
    }

    /**
     * Change segment
     *
     * @memberof FeelingService
     */
    changeSegment(segment, skipCache: boolean = false) {
        this.segment = segment;
        if (!skipCache) this.cache.set(this.cacheSlugSegment, this.segment);
    }
}

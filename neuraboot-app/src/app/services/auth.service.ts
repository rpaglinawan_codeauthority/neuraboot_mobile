import { Injectable } from '@angular/core';
import { MenuController, Platform, NavController } from '@ionic/angular';
import { CacheService } from './cache.service';
import { UiService } from './ui.service';
import { User } from '../../../../neuraboot-core/interfaces/user.interface';
import { ApiUserAuthResponse } from '../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
import { ApiErrorResponse } from '../../../../neuraboot-core/interfaces/api-error-response.interface';
import { environment } from '../../environments/environment';
import { Collection, Records } from '@reative/records';
import { setState, resetState, getState } from '@reative/state';
import { get, omit, isEmpty, cloneDeep } from 'lodash';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { Plugins } from '@capacitor/core';
import { HttpClient } from '@angular/common/http';
import { firebase, firestore } from '@reative/firebase';
const { FacebookLogin, GoogleAuth } = Plugins;

@Injectable({
    providedIn: 'root'
})
@Collection({
    name: 'users',
    endpoint: '/api_user'
})
export class AuthService {
    public $collection: Records;

    public loading: boolean = false;
    private _user: User = {};
    public omittedProperties: string[] = ['token', 'token_fcm', 'completed_wizard', 'completed_geolocation', 'completed_notification'];

    get user() {
        return omit(this._user, this.omittedProperties);
    }

    set user(instance) {
        this._user = instance;
        //
        // update user cache
        this.cache.set('user', instance);
    }

    constructor(
        private ui: UiService,
        private cache: CacheService,
        private menuCtrl: MenuController,
        private nav: NavController,
        private platform: Platform,
        private http: HttpClient
    ) { }

    /**
     * A way to get the full user object
     *
     * @returns
     * @memberof AuthService
     */
    getUser() {
        return this._user;
    }

    /**
     * A way to set user instance withou auto caching
     *
     * @param {User} instance
     * @memberof AuthService
     */
    setUser(instance: User) {
        this._user = instance;
    }

    /**
     * @param {string} property
     * @param {*} value
     * @memberof AuthService
     */
    async setUserProp(property: string, value: any) {
        this._user[property] = value;
        //
        // update user cache
        return this.cache.set('user', this._user);
    }

    async signOut() {
        //
        // Get current cache
        const wizardVideoState = this.cache.get('wizard:video');

        //
        // Close menu
        await this.menuCtrl.close();

        //
        // Clear cache
        this.cache.clear();

        //
        // Reset user status
        setState('user', { data: {} }, { merge: false });

        //
        // Clear state
        resetState();

        //
        // Clear user
        this.user = {};

        //
        // Set cache
        await this.cache.set('wizard:video', wizardVideoState);

        //
        // Go to account login
        await this.nav.navigateRoot(['/account-login']);

        //
        // Sign out from firebase
        firebase()
            .auth()
            .signOut();
    }

    /**
     *
     *
     * @param {string} email
     * @memberof AuthService
     */
    async forgot(email: string) {
        try {
            await firebase()
                .auth()
                .sendPasswordResetEmail(email);
            return true;
        } catch (err) {
            throw err;
        }
    }

    /**
     * @param {string} email
     * @returns {Promise<boolean>}
     * @memberof AuthService
     */
    changeEmail(email: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const reauth = () => {
                //
                // Sign out
                this.signOut();
                reject('Please login and try again');
            };
            return firebase().auth().currentUser
                ? firebase()
                    .auth()
                    .currentUser.updateEmail(email)
                    .then(r => {
                        firestore()
                            .collection('users')
                            .doc(this.user.uid)
                            .update({
                                updated_at: moment().toISOString(),
                                email: email
                            });
                        resolve(true);
                    })
                    .catch(async err => {
                        let msg;
                        console.log(err);
                        //
                        // Filter errors
                        switch (err.code) {
                            case 'auth/requires-recent-login':
                                reauth();
                                break;

                            default:
                                msg = 'Something went wrong';
                        }
                        reject(msg);
                    })
                : reauth();
        });
    }

    changeName(name: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const reauth = () => {
                //
                // Sign out
                this.signOut();
                reject('Please login and try again');
            };
            return firebase().auth().currentUser
                ? firebase()
                    .auth()
                    .currentUser.updateProfile({
                        displayName: name
                    })
                    .then(r => {
                        firestore()
                            .collection('users')
                            .doc(this.user.uid)
                            .update({
                                updated_at: moment().toISOString(),
                                display_name: name
                            });
                        resolve(true);
                    })
                    .catch(async err => {
                        let msg;
                        console.log(err);
                        //
                        // Filter errors
                        switch (err.code) {
                            case 'auth/requires-recent-login':
                                reauth();
                                break;

                            default:
                                msg = 'Something went wrong';
                        }
                        reject(msg);
                    })
                : reauth();
        });
    }

    /**
     * @param {string} password
     * @returns {Promise<boolean>}
     * @memberof AuthService
     */
    changePassword(password: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const reauth = () => {
                //
                // Sign out
                this.signOut();
                reject('Please login and try again');
            };
            return firebase().auth().currentUser
                ? firebase()
                    .auth()
                    .currentUser.updatePassword(password)
                    .then(r => resolve(true))
                    .catch(async err => {
                        let msg;
                        console.log(err);
                        //
                        // Filter errors
                        switch (err.code) {
                            case 'auth/weak-password':
                                msg = 'Password is too weak';
                                break;

                            case 'auth/requires-recent-login':
                                reauth();
                                break;

                            default:
                                msg = 'Something went wrong';
                        }
                        reject(msg);
                    })
                : reauth();
        });
    }

    /**
     * @param {any[]} [scope=['public_profile', 'email']]
     * @returns
     * @memberof AuthService
     */
    signInFacebook(scope: any[] = ['public_profile', 'email']) {
        return new Promise(async (resolve, reject) => {
            try {
                //
                // Check login status
                let facebookResponse = await FacebookLogin.getCurrentAccessToken().catch(reject);
                let token;

                //
                // Already connected to facebook, get token
                if (!isEmpty(facebookResponse)) {
                    token = facebookResponse;
                }

                //
                // Not connected to facebook, do login and get token
                else {
                    facebookResponse = await FacebookLogin.login({ permissions: scope });
                    token = await FacebookLogin.getCurrentAccessToken().catch(reject);
                }

                //
                // Get credential and sign in with firebase
                let credential = firebase().auth.FacebookAuthProvider.credential(token.accessToken.token);

                let firebaseResponse = await firebase()
                    .auth()
                    .signInWithCredential(credential)
                    .catch(reject);

                //
                // Set additional facebook params
                firebaseResponse.facebook_uid = firebaseResponse.providerData[0].uid;
                firebaseResponse.facebook_connected = true;

                resolve(firebaseResponse);
            } catch (err) {
                reject(err);
            }
        });
    }

    logoutFacebook() {
        return Plugins.FacebookLogin.logout();
    }

    /**
     * Sign in google
     *
     * @returns
     * @memberof AuthService
     */
    async signInGoogle() {
        return new Promise(async (resolve, reject) => {
            try {
                //
                // Login with google
                console.log('Plugins', Plugins);
                console.log('GoogleAuth', GoogleAuth);
                console.log('Plugins.GoogleAuth', Plugins.GoogleAuth);
                let googleResponse = await GoogleAuth.signIn();
                console.log('signInGoogle(): google response', googleResponse);
                const token = firebase().auth.GoogleAuthProvider.credential(googleResponse.authentication.idToken);
                console.log('signInGoogle(): token', token);
                //
                // Send credential to firebase
                let firebaseResponse = await firebase()
                    .auth()
                    .signInAndRetrieveDataWithCredential(
                        token
                    );

                console.log('signInGoogle(): firebaseResponse', firebaseResponse);

                //
                // Add additional google params
                firebaseResponse.user.google_uid = googleResponse.id;
                firebaseResponse.user.google_connected = true;

                return resolve(firebaseResponse.user);
            } catch (error) {
                console.log('error', error);
                return reject(error);
            }
        });
    }

    /*

    /**
     *
     *
     * @param {string} token
     * @returns
     * @memberof AuthService
     */
    signInWithCustomToken$(token: string) {
        return new Observable(observer => {
            firebase()
                .auth()
                .signInWithCustomToken(token)
                .then((r: any) => {
                    observer.next(r);
                    observer.complete();
                })
                .catch((err: any) => {
                    observer.error(err);
                    observer.complete();
                });
        });
    }

    /**
     * @private
     * @param {ApiUserAuthResponse[]} r
     * @returns
     * @memberof AuthService
     */
    public async setUserSession(r: ApiUserAuthResponse[]) {
        const user: User = cloneDeep(r[0].user);
        user.token = r[0].token.client;

        //
        // Set login data
        setState('user', { data: user }, { merge: false });

        //
        // Set login data
        this.user = user;

        return;
    }

    /**
     * @public
     * @param {ApiUserAuthResponse} r
     * @returns
     * @memberof AuthService
     */
    public authWithFirebase$(r: ApiUserAuthResponse) {
        return forkJoin(
            this.signInWithCustomToken$(r.token.auth).pipe(
                map(() => {
                    return r; // return previous result to next block since we don't need the result from signInWithCustomToken
                })
            )
        );
    }

    /**
     * @public
     * @param {ApiErrorResponse} err
     * @memberof AuthService
     */
    public async exception(err: ApiErrorResponse, loader: any = false) {
        //
        // Unset loaidng
        this.loading = false;
        if (loader) loader.dismiss();

        //
        // Show alert
        const alert = await this.ui.alert('Warning', err.message);
        await alert.present();
    }

    /**
     * Get firstname
     *
     * @description Get user firstname
     * @returns
     * @memberof AuthService
     */
    public getFirstname() {
        //
        // Hold display_name
        let firstname: string = this.user.display_name;

        //
        // Validate user
        if (!firstname || !firstname.length) return firstname;

        //
        // Return only firstname
        return firstname.split(' ')[0];
    }

    /**
     * Redirect after login
     *
     * @returns
     * @memberof AuthService
     */
    async redirectAfterLogin() {
        //
        // Has password?
        if (!this.user.completed_password)
            await this.nav.navigateForward(['/account-login-create-password'], { queryParams: { isNew: true } });
        //
        // Okay, go home
        else await this.nav.navigateRoot(['/home']);

        return;
    }

    /**
     * Get user web token to enable api
     * @deprecated
     *
     * @param {string} uid
     * @returns {Promise<string>}
     * @memberof AuthService
     */
    getToken(uid: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.http
                .post(environment.functions.baseUrl + '/api/token/' + uid, {})
                .subscribe((r: { token: string }) => resolve(r.token), reject);
        });
    }
}

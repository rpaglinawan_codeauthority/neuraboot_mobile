import { Injectable } from '@angular/core';
import { Collection, Records } from '@reative/records';
import { AuthService } from './auth.service';
import { UiService } from './ui.service';
import { AxiosRequestConfig } from 'axios';
import { environment } from 'src/environments/environment';
import { Version } from '../../../../neuraboot-core/utils/version';
import { ActionStep } from '../../../../neuraboot-core/interfaces/action-step.interface';
import { ActionStepHistory } from '../../../../neuraboot-core/interfaces/action-step-history.interface';
import { Guid } from '../utils/guid';
import { isEmpty } from 'lodash';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { CacheService } from './cache.service';
import { Observable } from 'rxjs';

@Injectable()
@Collection({
    name: 'challenge-history',
    endpoint: '/api_action_step/history'
})
export class ActionStepHistoryService {
    public active$: Observable<ActionStepHistory[]> = this.getLastActive();
    public $collection: Records;

    constructor(public auth: AuthService, public ui: UiService, public cache: CacheService) {
        this.$collection.http((config: AxiosRequestConfig) => {
            config.headers = {
                Authorization: `Bearer ${auth.getUser().token}`,
                'accept-version': Version.get(environment.version)
            };
        });
    }

    /**
     * Persist
     *
     *
     * @description Insert selected action step into history
     * @param {ActionStep} selected
     * @returns
     * @memberof ActionStepHistoryService
     */
    async persist(selected: ActionStep) {
        //
        // Generate new id
        const newID = Guid.make(2);

        //
        // Create insert data
        let data: ActionStepHistory = {
            id: newID,
            user: {
                uid: this.auth.user.uid,
                display_name: this.auth.user.display_name,
                email: this.auth.user.email
            },
            status: 'active',
            challenge: selected,
            created_at: moment().toISOString()
        };

        //
        // Add active_challenge to user
        this.auth.setUserProp('active_challenge', data);

        //
        // Insert and return
        return await this.$collection.post(data.id, data).toPromise();
    }

    /**
     * Get last active
     *
     * @description Get last active action step from history
     * @memberof ActionStepHistoryService
     */
    getLastActive() {
        return <any>this.$collection
            .where('status', '==', 'active')
            .where('user.uid', '==', this.auth.user.uid)
            .key('action-step-history/active')
            .findOne()
            .pipe(
                //
                // set local entries
                map((actionStep: ActionStep) => {
                    //
                    // Set local entry
                    const data: any = isEmpty(actionStep) ? null : actionStep;
                    this.auth.setUserProp('active_challenge', data);
                    return data;
                })
            );
    }

    /**
     * Clear active
     *
     * @description Clear active action step from service and cache
     * @returns
     * @memberof ActionStepHistoryService
     */
    async clearActive() {
        //
        // Remove active_challenge cache
        this.auth.setUserProp('active_challenge', null);

        //
        // Remove level cache
        await this.cache.remove('action-step-level/level');

        //
        // Remove charts cache
        await this.cache.remove('chart-week');
        await this.cache.remove('chart-month');
        await this.cache.remove('chart-year');

        //
        // Remove list cache
        await this.cache.remove('action-step-history/list/completed');
        await this.cache.remove('action-step-history/list/cancelled');

        return await this.cache.remove('action-step-history/active');
    }

    /**
     * Complete active
     *
     * @description Complete active action step
     * @returns
     * @memberof ActionStepHistoryService
     */
    async completeActive() {
        //
        // Set loader
        const loader = await this.ui.loader(60 * 1000);
        await loader.present();

        try {
            //
            // Mark as completed
            let result: any = await this.$collection
                .post('/complete', {
                    history: this.auth.getUser().active_challenge
                })
                .toPromise();
            console.log('completeActive(): result', result);
            //
            // Unset loader
            loader.dismiss();

            //
            // Clear active action step
            await this.clearActive();

            return result.data;
        } catch (error) {
            this.ui.say('Something went wrong, try again');

            //
            // Unset loader
            loader.dismiss();

            return false;
        }
    }

    /**
     * Cancel active
     *
     * @description Cancel active action step
     * @memberof ChallengeActiveListComponent
     */
    async cancelActive() {
        //
        // Set loader
        const loader = await this.ui.loader(60 * 1000);
        await loader.present();

        try {
            //
            // Mark as cancelled
            await this.$collection
                .post('/cancel', {
                    history: this.auth.getUser().active_challenge
                })
                .toPromise();

            //
            // Unset loader
            loader.dismiss();

            //
            // Clear active action step
            await this.clearActive();

            return true;
        } catch (error) {
            this.ui.say('Something went wrong, try again');

            //
            // Unset loader
            loader.dismiss();

            return false;
        }
    }
}

import { TestBed } from '@angular/core/testing';

import { FeelingHistoryService } from './feeling-history.service';

describe('FeelingHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeelingHistoryService = TestBed.get(FeelingHistoryService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { ActionStepService } from './action-step.service';

describe('ActionStepService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActionStepService = TestBed.get(ActionStepService);
    expect(service).toBeTruthy();
  });
});

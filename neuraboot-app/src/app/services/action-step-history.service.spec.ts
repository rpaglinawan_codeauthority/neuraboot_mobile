import { TestBed } from '@angular/core/testing';

import { ActionStepHistoryService } from './action-step-history.service';

describe('ActionStepHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActionStepHistoryService = TestBed.get(ActionStepHistoryService);
    expect(service).toBeTruthy();
  });
});

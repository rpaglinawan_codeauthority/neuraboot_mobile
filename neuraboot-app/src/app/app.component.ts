import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { AuthService } from './services/auth.service';
import { Version } from '../../../neuraboot-core/utils/version';
import { environment } from 'src/environments/environment';
import { UserService } from './services/user.service';
import { UiService } from './services/ui.service';
import { FCM } from 'capacitor-fcm';

const fcm = new FCM();

import { Plugins, StatusBarStyle } from '@capacitor/core';

const { StatusBar, SplashScreen, PushNotifications } = Plugins;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
    providers: [UserService]
})
export class AppComponent {
    public version: string = Version.get(parseInt(environment.version), true);

    constructor(private platform: Platform, public auth: AuthService, public user: UserService, public ui: UiService) {
        this.initializeApp();
    }

    showWalkthrough() {
        const modalElement = document.createElement('ion-modal');
        modalElement.component = 'how-it-works.page';
        modalElement.cssClass = 'hot-it-works.page';
    
        // present the modal
        document.body.appendChild(modalElement);
        return modalElement.present();
    }

    async initializeApp() {
        this.platform.ready().then(() => {
            //
            // Set status bar color
            if (this.platform.is('capacitor')) {
                StatusBar.setStyle({
                    style: StatusBarStyle.Dark
                });

                //
                // Hide splash
                SplashScreen.hide({
                    fadeOutDuration: 800
                });

                //
                // Subscribe to a specific topic
                PushNotifications.register()
                    .then(_ => {
                        fcm.subscribeTo({ topic: 'dailyActionStepTopic' })
                            .then(r => console.log(r))
                            .catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
            }
        });
    }
}

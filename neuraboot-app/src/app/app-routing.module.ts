import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule) },
    { path: 'account-login', loadChildren: () => import('./pages/account-login/account-login.module').then(m => m.AccountLoginPageModule) },
    {
        path: 'account-login-email',
        loadChildren: () => import('./pages/account-login-email/account-login-email.module').then(m => m.AccountLoginEmailPageModule)
    },
    { path: 'account', loadChildren: () => import('./pages/account/account.module').then(m => m.AccountPageModule) },
    {
        path: 'account-basic-info',
        loadChildren: () => import('./pages/account-basic-info/account-basic-info.module').then(m => m.AccountBasicInfoPageModule)
    },
    {
        path: 'account-password',
        loadChildren: () => import('./pages/account-password/account-password.module').then(m => m.AccountPasswordPageModule)
    },
    {
        path: 'account-login-social',
        loadChildren: () => import('./pages/account-login-social/account-login-social.module').then(m => m.AccountLoginSocialPageModule)
    },
    {
        path: 'account-login-create-password',
        loadChildren: () =>
            import('./pages/account-login-create-password/account-login-create-password.module').then(
                m => m.AccountLoginCreatePasswordPageModule
            )
    },
    {
        path: 'account-login-forgot-password',
        loadChildren: () =>
            import('./pages/account-login-forgot-password/account-login-forgot-password.module').then(
                m => m.AccountLoginForgotPasswordPageModule
            )
    },
    {
        path: 'account-login-register',
        loadChildren: () =>
            import('./pages/account-login-register/account-login-register.module').then(m => m.AccountLoginRegisterPageModule)
    },
    { path: 'my-profile', loadChildren: () => import('./pages/my-profile/my-profile.module').then(m => m.MyProfilePageModule) },
    { path: 'my-mood', loadChildren: () => import('./pages/my-mood/my-mood.module').then(m => m.MyMoodPageModule) },
    { path: 'my-progress', loadChildren: () => import('./pages/my-progress/my-progress.module').then(m => m.MyProgressPageModule) },
    { path: 'subscribe', loadChildren: './pages/subscribe/subscribe.module#SubscribePageModule' },
    { path: 'wizard', loadChildren: './pages/wizard/wizard.module#WizardPageModule' },
    { path: 'account-subscription', loadChildren: './pages/account-subscription/account-subscription.module#AccountSubscriptionPageModule' },
    { path: 'how-it-works', loadChildren: () => import('./pages/how-it-works/how-it-works.module').then(m => m.HowItWorksPageModule) },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]
})
export class AppRoutingModule { }

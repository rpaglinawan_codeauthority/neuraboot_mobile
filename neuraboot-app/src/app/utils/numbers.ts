
/**
 *
 *
 * @export
 * @class Numbers
 */
export class Numbers {
  static round(value: number, decimals: number): number {
    return Number(Math.round(parseFloat(value + 'e' + decimals)) + 'e-' + decimals);
  }
}
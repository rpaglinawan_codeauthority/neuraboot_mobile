import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { RecordsModule } from '@reative/angular';
import { FirebaseModule } from '@reative/firebase';
import { CacheModule } from '@reative/cache';
import { State, StateModule } from '@reative/state';
import { environment } from 'src/environments/environment';
import { MatSnackBarModule, MatIconModule, MatSlideToggleModule } from '@angular/material';
import { UserAvatarModule } from './modules/user-avatar/user-avatar.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxsModule } from '@ngxs/store';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        IonicModule.forRoot({
            swipeBackEnabled: false,
            scrollAssist: false,
            scrollPadding: false
        }),
        AppRoutingModule,
        MatSnackBarModule,
        MatIconModule,
        MatSlideToggleModule,
        FlexLayoutModule,
        UserAvatarModule,
        HttpClientModule,
        //
        // init rr for angular
        RecordsModule.forRoot({
            silent: environment.production,
            baseURL: environment.functions.baseUrl
        }),
        //
        // use rr for firebase
        FirebaseModule.forRoot({
            config: environment.firebase,
            persistence: true
        }),
        //
        // use rr cache
        CacheModule.forRoot({
            dbName: environment.name,
            dbStore: environment.store
        }),

        //
        // ngxs
        NgxsModule.forRoot([State], {
            developmentMode: false //!environment.production
        }),

        //
        // rr state
        StateModule.forRoot()
    ],
    providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
    bootstrap: [AppComponent]
})
export class AppModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalOverlayComponent } from './modal-overlay/modal-overlay.component';
import { ModalSkyComponent } from './modal-sky/modal-sky.component';
import { IonicModule } from '@ionic/angular';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
    declarations: [ModalOverlayComponent, ModalSkyComponent],
    imports: [CommonModule, IonicModule, FlexLayoutModule],
    exports: [IonicModule, ModalOverlayComponent]
})
export class ModalModule {}

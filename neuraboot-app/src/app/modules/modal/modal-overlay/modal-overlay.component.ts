import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { TimelineMax, Power1 } from 'gsap';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'modal-overlay',
    templateUrl: './modal-overlay.component.html',
    styleUrls: ['./modal-overlay.component.scss']
})
export class ModalOverlayComponent implements OnInit {
    @ViewChild('wrapper', { static: true }) public wrapper: ElementRef;
    @ViewChild('modalSky', { static: true, read: ElementRef }) public modalSky: ElementRef;

    @Input() public enableBackdropDismiss: boolean = false;
    @Input() public mode: string = 'default';

    public timeline: TimelineMax;
    public timelineSky: TimelineMax;

    constructor(public modal: ModalController) {}

    ngOnInit() {
        //
        // Apply animation for mode sky
        if (this.mode === 'sky') {
            this.timelineSky = new TimelineMax({ paused: true });
            this.timelineSky.set(this.modalSky.nativeElement, { autoAlpha: 0 });
            this.timelineSky.to(this.modalSky.nativeElement, 0.7, {
                ease: Power1.easeOut,
                autoAlpha: 1,
                onComplete: () => {}
            });

            this.timeline = new TimelineMax({ paused: true });
            this.timeline.set(this.wrapper.nativeElement, { autoAlpha: 0 });
            this.timeline.to(this.wrapper.nativeElement, 0.7, {
                ease: Power1.easeOut,
                autoAlpha: 1,
                onComplete: () => {}
            });

            this.timelineSky.play();
            this.timeline.play();
        }
    }

    /**
     * Close
     *
     * @memberof ModalOverlayComponent
     */
    close() {
        this.modal.dismiss();
    }

    /**
     * Backdrop dismiss
     *
     * @returns
     * @memberof ModalOverlayComponent
     */
    backdropDismiss() {
        //
        // Validate option
        if (!this.enableBackdropDismiss) return;

        //
        // Close
        this.close();
    }

    onClose(event: any = null) {
        //
        // Apply animation for mode sky
        if (this.mode === 'sky') {
            //
            // Reverse wrapper animation
            this.timelineSky.reverse();
            this.timeline.reverse();
            setTimeout(() => {
                //
                // Dismiss
                this.modal.dismiss(event);
            }, 1200);
        } else {
            //
            // Dismiss
            this.modal.dismiss(event);
        }
    }
}

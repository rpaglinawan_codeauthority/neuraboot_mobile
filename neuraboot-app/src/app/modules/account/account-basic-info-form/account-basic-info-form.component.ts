import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'account-basic-info-form',
    templateUrl: './account-basic-info-form.component.html',
    styleUrls: ['./account-basic-info-form.component.scss']
})
export class AccountBasicInfoFormComponent implements OnInit {
    public form: FormGroup;
    public loading: boolean = false;

    constructor(private fb: FormBuilder, private ui: UiService, private auth: AuthService) {}

    ngOnInit() {
        this.createForm();
    }

    /**
     * Create form
     */
    createForm() {
        let fields: any = {};

        fields.uid = [this.auth.user.uid];
        fields.display_name = [this.auth.user.display_name, [Validators.required]];
        fields.email = [this.auth.user.email];

        this.form = this.fb.group(fields);

        //
        // Disable email
        this.form.get('email').disable();
    }

    /**
     * Submit
     */
    async submit() {
        //
        // Set loading
        const loader = await this.ui.loader(60 * 1000);
        loader.present();

        //
        // Set name
        const name = this.form.get('display_name').value;

        //
        // Change name
        this.auth
            .changeName(name)
            .catch(async err => {
                const alert = await this.ui.alert('Error', err.message);

                loader.dismiss();

                if (err.code === 'expired') await this.auth.signOut();
                await alert.present();
            })
            .then(async result => {
                loader.dismiss();

                if (result) {
                    //
                    // Update cache and redirect
                    this.auth.setUserProp('display_name', name);

                    //
                    // Success msg
                    const alert: any = await this.ui.alert('Done', 'Basic info updated');
                    await alert.present();
                }
            });
    }
}

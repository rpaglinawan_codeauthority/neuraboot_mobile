import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController, ModalController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { CustomValidators } from 'ng2-validation';
import { forkJoin } from 'rxjs';
import { map, mergeMap, flatMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiUserAuthResponse } from '../../../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { Response } from '@reative/records';
import { AccountDisclaimerModalComponent } from '../account-disclaimer-modal/account-disclaimer-modal.component';

@Component({
    selector: 'account-register-form',
    templateUrl: './account-register-form.component.html',
    styleUrls: ['./account-register-form.component.scss'],
    providers: [UserService]
})
export class AccountRegisterFormComponent implements OnInit {
    public form: FormGroup;
    public loading: boolean = false;

    constructor(
        private fb: FormBuilder,
        public ui: UiService,
        public service: UserService,
        public auth: AuthService,
        public nav: NavController,
        private modal: ModalController
    ) {}

    async ngOnInit() {
        this.createForm();
    }

    createForm() {
        let fields: any = {};

        fields.display_name = ['', [Validators.required]];
        fields.email = ['', [Validators.required, CustomValidators.email]];
        fields.password = new FormControl('', [Validators.required, Validators.minLength(6)]);
        fields.confirmPassword = new FormControl('', [
            Validators.required,
            Validators.minLength(6),
            CustomValidators.equalTo(fields.password)
        ]);

        this.form = this.fb.group(fields);
    }

    async submit() {
        //
        // Create modal
        const modal = await this.modal.create({
            component: AccountDisclaimerModalComponent,
            componentProps: {},
            showBackdrop: false,
            animated: false
        });

        //
        // Present modal
        await modal.present();

        //
        // On dismiss
        await modal.onDidDismiss();

        const loader = await this.ui.loader(60 * 1000);
        loader.present();

        this.service.$collection
            .network(true)
            .cache(false)
            .key('user')
            .transform((data: Response) => data.data.user)
            .post('/auth/create', this.form.value)
            .pipe(
                //
                // auth within firebase
                mergeMap((r: ApiUserAuthResponse) => {
                    return forkJoin(
                        this.auth.signInWithCustomToken$(r.token.auth).pipe(
                            map(() => {
                                return r; // return previous result to next block since we do not need the result from signInWithCustomToken
                            })
                        )
                    );
                }),

                //
                // format user/token object and set to the session
                flatMap((r: ApiUserAuthResponse[]) => this.auth.setUserSession(r)),

                //
                // redirect to the first route
                map(async () => {
                    await loader.dismiss();
                    await this.nav.navigateRoot(['/home']);

                    this.ui.say(`Hi ${this.auth.user.display_name.split(' ')[0]}. Welcome to ${environment.title}!`);
                }),

                //
                // deal with exceptions
                catchError(async (err: ApiErrorResponse) => {
                    await loader.dismiss();
                    const alert = await this.ui.alert('Warning', err.message);
                    await alert.present();
                })
            )
            .toPromise();
    }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UiService } from 'src/app/services/ui.service';
import { NavController } from '@ionic/angular';
import { CustomValidators } from 'ng2-validation';
import { forkJoin, from, of } from 'rxjs';
import { map, mergeMap, tap, catchError, flatMap, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';
import { Response } from '@reative/records';
import { ApiUserAuthResponse } from '../../../../../../neuraboot-core/interfaces/api-user-auth-response.interface';
import { AuthService } from 'src/app/services/auth.service';
import { SubscriptionService } from 'src/app/services/subscription.service';
import { Subscription } from '../../../../../../neuraboot-core/interfaces/subscription.interface';
import { isEmpty } from 'lodash';
import { setState } from '@reative/state';

@Component({
    selector: 'account-login-form',
    templateUrl: './account-login-form.component.html',
    styleUrls: ['./account-login-form.component.scss'],
    providers: [UserService, SubscriptionService]
})
export class AccountLoginFormComponent implements OnInit {
    public form: FormGroup;
    public loading: boolean = false;

    constructor(
        private fb: FormBuilder,
        public ui: UiService,
        private nav: NavController,
        public user: UserService,
        public auth: AuthService,
        public subscription: SubscriptionService
    ) {}

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        let fields: any = {};

        fields.email = ['', [Validators.required, CustomValidators.email]];
        fields.password = ['', [Validators.required, Validators.minLength(6)]];

        this.form = this.fb.group(fields);
    }

    /**
     * Login form submission
     *
     * @memberof AccountLoginFormComponent
     */
    async submit() {
        this.loading = true;
        const loader = await this.ui.loader(60 * 1000);
        loader.present();

        //
        // Login user
        this.user.$collection
            .network(true)
            .cache(false)
            .key('user:login-response')
            .transform((data: Response) => data.data.user)
            .post('/auth/email', this.form.value)
            .pipe(
                //
                // auth within firebase
                mergeMap((r: ApiUserAuthResponse) => this.auth.authWithFirebase$(r)),

                //
                // format user/token object and set to the session
                flatMap((r: ApiUserAuthResponse[]) => this.auth.setUserSession(r)),

                //
                // After session
                tap(() => {
                    this.loginAuthorized(loader);
                }),

                //
                // deal with exceptions
                catchError(err => this.exception(err, loader))
            )
            .toPromise();
    }

    /**
     * Login authorized
     *
     * @description Hook after login was authorized
     * @param {*} loader
     * @memberof AccountLoginFormComponent
     */
    async loginAuthorized(loader) {
        //
        // Get subscription
        const subscription: Subscription = await this.subscription.$collection
            .cache(false)
            .network(true)
            .where('user.uid', '==', this.auth.user.uid)
            .findOne()
            .toPromise();

        //
        // Set subscription
        if (!isEmpty(subscription)) {
            //
            // Set subscription
            setState(
                'subscription',
                { data: subscription },
                {
                    merge: false
                }
            );
        }

        //
        // go to home
        this.nav.navigateRoot(['/home']);

        //
        // Remove loading
        loader.dismiss();
        this.loading = false;
    }

    exception(err, loader) {
        this.loading = false;
        return this.auth.exception(err, loader);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountLoginFormComponent } from './account-login-form/account-login-form.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatInputModule, MatSnackBarModule, MatCheckboxModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { AccountBasicInfoFormComponent } from './account-basic-info-form/account-basic-info-form.component';
import { AccountPasswordFormComponent } from './account-password-form/account-password-form.component';
import { AccountRegisterFormComponent } from './account-register-form/account-register-form.component';
import { AccountDisclaimerModalComponent } from './account-disclaimer-modal/account-disclaimer-modal.component';
import { ModalModule } from '../modal/modal.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        AccountLoginFormComponent,
        AccountBasicInfoFormComponent,
        AccountPasswordFormComponent,
        AccountRegisterFormComponent,
        AccountDisclaimerModalComponent
    ],
    imports: [
        CommonModule,
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatSnackBarModule,
        MatCheckboxModule,
        FormsModule,
        ReactiveFormsModule,
        CustomFormsModule,
        ModalModule
    ],
    exports: [
        AccountLoginFormComponent,
        AccountBasicInfoFormComponent,
        AccountPasswordFormComponent,
        AccountRegisterFormComponent,
        AccountDisclaimerModalComponent
    ],
    entryComponents: [AccountDisclaimerModalComponent]
})
export class AccountModule {}

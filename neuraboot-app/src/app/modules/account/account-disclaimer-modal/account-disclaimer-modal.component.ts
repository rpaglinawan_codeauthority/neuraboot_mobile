import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-account-disclaimer-modal',
    templateUrl: './account-disclaimer-modal.component.html',
    styleUrls: ['./account-disclaimer-modal.component.scss']
})
export class AccountDisclaimerModalComponent implements OnInit {
    public terms: boolean = false;

    constructor(public modal: ModalController) {}

    ngOnInit() {}

    close(result: any = {}) {
        //
        // Close modal
        this.modal.dismiss(result);
    }

    confirm() {
        this.close({
            success: true
        });
    }
}

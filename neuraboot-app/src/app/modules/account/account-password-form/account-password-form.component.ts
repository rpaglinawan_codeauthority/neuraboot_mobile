import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { CustomValidators } from 'ng2-validation';

@Component({
    selector: 'account-password-form',
    templateUrl: './account-password-form.component.html',
    styleUrls: ['./account-password-form.component.scss'],
    providers: [UserService]
})
export class AccountPasswordFormComponent implements OnInit {
    @Input() public redirect: boolean = false;
    @Input() public action: string = 'update';

    public form: FormGroup;
    public loading: boolean = false;

    constructor(private fb: FormBuilder, private ui: UiService, private auth: AuthService, public userService: UserService) {}

    ngOnInit() {
        this.createForm();
    }

    /**
     * Create form
     */
    createForm() {
        let fields: any = {};

        fields.password = new FormControl('', [Validators.required, Validators.minLength(6)]);
        fields.confirmPassword = new FormControl('', [
            Validators.required,
            Validators.minLength(6),
            CustomValidators.equalTo(fields.password)
        ]);

        this.form = this.fb.group(fields);
    }

    /**
     * Submit
     */
    async submit() {
        //
        // Set loading
        const loader = await this.ui.loader(60 * 1000);
        loader.present();

        //
        // Set password
        const password = this.form.get('password').value;

        this.auth
            .changePassword(password)
            .catch(async err => {
                loader.dismiss();

                if (err.code === 'expired') this.auth.signOut();

                const alert = await this.ui.alert('Warning', err.message ? err.message : err);
                await alert.present();
            })
            .then(async result => {
                loader.dismiss();

                if (result) {
                    //
                    // Update password flag
                    await this.auth.setUserProp('completed_password', true);

                    //
                    // Success msg
                    const alert = await this.ui.alert('Done', this.action === 'update' ? 'Password updated' : 'Password created');
                    await alert.present();

                    //
                    // Update user if new
                    if (this.action !== 'update') {
                        this.userService.$collection.set(this.auth.user.uid, this.auth.user).toPromise();
                    }

                    //
                    // Redirect ?
                    if (this.redirect) this.auth.redirectAfterLogin();
                }
            });
    }
}

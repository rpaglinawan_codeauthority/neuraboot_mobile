import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../../../../neuraboot-core/interfaces/user.interface';

@Component({
    selector: 'user-avatar',
    templateUrl: './user-avatar.component.html',
    styleUrls: ['./user-avatar.component.scss']
})
export class UserAvatarComponent implements OnInit {
    @Input() user: User = {};
    @Input() diameter: string = '50px';

    constructor() {}

    ngOnInit() {}
}

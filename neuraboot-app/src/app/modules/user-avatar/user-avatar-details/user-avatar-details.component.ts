import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'user-avatar-details',
    templateUrl: './user-avatar-details.component.html',
    styleUrls: ['./user-avatar-details.component.scss']
})
export class UserAvatarDetailsComponent implements OnInit {
    @Input() public logout: boolean = false;
    @Input() public fullname: boolean = false;

    constructor(public auth: AuthService) {}

    ngOnInit() {}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAvatarComponent } from './user-avatar/user-avatar.component';
import { MatCardModule, MatIconModule, MatButtonModule } from '@angular/material';
import { MomentModule } from 'ngx-moment';
import { UserAvatarDetailsComponent } from './user-avatar-details/user-avatar-details.component';

@NgModule({
    declarations: [UserAvatarComponent, UserAvatarDetailsComponent],
    imports: [CommonModule, MatCardModule, MatIconModule, MatButtonModule, MomentModule],
    exports: [UserAvatarComponent, UserAvatarDetailsComponent]
})
export class UserAvatarModule {}

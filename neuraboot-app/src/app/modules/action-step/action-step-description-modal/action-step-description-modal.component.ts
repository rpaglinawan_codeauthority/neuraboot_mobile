import { Component, OnInit, Input } from '@angular/core';
import { ActionStep } from '../../../../../../neuraboot-core/interfaces/action-step.interface';
import { UiService } from 'src/app/services/ui.service';
import { startCase } from 'lodash';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-action-step-description-modal',
    templateUrl: './action-step-description-modal.component.html',
    styleUrls: ['./action-step-description-modal.component.scss']
})
export class ActionStepDescriptionModalComponent implements OnInit {
    @Input() public actionStep: ActionStep = {};

    constructor(private ui: UiService, private modal: ModalController) {}

    ngOnInit() {}

    startCase(title: string) {
        return startCase(title);
    }

    openBrowser(link: string) {
        this.ui.browser(link);
    }

    close() {
        this.modal.dismiss();
    }
}

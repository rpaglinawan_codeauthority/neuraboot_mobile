import { Component, OnInit } from '@angular/core';
import { Segment } from '../../../../../../neuraboot-core/interfaces/segment.interface';
import { Chart } from '../../../../../../neuraboot-core/interfaces/chart.interface';
import { BarChartSetup } from '../../../../../../neuraboot-core/interfaces/bar-chart-setup.interface';
import { QueryUserChallengeCountDaysInWeek } from '../../../../../../neuraboot-core/queries/user-challenge-count-days-in-week';
import { QueryUserChallengeCountDaysInMonth } from '../../../../../../neuraboot-core/queries/user-challenge-count-days-in-month';
import { QueryUserChallengeCountMonthsInYear } from '../../../../../../neuraboot-core/queries/user-challenge-count-months-in-year';
import { AuthService } from 'src/app/services/auth.service';
import { isEmpty, find } from 'lodash';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import { ActionStepHistoryService } from 'src/app/services/action-step-history.service';

const moment = extendMoment(Moment);

@Component({
    selector: 'action-step-history-chart-group',
    templateUrl: './action-step-history-chart-group.component.html',
    styleUrls: ['./action-step-history-chart-group.component.scss'],
    providers: [ActionStepHistoryService]
})
export class ActionStepHistoryChartGroupComponent implements OnInit {
    public activeSegment: string = '';
    public segments: Segment[] = [
        {
            label: 'Week',
            slug: 'week'
        },
        {
            label: 'Month',
            slug: 'month'
        },
        {
            label: 'Year',
            slug: 'year'
        }
    ];

    public chartUpdate: any = false;
    public path: string = '/find';

    public weekGTE: any = moment()
        .startOf('isoWeek')
        .format('YYYY-MM-DD');

    public weekLTE: any = moment()
        .endOf('isoWeek')
        .format('YYYY-MM-DD');

    public weekTempData: Chart[] = [];
    public weekSetup: BarChartSetup = {
        service: this.actionStepHistory,
        query: QueryUserChallengeCountDaysInWeek(this.auth.user.uid, this.weekGTE, this.weekLTE),
        cacheKey: 'chart-week',
        postHook: this.postHookWeek.bind(this)
    };

    public monthGTE: any = moment()
        .startOf('month')
        .format('YYYY-MM-DD');

    public monthLTE: any = moment()
        .endOf('month')
        .format('YYYY-MM-DD');

    public monthTempData: Chart[] = [];
    public monthSetup: BarChartSetup = {
        service: this.actionStepHistory,
        query: QueryUserChallengeCountDaysInMonth(this.auth.user.uid, this.monthGTE, this.monthLTE),
        cacheKey: 'chart-month',
        postHook: this.postHookMonth.bind(this)
    };

    public yearGTE: any = moment()
        .startOf('year')
        .format('YYYY-MM-DD');

    public yearLTE: any = moment()
        .endOf('year')
        .format('YYYY-MM-DD');

    public yearTempData: Chart[] = [];
    public yearSetup: BarChartSetup = {
        service: this.actionStepHistory,
        query: QueryUserChallengeCountMonthsInYear(this.auth.user.uid, this.yearGTE, this.yearLTE),
        cacheKey: 'chart-year',
        postHook: this.postHookYear.bind(this)
    };

    constructor(public actionStepHistory: ActionStepHistoryService, public auth: AuthService) {
        //
        // Generate temp data for dates
        this.generateTempWeekData();
        this.generateTempMonthData();
        this.generateTempYearData();
    }

    ngOnInit() {
        setTimeout(() => {
            this.changeSegment('week');
        }, 600);
    }

    /**
     * Change segment
     *
     * @param {string} segment
     * @memberof ActionStepHistoryChartGroupComponent
     */
    changeSegment(segment: string) {
        //
        // Change segment
        this.activeSegment = segment;
    }

    /**
     * Post hook week
     *
     * @description Post hook to fill week zero value dates
     * @param {Chart[]} data
     * @returns
     * @memberof ActionStepHistoryChartGroupComponent
     */
    postHookWeek(data: Chart[]) {
        //
        // Merge
        let temp = [];
        this.weekTempData.forEach((row: Chart) => {
            //
            // Look for value in this date
            let o: Chart = find(data, ['name', row.name]);

            //
            // Insert value if found
            if (!isEmpty(o)) row.value = o.value;

            //
            // Push to temp
            temp.push(row);
        });

        return temp;
    }

    /**
     * Post hook month
     *
     * @description Post hook to fill month zero value dates
     * @param {Chart[]} data
     * @returns
     * @memberof ActionStepHistoryChartGroupComponent
     */
    postHookMonth(data: Chart[]) {
        //
        // Merge
        let temp = [];
        this.monthTempData.forEach((row: Chart) => {
            //
            // Look for value in this date
            let o: Chart = find(data, ['name', row.name]);

            //
            // Insert value if found
            if (!isEmpty(o)) row.value = o.value;

            //
            // Push to temp
            temp.push(row);
        });

        return temp;
    }

    /**
     * Post hook year
     *
     * @description Post hook to fill year zero value dates
     * @param {Chart[]} data
     * @returns
     * @memberof ActionStepHistoryChartGroupComponent
     */
    postHookYear(data: Chart[]) {
        //
        // Merge
        let temp = [];
        this.yearTempData.forEach((row: Chart) => {
            //
            // Look for value in this date
            let o: Chart = find(data, ['name', row.name]);

            //
            // Insert value if found
            if (!isEmpty(o)) row.value = o.value;

            //
            // Push to temp
            temp.push(row);
        });

        return temp;
    }

    /**
     * Generate temp week data
     *
     * @description Generate temp week data to fill zero value dates and merge with real data
     * @returns
     * @memberof ActionStepHistoryChartGroupComponent
     */
    generateTempWeekData() {
        //
        // Set range and generate days
        const range = moment.range(this.weekGTE, this.weekLTE);
        const days = Array.from(range.by('day'));

        //
        // Map data and generate days
        let temp: Chart[] = [];
        days.forEach((m: any) => {
            temp.push({
                name: m.format('ddd'),
                value: 0
            });
        });

        return (this.weekTempData = temp);
    }

    /**
     * Generate temp month data
     *
     * @description Generate temp month data to fill zero value dates and merge with real data
     * @returns
     * @memberof ActionStepHistoryChartGroupComponent
     */
    generateTempMonthData() {
        //
        // Set range and generate days
        const range = moment.range(this.monthGTE, this.monthLTE);
        const days = Array.from(range.by('day'));

        //
        // Map data and generate days
        let temp: Chart[] = [];
        days.forEach((m: any) => {
            temp.push({
                name: m.format('DD MMM'),
                value: 0
            });
        });

        return (this.monthTempData = temp);
    }

    /**
     * Generate temp year data
     *
     * @description Generate temp year data to fill zero value dates and merge with real data
     * @returns
     * @memberof ActionStepHistoryChartGroupComponent
     */
    generateTempYearData() {
        //
        // Set range and generate days
        const range = moment.range(this.yearGTE, this.yearLTE);
        const days = Array.from(range.by('month'));

        //
        // Map data and generate days
        let temp: Chart[] = [];
        days.forEach((m: any) => {
            temp.push({
                name: m.format('MMM'),
                value: 0
            });
        });

        return (this.yearTempData = temp);
    }

    /**
     * Set chart update
     *
     * @description Sets chart update method to current component
     * @param event
     * @memberof ActionStepHistoryChartGroupComponent
     */
    setChartUpdate(event) {
        this.chartUpdate = event.update;
    }
}

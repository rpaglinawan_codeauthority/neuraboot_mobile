import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionStepSliderComponent } from './action-step-slider/action-step-slider.component';
import { ModalModule } from '../modal/modal.module';
import { IonicModule } from '@ionic/angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ActionStepDescriptionModalComponent } from './action-step-description-modal/action-step-description-modal.component';
import { MatButtonModule, MatIconModule, MatTabsModule } from '@angular/material';
import { ChartModule } from '../chart/chart.module';
import { ActionStepHistoryChartGroupComponent } from './action-step-history-chart-group/action-step-history-chart-group.component';
import { ActionStepHistoryListComponent } from './action-step-history-list/action-step-history-list.component';
import { MomentModule } from 'ngx-moment';

@NgModule({
    declarations: [
        ActionStepSliderComponent,
        ActionStepDescriptionModalComponent,
        ActionStepHistoryChartGroupComponent,
        ActionStepHistoryListComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        MatButtonModule,
        MatIconModule,
        MatTabsModule,
        FlexLayoutModule,
        ModalModule,
        ChartModule,
        MomentModule
    ],
    exports: [
        ActionStepSliderComponent,
        ActionStepDescriptionModalComponent,
        ActionStepHistoryChartGroupComponent,
        ActionStepHistoryListComponent
    ],
    entryComponents: [ActionStepDescriptionModalComponent]
})
export class ActionStepModule {}

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { ActionStepHistory } from '../../../../../../neuraboot-core/interfaces/action-step-history.interface';
import { ActionStepHistoryService } from 'src/app/services/action-step-history.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { isEmpty, kebabCase } from 'lodash';
import { ActionStepService } from 'src/app/services/action-step.service';

@Component({
    selector: 'action-step-history-list',
    templateUrl: './action-step-history-list.component.html',
    styleUrls: ['./action-step-history-list.component.scss'],
    providers: [ActionStepService, ActionStepHistoryService]
})
export class ActionStepHistoryListComponent implements OnInit {
    @Output() public setUpdate: EventEmitter<any> = new EventEmitter();
    @Input() public filter: string;
    @Input() public date: string = null;
    @Input() public showDate: boolean = true;
    @Input() public showHeader: boolean = true;
    @Input() public category: string = null;
    @Input() public limit: number = 5;

    public path: string = '/find';
    public noData: boolean = false;
    public entries: ActionStepHistory[] = [];
    public entries$: Subscription;
    public query: any = {};

    constructor(
        public actionStepService: ActionStepService,
        public actionStepHistoryService: ActionStepHistoryService,
        public auth: AuthService,
        public ui: UiService
    ) {}

    ngOnInit() {
        //
        // Set update
        this.setUpdate.emit({
            update: this.update.bind(this)
        });

        //
        // Set entries
        this.entries$ = this.getEntries().subscribe();
    }

    ngOnDestroy() {
        this.entries$.unsubscribe();
    }

    /**
     * Get entries
     *
     * @description Get entries
     * @returns
     * @memberof ActionStepHistoryListComponent
     */
    getEntries() {
        //
        // Set query
        this.setQuery();

        //
        // Get entries
        return <any>this.actionStepHistoryService.$collection
            .key('action-step-history/list/' + this.filter)
            .network(true)
            .post(this.path + '/?size=' + this.limit + '&sort=created_at:desc', this.query)
            .pipe(
                //
                // Set entries
                map((entries: ActionStepHistory[]) => {
                    //
                    // Validate entries
                    if (isEmpty(entries)) {
                        this.noData = true;
                    } else {
                        this.noData = false;
                    }

                    //
                    // Set entries
                    this.entries = entries;
                    return entries;
                })
            );
    }

    /**
     * Update
     *
     * @description Update entries
     * @memberof ActionStepHistoryListComponent
     */
    update() {
        this.entries$.unsubscribe();
        this.entries$ = this.getEntries().subscribe();
    }

    setQuery() {
        //
        // Default query
        const query: any = {
            query: {
                bool: {
                    must: [
                        {
                            match: {
                                'user.uid': this.auth.user.uid
                            }
                        }
                    ]
                }
            }
        };

        //
        // Set filter
        if (this.filter) {
            query.query.bool.must.push({
                match: {
                    status: this.filter
                }
            });
        }

        //
        // Set category
        if (this.category) {
            query.query.bool.must.push({
                match: {
                    'challenge.category': kebabCase(this.category)
                }
            });
        }

        //
        // Set range
        if (this.date) {
            query.query.bool.must.push({
                range: {
                    created_at: {
                        gte: this.date,
                        lte: this.date
                    }
                }
            });
        }

        //
        // Set query
        this.query = query;
        return this.query;
    }
}

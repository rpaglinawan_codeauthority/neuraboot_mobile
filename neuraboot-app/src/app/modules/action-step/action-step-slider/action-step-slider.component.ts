import { Component, OnInit } from '@angular/core';
import { ActionStep } from '../../../../../../neuraboot-core/interfaces/action-step.interface';
import { startCase } from 'lodash';
import { ActionStepService } from 'src/app/services/action-step.service';

@Component({
    selector: 'action-step-slider',
    templateUrl: './action-step-slider.component.html',
    styleUrls: ['./action-step-slider.component.scss'],
    providers: []
})
export class ActionStepSliderComponent implements OnInit {
    public entries: ActionStep[] = [];
    public slideOpts = {
        centeredSlides: true,
        slidesPerView: 1,
        allowTouchMove: false
    };

    constructor(public actionStep: ActionStepService) {}

    ngOnInit() {}

    /**
     * On change slide
     *
     * @param {*} event
     * @memberof ActionStepSliderComponent
     */
    onChangeSlide(event) {
        //
        // Change selected action step
        this.actionStep.changeSelectedActionStep();
    }

    /**
     * On load slider
     *
     * @param {*} event
     * @memberof ActionStepSliderComponent
     */
    onLoadSlider(event) {
        this.actionStep.slider = event.target;
    }

    startCase(title: string) {
        return startCase(title);
    }
}

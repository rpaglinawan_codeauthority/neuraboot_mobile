import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressMoodDailyResumeModalComponent } from './progress-mood-daily-resume-modal.component';

describe('ProgressMoodDailyResumeModalComponent', () => {
  let component: ProgressMoodDailyResumeModalComponent;
  let fixture: ComponentFixture<ProgressMoodDailyResumeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressMoodDailyResumeModalComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressMoodDailyResumeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

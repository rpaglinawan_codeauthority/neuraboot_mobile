import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalOverlayComponent } from '../../modal/modal-overlay/modal-overlay.component';

@Component({
    selector: 'app-progress-mood-daily-resume-modal',
    templateUrl: './progress-mood-daily-resume-modal.component.html',
    styleUrls: ['./progress-mood-daily-resume-modal.component.scss']
})
export class ProgressMoodDailyResumeModalComponent implements OnInit {
    @ViewChild(ModalOverlayComponent, { static: true }) public modalOverlay: ModalOverlayComponent;

    @Input() public date: string = null;

    constructor() {}

    ngOnInit() {}

    close() {
        this.modalOverlay.onClose();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressHeartChartComponent } from './progress-heart-chart/progress-heart-chart.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule, MatButtonModule } from '@angular/material';
import { ProgressLevelResumeModalComponent } from './progress-level-resume-modal/progress-level-resume-modal.component';
import { ModalModule } from '../modal/modal.module';
import { IonicModule } from '@ionic/angular';
import { MoodModule } from '../mood/mood.module';
import { ActionStepModule } from '../action-step/action-step.module';
import { ProgressMoodDailyResumeModalComponent } from './progress-mood-daily-resume-modal/progress-mood-daily-resume-modal.component';
import { MomentModule } from 'ngx-moment';

@NgModule({
    declarations: [ProgressHeartChartComponent, ProgressLevelResumeModalComponent, ProgressMoodDailyResumeModalComponent],
    imports: [
        CommonModule,
        FlexLayoutModule,
        MatIconModule,
        MatButtonModule,
        ModalModule,
        IonicModule,
        MoodModule,
        ActionStepModule,
        MomentModule
    ],
    exports: [
        ProgressHeartChartComponent,
        ProgressLevelResumeModalComponent,
        ProgressMoodDailyResumeModalComponent,
        MoodModule,
        ActionStepModule
    ],
    entryComponents: [ProgressLevelResumeModalComponent, ProgressMoodDailyResumeModalComponent]
})
export class ProgressModule {}

import { Component, OnInit, Input } from '@angular/core';
import { isEmpty } from 'lodash';

export interface Hearts {
    filled: boolean;
    delay: string;
    i: number;
}

@Component({
    selector: 'progress-heart-chart',
    templateUrl: './progress-heart-chart.component.html',
    styleUrls: ['./progress-heart-chart.component.scss']
})
export class ProgressHeartChartComponent implements OnInit {
    @Input() public count: number = 0;
    @Input() public size: number = 10;
    @Input() public color: string = 'primary';
    public hearts: Hearts[] = [];

    constructor() {}

    ngOnInit() {}

    ngOnChanges(changes) {
        if (changes.count.currentValue || changes.count.currentValue === 0) this.generateHeartArray();
    }

    generateHeartArray() {
        //
        // Set initial dealy
        let delay: number = 0;
        let generated: boolean = true;

        //
        // Loop size to create array
        for (let i = 0; i < this.size; i++) {
            //
            // Set generated
            if (i === 0 && isEmpty(this.hearts)) {
                generated = false;
            }

            //
            // Set filled
            let filled: boolean = false;
            if (i < this.count) filled = true;

            //
            // Create if not generated
            if (!generated) {
                //
                // Push to hearts
                this.hearts.push({
                    filled: filled,
                    delay: delay + 's',
                    i: i
                });

                //
                // increase delay
                delay += 0.07;
            }

            //
            // Already generated, just update
            else {
                this.hearts[i].filled = filled;
            }
        }
    }
}

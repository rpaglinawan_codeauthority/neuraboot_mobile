import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActionStepLevelService } from 'src/app/services/action-step-level.service';
import { UiService } from 'src/app/services/ui.service';
import { ActionStepLevel } from '../../../../../../neuraboot-core/interfaces/action-step-level.interface';
import { ModalOverlayComponent } from '../../modal/modal-overlay/modal-overlay.component';

@Component({
    selector: 'app-progress-level-resume-modal',
    templateUrl: './progress-level-resume-modal.component.html',
    styleUrls: ['./progress-level-resume-modal.component.scss'],
    providers: [ActionStepLevelService]
})
export class ProgressLevelResumeModalComponent implements OnInit {
    @ViewChild(ModalOverlayComponent, { static: true }) public modalOverlay: ModalOverlayComponent;

    @Input() public level: ActionStepLevel = {};
    @Input() public category: string = null;

    public selectedLevel: any = {};

    constructor(public modal: ModalController, public actionStepLevel: ActionStepLevelService, public ui: UiService) {}

    ngOnInit() {
        //
        // Validate category
        if (!this.category) return;

        //
        // Set category levels
        this.setCategoryLevels();
    }

    /**
     * Set category levels
     *
     * @description Set category specific data
     * @memberof ProgressLevelResumeModalComponent
     */
    setCategoryLevels() {
        //
        // Category specific data
        this.selectedLevel.progress = this.level[`${this.category}_progress`];
        this.selectedLevel.attempted = this.level[`${this.category}_attempted`];
        this.selectedLevel.level = this.level[`${this.category}_level`];
        this.selectedLevel.total = this.level[`${this.category}_total`];
        this.selectedLevel._streak_day = this.level[`${this.category}_streak_day`];
        this.selectedLevel._streak_max = this.level[`${this.category}_streak_max`];
    }

    close() {
        this.modalOverlay.onClose();
    }
}

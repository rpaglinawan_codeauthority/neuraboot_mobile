import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionStepDisplayComponent } from './action-step-display/action-step-display.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule, MatButtonModule, MatMenuModule } from '@angular/material';
import { MomentModule } from 'ngx-moment';

@NgModule({
    declarations: [ActionStepDisplayComponent],
    imports: [CommonModule, FlexLayoutModule, MatIconModule, MatButtonModule, MatMenuModule, MomentModule],
    exports: [ActionStepDisplayComponent]
})
export class ActionStepDisplayModule {}

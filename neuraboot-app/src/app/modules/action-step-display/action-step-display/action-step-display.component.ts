import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { UiService } from 'src/app/services/ui.service';
import { ActionStepHistoryService } from 'src/app/services/action-step-history.service';
import { ActionStepLevel } from '../../../../../../neuraboot-core/interfaces/action-step-level.interface';
import { Subscription } from 'rxjs';
import { startCase } from 'lodash';

@Component({
    selector: 'action-step-display',
    templateUrl: './action-step-display.component.html',
    styleUrls: ['./action-step-display.component.scss'],
    providers: [ActionStepHistoryService]
})
export class ActionStepDisplayComponent implements OnInit {
    @Output() public onToggle: EventEmitter<any> = new EventEmitter();

    public actionStepHistoryActive$: Subscription;
    public closed: boolean = true;
    public modalOverlaySky: boolean = false;

    constructor(
        public actionStepHistory: ActionStepHistoryService,
        public auth: AuthService,
        private nav: NavController,
        public ui: UiService
    ) {}

    ngOnInit() {
        this.getActiveActionStep();
    }

    ngOnDestroy() {
        //
        // destroy listener
        this.actionStepHistoryActive$.unsubscribe();
    }

    /**
     * Get active action step
     *
     * @description Get current active action step
     * @memberof ActionStepDisplayComponent
     */
    async getActiveActionStep() {
        //
        // Get active action step
        this.actionStepHistoryActive$ = this.actionStepHistory.active$.subscribe();
    }

    /**
     * Start case
     *
     * @description make title start case
     * @param {string} title
     * @returns
     * @memberof ActionStepDisplayComponent
     */
    startCase(title: string) {
        return startCase(title);
    }

    /**
     * New action step
     *
     * @description Redirect to my mood page to get a new action step
     * @memberof ActionStepDisplayComponent
     */
    newChallenge() {
        this.nav.navigateForward(['/my-mood/challenge']);
    }

    /**
     * Complete active
     *
     * @description Complete active action step
     * @returns
     * @memberof ActionStepDisplayComponent
     */
    async completeActive() {
        //
        // Complete
        const data: ActionStepLevel = await this.actionStepHistory.completeActive();

        //
        // Level up ?
        if (data.levelUp === true) {
            //
            // Show level up directive
            // this.modalOverlaySky = true;
        }

        //
        // Send event
        this.onToggle.emit({
            action: 'complete'
        });
    }

    /**
     * Cancel active
     *
     * @description Cancel active action step
     * @memberof ActionStepDisplayComponent
     */
    async cancelActive() {
        //
        // Complete
        await this.actionStepHistory.cancelActive();

        //
        // Send event
        this.onToggle.emit({
            action: 'cancel'
        });
    }

    /**
     * Open brownser
     *
     * @description Open link in browser
     * @param {string} link
     * @memberof ActionStepDisplayComponent
     */
    openBrowser(link: string) {
        this.ui.browser(link);
    }
}

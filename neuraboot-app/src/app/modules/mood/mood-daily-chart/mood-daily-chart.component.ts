import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { Chart } from '../../../../../../neuraboot-core/interfaces/chart.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { QueryUserFeelingCountByType } from '../../../../../../neuraboot-core/queries/user-feeling-count-by-type';
import { Subscription, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { isEmpty, forEach, merge } from 'lodash';
import * as moment from 'moment-timezone';
import { Response } from '@reative/records';

moment.tz.setDefault(moment.tz.guess());

@Component({
    selector: 'mood-daily-chart',
    templateUrl: './mood-daily-chart.component.html',
    styleUrls: ['./mood-daily-chart.component.scss'],
    providers: [FeelingHistoryService]
})
export class MoodDailyChartComponent implements OnInit {
    public loading: boolean = false;
    public noData: boolean = false;
    public data$: Subscription;
    public categoryConfig: any = {
        happy: { key: 0, color: 'rgba(255, 174, 0, 1)' /*'#ffffff'*/ },
        anxious: { key: 1, color: 'rgba(131, 48, 162, 1)' /*'#c9faf8'*/ },
        depressed: { key: 2, color: 'rgba(28, 189, 253, 1)' /*'#9afffa'*/ },
        angry: { key: 3, color: 'rgba(116, 100, 103, 1)' /*'#37dcd7'*/ }
    };

    @ViewChild('chart', { static: false }) public chart: any;
    @ViewChild('chartIcons', { static: true }) public chartIcons: ElementRef;

    @Input() public path: string = '/find';
    @Input() public date: string = null;
    @Input() public showXAxis = true;
    @Input() public showYAxis = true;
    @Input() public gradient = false;
    @Input() public showLegend = false;
    @Input() public showXAxisLabel = false;
    @Input() public xAxisLabel = null;
    @Input() public showYAxisLabel = false;
    @Input() public yAxisLabel = null;
    @Input() public colorScheme = {
        domain: [
            this.categoryConfig.happy.color,
            this.categoryConfig.anxious.color,
            this.categoryConfig.depressed.color,
            this.categoryConfig.angry.color
        ]
    };
    @Input() public data: Chart[] = [
        {
            name: 'happy',
            value: 0
        },
        {
            name: 'anxious',
            value: 0
        },
        {
            name: 'depressed',
            value: 0
        },
        {
            name: 'angry',
            value: 0
        }
    ];

    constructor(private ui: UiService, public feelingHistory: FeelingHistoryService, public auth: AuthService) {}

    ngOnInit() {
        //
        // Set loading
        this.loading = true;

        //
        // Set data
        this.setData();
    }

    ngOnDestroy() {
        this.data$.unsubscribe();
    }

    /**
     * y format
     *
     * @description Format y axis value
     * @param {*} value
     * @returns
     * @memberof MoodDailyChartComponent
     */
    yFormat(value) {
        return value - Math.floor(value) !== 0 ? '' : value;
    }

    xFormat() {
        return '';
    }

    setData() {
        this.data$ = <any>this.feelingHistory.$collection
            .key('feeling-history/day/' + this.date)
            .raw(true)
            .post(this.path, QueryUserFeelingCountByType(this.auth.user.uid, this.date))
            .pipe(
                //
                // transform response
                map((response: Response) => {
                    //
                    // Set data
                    const agg = response.data.aggregations.group.buckets;

                    //
                    // Validate data
                    if (isEmpty(agg)) {
                        this.noData = true;
                        return agg;
                    } else {
                        this.noData = false;
                    }

                    return agg;
                }),

                //
                // Set chart
                tap((agg: any) => {
                    //
                    // Result
                    const chartData: Chart[] = merge([], this.data);

                    //
                    // Go into each date and set data
                    forEach(agg, (row: any) => {
                        //
                        // Set data key
                        const key = this.categoryConfig[row.key].key;

                        //
                        // Replace count inside data
                        chartData[key].value = row.doc_count;
                    });

                    //
                    // Set data
                    this.data = chartData;
                    this.loading = false;

                    //
                    // Adjust icons
                    this.adjustChartIcons();
                }),

                //
                // Handle errors
                catchError((err: ApiErrorResponse | any) => {
                    const message: string = err.message ? err.message : err;

                    this.ui.say(message);
                    return of(err);
                })
            )
            .subscribe();
    }

    /**
     * Adjust chart icons
     *
     * @memberof MoodDailyChartComponent
     */
    adjustChartIcons() {
        setTimeout(() => {
            //
            // Get chart dimensions
            let chartSize: any = this.chart.chartElement.nativeElement
                .querySelector('[ngx-charts-series-vertical]')
                .getBoundingClientRect();

            //
            // Set icons opacity
            this.chartIcons.nativeElement.style.opacity = '1';
        }, 850);
    }
}

import { Component, OnInit } from '@angular/core';
import { FeelingService } from 'src/app/services/feeling.service';

@Component({
    selector: 'mood-bg-load',
    templateUrl: './mood-bg-load.component.html',
    styleUrls: ['./mood-bg-load.component.scss']
})
export class MoodBgLoadComponent implements OnInit {
    constructor(public feeling: FeelingService) {}

    ngOnInit() {}
}

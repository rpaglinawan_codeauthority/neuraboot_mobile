import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FeelingService } from 'src/app/services/feeling.service';
import { FeelingPoint } from '../../../../../../neuraboot-core/interfaces/feeling-point.interface';
import { forEach, find, isEmpty } from 'lodash';

@Component({
    selector: 'mood-scale-form',
    templateUrl: './mood-scale-form.component.html',
    styleUrls: ['./mood-scale-form.component.scss']
})
export class MoodScaleFormComponent implements OnInit {
    @ViewChild('svg', { static: true }) public svg: ElementRef;
    @ViewChild('elementEllipse', { static: true }) public elementEllipse: ElementRef;

    public draggableSize: string = '14px';
    public pointSize: string = '1px';
    public xAxisPadding: number = 12.5;
    public yAxisPadding: number = 8;
    public xDefaultCX: string = null;
    public yDefaultCY: string = null;

    public points: any[] = [];
    public lines: any[] = [];

    public point: FeelingPoint;

    constructor(public feeling: FeelingService) {}

    ngOnInit() {
        //
        // Set ellipse element to feeling service
        this.feeling.elementEllipse = this.elementEllipse;

        //
        // Generate points and lines
        this.generatePoints();
        this.generateLines();

        //
        // Set default start point
        this.setDefaultPoint();
    }

    /**
     * Generate points
     *
     * @memberof FormFeelingComponent
     */
    generatePoints() {
        //
        // Set increment area
        // multiply position 2 times to count first and last element padding
        // remove one from xAxis and yAxis length to center
        let totalArea: number = 100;
        let xIncrement: number = (totalArea - this.xAxisPadding * 2) / (this.feeling.categories.length - 1);
        let yIncrement: number = (totalArea - this.yAxisPadding * 2) / (this.feeling.levels.length - 1);

        //
        // Hold initial position
        let xPosition: number = this.xAxisPadding;
        let yPosition: number = this.yAxisPadding;

        //
        // Generate points(categories/xAxis)..
        forEach(this.feeling.categories, x => {
            //
            // ..foreach line(levels/yAxis) in this item
            forEach(this.feeling.levels, y => {
                //
                // Add point
                this.points.push({
                    r: this.pointSize,
                    cx: xPosition + '%',
                    cy: yPosition + '%',
                    xValue: x.value,
                    xLabel: x.label,
                    yValue: y.value,
                    yLabel: y.label
                });

                //
                // Increment position
                yPosition += yIncrement;
            });

            //
            // Increment x position and clear y
            xPosition += xIncrement;
            yPosition = this.yAxisPadding;
        });
    }

    /**
     * Generate lines
     *
     * @memberof FormFeelingComponent
     */
    generateLines() {
        //
        // Set increment area
        // multiply position 2 times to count first and last element padding
        // remove one from lavels/yAxis length to center
        let totalArea: number = 100;
        let yIncrement: number = (totalArea - this.yAxisPadding * 2) / (this.feeling.levels.length - 1);

        //
        // Hold initial position
        let yPosition: number = this.yAxisPadding;

        //
        // Generate points(xAxis)..
        forEach(this.feeling.levels, y => {
            //
            // Add point
            this.lines.push({
                y: yPosition + '%'
            });

            //
            // Increment position
            yPosition += yIncrement;
        });
    }

    /**
     * Set default point
     *
     * @memberof FormFeelingComponent
     */
    setDefaultPoint() {
        //
        // Set center if we don't have default
        if (!this.feeling.current.category) return this.setPresentationView();

        //
        // Get default point
        let point: FeelingPoint = (this.point = find(this.points, {
            xValue: this.feeling.current.category as any,
            yValue: this.feeling.current.level as any
        }) || { cx: 1, cy: 2 });

        //
        // Set position
        this.xDefaultCX = point.cx;
        this.yDefaultCY = point.cy;

        //
        // Set selected and emit value change
        this.feeling.setSelectedPoint(point);
    }

    /**
     * Set presentation view
     *
     * @memberof FormFeelingComponent
     */
    setPresentationView() {
        //
        // Set center bottom
        this.xDefaultCX = '50%';
        this.yDefaultCY = 100 - this.yAxisPadding + '%';
    }

    /**
     * Get closest number
     *
     * @returns
     * @memberof FormFeelingComponent
     */
    getClosestNumber(x, y, arr) {
        var indexArr = arr.map(function(k) {
            return Math.hypot(k.xTarget - x, k.yTarget - y);
        });
        var min = indexArr.indexOf(Math.min(...indexArr));

        return arr[min];
    }

    /**
     * On select point
     *
     * @param {*} event
     * @memberof FormFeelingComponent
     */
    onSelectPoint(event) {
        //
        // Validate
        if (isEmpty(event) || isEmpty(event.selected)) return;

        //
        // Select point
        this.feeling.setSelectedPoint(event.selected);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatListModule } from '@angular/material';
import { MoodScaleFormComponent } from './mood-scale-form/mood-scale-form.component';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { MoodSpecificFeelingFormComponent } from './mood-specific-feeling-form/mood-specific-feeling-form.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MoodBgColorComponent } from './mood-bg-color/mood-bg-color.component';
import { MoodBgLoadComponent } from './mood-bg-load/mood-bg-load.component';
import { MoodHistoryChartComponent } from './mood-history-chart/mood-history-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MoodDailyChartComponent } from './mood-daily-chart/mood-daily-chart.component';
import { MoodHistoryDailyListComponent } from './mood-history-daily-list/mood-history-daily-list.component';
import { MomentModule } from 'ngx-moment';

@NgModule({
    declarations: [
        MoodScaleFormComponent,
        MoodSpecificFeelingFormComponent,
        MoodBgColorComponent,
        MoodBgLoadComponent,
        MoodHistoryChartComponent,
        MoodDailyChartComponent,
        MoodHistoryDailyListComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        DirectivesModule,
        FormsModule,
        NgxChartsModule,
        MomentModule
    ],
    exports: [
        MoodScaleFormComponent,
        MoodSpecificFeelingFormComponent,
        MoodBgColorComponent,
        MoodBgLoadComponent,
        MoodHistoryChartComponent,
        MoodDailyChartComponent,
        MoodHistoryDailyListComponent
    ]
})
export class MoodModule {}

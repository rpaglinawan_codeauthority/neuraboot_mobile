import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { UiService } from 'src/app/services/ui.service';
import { AuthService } from 'src/app/services/auth.service';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { QueryUserFeelingCountByDateAndType } from '../../../../../../neuraboot-core/queries/user-feeling-count-by-date-and-type';
import { Chart } from '../../../../../../neuraboot-core/interfaces/chart.interface';
import { Subscription, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { isEmpty, forEach, merge } from 'lodash';
import * as moment from 'moment-timezone';
import { ProgressMoodDailyResumeModalComponent } from '../../progress/progress-mood-daily-resume-modal/progress-mood-daily-resume-modal.component';

moment.tz.setDefault(moment.tz.guess());

@Component({
    selector: 'mood-history-chart',
    templateUrl: './mood-history-chart.component.html',
    styleUrls: ['./mood-history-chart.component.scss'],
    providers: [FeelingHistoryService]
})
export class MoodHistoryChartComponent implements OnInit {
    @Output() public setUpdate: EventEmitter<any> = new EventEmitter();

    @Input() public path: string = '/find';
    @Input() public data: any[] = [];
    @Input() public showXAxis = true;
    @Input() public showYAxis = true;
    @Input() public gradient = false;
    @Input() public showLegend = false;
    @Input() public showXAxisLabel = false;
    @Input() public xAxisLabel = null;
    @Input() public showYAxisLabel = false;
    @Input() public yAxisLabel = null;

    public modalOverlaySky: boolean = false;
    public selectedDate: string = null;
    public gte: any;
    public lte: any;
    public noData: boolean = false;
    public loading: boolean = false;
    public data$: Subscription;
    public categoryConfig: any = {
        happy: { key: 0, color: 'rgba(255, 174, 0, 1)' /*'#ffffff'*/ },
        anxious: { key: 1, color: 'rgba(131, 48, 162, 1)' /*'#c9faf8'*/ },
        depressed: { key: 2, color: 'rgba(28, 189, 253, 1)' /*'#9afffa'*/ },
        angry: { key: 3, color: 'rgba(116, 100, 103, 1)' /*'#37dcd7'*/ }
    };
    public defaultCategorySeries: any = [
        {
            name: 'Happy',
            value: 0
        },
        {
            name: 'Anxious',
            value: 0
        },
        {
            name: 'Sad',
            value: 0
        },
        {
            name: 'Angry',
            value: 0
        }
    ];

    @Input() public colorScheme = {
        domain: [
            this.categoryConfig.happy.color,
            this.categoryConfig.anxious.color,
            this.categoryConfig.depressed.color,
            this.categoryConfig.angry.color
        ]
    };

    constructor(
        private ui: UiService,
        public feelingHistory: FeelingHistoryService,
        public auth: AuthService,
        public modal: ModalController
    ) {}

    ngOnInit() {
        //
        // Set initial date
        this.setInitialDate();

        //
        // Set update
        this.setUpdate.emit({
            update: this.update.bind(this)
        });

        //
        // Set loading
        this.loading = true;

        //
        // Set data
        this.setData();
    }

    ngOnDestroy() {
        this.data$.unsubscribe();
    }

    setData() {
        this.data$ = <any>this.feelingHistory.$collection
            .network(true)
            .raw(true)
            .post(this.path, QueryUserFeelingCountByDateAndType(this.auth.user.uid, this.gte, this.lte))
            .pipe(
                //
                // transform response
                map((response: any) => {
                    //
                    // Set data
                    const data = response.data;

                    //
                    // Get first date aggs
                    let dates = data.aggregations.group.buckets;

                    //
                    // Validate dates
                    if (isEmpty(dates)) {
                        this.noData = true;
                        return dates;
                    } else {
                        this.noData = false;
                    }

                    return dates;
                }),

                //
                // Set chart
                tap((dates: any) => {
                    //
                    // Hold series and data
                    const series: any[] = [];

                    //
                    // Go into each date and set data
                    forEach(dates, (date: any) => {
                        //
                        // Hold data
                        let data: Chart[] = merge([], this.defaultCategorySeries);

                        //
                        // Go into each category
                        if (!isEmpty(date.group.buckets)) {
                            forEach(date.group.buckets, (categoryRow: any) => {
                                //
                                // Set data key
                                const key = this.categoryConfig[categoryRow.key].key;

                                //
                                // Replace count inside data
                                data[key].value = categoryRow.doc_count;

                                //
                                //
                                data[key].extra = date.key_as_string;
                            });
                        }

                        //
                        // Push data into series
                        series.push({
                            name: moment(date.key_as_string).format('DD/MMM'),
                            series: data
                        });
                    });

                    //
                    // Set data
                    this.data = series;
                    this.loading = false;
                }),

                //
                // Handle errors
                catchError((err: ApiErrorResponse | any) => {
                    const message: string = err.message ? err.message : err;

                    this.ui.say(message);
                    return of(err);
                })
            )
            .subscribe();
    }

    /**
     * y format
     *
     * @description Format y axis value
     * @param {*} value
     * @returns
     * @memberof MoodHistoryChartComponent
     */
    yFormat(value) {
        return value - Math.floor(value) !== 0 ? '' : value;
    }

    /**
     * Update
     *
     * @description Update chart data
     * @memberof MoodHistoryChartComponent
     */
    public update() {
        this.data$.unsubscribe();
        this.setData();
    }

    /**
     * Set initial date
     *
     * @memberof FeelingHistoryChartComponent
     */
    setInitialDate() {
        //
        // Hold date format
        const dateFormat: string = 'YYYY-MM-DD';

        //
        // Set initial date
        this.gte = moment()
            .subtract(2, 'day')
            .format(dateFormat);
        this.lte = moment().format(dateFormat);

        //
        // Set initial data
        this.setRangeData();
    }

    setRangeData() {
        //
        // Hold date format
        const dateFormat: string = 'YYYY-MM-DD';

        //
        // Set range day and hold current day
        const diff: number = Math.abs(moment(this.lte).diff(moment(this.gte), 'days')) + 1;
        let currentDay: string = this.gte;
        let series: any[] = [];

        //
        // Go into each date
        for (let i = 0; i < diff; i++) {
            //
            // Set series for current day
            series.push({
                name: moment(currentDay).format('DD/MMM'),
                series: this.defaultCategorySeries
            });

            //
            // Next day
            currentDay = moment(currentDay)
                .add(1, 'day')
                .format(dateFormat);
        }

        //
        // Set data
        this.data = series;
    }

    previusDate() {
        //
        // Hold date format
        const dateFormat: string = 'YYYY-MM-DD';

        //
        // Load
        this.loading = true;

        //
        // Set new dates
        this.lte = moment(this.gte)
            .subtract(1, 'day')
            .format(dateFormat);
        this.gte = moment(this.lte)
            .subtract(2, 'day')
            .format(dateFormat);
        //
        // Set range
        this.setRangeData();

        //
        // Get new data
        this.update();
    }

    nextDate() {
        //
        // Hold date format
        const dateFormat: string = 'YYYY-MM-DD';

        //
        // Load
        this.loading = true;

        //
        // Set new dates
        this.gte = moment(this.lte)
            .add(1, 'day')
            .format(dateFormat);

        this.lte = moment(this.gte)
            .add(2, 'day')
            .format(dateFormat);

        //
        // Set range
        this.setRangeData();

        //
        // Get new data
        this.update();
    }

    /**
     * On select day
     *
     * @param {*} event
     * @memberof FeelingHistoryChartComponent
     */
    onSelectDay(event) {
        //
        // Present modal
        this.presentModal(event.extra);
    }

    async presentModal(date: string = null) {
        //
        // Create modal
        const modal = await this.modal.create({
            component: ProgressMoodDailyResumeModalComponent,
            componentProps: {
                date: date
            },
            showBackdrop: false,
            animated: false
        });

        //
        // Present modal
        await modal.present();
    }
}

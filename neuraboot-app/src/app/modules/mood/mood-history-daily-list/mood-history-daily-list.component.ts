import { Component, OnInit, Input } from '@angular/core';
import { FeelingHistory } from '../../../../../../neuraboot-core/interfaces/feeling-history.interface';
import { FeelingHistoryService } from 'src/app/services/feeling-history.service';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'mood-history-daily-list',
    templateUrl: './mood-history-daily-list.component.html',
    styleUrls: ['./mood-history-daily-list.component.scss'],
    providers: [FeelingHistoryService]
})
export class MoodHistoryDailyListComponent implements OnInit {
    @Input() public date: string = null;
    public entries$: Subscription;
    public entries: FeelingHistory[] = [];
    public loading: boolean = false;
    public path: string = '/find';
    public categoryConfig: any = {
        happy: {
            icon: 'fa-smile',
            title: 'Happy'
        },
        anxious: {
            icon: 'fa-grimace',
            title: 'Anxious'
        },
        depressed: {
            icon: 'fa-sad-cry',
            title: 'Sad'
        },
        angry: {
            icon: 'fa-angry',
            title: 'Angry'
        }
    };
    public query: any = {};

    constructor(public feelingHistory: FeelingHistoryService, public auth: AuthService) {}

    ngOnInit() {
        //
        // Load
        this.loading = true;

        //
        // Set query
        this.setQuery();

        //
        // calc percentage
        this.entries$ = this.feelingHistory.$collection
            .key('feeling-history/day/list/' + this.date)
            .post(this.path, this.query)
            .pipe(
                map((entries: FeelingHistory[]) => {
                    //
                    // Set entries
                    this.entries = entries;

                    //
                    // Load
                    this.loading = false;
                    return this.entries;
                })
            )
            .subscribe();
    }

    setQuery() {
        return (this.query = {
            query: {
                bool: {
                    must: [
                        {
                            match: {
                                'user.uid': this.auth.user.uid
                            }
                        },
                        {
                            range: {
                                created_at: {
                                    gte: this.date,
                                    lte: this.date
                                }
                            }
                        }
                    ]
                }
            }
        });
    }

    ngOnDestroy() {
        this.entries$.unsubscribe();
    }
}

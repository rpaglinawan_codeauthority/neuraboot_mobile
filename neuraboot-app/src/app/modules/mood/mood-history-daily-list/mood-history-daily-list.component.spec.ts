import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodHistoryDailyListComponent } from './mood-history-daily-list.component';

describe('MoodHistoryDailyListComponent', () => {
  let component: MoodHistoryDailyListComponent;
  let fixture: ComponentFixture<MoodHistoryDailyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodHistoryDailyListComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoodHistoryDailyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

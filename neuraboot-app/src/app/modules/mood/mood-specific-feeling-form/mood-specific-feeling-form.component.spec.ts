import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodSpecificFeelingFormComponent } from './mood-specific-feeling-form.component';

describe('MoodSpecificFeelingFormComponent', () => {
  let component: MoodSpecificFeelingFormComponent;
  let fixture: ComponentFixture<MoodSpecificFeelingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodSpecificFeelingFormComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoodSpecificFeelingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { CacheService } from 'src/app/services/cache.service';
import { FeelingService } from 'src/app/services/feeling.service';
import { FeelingSpecificService } from 'src/app/services/feeling-specific.service';
import { catchError } from 'rxjs/operators';
import { isEmpty } from 'lodash';

@Component({
    selector: 'mood-specific-feeling-form',
    templateUrl: './mood-specific-feeling-form.component.html',
    styleUrls: ['./mood-specific-feeling-form.component.scss'],
    providers: [FeelingSpecificService]
})
export class MoodSpecificFeelingFormComponent implements OnInit {
    public opened: boolean = false;
    public panTimeout: any = false;
    public canSubmit: boolean = false;

    public option$: any;
    private limit: number = 1;

    public supportersLevel: string[] & any = ['very', 'extremely'];
    public validForm: boolean;
    constructor(
        private ui: UiService,
        private cache: CacheService,
        public feeling: FeelingService,
        public feelingSpecific: FeelingSpecificService
    ) {}

    ngOnInit() {}

    toggleOpening() {
        //
        // set specific options
        this.option$ = this.feelingSpecific.$collection
            .where('active', '==', true)
            .where('category', '==', this.feeling.current.category)
            .find()
            .pipe(
                catchError(async err => {
                    const alert = await this.ui.alert('Warning', err.message ? err.message : err);
                    await alert.present();
                })
            );

        //
        // toggle ui
        this.opened = !this.opened;
    }

    /**
     * On pan
     *
     * @param {*} event
     * @memberof MoodSpecificFeelingFormComponent
     */
    async onPan(event) {
        //
        // Clear timeout
        clearTimeout(this.panTimeout);

        //
        // Wait some time
        this.panTimeout = setTimeout(async () => {
            //
            // Validate disable
            this.validForm = this.feeling.validForm('feeling');
            if (!this.validForm) {
                //
                // Show alert
                const alert: any = await this.ui.alert('Validation', 'Select how you feel first');
                return await alert.present();
            }

            //
            // Validate direction
            if (event.offsetDirection !== 8 && event.offsetDirection !== 16) return;

            //
            // Open
            if (event.offsetDirection === 8) this.opened = true;
            if (event.offsetDirection === 16) this.opened = false;
        }, 250);
    }

    /**
     * On selection change
     *
     * @param {*} event
     * @memberof MoodSpecificFeelingFormComponent
     */
    async onSelectionChange(event) {
        //
        // Validate
        if (isEmpty(event) || isEmpty(event.option) || isEmpty(event.source)) return;

        //
        // Set selected length
        const selectedLength: number = event.source.selectedOptions.selected.length;

        //
        // Validate max options by the limit prop
        if (selectedLength > this.limit && event.option.selected === true) {
            //
            // Remove selection
            event.option.selected = false;

            //
            // Show alert
            const alert: any = await this.ui.alert('Limit', `You can choose up to ${this.limit} option${this.limit > 1 ? 's' : ''}`);
            return await alert.present();
        }

        //
        // Update selected options
        this.cache.set(this.feeling.cacheSlugForm, this.feeling.current);

        //
        // Show button?
        if (selectedLength > 0) this.canSubmit = true;
        else this.canSubmit = false;
    }

    /**
     * Save
     *
     * @memberof MoodSpecificFeelingFormComponent
     */
    async save() {
        //
        // Skip supporters when dont include supportersLevel
        if (!this.supportersLevel.includes(this.feeling.current.level)) {
            await this.feeling.save(false);
            this.opened = false;
        }

        //
        // Includes supporters level, ask permission to notify
        else {
            //
            // Set buttons and actions
            const buttons: any[] = [
                {
                    text: 'Not this time',
                    handler: async () => {
                        await this.feeling.save(false);
                        this.opened = false;
                    }
                },
                {
                    text: 'Notify',
                    handler: async () => {
                        await this.feeling.save(true);
                        this.opened = false;
                    }
                }
            ];

            //
            // Present
            const alert = await this.ui.alert(
                'Supporters',
                'We will let your supporters know how you are feeling',
                buttons,
                false,
                this.feeling.current.category !== 'happy'
                    ? '<br><b>There is Help.</b> you are not alone. To connect to crisistextonline.org, text <b>CONNECT</b> to <b>741741</b>'
                    : null
            );
            alert.present();
        }
    }
}

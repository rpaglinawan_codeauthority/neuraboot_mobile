import { Component, OnInit, Input } from '@angular/core';
import { forEach } from 'lodash';

@Component({
    selector: 'mood-bg-color',
    templateUrl: './mood-bg-color.component.html',
    styleUrls: ['./mood-bg-color.component.scss']
})
export class MoodBgColorComponent implements OnInit {
    @Input() public color: string = 'default-default';
    @Input() public lastColor: string = 'default-default';

    public colors: string[] = ['default', 'angry', 'depressed', 'anxious', 'happy'];
    public levels: string[] = ['default', 'little', 'pretty', 'very', 'extremely'];
    public classes: string[] = [];

    public slug: string = 'bg-color-';

    constructor() {}

    ngOnInit() {
        //
        // Generate classes
        this.generateClasses();
    }

    /**
     * Generate Classes
     *
     * @memberof MoodBgColorComponent
     */
    generateClasses() {
        //
        // Each colors
        forEach(this.colors, color => {
            //
            // Each level
            forEach(this.levels, level => {
                this.classes.push(this.slug + color + '-' + level);
            });
        });
    }
}

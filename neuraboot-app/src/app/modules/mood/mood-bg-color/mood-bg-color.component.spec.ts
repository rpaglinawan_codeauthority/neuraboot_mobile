import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodBgColorComponent } from './mood-bg-color.component';

describe('MoodBgColorComponent', () => {
  let component: MoodBgColorComponent;
  let fixture: ComponentFixture<MoodBgColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodBgColorComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoodBgColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

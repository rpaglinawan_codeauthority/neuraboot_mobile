import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartBarComponent } from './chart-bar/chart-bar.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material';

@NgModule({
    declarations: [ChartBarComponent],
    imports: [CommonModule, NgxChartsModule, FlexLayoutModule, MatIconModule],
    exports: [ChartBarComponent]
})
export class ChartModule {}

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { Chart } from '../../../../../../neuraboot-core/interfaces/chart.interface';
import { ApiErrorResponse } from '../../../../../../neuraboot-core/interfaces/api-error-response.interface';
import { BarChartSetup } from '../../../../../../neuraboot-core/interfaces/bar-chart-setup.interface';
import { Subscription, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { isEmpty } from 'lodash';
import { Response } from '@reative/records';

@Component({
    selector: 'chart-bar',
    templateUrl: './chart-bar.component.html',
    styleUrls: ['./chart-bar.component.scss']
})
export class ChartBarComponent implements OnInit {
    @Output() public setUpdate: EventEmitter<any> = new EventEmitter();

    @Input() public path: string = '/find';
    @Input() public setup: BarChartSetup = {};

    @Input() public data: Chart[] = [];
    @Input() public showXAxis = true;
    @Input() public showYAxis = true;
    @Input() public gradient = false;
    @Input() public showLegend = false;
    @Input() public showXAxisLabel = false;
    @Input() public xAxisLabel = null;
    @Input() public showYAxisLabel = false;
    @Input() public yAxisLabel = null;

    @Input() public colorScheme = {
        domain: ['#4dd0e1']
    };

    public noData: boolean = false;
    public loading: boolean = false;
    public data$: Subscription;

    constructor(private ui: UiService) {}

    ngOnInit() {
        //
        // Set update
        this.setUpdate.emit({
            update: this.update.bind(this)
        });

        //
        // Set loading
        this.loading = true;

        //
        // Set data
        this.setData();
    }

    ngOnDestroy() {
        this.data$.unsubscribe();
    }

    /**
     * Set data
     *
     * @description Set chart data
     * @memberof ChartBarComponent
     */
    setData() {
        //
        // Set data
        this.data$ = <any>this.setup.service.$collection
            .key(this.setup.cacheKey)
            .raw(true)
            .post(this.path, this.setup.query)
            .pipe(
                //
                // Map RR response
                map((r: Response) => r.data),

                //
                // Generate results
                tap((result: any) => {
                    let data: Chart[] = [];

                    //
                    // Get aggr keys
                    const aggr_key1: any = Object.keys(result.aggregations);

                    //
                    // Validate
                    if (
                        isEmpty(result) ||
                        isEmpty(result.aggregations) ||
                        isEmpty(result.aggregations[aggr_key1[0]]) ||
                        isEmpty(result.aggregations[aggr_key1[0]].buckets)
                    ) {
                        this.noData = true;
                        throw new Error('no data');
                    } else {
                        this.noData = false;
                    }

                    //
                    // Map data
                    result.aggregations[aggr_key1[0]].buckets.map(b => {
                        const name = b.key_as_string;
                        data.push({ name: name, value: b.doc_count });
                    });

                    //
                    // post hook?
                    if (this.setup.postHook) data = this.setup.postHook(data);

                    //
                    // Set data and unload
                    this.data = data;
                    this.loading = false;
                }),

                //
                // Handle errors
                catchError((err: ApiErrorResponse | any) => {
                    const message: string = err.message ? err.message : err;
                    this.loading = false;

                    // this.ui.say(message);
                    return of(err);
                })
            )
            .subscribe();
    }

    /**
     * y format
     *
     * @description Format y axis value
     * @param {*} value
     * @returns
     * @memberof ChartBarComponent
     */
    yFormat(value) {
        return value - Math.floor(value) !== 0 ? '' : value;
    }

    /**
     * Update
     *
     * @description Update chart data
     * @memberof ChartBarComponent
     */
    public update() {
        this.data$.unsubscribe();
        this.setData();
    }
}

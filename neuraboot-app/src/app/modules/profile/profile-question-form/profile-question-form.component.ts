import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Whoami } from '../../../../../../neuraboot-core/interfaces/whoami.interface';
import { WhoamiQuestionList } from '../../../../../../neuraboot-core/interfaces/whoami-question-list.interface';
import { WhoamiQuestion } from '../../../../../../neuraboot-core/interfaces/whoami-question.interface';
import { WhoamiQuestionItem } from '../../../../../../neuraboot-core/interfaces/whoami-question-item.interface';
import { QuestionService } from 'src/app/services/question.service';
import { QuestionHistoryService } from 'src/app/services/question-history.service';
import { AuthService } from 'src/app/services/auth.service';
import { UiService } from 'src/app/services/ui.service';
import { ActionStepService } from 'src/app/services/action-step.service';
import { ActionStepHistoryService } from 'src/app/services/action-step-history.service';
import { Response } from '@reative/records';
import { Guid } from 'src/app/utils/guid';
import { isEmpty, merge, sampleSize, cloneDeep, find } from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'profile-question-form',
    templateUrl: './profile-question-form.component.html',
    styleUrls: ['./profile-question-form.component.scss'],
    providers: [QuestionService, QuestionHistoryService, ActionStepService, ActionStepHistoryService]
})
export class ProfileQuestionFormComponent implements OnInit {
    public questions: WhoamiQuestionList[] = [];
    public saving: boolean = false;

    constructor(
        public question: QuestionService,
        public questionHistory: QuestionHistoryService,
        public auth: AuthService,
        public ui: UiService,
        private nav: NavController,
        private actionStepService: ActionStepService,
        private actionStepHistoryService: ActionStepHistoryService
    ) { }

    ngOnInit() {
        this.setQuestions();
        console.log('profile-question-component triggered');
    }

    /**
     * Set questions
     *
     * @memberof ProfileQuestionFormComponent
     */
    async setQuestions() {
        let questions: any = await this.question.$collection
            .key('whoami-questions/question')
            .where('active', '==', true)
            .find()
            .toPromise();

        const history: any = await this.questionHistory.$collection
            .where('user.uid', '==', this.auth.user.uid)
            .key('whoami-history/history')
            .findOne()
            .toPromise();

        //
        // Merge user history with current questions
        if (!isEmpty(history)) {
            questions = questions.map(o => {
                const result = cloneDeep(o);
                const questionHistory = find(history.questions, ['id', o.id]);

                if (!isEmpty(questionHistory)) result.selected = questionHistory.selected;

                return result;
            });
        }

        this.questions = cloneDeep(questions);
    }

    /**
     * Save
     *
     * @returns
     * @memberof ProfileQuestionFormComponent
     */
    async save() {
        //
        // Validate saving
        if (this.saving) return;

        //
        // Set loading
        let loader = await this.ui.loader();
        await loader.present();

        try {
            //
            // Set saving
            this.saving = true;

            //
            // Get who am i
            const hasWhoami: Whoami =
                (await this.questionHistory.$collection
                    .where('user.uid', '==', this.auth.user.uid)
                    .network(true)
                    .cache(false)
                    .findOne()
                    .toPromise()) || {};

            //
            // Has who am i
            const hasWhoamiD = hasWhoami ? hasWhoami.id : false;

            //
            // New who am i
            const newWhoamiD = Guid.make(2);

            //
            // Update
            if (hasWhoamiD) {
                await this.questionHistory.$collection.patch(hasWhoamiD, <Whoami>{ questions: this.questions }).toPromise();
            }

            //
            // Set new one
            else {
                await this.questionHistory.$collection
                    .post(newWhoamiD, <Whoami>{
                        id: newWhoamiD,
                        questions: this.questions,
                        user: this.auth.user,
                        created_at: moment().toISOString()
                    })
                    .toPromise();
            }

            //
            // @todo: implement after working with action steps
            // Set a new challenge
            // if user didn't complete completed_whoami_challenge_wizard
            if (!this.auth.user.completed_whoami_challenge_wizard) {
                //
                // Validate questions word
                const wordAnswer: WhoamiQuestion = this.questions.find((wordAnswer: WhoamiQuestionList) => wordAnswer.label === 'word');

                //
                // Has selected any word ?
                if (!wordAnswer.selected.length) {
                    //
                    // No word selected
                    return this.onFinishSave(loader);
                }

                //
                // Validate active_challenge
                if (this.auth.getUser().active_challenge) {
                    //
                    // Has an active action step
                    return this.onFinishSave(loader);
                }

                //
                // Pick a random word from selected
                const randomWord: WhoamiQuestionItem = sampleSize(wordAnswer.selected)[0];

                //
                // Change load msg
                await loader.dismiss();
                loader = await this.ui.loader(60 * 1000, 'Found an action step');
                await loader.present();

                //
                // Pick an action step with this word
                const actionStep = await this.actionStepService.$collection
                    .query({ field: 'word', operator: '==', value: randomWord.name })
                    .findOne()
                    .toPromise();

                //
                // Validate action step
                if (isEmpty(actionStep)) {
                    //
                    // No action step, just quit
                    return this.onFinishSave(loader);
                }

                //
                // Persist action step
                await this.actionStepHistoryService.persist(actionStep);

                //
                // Set as completed
                this.auth.setUserProp('completed_whoami_challenge_wizard', true);
            }

            //
            // No challenge, just quit
            this.onFinishSave(loader);
        } catch (error) {
            //
            // Unload
            await loader.dismiss();
            this.saving = false;
        }
    }

    /**
     * On finish save
     *
     * @param {*} loader
     * @memberof ProfileQuestionFormComponent
     */
    async onFinishSave(loader) {
        //
        // Unload
        await loader.dismiss();

        //
        // Back to home
        this.nav.navigateRoot(['/home']);
        this.saving = false;
    }
}
import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ProfileQuestionItemFormComponent } from '../profile-question-item-form/profile-question-item-form.component';

@Component({
    selector: 'app-profile-question-item-modal',
    templateUrl: './profile-question-item-modal.component.html',
    styleUrls: ['./profile-question-item-modal.component.scss']
})
export class ProfileQuestionItemModalComponent implements OnInit {
    public component: ProfileQuestionItemFormComponent;

    constructor(private modal: ModalController, private params: NavParams) {}

    ngOnInit() {
        //
        // Get values
        this.component = this.params.get('component');
    }

    /**
     * Dismiss
     *
     * @memberof ProfileQuestionItemModalComponent
     */
    dismiss() {
        this.modal.dismiss();
    }
}

import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ModalController, IonContent } from '@ionic/angular';
import { Guid } from 'src/app/utils/guid';
import { ProfileQuestionItemModalComponent } from '../profile-question-item-modal/profile-question-item-modal.component';
import { remove } from 'lodash';

@Component({
    selector: 'profile-question-item-form',
    templateUrl: './profile-question-item-form.component.html',
    styleUrls: ['./profile-question-item-form.component.scss']
})
export class ProfileQuestionItemFormComponent implements OnInit {
    @Input() label: string = 'word';
    @Input() action_label: string = 'a word';
    @Input() type: string = 'tag'; // tag or contact
    @Input() limit: number = 1;
    @Input() items: any[] = [];
    @Input() selectedItems: any[] = [];

    @Output() onOpen: EventEmitter<any> = new EventEmitter();
    @Output() onClose: EventEmitter<any> = new EventEmitter();

    constructor(private modal: ModalController, private content: IonContent) {}

    ngOnInit() {}

    /**
     * Add item
     *
     * @param {*} [item={ id: null, title: '', name: '' }]
     * @returns
     * @memberof ProfileQuestionItemFormComponent
     */
    addItem(item: any = { id: null, title: '', name: '' }) {
        //
        // Create a general id for item
        if (!item.id) item.id = Guid.make(2);

        //
        // check if already exists
        const exists = this.selectedItems.find(i => i.id === item.id);

        //
        // scroll to bottom
        this.content.scrollToBottom();

        return !exists && this.selectedItems.length < this.limit ? this.selectedItems.push(item) : '';
    }

    /**
     * Present modal
     *
     * @memberof ProfileQuestionItemFormComponent
     */
    async presentModal() {
        //
        // Validate type
        if (this.type != 'tag') return;

        //
        // Show modal
        const modal = await this.modal.create({
            component: ProfileQuestionItemModalComponent,
            componentProps: { component: this }
        });

        await modal.present();
    }

    /**
     * Remove item
     *
     * @param {*} item
     * @memberof ProfileQuestionItemFormComponent
     */
    removeItem(item: any) {
        remove(this.selectedItems, i => i.id === item.id);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatInputModule, MatCardModule, MatListModule } from '@angular/material';
import { ProfileQuestionFormComponent } from './profile-question-form/profile-question-form.component';
import { ProfileQuestionItemFormComponent } from './profile-question-item-form/profile-question-item-form.component';
import { FormsModule } from '@angular/forms';
import { ProfileQuestionItemModalComponent } from './profile-question-item-modal/profile-question-item-modal.component';
import { ModalModule } from '../modal/modal.module';

@NgModule({
    declarations: [ProfileQuestionFormComponent, ProfileQuestionItemFormComponent, ProfileQuestionItemModalComponent],
    imports: [
        CommonModule,
        FlexLayoutModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatCardModule,
        MatListModule,
        FormsModule,
        ModalModule
    ],
    exports: [ProfileQuestionFormComponent, ProfileQuestionItemFormComponent, ProfileQuestionItemModalComponent],
    entryComponents: [ProfileQuestionItemModalComponent]
})
export class ProfileModule {}

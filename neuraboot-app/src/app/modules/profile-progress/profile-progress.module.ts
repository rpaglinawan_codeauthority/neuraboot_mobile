import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileProgressComponent } from './profile-progress/profile-progress.component';

@NgModule({
    declarations: [ProfileProgressComponent],
    imports: [CommonModule],
    exports: [ProfileProgressComponent]
})
export class ProfileProgressModule {}

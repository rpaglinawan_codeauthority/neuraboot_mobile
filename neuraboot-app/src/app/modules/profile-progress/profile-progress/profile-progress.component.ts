import { Component, OnInit, Input } from '@angular/core';
import { Whoami } from '../../../../../../neuraboot-core/interfaces/whoami.interface';
import { WhoamiQuestionList } from '../../../../../../neuraboot-core/interfaces/whoami-question-list.interface';
import { QuestionHistoryService } from 'src/app/services/question-history.service';
import { AuthService } from 'src/app/services/auth.service';
import { Numbers } from '../../../utils/numbers';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { isEmpty } from 'lodash';

@Component({
    selector: 'profile-progress',
    templateUrl: './profile-progress.component.html',
    styleUrls: ['./profile-progress.component.scss'],
    providers: [QuestionHistoryService]
})
export class ProfileProgressComponent implements OnInit {
    @Input() public autoload: boolean = false;

    public progress: number = 0;
    public history$: Subscription;

    constructor(public questionHistory: QuestionHistoryService, public auth: AuthService) {}

    ngOnInit() {
        //
        // Autoload ?
        if (this.autoload) this.load();
    }

    ngOnDestroy() {
        this.unload();
    }

    load() {
        //
        // Set progress
        this.setProgress();
    }

    unload() {
        this.history$.unsubscribe();
    }

    setProgress() {
        //
        // Get history to set progress
        this.history$ = this.questionHistory.$collection
            .where('user.uid', '==', this.auth.user.uid)
            .findOne()
            .subscribe((history: Whoami) => {
                //
                // Set questions
                const questions: WhoamiQuestionList[] = <WhoamiQuestionList[]>history.questions;

                //
                // Validate questions
                if (isEmpty(questions)) return;

                //
                // Extract value
                let min: number = 0;
                let filled: number = 0;
                questions.map(q => {
                    min += q.min;
                    filled += q.selected.length <= q.min ? q.selected.length : q.min;
                });

                //
                // Set progress
                this.progress = Numbers.round((filled * 100) / min, 0);
            });
    }
}

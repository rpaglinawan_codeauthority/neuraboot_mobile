import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class NavigationService {
    constructor(private menuCtrl: MenuController) {}

    openMenu() {
        this.menuCtrl.open();
    }
}

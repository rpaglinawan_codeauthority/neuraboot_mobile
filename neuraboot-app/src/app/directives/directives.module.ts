import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlainDraggableDirective } from './plain-draggable.directive';

@NgModule({
    declarations: [PlainDraggableDirective],
    imports: [CommonModule],
    exports: [PlainDraggableDirective]
})
export class DirectivesModule {}

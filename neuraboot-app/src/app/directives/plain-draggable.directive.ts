import { Directive, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { FeelingPoint } from '../../../../neuraboot-core/interfaces/feeling-point.interface';
import { FeelingService } from '../services/feeling.service';

@Directive({
    selector: '[plainDraggable]'
})
export class PlainDraggableDirective {
    @Input() public points: any[] = [];
    @Input() public svg: any = {};

    @Output() public onSelect: EventEmitter<any> = new EventEmitter();

    public onDragTimeout: any = false;
    private draggable: any;

    constructor(private element: ElementRef, public feeling: FeelingService) {}

    ngAfterViewInit() {
        this.setDraggable();
    }

    ngOnDestroy() {
        //
        // Remove leader line
        document.querySelector('.lines-container').innerHTML = '';
    }

    /**
     * Get snap targets
     *
     * @returns
     * @memberof FormFeelingComponent
     */
    getSnapTargets() {
        //
        // Get all targets
        let targetElements = this.svg.querySelectorAll('.drag-target');

        //
        // Hold targets
        let targets: any[] = [];

        //
        // Get position
        _.forEach(targetElements, target => {
            //
            // Hold values
            let result: any = {};
            let bbox = target.getBBox();
            let bounding = target.getBoundingClientRect();

            //
            // Set target position and feed targets
            // add 1 more to x and y to adjust position
            result.x = bbox.x + 1;
            result.y = bbox.y + 1;

            targets.push(result);

            //
            // Add x and y to point
            let point = _.find(this.points, { xValue: target.getAttribute('xAxisValue'), yValue: target.getAttribute('yAxisValue') }) || {};

            point.xTarget = bounding.x;
            point.yTarget = bounding.y;
        });

        return targets;
    }

    /**
     * Set leader line
     *
     * @description Set leader line to start and end point
     * @memberof PlainDraggableDirective
     */
    setLeaderLine() {
        //
        // Get leader elements
        const leaderStart = this.svg.querySelector('.leader-start-target');
        const leaderEnd = this.svg.querySelector('.leader-end-target');

        //
        // Leader line options
        const leaderLineOptions = {
            path: 'fluid',
            endPlug: 'behind',
            color: 'rgba(255, 255, 255, .7)',
            hide: true
        };

        //
        // Set leader line
        const leaderLineStart = (this.feeling.leaderLineStart = new (<any>window).LeaderLine(
            leaderStart,
            this.element.nativeElement,
            _.extend({}, leaderLineOptions)
        ));

        const leaderLineEnd = (this.feeling.leaderLineEnd = new (<any>window).LeaderLine(
            leaderEnd,
            this.element.nativeElement,
            _.extend({}, leaderLineOptions)
        ));

        //
        // Add leader line elements to a container inside ion-container to fix z-index problems
        const leaderLines = document.querySelectorAll('.leader-line');
        document.querySelector('.lines-container').appendChild(leaderLines[0]);
        document.querySelector('.lines-container').appendChild(leaderLines[1]);

        //
        // Animate entrance
        this.feeling.leaderLineStart.show('draw', { duration: 300, timing: [0.58, 0, 0.42, 1] });
        this.feeling.leaderLineEnd.show('draw', { duration: 300, timing: [0.58, 0, 0.42, 1] });
    }

    /**
     * Set draggable
     *
     * @memberof FormFeelingComponent
     */
    setDraggable() {
        setTimeout(() => {
            const self = this;

            //
            // Get snapPoints
            this.getSnapTargets();

            //
            // Set leader line
            this.setLeaderLine();

            //
            // Set draggable
            const draggable = (this.draggable = new (<any>window).PlainDraggable(this.element.nativeElement, {}));

            //
            // Set on drag event
            draggable.onDrag = position => {
                //
                // Get closest point
                const closestPoint = self.getClosestPoint(position.left, position.top, self.points);

                self.onSelect.emit({
                    selected: closestPoint
                });
            };

            //
            // Set on move event
            draggable.onMove = () => {
                //
                // Update leader lines
                this.feeling.leaderLineStart.position();
                this.feeling.leaderLineEnd.position();
            };
        }, 800);
    }

    /**
     * Get closest point
     *
     * @returns
     * @memberof FormFeelingComponent
     */
    getClosestPoint(x, y, arr) {
        const indexArr = arr.map(function(k) {
            return Math.hypot(k.xTarget - x, k.yTarget - y);
        });
        const min = indexArr.indexOf(Math.min(...indexArr));
        return arr[min];
    }
}

import { User } from './user.interface';
import { Challenge } from './challenge.interface';

export interface ChallengeHistory {
    id?: string,
    status?: string,
    user?: User,
    challenge?: Challenge,
    created_at?: any
}
export interface Geopoint {
    lat?: number,
    lon?: number
}
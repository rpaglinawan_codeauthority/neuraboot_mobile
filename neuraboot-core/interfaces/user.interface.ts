import { Geolocation } from "./geolocation.interface";
import { ChallengeHistory } from "./challenge-history.interface";

export interface User {
    uid?: string,
    created_at?: any,
    updated_at?: any,
    online_at?: any,
    display_name?: string,
    photo_url?: string,
    email?: string,
    password?: string,                      // not persisted - just for testing
    passwordRepeat?: string,                // not persisted - just for testing
    facebook_connected?: boolean,
    facebook_uid?: string,
    google_connected?: boolean,
    google_uid?: string,
    roles?: string[],
    banned?: boolean,
    token?: string,                         // client token
    token_fcm?: string,                     // firebase cloud message token
    geopoint?: any,
    geolocation?: Geolocation,
    completed_password?: boolean,
    completed_feeling_wizard?: boolean,
    completed_whoami_challenge_wizard?: boolean,
    completed_wizard?: boolean,
    completed_geolocation?: boolean,
    completed_notification?: boolean,
    active_challenge?: ChallengeHistory,
    use_notification?: boolean
}
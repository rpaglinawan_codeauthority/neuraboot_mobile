import { User } from './user.interface';
import { WhoamiQuestion } from './whoami-question.interface';

export interface Whoami {
    id?: string,
    user?: User,
    questions?: WhoamiQuestion[],
    created_at?: any,
    updated_at?: any
}
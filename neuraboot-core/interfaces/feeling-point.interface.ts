export interface FeelingPoint {
    r?: string,
    cx?: string,
    cy?: string,
    xValue?: string,
    xLabel?: string,
    yValue?: string,
    yLabel?: string,
    xTarget?: number,
    yTarget?: number
}
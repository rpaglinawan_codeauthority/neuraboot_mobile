export interface WhoamiQuestionItem {
    id: string,
    name: string,
    title?: string,
    active?: boolean
}
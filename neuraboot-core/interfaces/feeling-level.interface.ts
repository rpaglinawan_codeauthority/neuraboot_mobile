export interface FeelingLevel {
    label?: string,
    value?: string
}
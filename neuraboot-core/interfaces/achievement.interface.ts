export interface Achievement {
    completed: boolean,
    medal: string
}
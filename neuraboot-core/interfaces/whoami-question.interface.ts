import { WhoamiQuestionItem } from "./whoami-question-item.interface";

export interface WhoamiQuestion {
    title: string,
    items: WhoamiQuestionItem[],
    selected: WhoamiQuestionItem[]
}
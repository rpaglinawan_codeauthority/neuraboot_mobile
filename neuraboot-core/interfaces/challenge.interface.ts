export interface Challenge {
    id?: string,
    category?: string,
    feeling?: string,
    text?: string,
    active?: boolean,
    link?: string,
    label?: string,
    word?: string,
    created_at?: any,
    updated_at?: any
}
export interface Segment {
    label: string,
    slug: string
}
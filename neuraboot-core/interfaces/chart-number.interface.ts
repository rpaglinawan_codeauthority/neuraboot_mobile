import { ElasticQuery } from "@ionfire/reactive-record";

export interface ChartNumberSetup {
    service: any,
    ttl?: number,
    path: string,
    query?: { query: ElasticQuery }
}
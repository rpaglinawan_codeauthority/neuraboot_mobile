import { User } from './user.interface';
import { FeelingPoint } from './feeling-point.interface';
import { FeelingSpecific } from './feeling-specific.interface';
export interface FeelingHistory {
    id?: string,
    created_at?: any,
    updated_at?: any,
    user?: User,
    category?: string,
    level?: string,
    specific?: FeelingSpecific[],
    point?: FeelingPoint,
    notifySupporters?: boolean
}
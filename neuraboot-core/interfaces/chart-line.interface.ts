import { ElasticQuery } from '@ionfire/reactive-record';

export interface ChartLineSetup {
    name: string,
    placeholder: string,
    title: string,

    // transform?: (index: number, response: any, data: ChartLineSetup[]) => any,
    xAxisTickFormatting?: (value: string) => string,

    charts?: [{
        name: string,
        series: { name: string, value }[]
    }],

    path: string,
    query: { query: ElasticQuery, aggs: any },
    service: any,
    ttl?: number
}
export interface FeelingSpecific {
    id?: string,
    name: string,
    label: string,
    active: true,
    category?: string
}
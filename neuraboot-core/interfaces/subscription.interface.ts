import { User } from './user.interface';

export interface Subscription {
  id?: string;
  created_at?: any;
  updated_at?: any;
  user?: User;
  status?: boolean;
  payment_type?: string;
  code?: string;
  os?: string;
  store?: any;
  product?: any;
}

export interface ActionStepBackground {
    connections?: string,
    fuel?: string,
    energyRelease?: string,
    awareness?: string,
    challenge?: string
}
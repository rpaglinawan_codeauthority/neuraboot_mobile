import { ElasticQuery } from '@ionfire/reactive-record';

export interface ChartBarVerticalSetup {
    name: string,
    placeholder: string,
    title: string,

    transform?: (index: number, response: any, data: ChartBarVerticalSetup[]) => any,
    formatChartXLabel?: (value: string) => string,

    charts?: [{
        name: string,
        series: { name: string, value }[]
    }],

    path: string,
    query: ElasticQuery,
    service: any,
    ttl?: number
}
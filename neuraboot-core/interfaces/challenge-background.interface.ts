export interface ChallengeBackground {
    connections?: string,
    fuel?: string,
    energyRelease?: string,
    awareness?: string,
    challenge?: string
}
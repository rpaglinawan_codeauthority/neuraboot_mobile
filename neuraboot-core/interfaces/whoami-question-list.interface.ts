import { WhoamiQuestion } from "./whoami-question.interface";

export interface WhoamiQuestionList extends WhoamiQuestion {
    id?: string,
    label?: string,
    action_label: string,
    limit: number,
    min: number,
    info: boolean,
    active: boolean,
    type: string // tag or contact
}
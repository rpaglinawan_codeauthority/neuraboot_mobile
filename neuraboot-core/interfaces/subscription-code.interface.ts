export interface SubscriptionCode {
  id?: string;
  code?: string;
}

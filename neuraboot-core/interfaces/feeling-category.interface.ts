export interface FeelingCategory {
    label?: string,
    value?: string,
    icon?: string
}
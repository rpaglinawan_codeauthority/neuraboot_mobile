export interface ActionStep {
    id?: string,
    category?: string,
    feeling?: string,
    text?: string,
    active?: boolean,
    link?: string,
    label?: string,
    word?: string,
    user?: string,
    created_at?: any,
    updated_at?: any
}
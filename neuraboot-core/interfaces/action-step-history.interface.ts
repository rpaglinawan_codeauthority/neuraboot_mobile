import { User } from './user.interface';
import { ActionStep } from './action-step.interface';

export interface ActionStepHistory {
    id?: string,
    status?: string,
    user?: User,
    challenge?: ActionStep,
    created_at?: any
}
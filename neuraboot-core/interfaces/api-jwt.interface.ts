export interface ApiJwt {
    uid: string,
    display_name: string,
    email: string,
    roles: string[]
}
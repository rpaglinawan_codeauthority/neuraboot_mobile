export interface ChallengeStats {
    category?: string,
    completed?: number,
    attempted?: number,
    dayStreak?: number,
    maxStreak?: number
}
/**
 * @export
 * @interface ApiToken
 */
export interface ApiToken {
    auth: string,               // used to log users in firebase by the client side (1 hour expiration time by default. we cant change that)
    client: string              // used by the client to access our protected api endpoints (no expiration)
}
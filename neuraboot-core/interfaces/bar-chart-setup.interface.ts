import { ElasticQuery } from '@ionfire/reactive-record';
import { Chart } from './chart.interface';

export interface BarChartSetup {
    service?: any,
    query?: ElasticQuery
    ttl?: number,
    cacheKey?: string,
    postHook?: (data: Chart[]) => any
}
import { User } from './user.interface';
import { ApiToken } from './api-token.interface';

export interface ApiUserAuthResponse {
    user: User,
    token: ApiToken
}
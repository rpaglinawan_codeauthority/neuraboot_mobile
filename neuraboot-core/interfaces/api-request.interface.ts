import { Request, Response } from "express";


/**
 * All endpoints should implement this interface
 * eg: see `api/v1/user/login`
 *
 * @export
 * @interface ApiRequest
 */
export interface ApiRequest {
    req: Request;           // express request
    res: Response;          // express response
    admin: any;             // firebase admin namespace


    /**
     * @param {string} method
     * @returns {(req: Request, res: Response) => any}
     * @memberof ApiRequest
     */
    hit(method: string): (req: Request, res: Response) => any
}
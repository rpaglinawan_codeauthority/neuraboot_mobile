
/**
 * standardizes error responses
 *
 * @export
 * @interface ApiErrorResponse
 */
export interface ApiErrorResponse {
    code: string,
    message: string
}
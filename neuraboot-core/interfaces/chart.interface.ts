export interface Chart {
    name: string,
    value: number,
    extra?: any
}
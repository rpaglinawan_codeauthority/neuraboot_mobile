export interface Geolocation {
    country_code?: string,
    country?: string,
    state?: string,
    city?: string,
    zip?: string,
    address?: string
}
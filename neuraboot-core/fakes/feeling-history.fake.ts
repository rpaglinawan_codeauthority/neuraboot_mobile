import { FeelingHistory } from '../interfaces/feeling-history.interface';

export const FeelingHistoryFake: FeelingHistory = {
    category: "happy",
    created_at: "2018-10-05T22:20:29.820Z",
    id: "d0e96c26",
    level: "extremely",
    point: {
        cx: "87.5%",
        cy: "8%",
        r: "1px",
        xLabel: "Happy",
        xTarget: 329.46875,
        xValue: "happy",
        yLabel: "Extremely",
        yTarget: 359.5062561035156,
        yValue: "extremely"
    },
    specific: [{ active: true, category: "happy", id: "a4e89cf6", label: "Loved", name: "loved" }],
    updated_at: "2018-10-05T22:20:29.820Z",
    user: {
        completed_feeling_wizard: true,
        created_at: "2018-10-05T19:20:51.899Z",
        display_name: "Stewan",
        email: "oi@stewan.io",
        online_at: "2018-10-05T19:20:51.899Z",
        uid: "ND2ubPJpRNWaWkb7Wyx3UfdXY582",
        updated_at: "2018-10-05T19:20:51.899Z"
    }
}

//
// test locally with firebase functions:shell
// the line below is just to not break this file
const trigger_notify_supporters = (data: FeelingHistory) => { };
//
// copy the code below and paste in functions:shell for local tests
trigger_notify_supporters({
    category: "happy",
    created_at: "2018-10-05T22:20:29.820Z",
    id: "d0e96c26",
    level: "extremely",
    point: {
        cx: "87.5%",
        cy: "8%",
        r: "1px",
        xLabel: "Happy",
        xTarget: 329.46875,
        xValue: "happy",
        yLabel: "Extremely",
        yTarget: 359.5062561035156,
        yValue: "extremely"
    },
    specific: [{ active: true, category: "happy", id: "a4e89cf6", label: "Loved", name: "loved" }],
    updated_at: "2018-10-05T22:20:29.820Z",
    user: {
        completed_feeling_wizard: true,
        created_at: "2018-10-05T19:20:51.899Z",
        display_name: "Stewan",
        email: "oi@stewan.io",
        online_at: "2018-10-05T19:20:51.899Z",
        uid: "ND2ubPJpRNWaWkb7Wyx3UfdXY582",
        updated_at: "2018-10-05T19:20:51.899Z"
    }
})
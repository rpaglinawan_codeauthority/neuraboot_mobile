import { ActionStepBackground } from '../interfaces/action-step-background.interface';

export var ActionStepBackgrounds: ActionStepBackground = {
    connections: '/assets/images/action-step-connections.jpg',
    fuel: '/assets/images/action-step-fuel.jpg',
    energyRelease: '/assets/images/action-step-energy-release.jpg',
    awareness: '/assets/images/action-step-awareness.jpg',
    challenge: '/assets/images/action-step-challenge.jpg'
};

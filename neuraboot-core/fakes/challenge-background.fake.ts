export var ChallengeBackgrounds = {
    connections: '/assets/images/challenge-connections.jpg',
    fuel: '/assets/images/challenge-fuel.jpg',
    energyRelease: '/assets/images/challenge-energy-release.jpg',
    awareness: '/assets/images/challenge-awareness.jpg',
    challenge: '/assets/images/challenge-challenge.jpg'
};
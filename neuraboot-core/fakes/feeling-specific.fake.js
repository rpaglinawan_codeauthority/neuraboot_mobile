// const Guid = require('../../neuraboot-server/functions/lib/neuraboot-server/functions/src/utils/guid').Guid;

module.exports = [
    {
        id: 'a3ac0060',
        name: 'calm',
        label: 'Calm',
        active: true,
        category: 'happy'
    },
    {
        id: 'c6b31d4c',
        name: 'delighted',
        label: 'Delighted',
        active: true,
        category: 'happy'
    },
    {
        id: '69a551d5',
        name: 'thankful',
        label: 'Thankful',
        active: true,
        category: 'happy'
    },
    {
        id: 'a4e89cf6',
        name: 'loved',
        label: 'Loved',
        active: true,
        category: 'happy'
    },
    {
        id: 'c61faafe',
        name: 'confident',
        label: 'Confident',
        active: true,
        category: 'happy'
    },
    {
        id: 'b61b56e4',
        name: 'worried',
        label: 'Worried',
        active: true, 
        category: 'anxious'
    },
    {
        id: '9b5d2a90',
        name: 'overwhelmed',
        label: 'Overwhelmed',
        active: true,
        category: 'anxious'
    },
    {
        id: 'd402b617',
        name: 'embarassed',
        label: 'Embarassed',
        active: true,
        category: 'anxious'
    },
    {
        id: '12afea29',
        name: 'uncertain',
        label: 'Uncertain',
        active: true,
        category: 'anxious'
    },
    {
        id: 'd903f6f8',
        name: 'apprehensive',
        label: 'Apprehensive',
        active: true,
        category: 'anxious'
    },
    {
        id: '402f08b6',
        name: 'scared',
        label: 'Scared',
        active: true,
        category: 'angry'
    },
    {
        id: 'e5ad635e',
        name: 'resentful',
        label: 'Resentful',
        active: true,
        category: 'angry'
    },
    {
        id: '62090124',
        name: 'frustrated',
        label: 'Frustrated',
        active: true,
        category: 'angry'
    },
    {
        id: '6b61227f',
        name: 'offended',
        label: 'Offended',
        active: true,
        category: 'angry'
    },
    {
        id: '6525a0b7',
        name: 'rebellious',
        label: 'Rebellious',
        active: true,
        category: 'angry'
    },
    {
        id: '711a02be',
        name: 'rejected-isolated',
        label: 'Rejected/Isolated',
        active: true,
        category: 'depressed'
    },

    {
        id: '407beaa0',
        name: 'unworthy',
        label: 'Unworthy',
        active: true,
        category: 'depressed'
    },
    {
        id: 'e4ebdfa2',
        name: 'numb',
        label: 'Numb',
        active: true,
        category: 'depressed'
    },
    {
        id: 'd4e62e30',
        name: 'bored-lost',
        label: 'Bored/Lost',
        active: true,
        category: 'depressed'
    },
    {
        id: 'a95a720e',
        name: 'grieving',
        label: 'Grieving',
        active: true,
        category: 'depressed'
    }];
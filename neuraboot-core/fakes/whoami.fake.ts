import * as _ from 'lodash';

import { WhoamiQuestionList } from "../interfaces/whoami-question-list.interface";
import { WhoamiQuestionItem } from "../interfaces/whoami-question-item.interface";
import { Guid } from '../../neuraboot-app/src/app/utils/guid';

export const WhoamiWordAnswers: WhoamiQuestionItem[] = [
    {
        id: '63779b81',
        name: 'courageous',
        title: 'Courageous',
        active: true
    },
    {
        id: 'a1a9cb8c',
        name: 'loving',
        title: 'Loving',
        active: true
    },
    {
        id: '89b8a153',
        name: 'hard working',
        title: 'Hard Working',
        active: true
    },
    {
        id: '1480c261',
        name: 'persistent',
        title: 'Persistent',
        active: true
    },
    {
        id: '8a38d539',
        name: 'open minded',
        title: 'Open Minded',
        active: true
    },
    {
        id: '8b99c297',
        name: 'confident',
        title: 'Confident',
        active: true
    },
    {
        id: '6365b6d5',
        name: 'kind',
        title: 'Kind',
        active: true
    },
    {
        id: '66b074c4',
        name: 'forgiving',
        title: 'Forgiving',
        active: true
    },
    {
        id: '543c9945',
        name: 'focused',
        title: 'Focused',
        active: true
    },
    {
        id: '05f09f7c',
        name: 'strong',
        title: 'Strong',
        active: true
    },
    {
        id: '83cd1406',
        name: 'adaptable',
        title: 'Adaptable',
        active: true
    },
    {
        id: '898eac4e',
        name: 'fun-loving',
        title: 'Fun Loving',
        active: true
    },
    {
        id: 'f0c0bc99',
        name: 'soulful',
        title: 'Soulful',
        active: true
    },
    {
        id: '972c3b4d',
        name: 'selfless',
        title: 'Selfless',
        active: true
    },
    {
        id: '58d2d45d',
        name: 'independent',
        title: 'Independent',
        active: true
    },
    {
        id: '1608bfe8',
        name: 'creative',
        title: 'Creative',
        active: true
    },
    {
        id: 'b7002e7e',
        name: 'considerate',
        title: 'Considerate',
        active: true
    }
]

export const WhoamiSolutionAnswers: WhoamiQuestionItem[] = [
    {
        id: 'b9945b49',
        name: '15-minutes-of-brisk-exercise',
        title: '15 minutes of brisk exercise',
        active: true
    },
    {
        id: '870640c0',
        name: '10-minute-walk-outdoors',
        title: '10 minute walk outdoors',
        active: true
    },
    {
        id: 'c1a43267',
        name: '',
        title: 'Reading',
        active: true
    },
    {
        id: '122cfca7',
        name: 'writing-my-thoughts-down',
        title: 'Writing my thoughts down',
        active: true
    },
    {
        id: '154c7a83',
        name: 'taking-a-bath',
        title: 'Taking a bath',
        active: true
    },
    {
        id: '86aa36f2',
        name: 'meditating-or-praying',
        title: 'Meditating or praying',
        active: true
    },
    {
        id: '757b19ae',
        name: 'connecting-with-a-friend',
        title: 'Connecting with a friend',
        active: true
    },
    {
        id: '5d67659b',
        name: 'doing-a-chore',
        title: 'Doing a chore',
        active: true
    },
    {
        id: 'e3743e88',
        name: 'eating-something-healthy',
        title: 'Eating something healthy',
        active: true
    },
    {
        id: 'a572cffa',
        name: 'other',
        title: 'Other',
        active: true
    }
]


export const WhoamiLoveAnswers: WhoamiQuestionItem[] = [
    {
        id: '534a16f0',
        name: 'words-of-affirmation',
        title: 'Words of affirmation',
        active: true
    },
    {
        id: 'debb9b33',
        name: 'quality-time',
        title: 'Quality time',
        active: true
    },
    {
        id: '605e7feb',
        name: 'receiving-gifts',
        title: 'Receiving gifts',
        active: true
    },
    {
        id: '73e511d2',
        name: 'acts-of-service',
        title: 'Acts of service',
        active: true
    },
    {
        id: '67795d5c',
        name: 'physical-touch',
        title: 'Physical touch',
        active: true
    }
]


export const WhoamiQuestions: WhoamiQuestionList[] = [
    {
        id: '86b912a1',
        label: 'word',
        action_label: 'a word',
        title: `What are <strong>three words</strong> that describe you?`,
        items: _.cloneDeep(WhoamiWordAnswers),
        selected: [],
        limit: 3,
        min: 3,
        info: true,
        type: 'tag',
        active: true
    },
    {
        id: '22b799dd',
        label: 'solution',
        action_label: 'a solution',
        title: `What are <strong>three solutions</strong> you use?`,
        items: _.cloneDeep(WhoamiSolutionAnswers),
        selected: [],
        limit: 3,
        min: 3,
        info: true,
        type: 'tag',
        active: true
    },
    {
        id: 'b85c7585',
        label: 'love language',
        action_label: 'a love language',
        title: `What is your <strong>love language</strong>?`,
        items: _.cloneDeep(WhoamiLoveAnswers),
        selected: [],
        limit: 3,
        min: 3,
        info: true,
        type: 'tag',
        active: true
    },
    {
        id: 'a0bd72d6',
        label: 'supporters',
        action_label: 'a supporter',
        title: `What are <strong>your supporters</strong> in time of need? `,
        items: [],
        selected: [],
        limit: 5,
        min: 2,
        info: false,
        type: 'contact',
        active: true
    }
]
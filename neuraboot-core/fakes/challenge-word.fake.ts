import { Guid } from '../../neuraboot-app/src/app/utils/guid';
import { Challenge } from '../interfaces/challenge.interface';

export const Challenges: Challenge[] = [
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Courageous. Say it out loud!',
    word: 'courageous'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Loving. Say it out loud!',
    word: 'loving'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Hard Working. Say it out loud!',
    word: 'hard working'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Persistent. Say it out loud!',
    word: 'persistent'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Open Minded. Say it out loud!',
    word: 'open minded'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Confident. Say it out loud!',
    word: 'confident'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Kind. Say it out loud!',
    word: 'kind'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are . Say it out loud!',
    word: ''
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Forgiving. Say it out loud!',
    word: 'forgiving'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Focused. Say it out loud!',
    word: 'focused'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Strong. Say it out loud!',
    word: 'strong'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Adaptable. Say it out loud!',
    word: 'adaptable'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Fun Loving. Say it out loud!',
    word: 'fun-loving'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Soulful. Say it out loud!',
    word: 'soulful'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Selfless. Say it out loud!',
    word: 'selfless'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Independent. Say it out loud!',
    word: 'independent'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Creative. Say it out loud!',
    word: 'creative'
  },
  {
    active: true,
    category: 'awareness',
    text: 'Remember you are Considerate. Say it out loud!',
    word: 'considerate'
  }
];

"use strict";
exports.__esModule = true;
exports.Challenges = [{
        id: '00636267',
        active: true,
        category: 'fuel',
        text: 'Take  Curcurmin (Turmeric) capsules (available at any grocery or drug store in the vitamin section.'
    }, {
        id: '06f8f02b',
        active: true,
        category: 'fuel',
        text: 'Drink a Kevita or Kombucha for a quick pick me up!  Available at Whole Foods and most grocery stores with the cold juices.'
    }, {
        id: '09b3cd5a',
        active: true,
        category: 'fuel',
        text: 'Eat at least 25 g of protein to quickly stabilize your mood. Any one of these or something like it will do the trick: protein shake, protein bar, 8 oz greek yogurt, 4 eggs, 1 cup cottage cheese, 3 oz can tuna, 3 oz steak, 3 oz chicken breast.'
    }, {
        id: '0b691af7',
        active: true,
        category: 'fuel',
        text: 'Drink 3 teaspoons lemon juice in 8 oz water.  Add honey if desired.'
    }, {
        id: '0b70159c',
        active: true,
        category: 'fuel',
        text: 'Grab cherries, blueberries or asparagus for a quick stress busting vitamin C boost.'
    }, {
        id: '0d38c14b',
        active: true,
        category: 'fuel',
        text: 'Take fish oil or krill oil capsules - available in grocery and drug stores'
    }, {
        id: '10759e95',
        active: true,
        category: 'fuel',
        text: 'Minerals matter for moods. Order Coral Calcium or Calm powder from Amazon and try it out.'
    }, {
        id: '10df1def',
        active: true,
        category: 'fuel',
        text: 'Run by Moe\'s and eat some black beans and chicken to kick away depression and stabilize your mood.'
    }, {
        id: '11410168',
        active: true,
        category: 'fuel',
        text: 'Eat an avocado, some salmon (even canned) or both.'
    }, {
        id: '192b6fe8',
        active: true,
        category: 'fuel',
        text: 'Check out Clean Food Crush on Facebook or online for crisp colorful recipes.'
    }, {
        id: '1c5952fb',
        active: true,
        category: 'fuel',
        text: 'Drink 12 oz water'
    }, {
        id: '227daf49',
        active: true,
        category: 'fuel',
        text: 'Try reducing caffeine if you are feeling a lot of anxiety.'
    }, {
        id: '234fefce',
        active: true,
        category: 'fuel',
        text: 'Assess your sleep.  Have you been getting enough? What can you change to make sure you are sleeping 7-8 hours?'
    }, {
        id: '28a13072',
        active: true,
        category: 'awareness',
        text: 'Check out TalkSpace.com.  Sign up for a session with this or similar online service.'
    }, {
        id: '28b68e62',
        active: true,
        category: 'awareness',
        text: 'Go to PsychologyToday.com and read an article on a subject that relates to how you are feeling right now.'
    }, {
        id: '2a847be3',
        active: true,
        category: 'awareness',
        label: 'Read this article',
        link: 'https://tinybuddha.com/blog/6-tips-to-tame-negative-thoughts-for-a-less-limited-life/'
    }, {
        id: '2f39515d',
        active: true,
        category: 'awareness',
        label: 'Listen to this podcast',
        link: 'https://soundcloud.com/liveawakepodcast/s02-our-warring-self-vs-our-infinate-self-releasing-anger'
    }, {
        id: '31f56184',
        active: true,
        category: 'awareness',
        text: 'I am. I decide. Get comfy and watch this video.',
        label: 'Go to the video',
        link: 'https://www.youtube.com/watch?v=1IH0digwjds'
    }, {
        id: '3257e233',
        active: true,
        category: 'awareness',
        text: 'Acknowledge how you feel, take a deep breath, and then choose whether you want the feeling to control you. You are in charge.'
    }, {
        id: '325d513e',
        active: true,
        category: 'awareness',
        text: 'Are you secure, anxious or avoidant? Read this article and log your thoughts.',
        link: 'https://www.psychalive.org/what-is-your-attachment-style/',
        label: 'Read the article'
    }, {
        id: '32b2f68b',
        active: true,
        category: 'awareness',
        label: 'Take a listen',
        link: 'https://www.youtube.com/watch?v=VrI1VSmrJs8'
    }, {
        id: '3ae1c708',
        active: true,
        category: 'awareness',
        text: 'Once you create a routine, every time you do it you will build inner confidence.'
    }, {
        id: '3b31b021',
        active: true,
        category: 'awareness',
        text: 'Feeling extremely depressed? Read this. The whole thing.',
        label: 'Read the article',
        link: 'https://www.msn.com/en-us/health/voices/like-anthony-bourdain-i-had-suicidal-depression-i-got-better-here%e2%80%99s-how/ar-AAyore0?li=BBnb7Kz'
    }, {
        id: '413b839c',
        active: true,
        category: 'awareness',
        text: 'Take a moment to practice mindfulness. This means taking in the moment and your surroundings, without daydreaming, ruminating about the past or being distracted. See, breathe, be.  You are here and you are enough.'
    }, {
        id: '417160e7',
        active: true,
        category: 'awareness',
        text: 'Give this a read and feel your world of possibilities open up.',
        label: 'Read the article',
        link: 'https://www.psychologytoday.com/us/blog/focus-forgiveness/201311/4-steps-release-limiting-beliefs-learned-childhood'
    }, {
        id: '43137aa1',
        active: true,
        category: 'challenge',
        text: 'Do something small for someone right now that reflects "giving love" rather than "getting love."  No agenda, no strings. Log what it was.'
    }, {
        id: '448ea84f',
        active: true,
        category: 'challenge',
        text: 'Say out loud, "I am grateful for _______." Write it on paper or on your phone notes app.'
    }, {
        id: '47b9f054',
        active: true,
        category: 'challenge',
        text: 'Say out loud, "Happiness follows accountability"'
    }, {
        id: '4ce861d0',
        active: true,
        category: 'challenge',
        text: 'Every season is not springtime. Sometimes we have to pull weeds and work the land before we are able to enjoy the harvest. Embrace your stage and feel its purpose for you.'
    }, {
        id: '52098d3d',
        active: true,
        category: 'challenge',
        text: 'Feeling indecisive and worried? Release the dilemma and ask the universe to help you see the outcome that is in the highest good of all.'
    }, {
        id: '579b9396',
        active: true,
        category: 'challenge',
        text: 'If you feel a negative emotion, ask yourself, "Can I let this emotion go?  Will I let this emotion go? What positive emotion do I want to replace it with?"'
    }, {
        id: '5afb259b',
        active: true,
        category: 'challenge',
        text: 'Feeling panicky?  Tap on the "karate chop point" (the edge of your hand between pinky finger and wrist) and say this out loud.  "I am strong and I will love and value myself.  The universe will support me."'
    }, {
        id: '5dc487c9',
        active: true,
        category: 'challenge',
        text: 'What we look for we will always find. If you search for flaws in others to validate your internal flawed belief that others are not to be trusted, you will find proof. If you search for flaws in yourself to validate your internal flawed belief that you are damaged and less than, you will find proof.  Practice courage to carefully govern what you search for, that you might find the proof that builds yourself and others up'
    }, {
        id: '62be6e5f',
        active: true,
        category: 'challenge',
        text: 'If you wear a watch, put it on  your other arm. If you don\'t, find something else simple to change to rewire your mind, like taking a different route home.'
    }, {
        id: '64ec3a85',
        active: true,
        category: 'challenge',
        text: 'Out loud, ask the universe to help you see what you need to see.'
    }, {
        id: '693ed1f3',
        active: true,
        category: 'challenge',
        text: 'Set your timer for 10 minutes and declutter your area. '
    }, {
        id: '6bf1c584',
        active: true,
        category: 'challenge',
        text: 'Does the future seem big and scary?   Setting concrete goals with detailed plans can reduce anxiety. Take 20 minutes to draw up a fun vision board for yourself with your ideas, goals and plans.'
    }, {
        id: '6cf2bad0',
        active: true,
        category: 'challenge',
        text: 'Feeling worried? Set a timer and spend 10 minutes writing down all the scenarios, issues and solutions.  When the timer is up, quit thinking about it.'
    }, {
        id: '74b2aaf9',
        active: true,
        category: 'challenge',
        text: 'Write up a plan for what you want to accomplish tomorrow.  Write it down on paper or on your phone. Make it realistic.  Pick one item on the list and do it now.'
    }, {
        id: '777148ac',
        active: true,
        category: 'challenge',
        text: 'What is a new responsibility you could take on?  Having daily responsibilites fights depression.  It grounds you and gives you a sense of accomplishment.  Start small.'
    }, {
        id: '79a14ffc',
        active: true,
        category: 'challenge',
        text: 'Consider your thoughts. What are they?   Release that ones that do not serve you becoming your best.'
    }, {
        id: '7a64cc01',
        active: true,
        category: 'challenge',
        text: 'Develop a morning routine.  Write it out. Start it tomorrow. '
    }, {
        id: '7bdd5e1f',
        active: true,
        category: 'challenge',
        text: 'Download the app Zombie Run.  Go outside and use it while on a 10 min walk or jog.  Your welcome!'
    }, {
        id: '80cbbbc8',
        active: true,
        category: 'challenge',
        text: 'Google global water crisis.  Read at least one article.   Try to think of at least 3 possible situations.  Stretching our brains is important, as is putting our problems in perspective.  We need you, your energy, your ideas to create a better planet. '
    }, {
        id: '81f9414c',
        active: true,
        category: 'challenge',
        text: 'Want to make money in your spare time and do something cool online?',
        link: 'https://www.mturk.com/worker',
        label: 'Learn more'
    }, {
        id: '82dd9b18',
        active: true,
        category: 'energy-release',
        text: 'Put your favorite music, book, or podcast on your phone and go walk outside for 15 minutes.'
    }, {
        id: '83098037',
        active: true,
        category: 'energy-release',
        text: 'Take a cold shower.  Yes, that\'s right.  A cold shower.'
    }, {
        id: '86aa7b5f',
        active: true,
        category: 'energy-release',
        text: 'Do a 1 min plank. Repeat 5 times.',
        link: 'https://www.youtube.com/watch?v=1IJp0aPcXtg',
        label: 'Learn more'
    }, {
        id: '8747527e',
        active: true,
        category: 'energy-release',
        text: 'Download the app "7 Minute Workout" and do one.'
    }, {
        id: '89d9be81',
        active: true,
        category: 'energy-release',
        text: 'Go dancing!  Even if you are at home. Pump up the music and move your body.'
    }, {
        id: '89eb77f4',
        active: true,
        category: 'energy-release',
        text: 'Do 20 lunges on each leg... wherever you are.',
        link: 'https://www.youtube.com/watch?v=QOVaHwm-Q6U',
        label: 'Check it out'
    }, {
        id: '8cff494a',
        active: true,
        category: 'energy-release',
        text: 'Put on your favorite high energy song and sing as loud as you can.'
    }, {
        id: '8ef4bd42',
        active: true,
        category: 'energy-release',
        text: 'Go to the gym or to a fitness class. You deserve it.'
    }, {
        id: '91d3f05e',
        active: true,
        category: 'energy-release',
        text: 'If you feel angry, honor and release those feelings. Punch into a pillow as hard as you can. Scream if you need to.  Let it out in a safe place. '
    }, {
        id: 'a069b9ba',
        active: true,
        category: 'energy-release',
        text: 'Take 4 very slow very deep breaths.'
    }, {
        id: 'a115722f',
        active: true,
        category: 'energy-release',
        text: 'Offer to take a friend\'s dog for a walk or join an app to become a dog sitter or dog walker.'
    }, {
        id: 'a279292f',
        active: true,
        category: 'energy-release',
        text: 'Find a way to play with kids for a few hours.  Playtime is important.'
    }, {
        id: 'a28dcaae',
        active: true,
        category: 'energy-release',
        text: 'Guided imagery. If you have anxious thoughts, take 5 minutes to close your eyes and visualize yourself handling the situation with calm, ease and clarity.'
    }, {
        id: 'ab57bf56',
        active: true,
        category: 'energy-release',
        text: 'Grab some pals. Be the instigator. Come up with something fun to do and invite others. If they say no, who cares. Go do it anyway.'
    }, {
        id: 'ae31089b',
        active: true,
        category: 'energy-release',
        text: 'Try an acupuncture session - just because'
    }, {
        id: 'af6d4349',
        active: true,
        category: 'energy-release',
        text: 'Try a yoga class - just because'
    }, {
        id: 'b0c427e9',
        active: true,
        category: 'energy-release',
        text: 'Color or paint or draw for 20 minutes - your choice.'
    }, {
        id: 'b1c49a29',
        active: true,
        category: 'energy-release',
        text: 'Try a cryotherapy tank.  Google nearest location.  3 minutes will reboot you physically and mentally.'
    }, {
        id: 'b38f755b',
        active: true,
        category: 'energy-release',
        text: 'Make sure your phone is at least 4 feet away from you anytime you are not using it and at night when you are sleeping.'
    }, {
        id: 'b50ff584',
        active: true,
        category: 'energy-release',
        text: 'Find a "pick it yourself" farm near you and go pick fruits or vegetables.  '
    }, {
        id: 'b8798051',
        active: true,
        category: 'energy-release',
        text: 'Go to your local humane society and spend some time giving some love.'
    }, {
        id: 'b939fb2e',
        active: true,
        category: 'connections',
        text: 'Look in the mirror and say out loud, "I Am Enough" out loud.  Then write it down on your phone or paper.'
    }, {
        id: 'bbcb0946',
        active: true,
        category: 'connections',
        text: 'Look in the mirror and say out loud, "I Am Strong" out loud.  Then write it down on your phone or paper.'
    }, {
        id: 'bcfe8495',
        active: true,
        category: 'connections',
        text: 'Say out loud, " I Choose to do the Work to Have Happiness" and then write it on the Notes App on your Phone.'
    }, {
        id: 'c11863ce',
        active: true,
        category: 'connections',
        text: 'Text someone and thank them for being in your life.  Be specific as to why you appreciate them.'
    }, {
        id: 'c612ba15',
        active: true,
        category: 'connections',
        text: 'Find a quiet place. Close your eyes.',
        link: 'https://soundcloud.com/liveawakepodcast/s03-remembering-your-worth',
        label: 'Listen to this'
    }, {
        id: 'ca5cf543',
        active: true,
        category: 'connections',
        label: 'Listen to this',
        link: 'https://soundcloud.com/liveawakepodcast/yourinnatewisdom'
    }, {
        id: 'cbb75c77',
        active: true,
        category: 'connections',
        text: 'Take a few minutes to appreciate your body and sync with your heart.',
        link: 'https://www.youtube.com/watch?v=oBu-pQG6sTY',
        label: 'Check it out'
    }, {
        id: 'd9b9c52f',
        active: true,
        category: 'connections',
        label: 'Try this',
        link: 'https://www.youtube.com/watch?v=i75XWPg9SLc'
    }, {
        id: 'dbb60646',
        active: true,
        category: 'connections',
        text: 'Let go of what you think you need and open up for something far better.',
        link: 'https://www.youtube.com/watch?v=AR5JSVdPaBM',
        label: 'Check it out'
    }, {
        id: 'e237daad',
        active: true,
        category: 'connections',
        text: 'Are you willing to trust that there is a voice?',
        link: 'https://www.youtube.com/watch?v=iGH-8v8nk34',
        label: 'Check it out'
    }, {
        id: 'e31b7434',
        active: true,
        category: 'connections',
        text: 'Spend 5 minutes in silence. Set a timer.  No phone, radio, TV or noise.  Just breathe and be. '
    }, {
        id: 'e49acb3c',
        active: true,
        category: 'connections',
        text: 'Buy an aromotherapy diffuser and oils. Enjoy.',
        label: 'Check it out',
        link: 'https://www.amazon.com/URPOWER-Ultrasonic-Aromatherapy-Adjustable-humidifier/dp/B071L6L14D/ref=sr_1_8?ie=UTF8&qid=1528049316&sr=8-8&keywords=aromatherapy%2Bdiffuser&th=1'
    }, {
        id: 'e72ab1a9',
        active: true,
        category: 'connections',
        text: 'Put your phone down and go do anything else, far away from your phone, for the next 30 minutes.  Walk, read, clean, laundry, journal, work out, take a bath, anything.'
    }, {
        id: 'e8e78772',
        active: true,
        category: 'connections',
        text: 'Think of someone you appreciate, even for a very simple reason.  Tell them now, even if just by message.  Make sure to tell them why.'
    }, {
        id: 'ed000b69',
        active: true,
        category: 'connections',
        text: 'Think of someone you can help, even if in a very simple way like doing the dishes.  Do it now.  With no agenda.'
    }, {
        id: 'f0a87701',
        active: true,
        category: 'connections',
        text: 'Create a fun weekly night routine.  Pick one night a week and make it a theme.  Let\'s take Wednesday for example.  Could it be board game night?  Cook something new night?  At home yoga night?  Learn spanish online night?  Whatever it is, pick a theme for the one night and stick with it for weeks. You\'ll find yourself looking forward to it!'
    }, {
        id: 'f3099348',
        active: true,
        category: 'connections',
        text: 'Think back to a specific day in your life when you felt absolutely amazing.  What were you doing that day?  What about that made you feel amazing?  How could you generate that feeling again?'
    }, {
        id: 'f9033a5d',
        active: true,
        category: 'connections',
        text: 'What did you dream last night?  Google it.  What does it mean?  What does it mean to you?'
    }, {
        id: 'fac733bc',
        active: true,
        category: 'connections',
        text: 'Get a massage'
    }, {
        id: 'facb7989',
        active: true,
        category: 'connections',
        text: 'Get a facial'
    }];

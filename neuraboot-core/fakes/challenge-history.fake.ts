import { ChallengeHistory } from '../interfaces/challenge-history.interface';

export const ChallengeHistoryFake: ChallengeHistory = {
    id: "d0e96c26",
    status: "completed",
    user: {
        completed_feeling_wizard: true,
        created_at: "2018-10-05T19:20:51.899Z",
        display_name: "Stewan",
        email: "oi@stewan.io",
        online_at: "2018-10-05T19:20:51.899Z",
        uid: "ND2ubPJpRNWaWkb7Wyx3UfdXY582",
        updated_at: "2018-10-05T19:20:51.899Z"
    },
    challenge: {
        active: true,
        category: "challenge",
        id: "ec751e64",
        text: "Write up a plan for what you want to accomplish tomorrow.  Write it down on paper or on your phone. Make it realistic.  Pick one item on the list and do it now.",
    },
    created_at: "2018-10-23T02:31:28.118Z"
}

//
// test locally with firebase functions:shell
// the line below is just to not break this file
const trigger_challenge_level = (data: ChallengeHistory) => { };
//
// copy the code below and paste in functions:shell for local tests
trigger_challenge_level({
    id: "d0e96c26",
    status: "completed",
    user: {
        completed_feeling_wizard: true,
        created_at: "2018-10-05T19:20:51.899Z",
        display_name: "Stewan",
        email: "oi@stewan.io",
        online_at: "2018-10-05T19:20:51.899Z",
        uid: "ND2ubPJpRNWaWkb7Wyx3UfdXY582",
        updated_at: "2018-10-05T19:20:51.899Z"
    },
    challenge: {
        active: true,
        category: "challenge",
        id: "ec751e64",
        text: "Write up a plan for what you want to accomplish tomorrow.  Write it down on paper or on your phone. Make it realistic.  Pick one item on the list and do it now.",
    },
    created_at: "2018-10-23T02:31:28.118Z"
})
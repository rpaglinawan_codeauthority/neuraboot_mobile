import { ElasticQuery } from "@ionfire/reactive-record";

export const ElasticQueryBase: { query: ElasticQuery, sort: any[] } = {
    "query": {
        "bool": {
            "must": [],
            "should": [
                // {
                //   "match": {
                //     "display_name": "stewan"
                //   }
                // },
                // {
                //   "match": {
                //     "email": "stewan"
                //   }
                // }
            ]
        }
    },
    "sort": [
        {
            "created_at": {
                "order": "desc"
            }
        }
    ]
}
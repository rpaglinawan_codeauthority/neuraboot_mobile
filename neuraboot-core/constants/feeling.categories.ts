export const FeelingCategories: any[] = [
    {
        placeholder: 'All',
        name: 'all',
        icon: 'filter_list',
        query: {}
    },
    {
        name: 'angry',
        placeholder: 'Angry',
        query: {
            match: {
                category: 'angry'
            }
        }
    },
    {
        name: 'depressed',
        placeholder: 'Depressed',
        query: {
            match: {
                category: 'depressed'
            }
        }
    },
    {
        name: 'anxious',
        placeholder: 'Anxious',
        query: {
            match: {
                category: 'anxious'
            }
        }
    },
    {
        name: 'happy',
        placeholder: 'Happy',
        query: {
            match: {
                category: 'happy'
            }
        }
    }
]
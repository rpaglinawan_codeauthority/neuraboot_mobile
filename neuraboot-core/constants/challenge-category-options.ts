import { FeelingCategory } from '../interfaces/feeling-category.interface';

export const ChallengeCategoryOptions: FeelingCategory[] = [{
    label: 'Connections',
    value: 'connections'
}, {
    label: 'Fuel',
    value: 'fuel'
}, {
    label: 'Energy release',
    value: 'energy-release'
}, {
    label: 'Awareness',
    value: 'awareness'
}, {
    label: 'Challenge',
    value: 'challenge'
}];
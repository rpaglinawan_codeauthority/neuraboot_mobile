export const ChallengeCategories: any[] = [
    {
        placeholder: 'All',
        name: 'all',
        icon: 'filter_list',
        query: {}
    },
    {
        name: 'connections',
        placeholder: 'Connections',
        query: {
            match: {
                category: 'connections'
            }
        }
    },
    {
        name: 'fuel',
        placeholder: 'Fuel',
        query: {
            match: {
                category: 'fuel'
            }
        }
    },
    {
        name: 'energy-release',
        placeholder: 'Energy release',
        query: {
            match: {
                category: 'energy-release'
            }
        }
    },
    {
        name: 'awareness',
        placeholder: 'Awareness',
        query: {
            match: {
                category: 'awareness'
            }
        }
    },
    {
        name: 'challenge',
        placeholder: 'Challenge',
        query: {
            match: {
                category: 'challenge'
            }
        }
    }
]
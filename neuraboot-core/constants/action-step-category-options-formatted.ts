export const ActionStepCategoryOptionsFormatted: any = {
    connections: {
        icon: 'fa-user-alt',
        color: 'primary',
        desc:
            'We fundamentally require connection.  Connection to ourselves, to a higher purpose and to others.  The action steps will help you build all three, helping you feel strong, whole, content and ready.'
    },
    fuel: {
        icon: 'fa-fire',
        color: 'warning',
        desc:
            'Because we are housed in a physical body, we require the right food, supplements, sleep, inspiration and relaxation time.   Modern society is not always conducive to our needs.  We help you incorporate these pieces back in, as you choose, on your time.'
    },
    energy_release: {
        icon: 'fa-music',
        color: 'success',
        desc:
            'We are energetic beings.  Releasing pent up and stuffed energy is key to our mental wellness and emotional wellbeing.  Modern life is sedentary and our emotional health pays the price.  As you select and complete our quick and easy energy release action steps, your strength and power will grow.'
    },
    awareness: {
        icon: 'fa-book-open',
        color: 'purple',
        desc:
            'As children, we are sponges.  Endlessly curious, open to possibilities, eager to take it all in.  The happiest, most productive people nurture and feed their inner spark to learn and grow.  Because of this they are never bored, not matter their environment.   Our awareness based action steps will keep your mind on fire.'
    },
    challenge: {
        icon: 'fa-trophy',
        color: 'danger',
        desc:
            'We crave it.  So much so, that when life is too easy we are miserable.  Without realizing it, we subconsciously create chaos and drama, especially relationship and financial drama, as an outlet to simply feel alive. Our biological need for this is real.  Channeling it into something constructive is the key.  Incredible fulfillment comes from accepting challenge and experiencing struggle.  The pain you feel from working through something hard makes you feel alive.   Pushing past our limits for a solid purpose is a rush like no other.  We’ll give you plenty of outlets to challenge yourself.'
    }
}
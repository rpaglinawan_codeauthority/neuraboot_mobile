import { FormatTimeFromNow } from './utils';
import { startCase, truncate } from 'lodash';
import { ChallengeCategoryAvatar } from './challenge-category-avatar';

const CategoryAvatar: any = ChallengeCategoryAvatar;
const FormatCategory: (v: any) => any = (v: any) => v ? startCase(v) : '---';
const FormatWithRow: (row: any, tooltip: boolean) => any = (row: any, tooltip: boolean) => {
    if (!row.text && row.label) return '[LINK ONLY] ' + row.label;
    else if (!row.text && !row.label) return '-- link only --';
    else return (row.text.length < 50 || tooltip) ? row.text : truncate(row.text, {
        'length': 50,
        omission: ' [...]'
    });
}
const FormatImageAvatar: (v: any) => any = (v: any) => 'url(/assets/images/' + CategoryAvatar[v.category] + ')';

export const ChallengeTableColumns = [
    { name: 'category-avatar', label: '', avatar: true, formatImageAvatar: FormatImageAvatar, sortable: false, filter: false, hideOnMobile: false, width: 35 },
    { name: 'text', label: 'Text', sortable: false, filter: true, hideOnMobile: false, formatWithRow: FormatWithRow, width: 270 },
    { name: 'category', label: 'Category', sortable: false, filter: false, format: FormatCategory, hideOnMobile: true, width: 0 }
];

export const ChallengeTableFilters: any[] = [
    {
        name: 'category',
        placeholder: 'Category',
        options: []
    }
]
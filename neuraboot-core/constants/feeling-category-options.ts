import { FeelingCategory } from '../interfaces/feeling-category.interface';

export const FeelingCategoryOptions: FeelingCategory[] = [
  {
    label: 'All',
    value: null
  },
  {
    label: 'Angry',
    value: 'angry'
  },
  {
    label: 'Sad',
    value: 'depressed'
  },
  {
    label: 'Anxious',
    value: 'anxious'
  },
  {
    label: 'Happy',
    value: 'happy'
  }
];

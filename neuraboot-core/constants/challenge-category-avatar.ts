export const ChallengeCategoryAvatar: any = {
    'connections': 'challenge-connections.jpg',
    'fuel': 'challenge-fuel.jpg',
    'energy-release': 'challenge-energy-release.jpg',
    'awareness': 'challenge-awareness.jpg',
    'challenge': 'challenge-challenge.jpg'
};
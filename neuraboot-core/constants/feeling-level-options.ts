import { FeelingLevel } from '../interfaces/feeling-level.interface';

export const FeelingLevelOptions: FeelingLevel[] = [
  {
    label: 'All',
    value: null
  },
  {
    label: 'Extremely',
    value: 'extremely'
  },
  {
    label: 'Very',
    value: 'very'
  },
  {
    label: 'Pretty',
    value: 'pretty'
  },
  {
    label: 'A Little',
    value: 'little'
  },
  {
    label: '',
    value: 'default'
  }
];

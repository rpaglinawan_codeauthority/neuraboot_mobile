import * as moment from 'moment';

export const FormatDay = (value: string) => {
    return moment(value, 'YY/MM/DD').format('ddd - MM/DD');
}

export const FormatMonth = (value: string) => {
    // console.log(value)
    return moment(value, 'YY/MM').format('MMM - YYYY');
}

import { FormatTimeFromNow } from './utils';
import { startCase } from 'lodash';

const FormatCategory: (v: any) => any = (v: any) => v ? startCase(v) : '---';
const FormatIcon: (v: any) => any = (v: any) => v === true ? 'success far fa-play-circle' : 'danger far fa-pause-circle';

export const FeelingSpecificTableColumns = [
    { name: 'active', label: '', sortable: false, filter: false, hideOnMobile: false, format: FormatIcon, faIcon: true, width: 0 },
    { name: 'label', label: 'Name', sortable: false, filter: true },
    { name: 'category', label: 'Category', sortable: false, filter: false, format: FormatCategory, hideOnMobile: true, width: 0 }
];

export const FeelingSpecificTableFilters: any[] = [
    {
        name: 'category',
        placeholder: 'Category',
        options: []
    }
]
import { FormatTimeFromNow } from './utils';

export const UserTableColumns = [
    { name: 'photo_url', label: '', avatar: true, sortable: false, filter: false, width: 40 },
    { name: 'display_name', label: 'Name', sortable: false, filter: true, width: 140 },
    { name: 'email', label: 'Email', sortable: false, filter: true, width: 200, hideOnMobile: true },
    { name: 'online_at', label: 'Last seen', sortable: false, filter: false, format: FormatTimeFromNow, hideOnMobile: true }
];

export const UserTableFilters: any[] = [
    {
        name: 'role',
        placeholder: 'Role',
        options: []
    }
]
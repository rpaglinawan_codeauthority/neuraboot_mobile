import * as moment from 'moment';

export const FormatBoolean: (v: any) => any = (v: any) => v ? 'Yes' : 'No';
export const FormatPercent: (v: any) => any = (v: any) => v ? v + '%' : 0 + '%';
export const FormatTimeFromNow: (v: any) => any = (v: any) => v ? moment(v).fromNow() : '-';

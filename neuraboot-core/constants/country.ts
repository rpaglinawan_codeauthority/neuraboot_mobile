import { Countries } from './countries';

export const getCountryEmoji = (name: string, onlyFlag: boolean = false) => {
    let result: any;
    for (let country in Countries) {
        if (country === name) result = Countries[country];
    }
    return (result ? onlyFlag ? result.emoji : (name === 'GB' ? 'UK' : name) + ' ' + result.emoji : name);
}
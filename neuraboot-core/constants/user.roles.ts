export const UserRoles: any[] = [
    {
        placeholder: 'All',
        name: 'all',
        icon: 'filter_list',
        query: {}
    },
    {
        name: 'admin',
        icon: 'verified_user',
        placeholder: 'Administrator',
        query: {
            match: {
                roles: 'admin'
            }
        }
    },
    {
        name: 'user',
        icon: 'face',
        placeholder: 'User',
        query: {
            match: {
                roles: 'user'
            }
        }
    },
    {
        name: 'dev',
        icon: 'code',
        placeholder: 'Developer',
        query: {
            match: {
                roles: 'dev'
            }
        }
    }
]
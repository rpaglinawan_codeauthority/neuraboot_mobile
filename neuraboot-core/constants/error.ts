import { ApiErrorResponse } from '../interfaces/api-error-response.interface';

//
// standardizes error responses
export const formatError = (err: any, code:string=''): ApiErrorResponse => {
    const response: ApiErrorResponse = {
        code: code,
        message: ''
    }
    if (typeof err === 'string') response.message = err; else response.message = err.message || 'There was an error with your request, please try again in a few moment.';
    return response;
}
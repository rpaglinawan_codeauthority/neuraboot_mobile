import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryUserTopFeelings = (uid: string) => {
    return <ElasticQuery>{
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "user.uid.keyword": uid
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_feelings": {
                "terms": {
                    "field": "specific.label.keyword",
                    "size": 5
                }
            }
        }
    }
}
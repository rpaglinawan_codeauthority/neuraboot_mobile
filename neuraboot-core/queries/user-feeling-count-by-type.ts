import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryUserFeelingCountByType = (uid: string, date: string): { query: ElasticQuery, aggs: any } => {
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "user.uid": uid
                        }
                    },
                    {
                        "range": {
                            "created_at": {
                                "gte": date,
                                "lte": date
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group": {
                "terms": {
                    "field": "category.keyword"
                }
            }
        }
    }
}
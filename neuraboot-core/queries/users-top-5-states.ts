import { ElasticQuery } from '@ionfire/reactive-record';

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html

export const QueryUsersTop5States: ElasticQuery = {
    "size": 1,
    "sort": {
        "created_at": {
            "order": "desc"
        }
    },
    "aggs": {
        "geolocation": {
            "nested": {
                "path": "geolocation"
            },
            "aggs": {
                "group_by_state": {
                    "terms": {
                        "field": "geolocation.state",
                        "size": 5
                    },
                    "aggs": {
                        "group_by_countryCode": {
                            "terms": {
                                "field": "geolocation.country_code",
                                "size": 5
                            }
                        }
                    }
                }
            }
        }
    }
}
import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryUserChallengeCountDaysInWeek = (uid: string, gte: string, lte: string): { query: ElasticQuery, aggs: any } => {
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "status": "completed"
                        }
                    },
                    {
                        "match": {
                            "user.uid": uid
                        }
                    },
                    {
                        "range": {
                            "created_at": {
                                "gte": gte,
                                "lte": lte
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_days_in_week": {
                "date_histogram": {
                    "field": "created_at",
                    "interval": "day",
                    "format": "E"
                }
            }
        }
    }
}
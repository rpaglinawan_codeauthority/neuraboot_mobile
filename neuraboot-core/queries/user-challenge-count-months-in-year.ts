import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryUserChallengeCountMonthsInYear = (uid: string, gte: string, lte: string): { query: ElasticQuery, aggs: any } => {
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "status": "completed"
                        }
                    }
                ],
                "should": [
                    {
                        "match": {
                            "user.uid": uid
                        }
                    },
                    {
                        "range": {
                            "created_at": {
                                "gte": gte,
                                "lte": lte
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_months_in_year": {
                "date_histogram": {
                    "field": "created_at",
                    "interval": "month",
                    "format": "MMM"
                }
            }
        }
    }
}
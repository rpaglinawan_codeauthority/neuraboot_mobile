import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryFeelingCategoriesTop: ElasticQuery = {
    "size": 1,
    "aggs": {
        "group_by_categories": {
            "terms": {
                "field": "specific.category.keyword"
            }
        }
    }
}
import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryChallengeTopUsers = (challenge: string) => {
    return <ElasticQuery>{
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "challenge.id.keyword": challenge
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_users": {
                "terms": {
                    "field": "user.display_name.keyword",
                    "size": 5
                }
            }
        }
    }
}
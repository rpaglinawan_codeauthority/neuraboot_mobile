import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryFeelingSpecificCountLastMonth = (feelingSpecific: string): { query: ElasticQuery, aggs: any } => {
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "specific.name.keyword": feelingSpecific
                        }
                    },
                    {
                        "range": {
                            "created_at": {
                                "gt": "now-30d",
                                "lte": "now"
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_day_last_month": {
                "date_histogram": {
                    "field": "created_at",
                    "interval": "day",
                    "format": "yy/MM/dd"
                }
            }
        }
    }
}
import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryChallengeCategoryCountLastYear = (challenge: string): { query: ElasticQuery, aggs: any } => {
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "challenge.category.keyword": challenge
                        }
                    },
                    {
                        "range": {
                            "created_at": {
                                "gt": "now-365d",
                                "lte": "now"
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_month_last_year": {
                "date_histogram": {
                    "field": "created_at",
                    "interval": "month",
                    "format": "yy/MM"
                }
            }
        }
    }
}
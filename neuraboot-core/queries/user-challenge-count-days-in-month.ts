import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryUserChallengeCountDaysInMonth = (uid: string, gte: string, lte: string): { query: ElasticQuery, aggs: any } => {
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "status": "completed"
                        }
                    }
                ],
                "should": [
                    {
                        "match": {
                            "user.uid": uid
                        }
                    },
                    {
                        "range": {
                            "created_at": {
                                "gte": gte,
                                "lte": lte
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group_by_days_in_month": {
                "date_histogram": {
                    "field": "created_at",
                    "interval": "day",
                    "format": "dd MMM"
                }
            }
        }
    }
}
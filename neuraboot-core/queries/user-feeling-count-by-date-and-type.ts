import { ElasticQuery } from "@ionfire/reactive-record";

//
// learn more at
// https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
export const QueryUserFeelingCountByDateAndType = (uid: string, gte: string, lte: string): { query: ElasticQuery, aggs: any } => {
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "user.uid": uid
                        }
                    },
                    {
                        "range": {
                            "created_at": {
                                "gte": gte,
                                "lte": lte
                            }
                        }
                    }
                ]
            }
        },
        "aggs": {
            "group": {
                "date_histogram": {
                    "field": "created_at",
                    "interval": "day",
                    "format": "yyyy-MM-dd",
                    "min_doc_count": 0,
                    "extended_bounds": {
                        "min": gte,
                        "max": lte
                    }
                },
                "aggs": {
                    "group": {
                        "terms": {
                        "field": "category.keyword"
                        }
                    }
                }
            }
        }
    }
}